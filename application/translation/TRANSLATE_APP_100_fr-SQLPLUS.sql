/*
The MIT License (MIT)

Copyright (c) 2018 Pretius Sp. z o.o. sk.
Żwirki i Wigury 16a
02-092 Warsaw, Poland
www.pretius.com
www.translate-apex.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

SET DEFINE OFF
ALTER SESSION SET NLS_LANGUAGE = AMERICAN;

    
DECLARE
  v_workspace_name VARCHAR2(100) := 'STPV'; -- APEX Workspace Name
  v_app_id NUMBER := 100; -- APEX Application ID
  v_session_id NUMBER := 1; -- APEX Session ID (doesn't matter)

  v_workspace_id apex_workspaces.workspace_id%type;

BEGIN
-- Get APEX workspace ID by name
  select 
    workspace_id
  into 
    v_workspace_id
  from 
    apex_workspaces
  where 
    upper(workspace) = upper(v_workspace_name);

-- Set APEX workspace ID
  apex_util.set_security_group_id(v_workspace_id);

-- Set APEX application ID
  apex_application.g_flow_id := v_app_id; 

-- Set APEX session ID
  apex_application.g_instance := v_session_id; 

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_IS_IN_THE_LAST',
  p_message_language => 'fr-ch',
  p_message_text => '%0 est dans les derniers %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORTS',
  p_message_language => 'fr-ch',
  p_message_text => 'Etats');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RESET',
  p_message_language => 'fr-ch',
  p_message_text => 'Réinitialiser');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_NOT_BETWEEN_MIN_MAX',
  p_message_language => 'fr-ch',
  p_message_text => 'La valeur #LABEL# ne figure pas dans la plage valide comprise entre %0 et %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_HOURS_AGO',
  p_message_language => 'fr-ch',
  p_message_text => 'Depuis %0 heures');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_MINUTES_AGO',
  p_message_language => 'fr-ch',
  p_message_text => 'Depuis %0 minutes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_UTILITIES.OK',
  p_message_language => 'fr-ch',
  p_message_text => 'OK');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ACTIONS_MENU',
  p_message_language => 'fr-ch',
  p_message_text => 'Menu Actions');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD',
  p_message_language => 'fr-ch',
  p_message_text => 'Ajouter');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGGREGATE_DESCRIPTION',
  p_message_language => 'fr-ch',
  p_message_text => 'Les agrégats s''affichent après chaque commande BREAK et à la fin de l''état.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_SUM',
  p_message_language => 'fr-ch',
  p_message_text => 'Total de %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALL_COLUMNS',
  p_message_language => 'fr-ch',
  p_message_text => 'Toutes les colonnes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALTERNATIVE_DEFAULT_NAME',
  p_message_language => 'fr-ch',
  p_message_text => 'Autre valeur par défaut : %0 ');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CATEGORY',
  p_message_language => 'fr-ch',
  p_message_text => 'Catégorie');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART',
  p_message_language => 'fr-ch',
  p_message_text => 'Graphique');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_HEADING',
  p_message_language => 'fr-ch',
  p_message_text => 'En-tête de colonne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_DOESNOT_CONTAIN',
  p_message_language => 'fr-ch',
  p_message_text => 'ne contient pas');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_ISNOT_IN_NEXT',
  p_message_language => 'fr-ch',
  p_message_text => 'ne figure pas sur la ligne suivante');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE',
  p_message_language => 'fr-ch',
  p_message_text => 'Supprimer');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DESCRIPTION',
  p_message_language => 'fr-ch',
  p_message_text => 'Description');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISPLAYED',
  p_message_language => 'fr-ch',
  p_message_text => 'Affiché');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_GROUP_BY',
  p_message_language => 'fr-ch',
  p_message_text => 'Modifier le groupement');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXAMPLES',
  p_message_language => 'fr-ch',
  p_message_text => 'Exemples');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FILTER',
  p_message_language => 'fr-ch',
  p_message_text => 'Filtrer');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FINDER_ALT',
  p_message_language => 'fr-ch',
  p_message_text => 'Sélectionner les colonnes à rechercher');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FORMAT_MASK',
  p_message_language => 'fr-ch',
  p_message_text => 'Masque de format %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FUNCTIONS',
  p_message_language => 'fr-ch',
  p_message_text => 'Fonctions %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GO',
  p_message_language => 'fr-ch',
  p_message_text => 'OK');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_COLUMN_HEADING_MENU',
  p_message_language => 'fr-ch',
  p_message_text => 'Si vous cliquez sur un en-tête de colonne, cela affiche un menu d''en-tête de colonne. Les options disponibles sont les suivantes : 
<p></p> 
<ul> 
<li><b>Icône de tri par ordre croissant</b> : permet de trier les états par ordre croissant.</li> 
<li><b>Icône de tri par ordre décroissant</b> : permet de trier les états par ordre décroissant.</li> 
<li><b>Masquer la colonne</b> : permet de masquer une colonne. Les colonnes ne peuvent pas toutes être masquées. Si une colonne ne peut pas l''être, cette icône n''est pas disponible.</li> 
<li><b>Colonne de section (BREAK)</b> : permet de créer un groupe de section sur la colonne, ce qui extrait la colonne de l''état comme enregistrement maître.</li> 
<li><b>Informations de colonne</b> : permet d''afficher l''aide de la colonne, si elle existe.</li> 
<li><b>Zone de texte</b> : permet de saisir des critères de recherche, 
sans distinction entre les majuscules et les minuscules (caractères génériques inutiles). Si vous saisissez une valeur, cela réduira  
la liste de valeurs au bas du menu. Vous pourrez ensuite sélectionner une valeur du  
bas du menu et cette valeur sera créée en tant que filtre grâce à l''opérateur ''='' 
(par exemple, <code>colonne = ''ABC''</code>). Vous pouvez également cliquer sur l''icône représentant une torche pour que la valeur saisie soit créée en tant que filtre avec le modificateur ''LIKE'' 
(par exemple, <code>colonne LIKE ''%ABC%''</code>).</li> 
<li>La <b>liste de valeurs uniques</b> contient les 500 premières valeurs uniques 
correspondant à vos filtres. Si la colonne est une date, une liste de 
 plages de dates s''affiche à la place. Si vous sélectionnez une valeur, un filtre sera créé avec 
l''opérateur ''='' (par exemple, <code>colonne = ''ABC''</code>).</li> 
</ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SORT',
  p_message_language => 'fr-ch',
  p_message_text => '<p>Utilisé pour modifier les colonnes à utiliser pour trier et déterminer si 
le tri doit être croissant ou décroissant. Vous pouvez également indiquer comment gérer 
les valeurs <code>NULL</code>. Le paramètre par défaut affiche toujours les valeurs <code>NULL</code> en premier ou en dernier. Le tri résultant est affiché à droite 
des en-têtes de colonnes dans l''état.</p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHTS',
  p_message_language => 'fr-ch',
  p_message_text => 'Mises en évidence');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHT_WHEN',
  p_message_language => 'fr-ch',
  p_message_text => 'Mettre en évidence lorsque');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_WEEK',
  p_message_language => 'fr-ch',
  p_message_text => 'La semaine dernière');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_X_HOURS',
  p_message_language => 'fr-ch',
  p_message_text => 'Les %0 dernières heures');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_X_DAYS',
  p_message_language => 'fr-ch',
  p_message_text => '%0 prochains jours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NULLS_ALWAYS_LAST',
  p_message_language => 'fr-ch',
  p_message_text => 'Valeurs NULL toujours en dernier');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PERCENT_OF_TOTAL_COUNT_X',
  p_message_language => 'fr-ch',
  p_message_text => 'Pourcentage du nombre total de %0 (%)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_FILTER',
  p_message_language => 'fr-ch',
  p_message_text => 'Enlever le filtre');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_HIGHLIGHT',
  p_message_language => 'fr-ch',
  p_message_text => 'Enlever la mise en évidence');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RENAME_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Renommer un état');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_VIEW',
  p_message_language => 'fr-ch',
  p_message_text => 'Vue d''état');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROWS_PER_PAGE',
  p_message_language => 'fr-ch',
  p_message_text => 'Lignes par page');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE',
  p_message_language => 'fr-ch',
  p_message_text => 'Enregistrer');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_AS_DEFAULT',
  p_message_language => 'fr-ch',
  p_message_text => 'Enregistrer en tant que valeur par défaut');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => '- Sélectionner une colonne -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_COLUMNS_FOOTER',
  p_message_language => 'fr-ch',
  p_message_text => 'Les colonnes calculées sont précédées par **.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SPACE_AS_IN_ONE_EMPTY_STRING',
  p_message_language => 'fr-ch',
  p_message_text => 'espace');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VCOLUMN',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonne verticale');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_WEEK',
  p_message_language => 'fr-ch',
  p_message_text => 'semaine');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_YEARS',
  p_message_language => 'fr-ch',
  p_message_text => '%0 ans');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_SORT_ORDER',
  p_message_language => 'fr-ch',
  p_message_text => 'Grouper par ordre de tri');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_PIVOT',
  p_message_language => 'fr-ch',
  p_message_text => 'Vous pouvez définir une vue de permutation lignes vers colonnes pour chaque état enregistré. Cela permet ensuite de basculer entre la vue de permutation lignes vers colonnes et la vue d''état à l''aide des icônes d''affichage de la barre de recherche. Pour créer une vue de permutation lignes vers colonnes, vous devez sélectionner :  
<p></p> 
<ul> 
<li>les colonnes sur lesquelles effectuer la permutation lignes vers colonnes ;</li> 
<li>les colonnes à afficher en tant que lignes ; </li> 
<li>les colonnes à agréger, ainsi que la fonction à exécuter (moyenne, somme, décompte, etc.).</li> 
</ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_FINDER',
  p_message_language => 'fr-ch',
  p_message_text => '<li>L''<b>icône Sélectionner des colonnes</b> permet d''indiquer les colonnes sur lesquelles doit porter la recherche (ou toutes les colonnes).</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SUBSCRIPTION',
  p_message_language => 'fr-ch',
  p_message_text => 'Lorsque vous ajoutez un abonnement, vous fournissez une adresse électronique (ou plusieurs adresses électroniques séparées par des virgules), un objet de courriel, une fréquence, et des dates de début et de fin. Les courriels obtenus seront une version HTML de l''état interactif contenant les données en cours utilisant les paramètres d''état présents lors de l''ajout de l''abonnement.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INACTIVE_SETTINGS',
  p_message_language => 'fr-ch',
  p_message_text => '%0 paramètres inactifs');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_FILTER_QUERY',
  p_message_language => 'fr-ch',
  p_message_text => 'Requête de filtrage non valide');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_MAX_ROW_CNT',
  p_message_language => 'fr-ch',
  p_message_text => 'Le nombre maximal de lignes pour la requête Permutation ligne vers colonnes limite le nombre de lignes dans la requête de base, et non le nombre de lignes affichées. La requête de base dépasse le nombre de lignes maximal de %0. Appliquez un filtre pour réduire le nombre d''enregistrements dans la requête de base.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_GROUP_BY',
  p_message_language => 'fr-ch',
  p_message_text => 'Enlever le regroupement');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_ALIAS_DOES_NOT_EXIST',
  p_message_language => 'fr-ch',
  p_message_text => 'L''état interactif enregistré portant l''alias %0 n''existe pas.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_COL_DIFF_FROM_PIVOT_COL',
  p_message_language => 'fr-ch',
  p_message_text => 'La colonne de ligne doit être différente de la colonne de pivot.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_COLUMNS',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonnes de ligne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_FILTER',
  p_message_language => 'fr-ch',
  p_message_text => 'Filtrer...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CONTROL_BREAK_COLUMNS',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonnes de commande BREAK');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DUPLICATE_PIVOT_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonne de pivot en double. La liste de colonnes de pivot doit être unique.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_PIVOT',
  p_message_language => 'fr-ch',
  p_message_text => 'Modifier le pivot');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_ROW',
  p_message_language => 'fr-ch',
  p_message_text => 'Sélectionner une ligne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECTED_COLUMNS',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonnes sélectionnées');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.CLOSE_NOTIFICATION',
  p_message_language => 'fr-ch',
  p_message_text => 'Fermer la notification');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SESSION.EXPIRED.NEW_SESSION',
  p_message_language => 'fr-ch',
  p_message_text => 'Cliquez <a href="%0">ici</a> pour créer une session.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MONTHLY',
  p_message_language => 'fr-ch',
  p_message_text => 'Mensuel');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEW_CATEGORY',
  p_message_language => 'fr-ch',
  p_message_text => '- Nouvelle catégorie -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PERCENT_OF_TOTAL_SUM_X',
  p_message_language => 'fr-ch',
  p_message_text => 'Pourcentage de la somme totale de %0 (%)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PRIVATE',
  p_message_language => 'fr-ch',
  p_message_text => 'Privé');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_INVALID',
  p_message_language => 'fr-ch',
  p_message_text => 'La valeur #LABEL# doit être numérique.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'PAGINATION.NEXT_SET',
  p_message_language => 'fr-ch',
  p_message_text => 'Jeu suivant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'PAGINATION.PREVIOUS_SET',
  p_message_language => 'fr-ch',
  p_message_text => 'Jeu précédent');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_MINUTES_FROM_NOW',
  p_message_language => 'fr-ch',
  p_message_text => '%0 minutes à partir de maintenant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_NOW',
  p_message_language => 'fr-ch',
  p_message_text => 'Maintenant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.FOUND_BUT_NOT_DISPLAYED',
  p_message_language => 'fr-ch',
  p_message_text => 'Ligne minimum demandée : %0, lignes trouvées mais non affichées : %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ACTIONS',
  p_message_language => 'fr-ch',
  p_message_text => 'Actions');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_COUNT',
  p_message_language => 'fr-ch',
  p_message_text => 'Nombre');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_MODE',
  p_message_language => 'fr-ch',
  p_message_text => 'Mode');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_BLUE',
  p_message_language => 'fr-ch',
  p_message_text => 'bleu');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CALENDAR',
  p_message_language => 'fr-ch',
  p_message_text => 'Calendrier');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_FOOTER_E2',
  p_message_language => 'fr-ch',
  p_message_text => 'INITCAP(B)||'', ''||INITCAP(C)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DAILY',
  p_message_language => 'fr-ch',
  p_message_text => 'Quotidien');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_FILTER',
  p_message_language => 'fr-ch',
  p_message_text => 'Modifier le filtre');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_FREQUENCY',
  p_message_language => 'fr-ch',
  p_message_text => 'Fréquence');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_NOT_CONFIGURED',
  p_message_language => 'fr-ch',
  p_message_text => 'La messagerie électronique n''a pas été configurée pour cette application. Contactez l''administrateur.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FUNCTIONS_OPERATORS',
  p_message_language => 'fr-ch',
  p_message_text => 'Fonctions/Opérateurs');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_CHART',
  p_message_language => 'fr-ch',
  p_message_text => 'Vous pouvez définir un graphique par état interactif. Une fois 
que vous l''avez défini, vous pouvez basculer de la vue du graphique à celle de l''état à l''aide des icônes de vue situés sous la barre de recherche. 
Les options disponibles sont les suivantes :   
<p> 
</p><ul> 
<li><b>Type de graphique</b> indique le type de graphique à inclure. 
Sélectionnez A barres horizontales, A barres verticales, A secteurs ou A courbes.</li> 
<li><b>Libellé</b> permet de sélectionner la colonne à utiliser comme libellé.</li> 
<li><b>Titre de l''axe pour le libellé</b> indique le titre qui apparaîtra sur l''axe associé à la colonne définie 
comme libellé. Cette option n''est pas disponible pour les graphiques à secteurs.</li> 
<li><b>Valeur</b> permet de sélectionner la colonne à utiliser comme valeur. Si votre fonction 
est de type COUNT, il est inutile de sélectionner une valeur.</li> 
<li><b>Titre de l''axe pour la valeur</b> indique le titre qui apparaîtra sur l''axe associé à la colonne définie 
comme valeur. Cette option n''est pas disponible pour les graphiques à secteurs.</li> 
<li><b>Fonction</b> est une fonction facultative à exécuter sur la colonne sélectionnée comme valeur.</li> 
<li><b>Trier</b> permet de trier l''ensemble de résultats.</li></ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_FLASHBACK',
  p_message_language => 'fr-ch',
  p_message_text => 'Une requête Flashback vous permet de visualiser les données telles qu''elles existaient 
à un moment donné. Par défaut, l''opération Flashback permet de remonter à 3 heures (ou 180 minutes) 
mais la durée réelle sera différente en fonction de la base de données.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INTERACTIVE_REPORT_HELP',
  p_message_language => 'fr-ch',
  p_message_text => 'Aide de l''état interactif');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_MONTH',
  p_message_language => 'fr-ch',
  p_message_text => 'Le mois dernier');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MAX_X',
  p_message_language => 'fr-ch',
  p_message_text => 'Maximum de %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MAX_ROW_CNT',
  p_message_language => 'fr-ch',
  p_message_text => 'Le nombre maximal de lignes pour cet état est de %0 lignes. Appliquez un filtre pour réduire le nombre d''enregistrements dans votre requête.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MIN_AGO',
  p_message_language => 'fr-ch',
  p_message_text => '%0 minutes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MORE_DATA',
  p_message_language => 'fr-ch',
  p_message_text => 'Données supplémentaires');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MOVE',
  p_message_language => 'fr-ch',
  p_message_text => 'Déplacer');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT',
  p_message_language => 'fr-ch',
  p_message_text => 'Suivant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO_AGGREGATION_DEFINED',
  p_message_language => 'fr-ch',
  p_message_text => 'Aucune agrégation définie.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PERCENT_TOTAL_COUNT',
  p_message_language => 'fr-ch',
  p_message_text => 'Pourcentage du nombre total');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROWS',
  p_message_language => 'fr-ch',
  p_message_text => 'Lignes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEARCH',
  p_message_language => 'fr-ch',
  p_message_text => 'Rechercher');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_VALUE',
  p_message_language => 'fr-ch',
  p_message_text => 'Sélectionner une valeur');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT_DESCENDING',
  p_message_language => 'fr-ch',
  p_message_text => 'Trier par ordre décroissant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_MINS',
  p_message_language => 'fr-ch',
  p_message_text => 'minutes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNSUPPORTED_DATA_TYPE',
  p_message_language => 'fr-ch',
  p_message_text => 'type de données non pris en charge');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALID_FORMAT_MASK',
  p_message_language => 'fr-ch',
  p_message_text => 'Entrez un masque de format valide.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_ICONS',
  p_message_language => 'fr-ch',
  p_message_text => 'Icônes Vue');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_MINS',
  p_message_language => 'fr-ch',
  p_message_text => '%0 minutes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_WEEKS',
  p_message_language => 'fr-ch',
  p_message_text => '%0 semaines');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'IR_AS_NAMED_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Comme état nommé');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.SLIDER.VALUE_NOT_MULTIPLE_OF_STEP',
  p_message_language => 'fr-ch',
  p_message_text => '#LABEL# n''est pas un multiple de %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.POPUP_LOV.ICON_TEXT',
  p_message_language => 'fr-ch',
  p_message_text => 'Liste de valeurs (LOV) instantanée : %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART_LABEL_NOT_NULL',
  p_message_language => 'fr-ch',
  p_message_text => 'Le libellé de graphique doit être indiqué.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_N',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonne %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT',
  p_message_language => 'fr-ch',
  p_message_text => 'Modifier');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_PIVOT_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => '- Sélectionner la colonne de pivot -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNGROUPED_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonne sans groupe');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'IR_AS_DEFAULT_REPORT_SETTING',
  p_message_language => 'fr-ch',
  p_message_text => 'Comme paramètre d''état par défaut');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_MAX_ROW_CNT',
  p_message_language => 'fr-ch',
  p_message_text => 'Le nombre maximal de lignes pour la requête Grouper par limite le nombre de lignes dans la requête de base, et non le nombre le lignes affichées. Votre requête de base dépasse le nombre de lignes maximal de %0. Appliquez un filtre pour réduire le nombre d''enregistrements dans votre requête de base.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'OUT_OF_RANGE',
  p_message_language => 'fr-ch',
  p_message_text => 'L''ensemble de lignes demandé n''est pas valide : les données source de l''état ont été modifiées.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MAX_QUERY_COST',
  p_message_language => 'fr-ch',
  p_message_text => 'Selon les estimations, la requête dépasse les ressources maximales autorisées. Modifiez les paramètres de votre état et réessayez.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_MONTH',
  p_message_language => 'fr-ch',
  p_message_text => 'Mois suivant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ORANGE',
  p_message_language => 'fr-ch',
  p_message_text => 'orange');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_CONTROL_BREAK',
  p_message_language => 'fr-ch',
  p_message_text => 'Enlever la commande BREAK');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_SECONDS_AGO',
  p_message_language => 'fr-ch',
  p_message_text => 'Depuis %0 secondes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_SUBSCRIPTION',
  p_message_language => 'fr-ch',
  p_message_text => 'Ajouter l''abonnement');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_MIN',
  p_message_language => 'fr-ch',
  p_message_text => 'Minimum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALTERNATIVE',
  p_message_language => 'fr-ch',
  p_message_text => 'Remplacement');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_APPLY',
  p_message_language => 'fr-ch',
  p_message_text => 'Appliquer');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHOOSE_DOWNLOAD_FORMAT',
  p_message_language => 'fr-ch',
  p_message_text => 'Choisir le format de téléchargement d''état');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_HEADING_MENU',
  p_message_language => 'fr-ch',
  p_message_text => 'Menu d''en-tête de colonne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTE',
  p_message_language => 'fr-ch',
  p_message_text => 'Calculer');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COUNT_DISTINCT',
  p_message_language => 'fr-ch',
  p_message_text => 'Nombre de valeurs distinctes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COUNT_DISTINCT_X',
  p_message_language => 'fr-ch',
  p_message_text => 'Nombre de valeurs distinctes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DATA_AS_OF',
  p_message_language => 'fr-ch',
  p_message_text => 'Données d''état datant d''il y a %0 minutes.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DESCENDING',
  p_message_language => 'fr-ch',
  p_message_text => 'Décroissant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISABLED',
  p_message_language => 'fr-ch',
  p_message_text => 'Désactivé');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_ALTERNATIVE_DEFAULT',
  p_message_language => 'fr-ch',
  p_message_text => 'Modifier l''autre valeur par défaut');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_BCC',
  p_message_language => 'fr-ch',
  p_message_text => 'Cci');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_BODY',
  p_message_language => 'fr-ch',
  p_message_text => 'Corps');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ENABLED',
  p_message_language => 'fr-ch',
  p_message_text => 'Activé');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ENABLE_DISABLE_ALT',
  p_message_language => 'fr-ch',
  p_message_text => 'Activer/Désactiver');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FLASHBACK_ERROR_MSG',
  p_message_language => 'fr-ch',
  p_message_text => 'Impossible d''exécuter la demande de Flashback');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_REPORT_SETTINGS',
  p_message_language => 'fr-ch',
  p_message_text => 'Si vous personnalisez un état interactif, les paramètres d''état seront affichés 
en dessous de la barre de recherche et au-dessus de l''état. Cette zone peut être réduite et développée à l''aide de l''icône de gauche. 
<p> 
Pour chaque paramètre d''état, vous pouvez effectuer les opérations suivantes : 
</p><ul> 
<li>Modifier un paramètre en cliquant sur le nom.</li> 
<li>Désactiver/Activer un paramètre en cochant ou en désélectionnant la case Activer/Désactiver. Ceci permet d''activer et de désactiver temporairement le paramètre.</li> 
<li>Enlever un paramètre en cliquant sur l''icône Enlever.</li> 
</ul> 
<p>Si vous avez créé un graphique, un regroupement ou un pivot, vous pouvez basculer entre eux 
et l''état de base via les liens de vue d''état, Vue de graphique, Vue Grouper par et Vue de pivot 
sur la droite. Si vous visualisez le graphique, le regroupement ou le pivot, vous 
pouvez également utiliser le lien Modifier pour modifier les paramètres.</p> 
');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR',
  p_message_language => 'fr-ch',
  p_message_text => 'Une région de recherche se trouve dans la partie supérieure de chaque page d''état. Cette région (ou barre de recherche) fournit les fonctionnalités suivantes :');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NULLS_ALWAYS_FIRST',
  p_message_language => 'fr-ch',
  p_message_text => 'Valeurs NULL toujours en premier');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PRIMARY',
  p_message_language => 'fr-ch',
  p_message_text => 'Principal');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PRIMARY_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Etat principal');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PUBLIC',
  p_message_language => 'fr-ch',
  p_message_text => 'Public');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_FLASHBACK',
  p_message_language => 'fr-ch',
  p_message_text => 'Enlever l''opération Flashback');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_OF',
  p_message_language => 'fr-ch',
  p_message_text => 'Ligne %0 sur %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_DEFAULT_CONFIRM',
  p_message_language => 'fr-ch',
  p_message_text => 'Les paramètres en cours de l''état seront utilisés comme paramètres par défaut pour tous les utilisateurs.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_CATEGORY',
  p_message_language => 'fr-ch',
  p_message_text => '- Sélectionner une catégorie -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_COLUMNS',
  p_message_language => 'fr-ch',
  p_message_text => 'Sélectionner des colonnes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_WEEKS',
  p_message_language => 'fr-ch',
  p_message_text => 'semaines');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TOP',
  p_message_language => 'fr-ch',
  p_message_text => 'Tout en haut');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNIQUE_HIGHLIGHT_NAME',
  p_message_language => 'fr-ch',
  p_message_text => 'Le nom de mise en évidence doit être unique.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Voir l''état');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_WORKING_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Etat de travail');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_YES',
  p_message_language => 'fr-ch',
  p_message_text => 'Oui');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.AUTHENTICATION.LOGIN_THROTTLE.ERROR',
  p_message_language => 'fr-ch',
  p_message_text => 'La tentative de connexion a été bloquée.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_FORMAT',
  p_message_language => 'fr-ch',
  p_message_text => '<p>L''élément Format permet de personnaliser l''affichage d''un état. 
Il comprend les sous-menus suivants :</p> 
<ul><li>Trier</li> 
<li>Commande BREAK</li> 
<li>Mise en évidence</li> 
<li>Calculer</li> 
<li>Agrégat</li> 
<li>Graphique</li> 
<li>Grouper par</li> 
<li>Pivot</li> 
</ul> 
');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LABEL_PREFIX',
  p_message_language => 'fr-ch',
  p_message_text => 'Préfixe de libellé');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_ID_DOES_NOT_EXIST',
  p_message_language => 'fr-ch',
  p_message_text => 'L''ID d''état interactif enregistré, %0, n''existe pas.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_FILTER',
  p_message_language => 'fr-ch',
  p_message_text => 'Filtre de ligne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_ROW_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => 'Ajouter une ligne /colonne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART_MAX_ROW_CNT',
  p_message_language => 'fr-ch',
  p_message_text => 'Le nombre maximal de lignes pour la requête Graphique limite le nombre de lignes dans la requête de base, et non le nombre de lignes affichées. Votre requête de base dépasse le nombre de lignes maximal de %0. Appliquez un filtre pour réduire le nombre d''enregistrements dans votre requête de base.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_ROW_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => '- Sélectionner la colonne de ligne -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TOGGLE',
  p_message_language => 'fr-ch',
  p_message_text => 'Activer/désactiver');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_YEAR',
  p_message_language => 'fr-ch',
  p_message_text => 'Année');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_GROUP_BY',
  p_message_language => 'fr-ch',
  p_message_text => 'Vous pouvez définir une vue Grouper par pour chaque état 
enregistré. Une fois défini, vous pouvez basculer entre la vue Grouper par et la vue 
d''état à l''aide des icônes de vue situées en dessous de la barre de recherche. Pour créer une vue Grouper par, 
vous sélectionnez : 
<p></p><ul> 
<li>les colonnes sur la base desquelles former le groupe</li> 
<li>les colonnes à agréger, ainsi que la fonction à exécuter (moyenne, somme, nombre, etc.)</li> 
</ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_FILTER',
  p_message_language => 'fr-ch',
  p_message_text => 'Expression de filtre non valide. %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SESSION_STATE.SSP_VIOLATION',
  p_message_language => 'fr-ch',
  p_message_text => 'Violation de protection du statut de session. Cause possible : modification manuelle d''une URL contenant un checksum ou utilisation d''un lien dont le checksum est incorrect ou absent. Si vous n''êtes pas sûr de la cause de l''erreur, contactez votre administrateur pour obtenir de l''aide.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LABEL',
  p_message_language => 'fr-ch',
  p_message_text => 'Libellé %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_X_DAYS',
  p_message_language => 'fr-ch',
  p_message_text => 'Les %0 derniers jours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATA_HAS_CHANGED',
  p_message_language => 'fr-ch',
  p_message_text => 'La version en cours des données de la base a changé depuis que l''utilisateur a lancé le traitement de mise à jour. Identificateur de version de ligne en cours = "%0", identificateur de version de ligne de l''application = "%1"');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_GREATER_MAX_VALUE',
  p_message_language => 'fr-ch',
  p_message_text => 'La valeur #LABEL# est supérieure à la valeur maximale indiquée %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_LESS_MIN_VALUE',
  p_message_language => 'fr-ch',
  p_message_text => 'La valeur #LABEL# est inférieure à la valeur minimale indiquée %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.PAGE_ITEM_IS_REQUIRED',
  p_message_language => 'fr-ch',
  p_message_text => 'La valeur #LABEL# doit comporter une valeur.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'INVALID_CREDENTIALS',
  p_message_language => 'fr-ch',
  p_message_text => 'Informations d''identification non valides');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'PAGINATION.NEXT',
  p_message_language => 'fr-ch',
  p_message_text => 'Suivant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_DAYS_FROM_NOW',
  p_message_language => 'fr-ch',
  p_message_text => 'Dans %0 jours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.SORT_BY_THIS_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => 'Trier sur cette colonne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_AVG',
  p_message_language => 'fr-ch',
  p_message_text => 'Moyenne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_MEDIAN',
  p_message_language => 'fr-ch',
  p_message_text => 'Médiane');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AVERAGE_X',
  p_message_language => 'fr-ch',
  p_message_text => 'Moyenne de %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CELL',
  p_message_language => 'fr-ch',
  p_message_text => 'Cellule');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART_INITIALIZING',
  p_message_language => 'fr-ch',
  p_message_text => 'Initialisation...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART_TYPE',
  p_message_language => 'fr-ch',
  p_message_text => 'Type de graphique');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_REGEXP_LIKE',
  p_message_language => 'fr-ch',
  p_message_text => 'recherche une expression régulière');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_FOOTER_E3',
  p_message_language => 'fr-ch',
  p_message_text => 'CASE WHEN A = 10 THEN B + C ELSE B END');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DATE',
  p_message_language => 'fr-ch',
  p_message_text => 'Date');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DEFAULT',
  p_message_language => 'fr-ch',
  p_message_text => 'Valeur par défaut');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DEFAULT_REPORT_TYPE',
  p_message_language => 'fr-ch',
  p_message_text => 'Type d''état par défaut');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DETAIL_VIEW',
  p_message_language => 'fr-ch',
  p_message_text => 'Vue monoligne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DIRECTION',
  p_message_language => 'fr-ch',
  p_message_text => 'Direction %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISABLE',
  p_message_language => 'fr-ch',
  p_message_text => 'Désactiver');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DOWNLOAD',
  p_message_language => 'fr-ch',
  p_message_text => 'Télécharger en local');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_CHART2',
  p_message_language => 'fr-ch',
  p_message_text => 'Modifier un graphique');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_HIGHLIGHT',
  p_message_language => 'fr-ch',
  p_message_text => 'Modifier la mise en évidence');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL',
  p_message_language => 'fr-ch',
  p_message_text => 'Adresse électronique');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ERROR',
  p_message_language => 'fr-ch',
  p_message_text => 'Erreur');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXPRESSION',
  p_message_language => 'fr-ch',
  p_message_text => 'Expression');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GREEN',
  p_message_language => 'fr-ch',
  p_message_text => 'vert');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_RESET',
  p_message_language => 'fr-ch',
  p_message_text => 'Réinitialise l''état pour retrouver les paramètres par défaut, ce qui enlève toute personnalisation effectuée.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHT_CONDITION',
  p_message_language => 'fr-ch',
  p_message_text => 'Conditions de mises en évidence');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LABEL_AXIS_TITLE',
  p_message_language => 'fr-ch',
  p_message_text => 'Titre de l''axe pour le libellé');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_DAY',
  p_message_language => 'fr-ch',
  p_message_text => 'Dernier jour');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_X_YEARS',
  p_message_language => 'fr-ch',
  p_message_text => '%0 dernières années');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LINE',
  p_message_language => 'fr-ch',
  p_message_text => 'Ligne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEW_AGGREGATION',
  p_message_language => 'fr-ch',
  p_message_text => 'Nouvelle agrégation');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEW_COMPUTATION',
  p_message_language => 'fr-ch',
  p_message_text => 'Nouveau calcul');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_WEEK',
  p_message_language => 'fr-ch',
  p_message_text => 'Semaine suivante');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_X_HOURS',
  p_message_language => 'fr-ch',
  p_message_text => '%0 prochaines heures');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NULL_SORTING',
  p_message_language => 'fr-ch',
  p_message_text => 'Tri des valeurs NULL %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PREVIOUS',
  p_message_language => 'fr-ch',
  p_message_text => 'Précédent');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Etat');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_SETTINGS',
  p_message_language => 'fr-ch',
  p_message_text => 'Paramètres d''état');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVED_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Etat enregistré');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonne de tri %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SUBSCRIPTION_ENDING',
  p_message_language => 'fr-ch',
  p_message_text => 'Fin');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_DAYS',
  p_message_language => 'fr-ch',
  p_message_text => 'jours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_MONTHS',
  p_message_language => 'fr-ch',
  p_message_text => 'mois');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_YEARS',
  p_message_language => 'fr-ch',
  p_message_text => 'années');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNAUTHORIZED',
  p_message_language => 'fr-ch',
  p_message_text => 'Non autorisé');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALID_COLOR',
  p_message_language => 'fr-ch',
  p_message_text => 'Entrez une couleur valide.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_DETAIL',
  p_message_language => 'fr-ch',
  p_message_text => 'Visualiser les détails');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_HOURS',
  p_message_language => 'fr-ch',
  p_message_text => '%0 heures');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'IR_STAR',
  p_message_language => 'fr-ch',
  p_message_text => 'S''affiche uniquement pour les développeurs');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.AUTHENTICATION.LOGIN_THROTTLE.COUNTER',
  p_message_language => 'fr-ch',
  p_message_text => 'Veuillez patienter pendant <span id="apex_login_throttle_sec">%0</span> secondes avant de vous reconnecter.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.REGION.JQM_LIST_VIEW.SEARCH',
  p_message_language => 'fr-ch',
  p_message_text => 'Rechercher...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.REGION.JQM_LIST_VIEW.LOAD_MORE',
  p_message_language => 'fr-ch',
  p_message_text => 'Charger plus d''éléments...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_TEXTBOX',
  p_message_language => 'fr-ch',
  p_message_text => '<li>La <b>zone de texte</b> permet d''entrer des critères de recherche sans distinction entre les majuscules et les minuscules (les caractères génériques sont implicites).</li> 
<li>Le <b>bouton OK</b> permet de lancer la recherche. Si vous appuyez sur la touche Entrée, la recherche sera également exécutée lorsque le curseur se trouve dans la zone de recherche de texte.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_END_DATE',
  p_message_language => 'fr-ch',
  p_message_text => 'La date de fin doit être postérieure à la date de début.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NOT_VALID_EMAIL',
  p_message_language => 'fr-ch',
  p_message_text => 'Adresse électronique non valide.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_AGG_NOT_NULL',
  p_message_language => 'fr-ch',
  p_message_text => 'L''agrégation doit être indiquée.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_COLUMN_N',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonne de ligne %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_COLUMN_NOT_NULL',
  p_message_language => 'fr-ch',
  p_message_text => 'La colonne de ligne doit être indiquée.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEARCH_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => 'Rechercher : %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_GROUP_BY_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => 'Ajouter une colonne de regroupement');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_ROWS_PER_PAGE',
  p_message_language => 'fr-ch',
  p_message_text => 'Définit le nombre d''enregistrements à afficher par page.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'MAXIMIZE',
  p_message_language => 'fr-ch',
  p_message_text => 'Agrandir');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHT_TYPE',
  p_message_language => 'fr-ch',
  p_message_text => 'Mettre le type en évidence');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NAME',
  p_message_language => 'fr-ch',
  p_message_text => 'Nom');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE',
  p_message_language => 'fr-ch',
  p_message_text => 'Enlever');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'FLOW.VALIDATION_ERROR',
  p_message_language => 'fr-ch',
  p_message_text => '%0 erreurs se sont produites');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'REPORT_TOTAL',
  p_message_language => 'fr-ch',
  p_message_text => 'Nombre total d''états');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_DAYS_AGO',
  p_message_language => 'fr-ch',
  p_message_text => 'Depuis %0 jours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_YEARS_AGO',
  p_message_language => 'fr-ch',
  p_message_text => 'Depuis %0 ans');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.X_Y_OF_Z',
  p_message_language => 'fr-ch',
  p_message_text => 'ligne(s) %0 - %1 sur %2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGGREGATION',
  p_message_language => 'fr-ch',
  p_message_text => 'Agrégation');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ASCENDING',
  p_message_language => 'fr-ch',
  p_message_text => 'Croissant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AS_OF',
  p_message_language => 'fr-ch',
  p_message_text => 'Il y a %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_BGCOLOR',
  p_message_language => 'fr-ch',
  p_message_text => 'Couleur d''arrière-plan');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMNS',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonnes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IS_IN_LAST',
  p_message_language => 'fr-ch',
  p_message_text => 'est dans les dern.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IS_NOT_NULL',
  p_message_language => 'fr-ch',
  p_message_text => 'n''est pas NULL');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_FOOTER',
  p_message_language => 'fr-ch',
  p_message_text => 'Créer un calcul en utilisant les alias de colonne.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CONTROL_BREAKS',
  p_message_language => 'fr-ch',
  p_message_text => 'Commandes BREAK');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COUNT_X',
  p_message_language => 'fr-ch',
  p_message_text => 'Nombre de %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_CHECKED',
  p_message_language => 'fr-ch',
  p_message_text => 'Supprimer ce qui est coché');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISPLAY',
  p_message_language => 'fr-ch',
  p_message_text => 'Afficher');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_CHART',
  p_message_language => 'fr-ch',
  p_message_text => 'Modifier les paramètres du graphique');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_CC',
  p_message_language => 'fr-ch',
  p_message_text => 'Cc');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_SEE_ATTACHED',
  p_message_language => 'fr-ch',
  p_message_text => 'Consulter la pièce jointe');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_SUBJECT',
  p_message_language => 'fr-ch',
  p_message_text => 'Objet');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_TO',
  p_message_language => 'fr-ch',
  p_message_text => 'A');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXPAND_COLLAPSE_ALT',
  p_message_language => 'fr-ch',
  p_message_text => 'Développer/Réduire');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FILTERS',
  p_message_language => 'fr-ch',
  p_message_text => 'Filtres');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY',
  p_message_language => 'fr-ch',
  p_message_text => 'Grouper par');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HCOLUMN',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonne horizontale');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP',
  p_message_language => 'fr-ch',
  p_message_text => 'Aide');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_ACTIONS_MENU',
  p_message_language => 'fr-ch',
  p_message_text => 'Le menu Actions se trouve à droite du bouton Exécuter dans la barre de recherche. Utilisez ce menu pour personnaliser un état interactif.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_COMPUTE',
  p_message_language => 'fr-ch',
  p_message_text => 'Permet d''ajouter des colonnes calculées à votre état. Il peut s''agir de calculs mathématiques (comme <code>NBR_HOURS/24</code>) ou de fonctions Oracle 
standard appliquées à des colonnes existantes (certaines sont affichées comme exemple tandis que d''autres, telles que <code>TO_DATE</code>, peuvent également être utilisées). 
Les options disponibles sont les suivantes : 
<p></p> 
<ul> 
<li><b>Calcul</b> permet de sélectionner un calcul prédéfini à modifier.</li> 
<li><b>En-tête de colonne</b> désigne l''en-tête de la nouvelle colonne.</li> 
<li><b>Masque de format</b> désigne un masque de format Oracle à appliquer à la colonne (par exemple, S9999).</li> 
<li><b>Calcul</b> désigne le calcul à exécuter. Dans le calcul, les colonnes sont référencées à l''aide des alias affichés.</li> 
</ul> 
<p>Sous le calcul, les colonnes de votre requête sont affichées avec 
leur alias associé. Si vous cliquez sur un nom de colonne ou un alias, 
celui-ci sera inscrit dans le calcul. En regard des colonnes, un clavier fournit simplement 
des raccourcis pour les touches fréquemment utilisées. A droite se trouvent les fonctions.</p> 
<p>Voici un exemple de calcul permettant d''afficher la rémunération totale :</p> 
<pre>CASE WHEN A = ''SALES'' THEN B + C ELSE B END</pre> 
(où A est ORGANIZATION, B est SALARY et C, COMMISSION)</p> 
');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_CONTROL_BREAK',
  p_message_language => 'fr-ch',
  p_message_text => 'Utilisé pour créer un groupe de section sur une ou plusieurs colonnes, ce qui extrait les colonnes de l''état interactif et les affiche comme enregistrements maître.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_DETAIL_VIEW',
  p_message_language => 'fr-ch',
  p_message_text => 'Pour afficher les détails d''une seule ligne à la fois, cliquez sur l''icône de vue monoligne sur la ligne que vous souhaitez visualiser. Si l''option est disponible, la vue monoligne sera toujours la première colonne. En fonction de la personnalisation de l''état interactif, la vue monoligne peut être la vue standard ou une page personnalisée pouvant être mise à jour.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_COMPUTATION',
  p_message_language => 'fr-ch',
  p_message_text => 'Expression de calcul non valide. %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_IS_IN_THE_NEXT',
  p_message_language => 'fr-ch',
  p_message_text => '%0 est dans les %1 suivants');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_KEYPAD',
  p_message_language => 'fr-ch',
  p_message_text => 'Clavier');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MONTH',
  p_message_language => 'fr-ch',
  p_message_text => 'Mois');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MOVE_ALL',
  p_message_language => 'fr-ch',
  p_message_text => 'Déplacer tout');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PERCENT_TOTAL_SUM',
  p_message_language => 'fr-ch',
  p_message_text => 'Pourcentage de la somme totale');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEQUENCE',
  p_message_language => 'fr-ch',
  p_message_text => 'Séquence');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT_ASCENDING',
  p_message_language => 'fr-ch',
  p_message_text => 'Trier par ordre croissant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNIQUE_COLUMN_HEADING',
  p_message_language => 'fr-ch',
  p_message_text => 'L''en-tête de colonne doit être unique.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALUE_REQUIRED',
  p_message_language => 'fr-ch',
  p_message_text => 'Valeur obligatoire');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'LAYOUT.T_CONDITION_EXPR2',
  p_message_language => 'fr-ch',
  p_message_text => 'Expression 2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_SORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Grouper par tri');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_REPORTS',
  p_message_language => 'fr-ch',
  p_message_text => '<li>L''option <b>Etats</b> permet d''afficher les autres états par défaut, et les états enregistrés privés ou publics.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_COLUMN_N',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonne de pivot %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_COLUMN_NOT_NULL',
  p_message_language => 'fr-ch',
  p_message_text => 'La colonne de pivot doit être indiquée.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_COLUMNS',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonnes de pivot');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RPT_DISP_COL_EXCEED',
  p_message_language => 'fr-ch',
  p_message_text => 'Le nombre maximal de colonnes d''affichage de l''état a été atteint. Cliquez sur Sélectionner des colonnes dans le menu Actions pour réduire la liste de colonnes d''affichage de l''état.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_DEFAULT_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Enregistrer l''état par défaut');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_GROUP_BY_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => '- Sélectionner une colonne de regroupement -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALL_ROWS',
  p_message_language => 'fr-ch',
  p_message_text => 'Toutes les lignes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_DAYS',
  p_message_language => 'fr-ch',
  p_message_text => '%0 jours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_MONTHS',
  p_message_language => 'fr-ch',
  p_message_text => '%0 mois');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_ROWS',
  p_message_language => 'fr-ch',
  p_message_text => '<li>Le champ <b>Lignes</b> permet de définir le nombre d''enregistrements à afficher par page.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SAVED_REPORTS.PRIMARY.DEFAULT',
  p_message_language => 'fr-ch',
  p_message_text => 'Val. par défaut du principal');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.X_Y_OF_Z_2',
  p_message_language => 'fr-ch',
  p_message_text => '%0 - %1 de %2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_HOUR',
  p_message_language => 'fr-ch',
  p_message_text => 'Dernière heure');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MIN_X',
  p_message_language => 'fr-ch',
  p_message_text => 'Minimum de %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NONE',
  p_message_language => 'fr-ch',
  p_message_text => '- Aucun -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_GREATER_MAX_DATE',
  p_message_language => 'fr-ch',
  p_message_text => 'La valeur #LABEL# est supérieure à la date maximale indiquée %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_NOT_IN_YEAR_RANGE',
  p_message_language => 'fr-ch',
  p_message_text => 'La valeur #LABEL# ne figure pas dans la plage d''années valide comprise entre %0 et %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GO_TO_ERROR',
  p_message_language => 'fr-ch',
  p_message_text => 'Accéder à l''erreur');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_INVALID2',
  p_message_language => 'fr-ch',
  p_message_text => 'La valeur #LABEL# ne correspond pas au format numérique %0 (exemple : %1).');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'FLOW.SINGLE_VALIDATION_ERROR',
  p_message_language => 'fr-ch',
  p_message_text => '1 erreur s''est produite');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'PAGINATION.PREVIOUS',
  p_message_language => 'fr-ch',
  p_message_text => 'Précédent');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_WEEKS_AGO',
  p_message_language => 'fr-ch',
  p_message_text => 'Depuis %0 semaines');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_YEARS_FROM_NOW',
  p_message_language => 'fr-ch',
  p_message_text => '%0 années à partir de maintenant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'TOTAL',
  p_message_language => 'fr-ch',
  p_message_text => 'Total');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.X_Y_OF_MORE_THAN_Z',
  p_message_language => 'fr-ch',
  p_message_text => 'ligne(s) %0 - %1 de plus de %2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CANCEL',
  p_message_language => 'fr-ch',
  p_message_text => 'Annuler');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_NOT_IN',
  p_message_language => 'fr-ch',
  p_message_text => 'n''est pas dans');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION',
  p_message_language => 'fr-ch',
  p_message_text => 'Calcul');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Supprimer l''état');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DOWN',
  p_message_language => 'fr-ch',
  p_message_text => 'Bas');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FLASHBACK',
  p_message_language => 'fr-ch',
  p_message_text => 'Flashback');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FLASHBACK_DESCRIPTION',
  p_message_language => 'fr-ch',
  p_message_text => 'Une requête Flashback permet de visualiser les données telles qu''elles existaient à un moment donné.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => 'Grouper par colonne %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_AGGREGATE',
  p_message_language => 'fr-ch',
  p_message_text => 'Les agrégats sont des calculs mathématiques réalisés sur une colonne. Ils sont affichés après chaque commande BREAK et à la fin de l''état dans la colonne où ils sont définis. Les options disponibles sont les suivantes : 
<p> 
</p><ul> 
<ul><li><b>Agrégation</b> permet de sélectionner un agrégat 
prédéfini à modifier.</li> 
<li><b>Fonction</b> est la fonction à exécuter (exemple : SUM, MIN).</li> 
<li><b>Colonne</b> permet de sélectionner la colonne à laquelle appliquer la fonction mathématique. Seules les colonnes numériques 
seront affichées.</li> 
</ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SELECT_COLUMNS',
  p_message_language => 'fr-ch',
  p_message_text => 'Utilisé pour modifier les colonnes affichées. Les colonnes situées sur la droite sont affichées, tandis que celles situées sur la gauche sont masquées. Vous pouvez re-trier les colonnes affichées à l''aide de la flèche située à droite. Les colonnes calculées sont précédées par <b>**</b>.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHT',
  p_message_language => 'fr-ch',
  p_message_text => 'Mettre en évidence');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID',
  p_message_language => 'fr-ch',
  p_message_text => 'Non valide');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_YEAR',
  p_message_language => 'fr-ch',
  p_message_text => 'L''année dernière');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_DAY',
  p_message_language => 'fr-ch',
  p_message_text => 'Jour suivant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NUMERIC_SEQUENCE',
  p_message_language => 'fr-ch',
  p_message_text => 'La séquence doit être numérique.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RED',
  p_message_language => 'fr-ch',
  p_message_text => 'rouge');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_ALL',
  p_message_language => 'fr-ch',
  p_message_text => 'Enlever tout');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_DOES_NOT_EXIST',
  p_message_language => 'fr-ch',
  p_message_text => 'L''état n''existe pas.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW',
  p_message_language => 'fr-ch',
  p_message_text => 'Ligne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_TEXT_CONTAINS',
  p_message_language => 'fr-ch',
  p_message_text => 'Le texte de ligne contient');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVED_REPORT_MSG',
  p_message_language => 'fr-ch',
  p_message_text => 'Etat enregistré = "%0"');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEARCH_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Rechercher un état');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UP',
  p_message_language => 'fr-ch',
  p_message_text => 'Haut');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALUE',
  p_message_language => 'fr-ch',
  p_message_text => 'Valeur');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_CHART',
  p_message_language => 'fr-ch',
  p_message_text => 'Visualiser le graphique');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_COL_NOT_NULL',
  p_message_language => 'fr-ch',
  p_message_text => 'La colonne de regroupement doit être indiquée.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_ACTIONS_MENU',
  p_message_language => 'fr-ch',
  p_message_text => '<li>Le <b>menu Actions</b> permet de personnaliser un état. Reportez-vous aux sections qui suivent.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_VIEW',
  p_message_language => 'fr-ch',
  p_message_text => '<li>L''option <b>Icônes Vue</b> permet de basculer entre les vues d''icône, d''état, de détail, de graphique, de regroupement et de pivot de l''état si elles sont définies.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_SORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Tri de pivot');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_PIVOT',
  p_message_language => 'fr-ch',
  p_message_text => 'Enlever le pivot');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_DISPLAY_COLUMN_LIMIT_REACHED',
  p_message_language => 'fr-ch',
  p_message_text => 'Le nombre maximal de colonnes d''affichage de l''état a été atteint. Cliquez sur Sélectionner des colonnes dans le menu Actions pour réduire la liste de colonnes d''affichage de l''état.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_PIVOT_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => 'Ajouter une colonne pour la permutation lignes vers colonnes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_REQUIRED',
  p_message_language => 'fr-ch',
  p_message_text => 'L''adresse électronique doit être indiquée.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_SUBJECT_REQUIRED',
  p_message_language => 'fr-ch',
  p_message_text => 'L''objet du courriel doit être indiqué.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEND',
  p_message_language => 'fr-ch',
  p_message_text => 'Envoyer');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TABLE_SUMMARY',
  p_message_language => 'fr-ch',
  p_message_text => 'Région = %0, état = %1, vue = %2, début des lignes affichées = %3, fin des lignes affichées = %4, nombre total de lignes = %5');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WELCOME_USER',
  p_message_language => 'fr-ch',
  p_message_text => 'Bienvenue, %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'RESTORE',
  p_message_language => 'fr-ch',
  p_message_text => 'Restaurer');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SESSION.EXPIRED',
  p_message_language => 'fr-ch',
  p_message_text => 'Votre session a expiré');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Etat');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'REPORTING_PERIOD',
  p_message_language => 'fr-ch',
  p_message_text => 'Période de génération d''état');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_LESS_MIN_DATE',
  p_message_language => 'fr-ch',
  p_message_text => 'La valeur #LABEL# est inférieure à la date minimale indiquée %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_NOT_BETWEEN_MIN_MAX',
  p_message_language => 'fr-ch',
  p_message_text => 'La valeur #LABEL# ne figure pas dans la plage valide comprise entre %0 et %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_HOURS_FROM_NOW',
  p_message_language => 'fr-ch',
  p_message_text => '%0 heures à partir de maintenant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_MONTHS_AGO',
  p_message_language => 'fr-ch',
  p_message_text => 'Depuis %0 mois');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_WEEKS_FROM_NOW',
  p_message_language => 'fr-ch',
  p_message_text => '%0 semaines à partir de maintenant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_UTILITIES.CLOSE',
  p_message_language => 'fr-ch',
  p_message_text => 'Fermer');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.UNSAVED_DATA',
  p_message_language => 'fr-ch',
  p_message_text => 'Ce panneau contient des modifications non enregistrées. Cliquez sur OK pour continuer sans enregistrer vos modifications. ');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => '4150_COLUMN_NUMBER',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonne %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_3D',
  p_message_language => 'fr-ch',
  p_message_text => '3D');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_MAX',
  p_message_language => 'fr-ch',
  p_message_text => 'Maximum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALL',
  p_message_language => 'fr-ch',
  p_message_text => 'Tout');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AND',
  p_message_language => 'fr-ch',
  p_message_text => 'et');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_BETWEEN',
  p_message_language => 'fr-ch',
  p_message_text => 'entre');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_BOTTOM',
  p_message_language => 'fr-ch',
  p_message_text => 'Tout en bas');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CLEAR',
  p_message_language => 'fr-ch',
  p_message_text => 'effacer');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_INFO',
  p_message_language => 'fr-ch',
  p_message_text => 'Informations de colonne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_ISNOT_IN_LAST',
  p_message_language => 'fr-ch',
  p_message_text => 'n''est pas dans les dern.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IS_IN_NEXT',
  p_message_language => 'fr-ch',
  p_message_text => 'figure sur la ligne suivante');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_LIKE',
  p_message_language => 'fr-ch',
  p_message_text => 'est comme');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_CONFIRM_JS_DIALOG',
  p_message_language => 'fr-ch',
  p_message_text => 'Voulez-vous effectuer cette action de suppression ?');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISPLAYED_COLUMNS',
  p_message_language => 'fr-ch',
  p_message_text => 'Colonnes affichées');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISPLAY_IN_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Afficher dans l''état');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DO_NOT_DISPLAY',
  p_message_language => 'fr-ch',
  p_message_text => 'Ne pas afficher');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXCLUDE_NULL',
  p_message_language => 'fr-ch',
  p_message_text => 'Exclure les valeurs NULL');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FILTER_TYPE',
  p_message_language => 'fr-ch',
  p_message_text => 'Type de filtre');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_FILTER',
  p_message_language => 'fr-ch',
  p_message_text => 'Active l''état en ajoutant ou modifiant la clause <code>WHERE</code> de la requête. Vous pouvez filtrer par colonne ou par ligne.   
<p>Si vous filtrez par colonne, vous devez sélectionner une colonne (pas nécessairement 
une de celles affichées), choisir un opérateur Oracle standard (=, !=, n''est pas dans, entre), puis saisir une expression à comparer. Les expressions font la distinction entre les majuscules et les minuscules. Vous pouvez utiliser le caractère générique % (par exemple, <code>STATE_NAME 
like A%)</code>.</p> 
<p>Si vous filtrez par ligne, vous pouvez créer des clauses <code>WHERE</code> complexes à l''aide 
d''alias de colonne et de toute fonction ou tout opérateur Oracle (par exemple, <code>G = ''VA'' ou G = ''CT''</code>, où 
<code>G</code> est l''alias de <code>CUSTOMER_STATE</code>).</p> 
');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_IS_NOT_IN_THE_LAST',
  p_message_language => 'fr-ch',
  p_message_text => '%0 n''est pas dans les derniers %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_YEAR',
  p_message_language => 'fr-ch',
  p_message_text => 'Année suivante');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO',
  p_message_language => 'fr-ch',
  p_message_text => 'Non');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO_END_DATE',
  p_message_language => 'fr-ch',
  p_message_text => '- Aucune date de fin -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO_COLUMN_INFO',
  p_message_language => 'fr-ch',
  p_message_text => 'Aucune information de colonne disponible.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_OPERATOR',
  p_message_language => 'fr-ch',
  p_message_text => 'Opérateur');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIE',
  p_message_language => 'fr-ch',
  p_message_text => 'A secteurs');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_AGGREGATE',
  p_message_language => 'fr-ch',
  p_message_text => 'Enlever un agrégat');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RESET_CONFIRM',
  p_message_language => 'fr-ch',
  p_message_text => 'Restaurer les paramètres par défaut de l''état.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Enregistrer un état');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Trier');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_STATUS',
  p_message_language => 'fr-ch',
  p_message_text => 'Statut %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SUM_X',
  p_message_language => 'fr-ch',
  p_message_text => 'Total de %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALUE_AXIS_TITLE',
  p_message_language => 'fr-ch',
  p_message_text => 'Titre de l''axe pour la valeur');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.YES_NO.INVALID_VALUE',
  p_message_language => 'fr-ch',
  p_message_text => '#LABEL# doit correspondre aux valeurs %0 et %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_SETTINGS',
  p_message_language => 'fr-ch',
  p_message_text => '%0 paramètres non valides');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT',
  p_message_language => 'fr-ch',
  p_message_text => 'Permutation lignes vers colonnes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_CHART',
  p_message_language => 'fr-ch',
  p_message_text => 'Enlever le graphique');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_REPORT_DEFAULT',
  p_message_language => 'fr-ch',
  p_message_text => 'Enregistrer un état *');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHECK_ALL',
  p_message_language => 'fr-ch',
  p_message_text => 'Cocher tout');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_EXPRESSION',
  p_message_language => 'fr-ch',
  p_message_text => 'Expression de calcul');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SUBSCRIPTION',
  p_message_language => 'fr-ch',
  p_message_text => 'Abonnement');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'RESET',
  p_message_language => 'fr-ch',
  p_message_text => 'Réinitialiser la pagination');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_X_YEARS',
  p_message_language => 'fr-ch',
  p_message_text => '%0 prochaines années');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO_COMPUTATION_DEFINED',
  p_message_language => 'fr-ch',
  p_message_text => 'Aucun calcul défini.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_INVALID',
  p_message_language => 'fr-ch',
  p_message_text => 'La valeur #LABEL# ne concorde pas avec le format %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.FILE_BROWSE.DOWNLOAD_LINK_TEXT',
  p_message_language => 'fr-ch',
  p_message_text => 'Télécharger en local');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_MONTHS_FROM_NOW',
  p_message_language => 'fr-ch',
  p_message_text => 'Dans %0 mois');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_SECONDS_FROM_NOW',
  p_message_language => 'fr-ch',
  p_message_text => '%0 secondes à partir de maintenant');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_UTILITIES.CAL',
  p_message_language => 'fr-ch',
  p_message_text => 'Calendrier');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGGREGATE',
  p_message_language => 'fr-ch',
  p_message_text => 'Agréger');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_CONTAINS',
  p_message_language => 'fr-ch',
  p_message_text => 'contient');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IN',
  p_message_language => 'fr-ch',
  p_message_text => 'dans');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IS_NULL',
  p_message_language => 'fr-ch',
  p_message_text => 'est NULL');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_NOT_LIKE',
  p_message_language => 'fr-ch',
  p_message_text => 'n''est pas comme');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_FOOTER_E1',
  p_message_language => 'fr-ch',
  p_message_text => '(B+C)*100');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CONTROL_BREAK',
  p_message_language => 'fr-ch',
  p_message_text => 'Commande BREAK');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DAY',
  p_message_language => 'fr-ch',
  p_message_text => 'jour');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_CONFIRM',
  p_message_language => 'fr-ch',
  p_message_text => 'Voulez-vous supprimer ces paramètres d''état ?');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DO_NOT_AGGREGATE',
  p_message_language => 'fr-ch',
  p_message_text => '- Ne pas agréger -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_ADDRESS',
  p_message_language => 'fr-ch',
  p_message_text => 'Adresse électronique');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ENABLE',
  p_message_language => 'fr-ch',
  p_message_text => 'Activer');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXAMPLES_WITH_COLON',
  p_message_language => 'fr-ch',
  p_message_text => 'Exemples :');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FILTER_EXPRESSION',
  p_message_language => 'fr-ch',
  p_message_text => 'Expression de filtre');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FORMAT',
  p_message_language => 'fr-ch',
  p_message_text => 'Format');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FUNCTION',
  p_message_language => 'fr-ch',
  p_message_text => 'Fonction');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_01',
  p_message_language => 'fr-ch',
  p_message_text => 'Les régions d''état interactif permettent aux utilisateurs finals de personnaliser les états. Les utilisateurs peuvent modifier la mise en page des données d''état en choisissant des colonnes, en appliquant des filtres, en mettant des valeurs en évidence et en les triant. Ils peuvent également définir des sections, des agrégations, des graphiques ainsi que des regroupements et ajouter des calculs personnalisés. Ils peuvent aussi définir un abonnement pour l''envoi par courriel d''une version HTML de l''état à un intervalle défini. Il est possible de créer plusieurs versions d''un état et de les enregistrer sous forme d''états nommés, pour une utilisation publique ou privée.  
<p/> 
Les sections qui suivent récapitulent les méthodes de personnalisation d''un état interactif. Pour en savoir plus, reportez-vous à "Using Interactive Reports" dans le manuel <a href="http://www.oracle.com/pls/topic/lookup?ctx=E37097_01&id=AEEUG453" target="_blank"><i>Oracle Application Express End User''s Guide</i></a>.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_DOWNLOAD',
  p_message_language => 'fr-ch',
  p_message_text => 'Permet le téléchargement de l''ensemble de résultats en cours. Les formats de téléchargement 
seront différents en fonction de votre installation et de la définition d''état, 
et pourront inclure CSV, HTML, courriel, XLS, PDF ou RTF.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_HIGHLIGHT',
  p_message_language => 'fr-ch',
  p_message_text => '<p>Permet de définir un filtre. Les lignes correspondant aux critères du filtre sont mises en évidence à l''aide des caractéristiques associées au filtre. Les options disponibles sont les suivantes :</p> 
<ul> 
<li><b>Nom</b> est utilisé uniquement pour l''affichage.</li> 
<li><b>Séquence</b> identifie la séquence utilisée pour l''évaluation des règles.</li> 
<li><b>Activé</b> indique si la règle est activée ou non.</li> 
<li><b>Mettre le type en évidence</b> indique si la ligne ou la cellule doit être 
mise en évidence. Si vous sélectionnez la cellule, la colonne référencée sous 
Conditions de mise en évidence sera mise en évidence.</li> 
<li><b>Couleur d''arrière-plan</b> indique la nouvelle couleur d''arrière-plan pour les zones mises en évidence.</li> 
<li><b>Couleur du texte</b> indique la nouvelle couleur de texte pour les zones mises en évidence.</li> 
<li><b>Conditions de mise en évidence</b> définit vos conditions de filtrage.</li> 
</ul> 
');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SAVE_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => '<p>Enregistre l''état personnalisé en vue d''une utilisation ultérieure. Vous devez indiquer un nom, pouvez entrer une description et rendre l''état accessible au public (à savoir, à tous les utilisateurs pouvant accéder à l''état par défaut principal). Vous pouvez enregistrer quatre types d''état interactif :</p> 
<ul> 
<li><strong>Val. par défaut du principal</strong> (développeur uniquement) : l''état par défaut principal est celui qui est affiché initialement. Les états de ce type ne peuvent être ni renommés, ni supprimés.</li> 
<li><strong>Etat de remplacement</strong> (développeur uniquement) : permet aux développeurs de créer différentes mises en page d''état. Seuls les développeurs sont autorisés à enregistrer, renommer et supprimer ce type d''état.</li> 
<li><strong>Etat public</strong> (utilisateur final) : ce type d''état peut être enregistré, renommé et supprimé par l''utilisateur final l''ayant créé. Les autres utilisateurs peuvent visualiser et enregistrer la mise en page, comme pour les autres états.</li> 
<li><strong>Etat privé</strong> (utilisateur final) : seul l''utilisateur final ayant créé l''état est autorisé à le visualiser, l''enregistrer, le renommer et le supprimer.</li> 
</ul> 
<p>Lorsque vous enregistrez des états personnalisés, un sélecteur d''états apparaît dans la barre de recherche à gauche du sélecteur de lignes (à condition que cette fonctionnalité soit activée).</p> 
');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIDE_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => 'Masquer la colonne');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_IS_NOT_IN_THE_NEXT',
  p_message_language => 'fr-ch',
  p_message_text => '%0 n''est pas dans les %1 suivants');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MEDIAN_X',
  p_message_language => 'fr-ch',
  p_message_text => 'Médiane de %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_HOUR',
  p_message_language => 'fr-ch',
  p_message_text => 'Heure suivante');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NUMERIC_FLASHBACK_TIME',
  p_message_language => 'fr-ch',
  p_message_text => 'L''heure de l''opération Flashback doit être une valeur numérique.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_OTHER',
  p_message_language => 'fr-ch',
  p_message_text => 'Autre');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_ORDER',
  p_message_language => 'fr-ch',
  p_message_text => 'Tri des lignes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEARCH_BAR',
  p_message_language => 'fr-ch',
  p_message_text => 'Barre de recherche');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_FUNCTION',
  p_message_language => 'fr-ch',
  p_message_text => '- Sélectionner une fonction -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_SORT_COLUMN',
  p_message_language => 'fr-ch',
  p_message_text => '- Sélectionner une colonne de tri -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SUBSCRIPTION_STARTING_FROM',
  p_message_language => 'fr-ch',
  p_message_text => 'Début');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TEXT_COLOR',
  p_message_language => 'fr-ch',
  p_message_text => 'Couleur du texte');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_HOURS',
  p_message_language => 'fr-ch',
  p_message_text => 'heures');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_GROUP_BY',
  p_message_language => 'fr-ch',
  p_message_text => 'Visualiser le groupement');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_WEEKLY',
  p_message_language => 'fr-ch',
  p_message_text => 'Hebdomadaire');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_YELLOW',
  p_message_language => 'fr-ch',
  p_message_text => 'jaune');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER.ICON_TEXT',
  p_message_language => 'fr-ch',
  p_message_text => 'Calendrier instantané : %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.SLIDER.VALUE_NOT_BETWEEN_MIN_MAX',
  p_message_language => 'fr-ch',
  p_message_text => 'La valeur #LABEL# ne figure pas dans la plage valide comprise entre %0 et %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.YES_NO.NO_LABEL',
  p_message_language => 'fr-ch',
  p_message_text => 'Non');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.YES_NO.YES_LABEL',
  p_message_language => 'fr-ch',
  p_message_text => 'Oui');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FUNCTION_N',
  p_message_language => 'fr-ch',
  p_message_text => 'Fonction %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INACTIVE_SETTING',
  p_message_language => 'fr-ch',
  p_message_text => '1 paramètre inactif');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_SETTING',
  p_message_language => 'fr-ch',
  p_message_text => '1 paramètre non valide');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_AGG_NOT_ON_ROW_COL',
  p_message_language => 'fr-ch',
  p_message_text => 'Vous ne pouvez pas effectuer l''agrégation sur une colonne sélectionnée en tant que colonne de ligne.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RENAME_DEFAULT_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Renommer l''état par défaut');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_FUNCTION',
  p_message_language => 'fr-ch',
  p_message_text => 'Ajouter une fonction');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_DEFAULT_REPORT',
  p_message_language => 'fr-ch',
  p_message_text => 'Supprimer l''état par défaut');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT_ORDER',
  p_message_language => 'fr-ch',
  p_message_text => 'Ordre de tri');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_PIVOT',
  p_message_language => 'fr-ch',
  p_message_text => 'Afficher le pivot');



COMMIT; END;
/