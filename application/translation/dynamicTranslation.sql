DECLARE
  v_workspace_name VARCHAR2(100) := 'STPV'; -- APEX Workspace Name
  v_app_id NUMBER := 100; -- APEX Application ID
  v_session_id NUMBER := 1; -- APEX Session ID (doesn't matter)
  v_language varchar2(6) := 'fr-ch';
  v_workspace_id apex_workspaces.workspace_id%type;
begin

-- Get APEX workspace ID by name
  select 
    workspace_id
  into 
    v_workspace_id
  from 
    apex_workspaces
  where 
    upper(workspace) = upper(v_workspace_name);

-- Set APEX workspace ID
  apex_util.set_security_group_id(v_workspace_id);

-- Set APEX application ID
  apex_application.g_flow_id := v_app_id; 

-- Set APEX session ID
  apex_application.g_instance := v_session_id; 

  wwv_flow_api.remove_dyanamic_translation (p_flow_id => v_app_id, p_language => 'de-ch' );
  wwv_flow_api.remove_dyanamic_translation (p_flow_id => v_app_id, p_language => v_language );
  
  wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Frau', p_to => 'Madame');
  wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Herr', p_to => 'Monsieur');
  wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Firma', p_to => 'Société');
  
  wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Ja', p_to => 'Oui');
  wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Nein', p_to => 'Non');
--exec wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>'de-ch', p_from=>'Herr', p_to => 'Herr');

wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Instrumentalkurs', p_to => 'cours instrumental');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'J+M-Ausbildner', p_to => 'formateur J+M');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'J+M-Experte', p_to => 'expert J+M');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'J+M-Leiter', p_to => 'moniteur J+M');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Jugend+Musik', p_to => 'jeunesse et musique');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Jury', p_to => 'jury');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Jurygrundkurs', p_to => 'cours de jury de base');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Leiter Jungtambour/-pfeifer', p_to => 'moniteur jeunes tambours / fifres');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Leiterkurs', p_to => 'cours moniteurs');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Leiterkurs 1', p_to => 'cours moniteurs 1');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Leiterkurs 2', p_to => 'cours moniteurs 2');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Leiterkurs 3', p_to => 'cours moniteurs 3');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Mittelstufe', p_to => 'niveau intermédiaire');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Oberstufe', p_to => 'niveau supérieur');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Unterstufe', p_to => 'niveau de base');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'BM', p_to => 'BM');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Keine', p_to => 'aucun');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Trommel-Komposition', p_to => 'composition tambour');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Trommel-Marsch', p_to => 'marche tambour');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Ex-Military', p_to => 'ex-militaire');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Military', p_to => 'militaire');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'n/a', p_to => 'n/a');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'No Military', p_to => 'non militaire');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Aktivmitglied', p_to => 'membre actif');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'ausgetreten', p_to => 'démissionné');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'divers', p_to => 'divers');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Ehrenmitglied', p_to => 'membre d''honneur');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Ehrung erwünscht', p_to => 'honorariat désiré');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Freimitglied', p_to => 'membre libre');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'keine Ehrung erwünscht', p_to => 'honorariat pas désiré');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'männlich', p_to => 'masculin');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'nicht aktiv, in Ausbildung', p_to => 'pas actif, en formation');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'nicht aktiv, sonstige Funktion', p_to => 'pas actif, autre fonction');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Träger der goldenen Ehrennadel', p_to => 'porteur de l''insigne d''honneur or');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'weiblich', p_to => 'féminin');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'aktiv', p_to => 'actif');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'inaktiv', p_to => 'inactif');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Deutschland', p_to => 'Allemagne');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Frankreich', p_to => 'France');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Fürstentum Liechtenstein', p_to => 'principauté du Liechtenstein');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Grossbritannien', p_to => 'Royaume-Uni');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Holland', p_to => 'Pays-Bas');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Irland', p_to => 'Irlande');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Österreich', p_to => 'Autriche');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Schweiz', p_to => 'Suisse');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'USA', p_to => 'USA');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Clairon', p_to => 'clairon');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Ehrengarde', p_to => 'garde d''honneur');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Fähnrich', p_to => 'baneret - porte-drapeau');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Fifre ancien', p_to => 'fifre ancien');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Hellebardier', p_to => 'hallebardier');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Natwaerisch', p_to => 'Natwärisch');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Piccolo', p_to => 'fifre');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Trommel', p_to => 'tambour');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Experte', p_to => 'expert');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Fähnrich, Bannerträger', p_to => 'baneret - porte-drapeau');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Jury Clairon', p_to => 'jury clairon');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Jury Natwärisch', p_to => 'jury Natwärisch');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Jury Piccolo', p_to => 'jury fifre');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Jury Tambour', p_to => 'jury tambour');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Jury TC', p_to => 'jury TC');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Jury TN', p_to => 'jury TN');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Jury TP', p_to => 'jury TP');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Jury TPer', p_to => 'jury TPer');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Leiter BK Noten', p_to => 'responsable registre instruments à vent');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Leiter Bläserkommission', p_to => 'chef commission instruments à vent');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Leiter Jungpfeifer/Jungbläser', p_to => 'moniteur jeunes fifres');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Leiter Jungtambouren', p_to => 'moniteur jeunes tambours');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Leiter Klako BK', p_to => 'responsable Klako BK');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Leiter Pfeifer / Bläser', p_to => 'moniteur fifres');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Leiter Tambouren', p_to => 'moniteur tambours');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Leiter Tambourenkommission', p_to => 'chef commission tambours');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Material-/Instrumentenverwalter', p_to => 'responsable matériel et instruments');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Mitglied', p_to => 'membre');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Mitglied  BK Noten', p_to => 'membre registre instruments à vent');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Mitglied Bläserkommission', p_to => 'membre commission instruments à vent');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Mitglied Klako BK', p_to => 'membre Klako BK');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Mitglied Tambourenkommission', p_to => 'membre commission tambour');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Pfeifer-Bläserchef', p_to => 'chef fifres');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Rolle Vorstand', p_to => 'rôle comité');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Sektionsleiter / musikalische Gesamtleitung', p_to => 'moniteur de section / direction musicale générale');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Dachverband', p_to => 'association faîtière');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Pfeifergruppe', p_to => 'groupe fifres');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Tambouren- und Clairon Verein', p_to => 'société tambours et clairons');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Tambouren- und Pfeiferverein', p_to => 'société tambours et fifres');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Tambourengruppe', p_to => 'groupe tambours');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Tambourenverein', p_to => 'société tambours');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Verband', p_to => 'association');

wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Administrator', p_to => 'Adminstrateur');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Chef Musikkommision', p_to => 'Chef de la commission de musique');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Kassier', p_to => 'Caissier');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Militärverantwortlicher', p_to => 'Responsable militaire');

wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Präsident', p_to => 'Président');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Rechnungsprüfer/GPK', p_to => 'Verificateur des comptes/CVA');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Rolle Vorstand', p_to => 'Rôle dans le comité');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Sekretär Bläserkommission', p_to => 'Secrétaire CF');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Sekretär Tambourenkommission', p_to => 'Secrétaire CT');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Sekretär/Aktuar', p_to => 'Secrétaire');

wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Vizepräsident', p_to => 'Vice-président');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Vorstand', p_to => 'Comité');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Mitglied  BK Noten', p_to => 'Membre registre instruments à vent');
wwv_flow_api.create_dynamic_translation(p_flow_id=>v_app_id, p_language=>v_language, p_from=>'Sektionsleiter / musikalische Gesamtleitung', p_to => 'Moniteur /  direction musicale');


end;
/

commit;

DECLARE
  v_workspace_name VARCHAR2(100) := 'STPV'; -- APEX Workspace Name
  v_app_id NUMBER := 100; -- APEX Application ID
  v_session_id NUMBER := 1; -- APEX Session ID (doesn't matter)

  v_workspace_id apex_workspaces.workspace_id%type;
begin

-- Get APEX workspace ID by name
  select 
    workspace_id
  into 
    v_workspace_id
  from 
    apex_workspaces
  where 
    upper(workspace) = upper(v_workspace_name);

-- Set APEX workspace ID
  apex_util.set_security_group_id(v_workspace_id);

-- Set APEX application ID
  apex_application.g_flow_id := v_app_id; 

-- Set APEX session ID
  apex_application.g_instance := v_session_id; 


  wwv_flow_api.remove_dyanamic_translation (p_flow_id => v_app_id, p_language => 'de-ch' );
end;
/


select  distinct lipa_module, 
lipa_disp_value from cm_list_parameter
where lipa_module not in ( 'ACCESS', 'COMMON')
and lipa_name not in ('ANREDE')
and lipa_disp_value not in ('Ja', 'Nein')
order by 1, 2;

DECLARE
  v_workspace_name VARCHAR2(100) := 'STPV'; -- APEX Workspace Name
  v_app_id NUMBER := 100; -- APEX Application ID
  v_session_id NUMBER := 1; -- APEX Session ID (doesn't matter)
  v_language varchar2(6) := 'fr-ch';
  v_workspace_id apex_workspaces.workspace_id%type;
begin

-- Get APEX workspace ID by name
  select 
    workspace_id
  into 
    v_workspace_id
  from 
    apex_workspaces
  where 
    upper(workspace) = upper(v_workspace_name);

-- Set APEX workspace ID
  apex_util.set_security_group_id(v_workspace_id);

-- Set APEX application ID
  apex_application.g_flow_id := v_app_id; 

-- Set APEX session ID
  apex_application.g_instance := v_session_id; 


end;
/

DECLARE
  v_workspace_name VARCHAR2(100) := 'STPV'; -- APEX Workspace Name
  v_app_id NUMBER := 100; -- APEX Application ID
  v_session_id NUMBER := 1; -- APEX Session ID (doesn't matter)
  v_language varchar2(6) := 'fr-ch';
  v_workspace_id apex_workspaces.workspace_id%type;
begin

-- Get APEX workspace ID by name
  select 
    workspace_id
  into 
    v_workspace_id
  from 
    apex_workspaces
  where 
    upper(workspace) = upper(v_workspace_name);

-- Set APEX workspace ID
  apex_util.set_security_group_id(v_workspace_id);

-- Set APEX application ID
  apex_application.g_flow_id := v_app_id; 

-- Set APEX session ID
  apex_application.g_instance := v_session_id; 

end;
/
