/*
The MIT License (MIT)

Copyright (c) 2018 Pretius Sp. z o.o. sk.
Żwirki i Wigury 16a
02-092 Warsaw, Poland
www.pretius.com
www.translate-apex.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

SET DEFINE OFF
ALTER SESSION SET NLS_LANGUAGE = AMERICAN;

    
DECLARE
  v_workspace_name VARCHAR2(100) := 'STPV'; -- APEX Workspace Name
  v_app_id NUMBER := 100; -- APEX Application ID
  v_session_id NUMBER := 1; -- APEX Session ID (doesn't matter)

  v_workspace_id apex_workspaces.workspace_id%type;

BEGIN
-- Get APEX workspace ID by name
  select 
    workspace_id
  into 
    v_workspace_id
  from 
    apex_workspaces
  where 
    upper(workspace) = upper(v_workspace_name);

-- Set APEX workspace ID
  apex_util.set_security_group_id(v_workspace_id);

-- Set APEX application ID
  apex_application.g_flow_id := v_app_id; 

-- Set APEX session ID
  apex_application.g_instance := v_session_id; 

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'RESET',
  p_message_language => 'de-ch',
  p_message_text => 'Seitennummerierung zurücksetzen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHT_TYPE',
  p_message_language => 'de-ch',
  p_message_text => 'Markierungstyp');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MAX_QUERY_COST',
  p_message_language => 'de-ch',
  p_message_text => 'Die Abfrage wird wahrscheinlich die maximal zulässigen Ressourcen überschreiten. Ändern Sie die Berichteinstellungen, und wiederholen Sie den Vorgang.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEW_CATEGORY',
  p_message_language => 'de-ch',
  p_message_text => '- Neue Kategorie -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO_COMPUTATION_DEFINED',
  p_message_language => 'de-ch',
  p_message_text => 'Keine Berechnung definiert.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_INVALID2',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# entspricht nicht dem Zahlenformat %0 (Beispiel: %1).');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'PAGINATION.NEXT_SET',
  p_message_language => 'de-ch',
  p_message_text => 'Nächste Gruppe');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'PAGINATION.PREVIOUS_SET',
  p_message_language => 'de-ch',
  p_message_text => 'Vorherige Gruppe');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_MONTHS_AGO',
  p_message_language => 'de-ch',
  p_message_text => 'vor %0 Monaten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_WEEKS_AGO',
  p_message_language => 'de-ch',
  p_message_text => 'vor %0 Wochen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_UTILITIES.CAL',
  p_message_language => 'de-ch',
  p_message_text => 'Kalender');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALL_COLUMNS',
  p_message_language => 'de-ch',
  p_message_text => 'Alle Spalten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AND',
  p_message_language => 'de-ch',
  p_message_text => 'und');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_BLUE',
  p_message_language => 'de-ch',
  p_message_text => 'blau');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_HEADING_MENU',
  p_message_language => 'de-ch',
  p_message_text => 'Menü "Spaltenüberschrift"');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_INFO',
  p_message_language => 'de-ch',
  p_message_text => 'Spalteninformationen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_ISNOT_IN_LAST',
  p_message_language => 'de-ch',
  p_message_text => 'ist nicht in den letzten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_NOT_LIKE',
  p_message_language => 'de-ch',
  p_message_text => 'nicht wie');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_FOOTER_E1',
  p_message_language => 'de-ch',
  p_message_text => '(B+C)*100');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COUNT_DISTINCT',
  p_message_language => 'de-ch',
  p_message_text => 'Nur eindeutige Reihen zählen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE',
  p_message_language => 'de-ch',
  p_message_text => 'Löschen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_CONFIRM_JS_DIALOG',
  p_message_language => 'de-ch',
  p_message_text => 'Soll dieser Löschvorgang ausgeführt werden?');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DETAIL_VIEW',
  p_message_language => 'de-ch',
  p_message_text => 'Single Row-Ansicht');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_HIGHLIGHT',
  p_message_language => 'de-ch',
  p_message_text => 'Markierung bearbeiten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ERROR',
  p_message_language => 'de-ch',
  p_message_text => 'Fehler');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HCOLUMN',
  p_message_language => 'de-ch',
  p_message_text => 'Horizontale Spalte');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP',
  p_message_language => 'de-ch',
  p_message_text => 'Hilfe');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_ACTIONS_MENU',
  p_message_language => 'de-ch',
  p_message_text => 'Das Menü "Aktion" wird in der Suchleiste rechts der Schaltfläche "Los" angezeigt. Mit diesem Menü können Sie interaktive Berichte anpassen.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_COMPUTE',
  p_message_language => 'de-ch',
  p_message_text => 'Lässt Sie berechnete Spalten zu dem Bericht hinzufügen. Dabei kann es sich um mathematische Berechnungen (z.B. <code>NBR_HOURS/24</code>) oder Oracle-Standardfunktionen handeln, die für vorhandene Spalten angewendet werden. Einige werden als Beispiel angezeigt, andere, wie <code>TO_DATE</code>, können ebenfalls verwendet werden. Zu den Optionen gehören: 
<p/> 
<ul><li><b>Berechnung</b>: Ermöglicht die Wahl einer vorher definierten Berechnung zur Bearbeitung.</li> 
<li><b>Spaltenüberschrift</b>: Ist die Spaltenüberschrift für die neue Spalte.</li> 
<li><b>Formatmaske</b>: Ist eine Oracle-Formatmaske, die für die Spalte angewendet werden soll (z.B. S9999).</li> 
<li><b>Berechnung</b>: Ist die auszuführende Berechnung. Innerhalb der Berechnung werden Spalten mit den angezeigten Aliasnamen referenziert.</li> 
</ul> 
<p>Unter der Berechnung werden die Spalten in der Abfrage mit den zugehörigen Aliasnamen angezeigt. Wenn Sie auf den Spalten- oder Aliasnamen klicken, wird dieser in die Berechnung eingefügt. Neben "Spalten" befindet sich eine Zehnertastatur. Sie enthält Shortcuts für häufig verwendete Tasten. Ganz außen rechts stehen Funktionen</p> 
<p>Eine Beispielberechnung zur Anzeige der Gesamtvergütung folgt: 
<p/> 
<pre>CASE WHEN A = ''SALES'' THEN B + C ELSE B END</pre> 
(wobei A ORGANIZATION [Unternehmen], B SALARY [Gehalt] und C COMMISSION [Provision] ist)</p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_FILTER',
  p_message_language => 'de-ch',
  p_message_text => 'Fokussiert den Bericht durch Hinzufügen oder Ändern der <code>WHERE</code>-Klausel der Abfrage. Sie können nach Spalte oder Zeile filtern.    
<p>IWenn Sie nach Spalte filtern, wählen Sie zunächst eine Spalte (nicht unbedingt die angezeigte Spalte). Wählen Sie dann einen Operator aus der Liste mit Oracle-Standardoperatoren (=, !=, nicht in, zwischen), und geben Sie einen Ausdruck für den Vergleich ein. Beim Ausdruck muss die Groß-/Kleinschreibung beachtet werden. Sie können % als Platzhalterzeichen verwenden (Beispiel: <code>STATE_NAME wie A%)</code>.</p> 
<p>Wenn Sie nach Zeile filtern, können Sie komplexe <code>WHERE</code>-Klauseln mit 
Spaltenaliasnamen und beliebigen Oracle-Funktionen oder -Operatoren erstellen (Beispiel: <code>G = ''VA'' oder G = ''CT''</code>, wobei <code>G</code> der Alias für <code>CUSTOMER_STATE</code> ist).</p> 
');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_FLASHBACK',
  p_message_language => 'de-ch',
  p_message_text => 'Flashback Query ermöglicht Ihnen, die Daten in dem Status anzuzeigen, den sie zu einem früheren Zeitpunkt hatten. Die Standardzeit für das Flashback beträgt 3 Stunden (oder 180 Minuten), die tatsächliche Zeit differiert jedoch je nach Datenbank.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIDE_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => 'Spalte ausblenden');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHT_WHEN',
  p_message_language => 'de-ch',
  p_message_text => 'Markieren wenn');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_DAY',
  p_message_language => 'de-ch',
  p_message_text => 'Nächster Tag');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_YEAR',
  p_message_language => 'de-ch',
  p_message_text => 'Nächstes Jahr');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO',
  p_message_language => 'de-ch',
  p_message_text => 'Nein');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO_END_DATE',
  p_message_language => 'de-ch',
  p_message_text => '- Kein Enddatum -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PRIMARY_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Hauptbericht');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_HIGHLIGHT',
  p_message_language => 'de-ch',
  p_message_text => 'Markierung entfernen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_SETTINGS',
  p_message_language => 'de-ch',
  p_message_text => 'Berichteinstellungen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROWS',
  p_message_language => 'de-ch',
  p_message_text => 'Zeilen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_TEXT_CONTAINS',
  p_message_language => 'de-ch',
  p_message_text => 'Zeilentext enthält');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE',
  p_message_language => 'de-ch',
  p_message_text => 'Speichern');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Bericht speichern');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_COLUMNS_FOOTER',
  p_message_language => 'de-ch',
  p_message_text => 'Vor berechneten Spalten steht **.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_WEEKS',
  p_message_language => 'de-ch',
  p_message_text => 'Wochen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNAUTHORIZED',
  p_message_language => 'de-ch',
  p_message_text => 'Nicht autorisiert');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALID_FORMAT_MASK',
  p_message_language => 'de-ch',
  p_message_text => 'Geben Sie eine gültige Formatmaske ein.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALUE_AXIS_TITLE',
  p_message_language => 'de-ch',
  p_message_text => 'Achsenbeschriftung für Wert');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.REGION.JQM_LIST_VIEW.SEARCH',
  p_message_language => 'de-ch',
  p_message_text => 'Suchen...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_FILTER_QUERY',
  p_message_language => 'de-ch',
  p_message_text => 'Ungültige Filterabfrage');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT',
  p_message_language => 'de-ch',
  p_message_text => 'Pivot');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_COLUMNS',
  p_message_language => 'de-ch',
  p_message_text => 'Zeilenspalten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DUPLICATE_PIVOT_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => 'Doppelt vorhandene Pivotspalte. Pivotspaltenliste muss eindeutig sein.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SUBSCRIPTION',
  p_message_language => 'de-ch',
  p_message_text => 'Subscription');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_ROWS_PER_PAGE',
  p_message_language => 'de-ch',
  p_message_text => 'Legt die Anzahl der pro Seite anzuzeigenden Datensätze fest');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'RESTORE',
  p_message_language => 'de-ch',
  p_message_text => 'Wiederherstellen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SESSION.EXPIRED',
  p_message_language => 'de-ch',
  p_message_text => 'Ihre Session ist abgelaufen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'OUT_OF_RANGE',
  p_message_language => 'de-ch',
  p_message_text => 'Ungültige Zeilenmenge angefordert, die Quelldaten des Berichts wurden geändert.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORTS',
  p_message_language => 'de-ch',
  p_message_text => 'Berichte');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_GREATER_MAX_VALUE',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# ist größer als das angegebene Maximum %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_HOURS_AGO',
  p_message_language => 'de-ch',
  p_message_text => 'vor %0 Stunden');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_NOW',
  p_message_language => 'de-ch',
  p_message_text => 'Jetzt');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_SECONDS_AGO',
  p_message_language => 'de-ch',
  p_message_text => 'vor %0 Sekunden');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_SECONDS_FROM_NOW',
  p_message_language => 'de-ch',
  p_message_text => '%0 Sekunden ab jetzt');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_YEARS_FROM_NOW',
  p_message_language => 'de-ch',
  p_message_text => '%0 Jahre ab jetzt');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.SORT_BY_THIS_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => 'Nach dieser Spalte sortieren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.UNSAVED_DATA',
  p_message_language => 'de-ch',
  p_message_text => 'Diese Form enthält ungespeicherte Änderungen. Klicken Sie auf "OK", um fortzufahren, ohne Ihre Änderungen zu speichern. ');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.X_Y_OF_MORE_THAN_Z',
  p_message_language => 'de-ch',
  p_message_text => 'Zeile(n) %0 - %1 von mehr als %2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ACTIONS_MENU',
  p_message_language => 'de-ch',
  p_message_text => 'Menü "Aktionen"');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_COUNT',
  p_message_language => 'de-ch',
  p_message_text => 'Anzahl');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALTERNATIVE_DEFAULT_NAME',
  p_message_language => 'de-ch',
  p_message_text => 'Alternativer Standardwert: %0 ');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CELL',
  p_message_language => 'de-ch',
  p_message_text => 'Zelle');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_HEADING',
  p_message_language => 'de-ch',
  p_message_text => 'Spaltenüberschrift');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IS_NOT_NULL',
  p_message_language => 'de-ch',
  p_message_text => 'ist nicht Null');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COUNT_X',
  p_message_language => 'de-ch',
  p_message_text => 'Anzahl %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DAILY',
  p_message_language => 'de-ch',
  p_message_text => 'Täglich');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DEFAULT',
  p_message_language => 'de-ch',
  p_message_text => 'Standard');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DEFAULT_REPORT_TYPE',
  p_message_language => 'de-ch',
  p_message_text => 'Standardmäßiger Berichtstyp');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Bericht löschen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISPLAYED_COLUMNS',
  p_message_language => 'de-ch',
  p_message_text => 'Angezeigte Spalten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DO_NOT_AGGREGATE',
  p_message_language => 'de-ch',
  p_message_text => '- Nicht aggregieren -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DO_NOT_DISPLAY',
  p_message_language => 'de-ch',
  p_message_text => 'Nicht anzeigen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_FILTER',
  p_message_language => 'de-ch',
  p_message_text => 'Filter bearbeiten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_ADDRESS',
  p_message_language => 'de-ch',
  p_message_text => 'E-Mail-Adresse');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_FREQUENCY',
  p_message_language => 'de-ch',
  p_message_text => 'Häufigkeit');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_SEE_ATTACHED',
  p_message_language => 'de-ch',
  p_message_text => 'Siehe Anhang');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXAMPLES_WITH_COLON',
  p_message_language => 'de-ch',
  p_message_text => 'Beispiele:');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXCLUDE_NULL',
  p_message_language => 'de-ch',
  p_message_text => 'Nullwerte ausschließen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXPRESSION',
  p_message_language => 'de-ch',
  p_message_text => 'Ausdruck');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FLASHBACK_DESCRIPTION',
  p_message_language => 'de-ch',
  p_message_text => 'Mit Flashback Query können Sie die Daten in dem Status anzeigen, den sie zu einem vorherigen Zeitpunkt hatten.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => 'Nach Spalte %0 gruppieren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GREEN',
  p_message_language => 'de-ch',
  p_message_text => 'grün');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_CHART',
  p_message_language => 'de-ch',
  p_message_text => 'Sie können ein Diagramm pro gespeicherten Bericht definieren. Nach der Definition können Sie mit den Ansichtsymbolen in der Suchleiste zwischen der Diagramm- und Berichtsansicht wechseln. Zu den Optionen gehören:  
<p> 
</p><ul> 
<li><b>Diagrammtyp</b>: Gibt den aufzunehmenden Diagrammtyp an.  
Optionen: Horizontaler Balken, Vertikaler Balken, Kreis oder Linie.</li> 
<li><b>Label</b>: Wählen Sie die Spalte, die als Label verwendet werden soll.</li> 
<li><b>Achsenbeschriftung für Label</b>: Diese Beschriftung wird auf der Achse angezeigt, die mit der  
als Label gewählten Spalte verknüpft ist. Diese Option ist bei Tortendiagrammen nicht verfügbar.</li> 
<li><b>Wert</b>: Wählen Sie die Spalte, die als Wert verwendet werden soll. Wenn als Funktion  
COUNT verwendet wird, muss kein Wert gewählt werden.</li> 
<li><b>Achsenbeschriftung für Wert</b>: Diese Beschriftung wird auf der Achse angezeigt, die mit der als 
Wert gewählten Spalte verknüpft ist. Diese Option ist bei Tortendiagrammen nicht verfügbar.</li> 
<li><b>Funktion</b>: Ist eine optionale Funktion, die für die als Wert gewählte Spalte ausgeführt werden soll.</li> 
<li><b>Sortieren</b>: Zum Sortieren der Ergebnismenge.</li></ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SELECT_COLUMNS',
  p_message_language => 'de-ch',
  p_message_text => 'Wird zur Änderung der angezeigten Spalten verwendet. Die Spalten auf der rechten Seite werden angezeigt. Die Spalten auf der linken Seite werden ausgeblendet. Sie können die angezeigten Spalten mit den Pfeilen ganz außen rechts neu anordnen. Vor berechneten Spalten steht <b>**</b>.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_X_YEARS',
  p_message_language => 'de-ch',
  p_message_text => 'Letzte %0 Jahre');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_YEAR',
  p_message_language => 'de-ch',
  p_message_text => 'Letztes Jahr');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LINE',
  p_message_language => 'de-ch',
  p_message_text => 'Zeile');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEW_AGGREGATION',
  p_message_language => 'de-ch',
  p_message_text => 'Neue Aggregation');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_HOUR',
  p_message_language => 'de-ch',
  p_message_text => 'Nächste Stunde');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_X_DAYS',
  p_message_language => 'de-ch',
  p_message_text => 'Nächste %0 Tage');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO_COLUMN_INFO',
  p_message_language => 'de-ch',
  p_message_text => 'Keine Spalteninformationen verfügbar.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_DOES_NOT_EXIST',
  p_message_language => 'de-ch',
  p_message_text => 'Bericht ist nicht vorhanden.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVED_REPORT_MSG',
  p_message_language => 'de-ch',
  p_message_text => 'Gespeicherter Bericht = "%0"');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_AS_DEFAULT',
  p_message_language => 'de-ch',
  p_message_text => 'Als Standard speichern');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEARCH_BAR',
  p_message_language => 'de-ch',
  p_message_text => 'Suchleiste');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => '- Spalte wählen -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT_DESCENDING',
  p_message_language => 'de-ch',
  p_message_text => 'Absteigend sortieren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_STATUS',
  p_message_language => 'de-ch',
  p_message_text => 'Status %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_MONTHS',
  p_message_language => 'de-ch',
  p_message_text => 'Monaten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_YEARS',
  p_message_language => 'de-ch',
  p_message_text => 'Jahren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TOP',
  p_message_language => 'de-ch',
  p_message_text => 'Oben');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_GROUP_BY',
  p_message_language => 'de-ch',
  p_message_text => '"Gruppieren nach" anzeigen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Bericht anzeigen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER.ICON_TEXT',
  p_message_language => 'de-ch',
  p_message_text => 'Popup-Kalender: %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.YES_NO.INVALID_VALUE',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# muss den Werten %0 und %1 entsprechen.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_SORT',
  p_message_language => 'de-ch',
  p_message_text => 'Nach Sortierung gruppieren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_SORT_ORDER',
  p_message_language => 'de-ch',
  p_message_text => 'Nach Sortierfolge gruppieren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_COLUMNS',
  p_message_language => 'de-ch',
  p_message_text => 'Pivot-Spalten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_ID_DOES_NOT_EXIST',
  p_message_language => 'de-ch',
  p_message_text => 'ID des gespeicherten interaktiven Berichts %0 ist nicht vorhanden.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_REPORT_DEFAULT',
  p_message_language => 'de-ch',
  p_message_text => 'Bericht speichern *');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART_MAX_ROW_CNT',
  p_message_language => 'de-ch',
  p_message_text => 'Die maximale Zeilenanzahl für eine "Diagramm"-Abfrage begrenzt die Anzahl der Zeilen in der Basisabfrage, aber nicht die Anzahl der angezeigten Zeilen. Die Basisabfrage überschreitet die maximale Zeilenanzahl von %0. Wenden Sie einen Filter an, um die Anzahl der Datensätze in der Basisabfrage zu reduzieren.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECTED_COLUMNS',
  p_message_language => 'de-ch',
  p_message_text => 'Ausgewählte Spalten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_MAX_ROW_CNT',
  p_message_language => 'de-ch',
  p_message_text => 'Die maximale Zeilenanzahl für eine "Gruppieren nach"-Abfrage begrenzt die Anzahl der Zeilen in der Basisabfrage, aber nicht die Anzahl der angezeigten Zeilen. Die Basisabfrage überschreitet die maximale Zeilenanzahl von %0. Wenden Sie einen Filter an, um die Anzahl der Datensätze in der Basisabfrage zu reduzieren.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_HOUR',
  p_message_language => 'de-ch',
  p_message_text => 'Letzte Stunde');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_X_YEARS',
  p_message_language => 'de-ch',
  p_message_text => 'Nächste %0 Jahre');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_NOT_IN_YEAR_RANGE',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# liegt nicht innerhalb des gültigen Jahresbereichs von %0 und %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'FLOW.SINGLE_VALIDATION_ERROR',
  p_message_language => 'de-ch',
  p_message_text => '1 Fehler aufgetreten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_DAYS_AGO',
  p_message_language => 'de-ch',
  p_message_text => 'vor %0 Tagen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_UTILITIES.CLOSE',
  p_message_language => 'de-ch',
  p_message_text => 'Schließen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.X_Y_OF_Z',
  p_message_language => 'de-ch',
  p_message_text => 'Zeile(n) %0 - %1 von %2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ACTIONS',
  p_message_language => 'de-ch',
  p_message_text => 'Aktionen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD',
  p_message_language => 'de-ch',
  p_message_text => 'Hinzufügen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_SUBSCRIPTION',
  p_message_language => 'de-ch',
  p_message_text => 'Subscription hinzufügen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGGREGATE',
  p_message_language => 'de-ch',
  p_message_text => 'Aggregat');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_MIN',
  p_message_language => 'de-ch',
  p_message_text => 'Minimal');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_MODE',
  p_message_language => 'de-ch',
  p_message_text => 'Modus');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_SUM',
  p_message_language => 'de-ch',
  p_message_text => 'Summe %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ASCENDING',
  p_message_language => 'de-ch',
  p_message_text => 'Aufsteigend');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CALENDAR',
  p_message_language => 'de-ch',
  p_message_text => 'Kalender');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART_TYPE',
  p_message_language => 'de-ch',
  p_message_text => 'Diagrammtyp');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_ISNOT_IN_NEXT',
  p_message_language => 'de-ch',
  p_message_text => 'ist nicht in den nächsten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IS_IN_LAST',
  p_message_language => 'de-ch',
  p_message_text => 'ist in den letzten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_FOOTER_E2',
  p_message_language => 'de-ch',
  p_message_text => 'INITCAP(B)||'', ''||INITCAP(C)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_FOOTER_E3',
  p_message_language => 'de-ch',
  p_message_text => 'CASE WHEN A = 10 THEN B + C ELSE B END');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DATE',
  p_message_language => 'de-ch',
  p_message_text => 'Datum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FUNCTIONS',
  p_message_language => 'de-ch',
  p_message_text => 'Funktionen %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GO',
  p_message_language => 'de-ch',
  p_message_text => 'Los');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_01',
  p_message_language => 'de-ch',
  p_message_text => 'In den interaktiven Berichtsregionen können Endbenutzer Berichte anpassen. Benutzer können das Layout von Berichtsdaten ändern, indem sie Spalten wählen, Filter anwenden, Bereiche hervorheben oder sortieren. Benutzer können darüber hinaus Gruppenwechsel, Aggregationen, Diagramme und Gruppierungen definieren und ihre eigenen Berechnungen hinzufügen. Sie können auch eine Subscription einrichten, sodass sie HTML-Versionen des Berichts per E-Mail in einem festgelegten Intervall erhalten. Benutzer können mehrere Variationen eines Berichts erstellen und diese als benannte Berichte speichern, die entweder öffentlich oder privat angezeigt werden können. 
<p/> 
In den folgenden Abschnitten wird zusammengefasst, wie Sie einen interaktiven Bericht anpassen können. Weitere Informationen finden Sie unter "Using Interactive Reports" in <a href="http://www.oracle.com/pls/topic/lookup?ctx=E37097_01&id=AEEUG453" target="_blank"><i>Oracle Application Express End User''s Guide</i></a>.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_COLUMN_HEADING_MENU',
  p_message_language => 'de-ch',
  p_message_text => 'Wenn Sie auf eine Spaltenüberschrift klicken, wird ein Menü für Spaltenüberschriften angezeigt. Zu den Optionen gehören: 
<p/> 
<ul><li><b>Symbol "Aufsteigend sortieren"</b>: sortiert den Bericht nach Spalte in aufsteigender Reihenfolge.</li> 
<li><b>Symbol "Absteigend sortieren"</b>: sortiert den Bericht nach Spalte in absteigender Reihenfolge.</li> 
<li><b>Spalte ausblenden</b>: blendet die Spalte aus.</li> 
<li><b>Gruppenwechselspalte</b>: erstellt einen Gruppenwechsel für die Spalte. Dadurch wird die Spalte als Master-Datensatz aus dem Bericht herausgezogen.</li> 
<li><b>Spalteninformationen</b>: zeigt einen Hilfetext zur Spalte an, falls verfügbar.</li> 
<li><b>Textbereich</b> wird zur Eingabe von Suchkriterien verwendet (keine Platzhalter erforderlich). Die Groß-/Kleinschreibung wird dabei ignoriert. Durch die Eingabe eines Wertes wird die Werteliste am Ende des Menüs gekürzt. Sie können dann einen Wert vom Ende der Liste wählen, und der gewählte Wert wird als Filter mit ''='' erstellt (z.B. <code>column = ''ABC''</code>). Alternativ können Sie auf das Taschenlampensymbol klicken und einen Wert eingeben, der dann als Filter mit dem Modifier ''LIKE'' erstellt wird (z.B. <code>column LIKE ''%ABC%''</code>). 
<li> <b>Liste mit eindeutigen Werten</b> enthält die ersten 500 eindeutigen Werte, die den Filterkriterien entsprechen. Wenn die Spalte ein Datum darstellt, wird stattdessen eine Liste mit Datumsbereichen angezeigt. Wenn Sie einen Wert wählen, wird ein Filter mit ''='' erstellt (z. B. <code>column = ''ABC''</code>).</li> </ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_CONTROL_BREAK',
  p_message_language => 'de-ch',
  p_message_text => 'Wird verwendet, um einen Gruppenwechsel auf einer oder mehreren Spalten zu erstellen. Dadurch werden die Spalten aus dem interaktiven Bericht herausgezogen und als Master-Datensatz angezeigt.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_HIGHLIGHT',
  p_message_language => 'de-ch',
  p_message_text => '<p>Ermöglicht Ihnen, Filter zu definieren. Die Zeilen, die den Filterkriterien entsprechen, werden entsprechend den Eigenschaften markiert, die mit dem Filter verknüpft sind. Zu den Optionen gehören: 
<p/> 
<ul><li><b>Name</b>: Wird nur zur Anzeige verwendet.</li> 
<li><b>Sequence</b>: Gibt die Reihenfolge an, in der die Regeln ausgewertet werden.</li> 
<li><b>Aktiviert</b>: Gibt an, ob die Regel aktiviert oder deaktiviert ist.</li> 
<li><b>Markierungstyp</b>: Gibt an, ob die Zeile oder Zelle markiert werden soll. Wenn "Zelle" gewählt wird, wird die Spalte markiert, die in der Markierungsbedingung referenziert wird.</li> 
<li><b>Hintergrundfarbe</b>: Ist die neue Farbe für den Hintergrund des markierten Bereichs.</li> 
<li><b>Textfarbe</b>: Ist die neue Farbe für den Text im markierten Bereich.</li> 
<li><b>Markierungsbedingung</b>: Definiert Ihre Filterbedingung.</li></ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_RESET',
  p_message_language => 'de-ch',
  p_message_text => 'Setzt den Bericht auf die Standardeinstellungen zurück, wobei von Ihnen vorgenommene Anpassungen entfernt werden. ');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SORT',
  p_message_language => 'de-ch',
  p_message_text => '<p>Wird verwendet, um die zu sortierenden Spalten zu wechseln und anzugeben, ob in aufsteigender oder absteigender Reihenfolge sortiert werden soll. Sie können auch angeben, wie <code>Nullwerte</code> behandelt werden sollen. Die Standardeinstellung zeigt die <code>Nullwerte</code> immer am Ende an; Sie können sie alternativ immer am Anfang anzeigen. Die Ergebnissortierung wird rechts neben den Spaltenüberschriften im Bericht angezeigt.</p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHT',
  p_message_language => 'de-ch',
  p_message_text => 'Markierung');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_IS_IN_THE_NEXT',
  p_message_language => 'de-ch',
  p_message_text => '%0 ist in den nächsten %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MAX_ROW_CNT',
  p_message_language => 'de-ch',
  p_message_text => 'Die maximale Anzahl der Zeilen für diesen Bericht ist %0. Wenden Sie einen Filter an, um die Anzahl der Datensätze in der Abfrage zu reduzieren.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MORE_DATA',
  p_message_language => 'de-ch',
  p_message_text => 'Weitere Daten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO_AGGREGATION_DEFINED',
  p_message_language => 'de-ch',
  p_message_text => 'Keine Aggregation definiert.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_FUNCTION',
  p_message_language => 'de-ch',
  p_message_text => '- Funktion wählen -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT_ASCENDING',
  p_message_language => 'de-ch',
  p_message_text => 'Aufsteigend sortieren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => 'Sortierspalte %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_MINS',
  p_message_language => 'de-ch',
  p_message_text => 'Minuten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNIQUE_HIGHLIGHT_NAME',
  p_message_language => 'de-ch',
  p_message_text => 'Markierungsname muss eindeutig sein.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNSUPPORTED_DATA_TYPE',
  p_message_language => 'de-ch',
  p_message_text => 'Nicht unterstützter Datentyp');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_WORKING_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Arbeitsbericht');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'LAYOUT.T_CONDITION_EXPR2',
  p_message_language => 'de-ch',
  p_message_text => 'Ausdruck 2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.SLIDER.VALUE_NOT_BETWEEN_MIN_MAX',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# liegt nicht im gültigen Bereich zwischen %0 und %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FUNCTION_N',
  p_message_language => 'de-ch',
  p_message_text => 'Funktion %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_FINDER',
  p_message_language => 'de-ch',
  p_message_text => 'Mit dem Symbol <li><b>Spalten auswählen</b> können Sie angeben, welche Spalten durchsucht werden sollen, bzw. ob alle Spalten durchsucht werden sollen.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INACTIVE_SETTING',
  p_message_language => 'de-ch',
  p_message_text => '1 inaktive Einstellung');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_AGG_NOT_NULL',
  p_message_language => 'de-ch',
  p_message_text => 'Aggregat muss angegeben werden.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_GROUP_BY',
  p_message_language => 'de-ch',
  p_message_text => '"Gruppieren nach" entfernen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_PIVOT',
  p_message_language => 'de-ch',
  p_message_text => 'Pivot entfernen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RPT_DISP_COL_EXCEED',
  p_message_language => 'de-ch',
  p_message_text => 'Die Anzahl der Anzeigespalten im Bericht hat den Grenzwert erreicht. Klicken Sie im Menü "Aktionen" auf "Spalten auswählen", um die Liste der Berichtsanzeigespalten zu minimieren.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEARCH_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => 'Suchen: %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_GROUP_BY_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => 'Gruppe nach Spalte hinzufügen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_ROW_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => 'Zeilenspalte hinzufügen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART_LABEL_NOT_NULL',
  p_message_language => 'de-ch',
  p_message_text => 'Das Diagrammlabel muss angegeben werden.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT_ORDER',
  p_message_language => 'de-ch',
  p_message_text => 'Sortierreihenfolge');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'MAXIMIZE',
  p_message_language => 'de-ch',
  p_message_text => 'Maximieren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LABEL',
  p_message_language => 'de-ch',
  p_message_text => 'Beschriftung %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NONE',
  p_message_language => 'de-ch',
  p_message_text => '- Keine -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PERCENT_OF_TOTAL_SUM_X',
  p_message_language => 'de-ch',
  p_message_text => 'Prozentsatz der Gesamtsumme %0 (%)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_GREATER_MAX_DATE',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# ist später als das angegebene späteste Datum %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'REPORT_TOTAL',
  p_message_language => 'de-ch',
  p_message_text => 'Bericht Summe');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_WEEKS_FROM_NOW',
  p_message_language => 'de-ch',
  p_message_text => '%0 Wochen ab jetzt');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AVERAGE_X',
  p_message_language => 'de-ch',
  p_message_text => 'Durchschnitt %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_BOTTOM',
  p_message_language => 'de-ch',
  p_message_text => 'Unten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART',
  p_message_language => 'de-ch',
  p_message_text => 'Diagramm');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHOOSE_DOWNLOAD_FORMAT',
  p_message_language => 'de-ch',
  p_message_text => 'Format für Herunterladen von Bericht wählen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CONTROL_BREAKS',
  p_message_language => 'de-ch',
  p_message_text => 'Kontrollgruppenwechsel');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COUNT_DISTINCT_X',
  p_message_language => 'de-ch',
  p_message_text => 'Nur eindeutige Reihen zählen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DATA_AS_OF',
  p_message_language => 'de-ch',
  p_message_text => 'Bericht für Daten im Status vor %0 Minuten erstellen.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DIRECTION',
  p_message_language => 'de-ch',
  p_message_text => 'Richtung %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISPLAY_IN_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'In Bericht anzeigen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_BODY',
  p_message_language => 'de-ch',
  p_message_text => 'Body');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ENABLE',
  p_message_language => 'de-ch',
  p_message_text => 'Aktivieren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FILTER_EXPRESSION',
  p_message_language => 'de-ch',
  p_message_text => 'Filterausdruck');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FLASHBACK',
  p_message_language => 'de-ch',
  p_message_text => 'Flashback');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FUNCTION',
  p_message_language => 'de-ch',
  p_message_text => 'Funktion');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID',
  p_message_language => 'de-ch',
  p_message_text => 'Ungültig');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_IS_NOT_IN_THE_LAST',
  p_message_language => 'de-ch',
  p_message_text => '%0 ist nicht in den letzten %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_IS_NOT_IN_THE_NEXT',
  p_message_language => 'de-ch',
  p_message_text => '%0 ist nicht in den nächsten %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_DAY',
  p_message_language => 'de-ch',
  p_message_text => 'Letzter Tag');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NULLS_ALWAYS_LAST',
  p_message_language => 'de-ch',
  p_message_text => 'Nullwerte immer am Ende');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PERCENT_OF_TOTAL_COUNT_X',
  p_message_language => 'de-ch',
  p_message_text => 'Prozentsatz der Gesamtanzahl %0 (%)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIE',
  p_message_language => 'de-ch',
  p_message_text => 'Kreis');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PREVIOUS',
  p_message_language => 'de-ch',
  p_message_text => 'Zurück');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RENAME_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Bericht umbenennen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_CATEGORY',
  p_message_language => 'de-ch',
  p_message_text => '- Kategorie wählen -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT',
  p_message_language => 'de-ch',
  p_message_text => 'Sortierung');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SPACE_AS_IN_ONE_EMPTY_STRING',
  p_message_language => 'de-ch',
  p_message_text => 'Speicherplatz');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SUM_X',
  p_message_language => 'de-ch',
  p_message_text => 'Summe %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TEXT_COLOR',
  p_message_language => 'de-ch',
  p_message_text => 'Textfarbe');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_DAYS',
  p_message_language => 'de-ch',
  p_message_text => 'Tage');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UP',
  p_message_language => 'de-ch',
  p_message_text => 'Nach oben');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALID_COLOR',
  p_message_language => 'de-ch',
  p_message_text => 'Geben Sie eine gültige Farbe ein.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALUE_REQUIRED',
  p_message_language => 'de-ch',
  p_message_text => 'Wert erforderlich');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_CHART',
  p_message_language => 'de-ch',
  p_message_text => 'Diagramm anzeigen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_YEARS',
  p_message_language => 'de-ch',
  p_message_text => '%0 Jahre');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.SLIDER.VALUE_NOT_MULTIPLE_OF_STEP',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# ist kein Mehrfaches von %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.REGION.JQM_LIST_VIEW.LOAD_MORE',
  p_message_language => 'de-ch',
  p_message_text => 'Mehr laden...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_PIVOT',
  p_message_language => 'de-ch',
  p_message_text => 'Sie können eine Pivot-Ansicht pro gespeicherten Bericht definieren. Nach der Definition können Sie zwischen der Pivot- und Berichtsansicht mit Ansichtsymbolen in der Suchleiste wechseln. Um eine Pivot-Ansicht zu erstellen, wählen Sie Folgendes:  
<p></p> 
<ul> 
<li>die Spalten, die pivotiert werden sollen</li> 
<li>die Spalten, die als Zeilen angezeigt werden sollen</li> 
<li>die Spalten, die zusammen mit der auszuführenden Funktion aggregiert werden sollen (Durchschnitt, Summe, Anzahl usw.)</li> 
</ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INACTIVE_SETTINGS',
  p_message_language => 'de-ch',
  p_message_text => '%0 inaktive Einstellungen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_DISPLAY_COLUMN_LIMIT_REACHED',
  p_message_language => 'de-ch',
  p_message_text => 'Die Anzahl der Anzeigespalten im Bericht hat den Grenzwert erreicht. Klicken Sie im Menü "Aktionen" auf "Spalten auswählen", um die Liste der Berichtsanzeigespalten zu minimieren.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_COLUMN_NOT_NULL',
  p_message_language => 'de-ch',
  p_message_text => 'Zeilenspalte muss angegeben werden.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_PIVOT_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => 'Pivot-Spalte hinzufügen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_PIVOT',
  p_message_language => 'de-ch',
  p_message_text => 'Pivot anzeigen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_MONTHS',
  p_message_language => 'de-ch',
  p_message_text => '%0 Monate');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'IR_AS_DEFAULT_REPORT_SETTING',
  p_message_language => 'de-ch',
  p_message_text => 'Als Standardberichteinstellungen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Bericht');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_IS_IN_THE_LAST',
  p_message_language => 'de-ch',
  p_message_text => '%0 ist in den letzten %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MONTHLY',
  p_message_language => 'de-ch',
  p_message_text => 'Monatlich');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE',
  p_message_language => 'de-ch',
  p_message_text => 'Entfernen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_CONTROL_BREAK',
  p_message_language => 'de-ch',
  p_message_text => 'Kontrollgruppenwechsel entfernen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_NOT_BETWEEN_MIN_MAX',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# liegt nicht im gültigen Bereich zwischen %0 und %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_NOT_BETWEEN_MIN_MAX',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# liegt nicht im gültigen Bereich zwischen %0 und %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'INVALID_CREDENTIALS',
  p_message_language => 'de-ch',
  p_message_text => 'Ungültige Zugangsdaten für die Anmeldung');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_DAYS_FROM_NOW',
  p_message_language => 'de-ch',
  p_message_text => '%0 Tage ab jetzt');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_HOURS_FROM_NOW',
  p_message_language => 'de-ch',
  p_message_text => '%0 Stunden ab jetzt');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_UTILITIES.OK',
  p_message_language => 'de-ch',
  p_message_text => 'OK');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGGREGATE_DESCRIPTION',
  p_message_language => 'de-ch',
  p_message_text => 'Aggregate werden nach jedem Kontrollgruppenwechsel und am Ende des Berichts angezeigt.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_AVG',
  p_message_language => 'de-ch',
  p_message_text => 'Durchschnitt');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_MAX',
  p_message_language => 'de-ch',
  p_message_text => 'Maximal');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CANCEL',
  p_message_language => 'de-ch',
  p_message_text => 'Abbrechen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CATEGORY',
  p_message_language => 'de-ch',
  p_message_text => 'Kategorie');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CLEAR',
  p_message_language => 'de-ch',
  p_message_text => 'löschen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_CONTAINS',
  p_message_language => 'de-ch',
  p_message_text => 'enthält');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IN',
  p_message_language => 'de-ch',
  p_message_text => 'in');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_REGEXP_LIKE',
  p_message_language => 'de-ch',
  p_message_text => 'entspricht regulärem Ausdruck');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DESCRIPTION',
  p_message_language => 'de-ch',
  p_message_text => 'Beschreibung');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISABLE',
  p_message_language => 'de-ch',
  p_message_text => 'Deaktivieren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISABLED',
  p_message_language => 'de-ch',
  p_message_text => 'Deaktiviert');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_CHART',
  p_message_language => 'de-ch',
  p_message_text => 'Diagrammeinstellungen bearbeiten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_CC',
  p_message_language => 'de-ch',
  p_message_text => 'Cc');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_NOT_CONFIGURED',
  p_message_language => 'de-ch',
  p_message_text => 'Für diese Anwendung wurde keine E-Mail konfiguriert. Wenden Sie sich an den Administrator.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ENABLED',
  p_message_language => 'de-ch',
  p_message_text => 'Aktiviert');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FLASHBACK_ERROR_MSG',
  p_message_language => 'de-ch',
  p_message_text => 'Flashback-Anforderung kann nicht ausgeführt werden');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY',
  p_message_language => 'de-ch',
  p_message_text => 'Gruppieren nach');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INTERACTIVE_REPORT_HELP',
  p_message_language => 'de-ch',
  p_message_text => 'Hilfe zu interaktivem Bericht');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_COMPUTATION',
  p_message_language => 'de-ch',
  p_message_text => 'Ungültiger Berechnungsausdruck. %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LABEL_AXIS_TITLE',
  p_message_language => 'de-ch',
  p_message_text => 'Achsenbeschriftung für Label');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_MONTH',
  p_message_language => 'de-ch',
  p_message_text => 'Letzter Monat');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_WEEK',
  p_message_language => 'de-ch',
  p_message_text => 'Letzte Woche');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_X_HOURS',
  p_message_language => 'de-ch',
  p_message_text => 'Letzte %0 Stunden');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_WEEK',
  p_message_language => 'de-ch',
  p_message_text => 'Nächste Woche');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NULLS_ALWAYS_FIRST',
  p_message_language => 'de-ch',
  p_message_text => 'Nullwerte immer am Anfang');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_OTHER',
  p_message_language => 'de-ch',
  p_message_text => 'Weitere');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PERCENT_TOTAL_COUNT',
  p_message_language => 'de-ch',
  p_message_text => 'Prozentsatz der Gesamtanzahl');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RED',
  p_message_language => 'de-ch',
  p_message_text => 'rot');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_VIEW',
  p_message_language => 'de-ch',
  p_message_text => 'Berichtsansicht');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RESET_CONFIRM',
  p_message_language => 'de-ch',
  p_message_text => 'Stellen Sie die Standardeinstellungen für den Bericht wieder her.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVED_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Gespeicherter Bericht');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_COLUMNS',
  p_message_language => 'de-ch',
  p_message_text => 'Spalten wählen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_VALUE',
  p_message_language => 'de-ch',
  p_message_text => 'Wert wählen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEQUENCE',
  p_message_language => 'de-ch',
  p_message_text => 'Sequence');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_DETAIL',
  p_message_language => 'de-ch',
  p_message_text => 'Detail anzeigen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_HOURS',
  p_message_language => 'de-ch',
  p_message_text => '%0 Stunden');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_MINS',
  p_message_language => 'de-ch',
  p_message_text => '%0 Minuten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'IR_STAR',
  p_message_language => 'de-ch',
  p_message_text => 'Wird nur für Entwickler angezeigt');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.AUTHENTICATION.LOGIN_THROTTLE.ERROR',
  p_message_language => 'de-ch',
  p_message_text => 'Der Anmeldeversuch wurde blockiert.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.YES_NO.NO_LABEL',
  p_message_language => 'de-ch',
  p_message_text => 'Nein');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_END_DATE',
  p_message_language => 'de-ch',
  p_message_text => 'Das Enddatum muss nach dem Anfangdatum liegen.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_SETTING',
  p_message_language => 'de-ch',
  p_message_text => '1 ungültige Einstellung');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_COLUMN_N',
  p_message_language => 'de-ch',
  p_message_text => 'Pivot-Spalte %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_MAX_ROW_CNT',
  p_message_language => 'de-ch',
  p_message_text => 'Die maximale Zeilenanzahl für eine "Pivot"-Abfrage begrenzt die Anzahl der Zeilen in der Basisabfrage, aber nicht die Anzahl der angezeigten Zeilen. Die Basisabfrage überschreitet die maximale Zeilenanzahl von %0. Wenden Sie einen Filter an, um die Anzahl der Datensätze in der Basisabfrage zu reduzieren.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_CHART',
  p_message_language => 'de-ch',
  p_message_text => 'Diagramm entfernen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_FILTER',
  p_message_language => 'de-ch',
  p_message_text => 'Filtern ...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_PIVOT_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => '- Pivot-Spalte wählen -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_ROW',
  p_message_language => 'de-ch',
  p_message_text => 'Zeile wählen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TOGGLE',
  p_message_language => 'de-ch',
  p_message_text => 'Ein-/ausschalten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNGROUPED_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => 'Nicht gruppierte Spalte');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_GROUP_BY',
  p_message_language => 'de-ch',
  p_message_text => 'Pro gespeicherten Bericht können Sie eine Ansicht "Gruppieren nach" definieren.  
Nach der Definition können Sie zwischen den "Gruppieren nach"- und den Berichtsansichten umschalten.  
Verwenden Sie hierfür die Ansichtssymbole in der Suchleiste. Um eine Ansicht "Gruppieren nach" zu erstellen, 
wählen Sie: 
<p></p><ul> 
<li>die Spalten, nach denen gruppiert werden soll</li> 
<li>die Spalten, die zusammen mit der auszuführenden Funktion (Durchschnitt, Summe, Anzahl usw.) aggregiert werden sollen</li> 
</ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_FILTER',
  p_message_language => 'de-ch',
  p_message_text => 'Ungültiger Filterausdruck. %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.X_Y_OF_Z_2',
  p_message_language => 'de-ch',
  p_message_text => '%0 - %1 von %2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_X_DAYS',
  p_message_language => 'de-ch',
  p_message_text => 'Letzte %0 Tage');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MIN_X',
  p_message_language => 'de-ch',
  p_message_text => 'Minimum %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NAME',
  p_message_language => 'de-ch',
  p_message_text => 'Name');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.FILE_BROWSE.DOWNLOAD_LINK_TEXT',
  p_message_language => 'de-ch',
  p_message_text => 'Herunterladen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GO_TO_ERROR',
  p_message_language => 'de-ch',
  p_message_text => 'Gehen Sie zum Fehler');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.PAGE_ITEM_IS_REQUIRED',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# muss einen Wert haben.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_MINUTES_AGO',
  p_message_language => 'de-ch',
  p_message_text => 'vor %0 Minuten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_MINUTES_FROM_NOW',
  p_message_language => 'de-ch',
  p_message_text => '%0 Minuten ab jetzt');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_YEARS_AGO',
  p_message_language => 'de-ch',
  p_message_text => 'vor %0 Jahren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'TOTAL',
  p_message_language => 'de-ch',
  p_message_text => 'Gesamt');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_MEDIAN',
  p_message_language => 'de-ch',
  p_message_text => 'Medianwert');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALL',
  p_message_language => 'de-ch',
  p_message_text => 'Alle');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_BETWEEN',
  p_message_language => 'de-ch',
  p_message_text => 'zwischen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_DOESNOT_CONTAIN',
  p_message_language => 'de-ch',
  p_message_text => 'enthält nicht');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IS_IN_NEXT',
  p_message_language => 'de-ch',
  p_message_text => 'ist in den nächsten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IS_NULL',
  p_message_language => 'de-ch',
  p_message_text => 'ist Null');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_NOT_IN',
  p_message_language => 'de-ch',
  p_message_text => 'nicht in');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_FOOTER',
  p_message_language => 'de-ch',
  p_message_text => 'Erstellen Sie eine Berechnung mit Spaltenaliasnamen.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTE',
  p_message_language => 'de-ch',
  p_message_text => 'Berechnen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DOWN',
  p_message_language => 'de-ch',
  p_message_text => 'Nach unten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_GROUP_BY',
  p_message_language => 'de-ch',
  p_message_text => '"Gruppieren nach" bearbeiten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL',
  p_message_language => 'de-ch',
  p_message_text => 'E-Mail');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_TO',
  p_message_language => 'de-ch',
  p_message_text => 'An');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ENABLE_DISABLE_ALT',
  p_message_language => 'de-ch',
  p_message_text => 'Aktivieren/Deaktivieren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FILTERS',
  p_message_language => 'de-ch',
  p_message_text => 'Filter');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FINDER_ALT',
  p_message_language => 'de-ch',
  p_message_text => 'Zu suchende Spalten wählen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_DETAIL_VIEW',
  p_message_language => 'de-ch',
  p_message_text => 'Um die Details von jeweils einer Zeile anzuzeigen, klicken Sie auf das Symbol für die Single Row-Ansicht auf der Zeile, die angezeigt werden soll. Wenn verfügbar, ist die Single Row-Ansicht immer die erste Spalte. Je nach Anpassung des interaktiven Berichts kann die Single Row-Ansicht eine Standardansicht oder eine benutzerdefinierte Seite sein, die eine Aktualisierung zulässt.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR',
  p_message_language => 'de-ch',
  p_message_text => 'Im oberen Bereich der einzelnen Berichtsseiten befindet sich eine Suchregion. Diese Region (bzw. die Suchleiste) enthält die folgenden Funktionen:');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MIN_AGO',
  p_message_language => 'de-ch',
  p_message_text => 'vor %0 Minuten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MONTH',
  p_message_language => 'de-ch',
  p_message_text => 'Monat');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MOVE',
  p_message_language => 'de-ch',
  p_message_text => 'Verschieben');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MOVE_ALL',
  p_message_language => 'de-ch',
  p_message_text => 'Alle verschieben');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT',
  p_message_language => 'de-ch',
  p_message_text => 'Weiter');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NULL_SORTING',
  p_message_language => 'de-ch',
  p_message_text => 'Keine Sortierung %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NUMERIC_SEQUENCE',
  p_message_language => 'de-ch',
  p_message_text => 'Sequence muss numerisch sein.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PUBLIC',
  p_message_language => 'de-ch',
  p_message_text => 'Öffentlich');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_ALL',
  p_message_language => 'de-ch',
  p_message_text => 'Alle entfernen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_FILTER',
  p_message_language => 'de-ch',
  p_message_text => 'Filter entfernen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Bericht');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEARCH',
  p_message_language => 'de-ch',
  p_message_text => 'Suche');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEARCH_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Suchbericht');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SUBSCRIPTION_ENDING',
  p_message_language => 'de-ch',
  p_message_text => 'Bis');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_ICONS',
  p_message_language => 'de-ch',
  p_message_text => 'Symbole anzeigen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_WEEKLY',
  p_message_language => 'de-ch',
  p_message_text => 'Wöchentlich');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_YELLOW',
  p_message_language => 'de-ch',
  p_message_text => 'gelb');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'IR_AS_NAMED_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Als benannter Bericht');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.AUTHENTICATION.LOGIN_THROTTLE.COUNTER',
  p_message_language => 'de-ch',
  p_message_text => 'Warten Sie <span id="apex_login_throttle_sec">%0</span> Sekunden, bis Sie sich erneut anmelden.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_ACTIONS_MENU',
  p_message_language => 'de-ch',
  p_message_text => '<li>Das <b>Aktionsmenü</b> ermöglicht die Anpassung von Berichten. Weitere Informationen finden Sie in den folgenden Abschnitten.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_TEXTBOX',
  p_message_language => 'de-ch',
  p_message_text => '<li>Im <b>Textbereich</b> können Sie Suchkriterien eingeben, bei denen die Groß-/Kleinschreibung nicht beachtet werden muss (Platzhalterzeichen sind impliziert).</li> 
<li><b>Schaltfläche "Los"</b> führt die Suche durch. Wenn Sie die Eingabetaste drücken, während sich der Cursor im Suchtextbereich befindet, wird die Suche ebenfalls ausgeführt.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SUBSCRIPTION',
  p_message_language => 'de-ch',
  p_message_text => 'Wenn Sie eine Subscription hinzufügen, geben Sie E-Mail-Adresse (oder mehrere, durch Komma getrennte E-Mail-Adressen), E-Mail-Betreff, Häufigkeit sowie die Angaben zum Start- und Enddatum ein. Die resultierenden E-Mails enthalten eine HTML-Version des interaktiven Berichts. Sie enthalten die aktuellen Daten mit den Berichtseinstellungen, die bei Hinzufügen der Subscription vorhanden waren.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_SETTINGS',
  p_message_language => 'de-ch',
  p_message_text => '%0 ungültige Einstellungen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LABEL_PREFIX',
  p_message_language => 'de-ch',
  p_message_text => 'Labelpräfix');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_SORT',
  p_message_language => 'de-ch',
  p_message_text => 'Pivot-Sortierung');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_COL_DIFF_FROM_PIVOT_COL',
  p_message_language => 'de-ch',
  p_message_text => 'Zeilenspalte darf nicht identisch mit der Pivot-Spalte sein.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_COLUMN_N',
  p_message_language => 'de-ch',
  p_message_text => 'Zeilenspalte %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_DEFAULT_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Standardbericht speichern');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_DEFAULT_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Standardbericht löschen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_PIVOT',
  p_message_language => 'de-ch',
  p_message_text => 'Pivot bearbeiten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEND',
  p_message_language => 'de-ch',
  p_message_text => 'Senden');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_ROWS',
  p_message_language => 'de-ch',
  p_message_text => 'In <li><b>Zeilen</b> wird die Anzahl der pro Seite anzuzeigenden Datensätze angezeigt.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SESSION_STATE.SSP_VIOLATION',
  p_message_language => 'de-ch',
  p_message_text => 'Schutzverletzung bei Sessionstatus: Dieser Fehler wurde möglicherweise verursacht, weil der URL geändert wurde, der eine Prüfsumme enthält. Es wurde möglicherweise auch ein Link mit einer falschen oder fehlenden Prüfsumme verwendet. Wenn Sie nicht wissen, wodurch dieser Fehler verursacht wurde, wenden Sie sich an den Anwendungsadministrator.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'REPORTING_PERIOD',
  p_message_language => 'de-ch',
  p_message_text => 'Berichtzeitraum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ORANGE',
  p_message_language => 'de-ch',
  p_message_text => 'orange');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PRIVATE',
  p_message_language => 'de-ch',
  p_message_text => 'Privat');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATA_HAS_CHANGED',
  p_message_language => 'de-ch',
  p_message_text => 'Die aktuelle Version der Daten in der Datenbank wurde geändert, seit der Benutzer einen Updateprozess eingeleitet hat. Aktuelle Zeilenversions-ID = "%0" ID der Anwendungszeilenversion = "%1"');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_LESS_MIN_DATE',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# ist früher als das angegebene früheste Datum %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_INVALID',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# muss numerisch sein.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_LESS_MIN_VALUE',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# liegt unter dem angegebenen Mindestwert %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'FLOW.VALIDATION_ERROR',
  p_message_language => 'de-ch',
  p_message_text => '%0 Fehler sind aufgetreten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'PAGINATION.PREVIOUS',
  p_message_language => 'de-ch',
  p_message_text => 'Zurück');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_MONTHS_FROM_NOW',
  p_message_language => 'de-ch',
  p_message_text => '%0 Monate ab jetzt');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.FOUND_BUT_NOT_DISPLAYED',
  p_message_language => 'de-ch',
  p_message_text => 'Minimale Zeile angefordert: %0, Zeilen gefunden, die jedoch nicht angezeigt werden: %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => '4150_COLUMN_NUMBER',
  p_message_language => 'de-ch',
  p_message_text => 'Spalte %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_3D',
  p_message_language => 'de-ch',
  p_message_text => '3D');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGGREGATION',
  p_message_language => 'de-ch',
  p_message_text => 'Aggregation');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_APPLY',
  p_message_language => 'de-ch',
  p_message_text => 'Anwenden');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_BGCOLOR',
  p_message_language => 'de-ch',
  p_message_text => 'Hintergrundfarbe');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => 'Spalte');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_LIKE',
  p_message_language => 'de-ch',
  p_message_text => 'wie');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_CHECKED',
  p_message_language => 'de-ch',
  p_message_text => 'Markierte Elemente löschen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_CONFIRM',
  p_message_language => 'de-ch',
  p_message_text => 'Möchten Sie diese Berichtseinstellungen löschen?');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_BCC',
  p_message_language => 'de-ch',
  p_message_text => 'Bcc');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_SUBJECT',
  p_message_language => 'de-ch',
  p_message_text => 'Betreff');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FILTER_TYPE',
  p_message_language => 'de-ch',
  p_message_text => 'Filtertyp');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FORMAT',
  p_message_language => 'de-ch',
  p_message_text => 'Formatieren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_AGGREGATE',
  p_message_language => 'de-ch',
  p_message_text => 'Aggregate sind mathematische Berechnungen für eine Spalte. Aggregate werden nach jedem Kontrollgruppenwechsel und am Ende des Berichts in der Spalte angezeigt, in der sie definiert werden. Zu den Optionen gehören: 
<p> 
</p><ul> 
<li><b>Aggregation</b>: Ermöglicht die Wahl einer vorher 
definierten Aggregation zur Bearbeitung.</li> 
<li><b>Funktion</b>: Ist die auszuführende Funktion (z.B. SUM, MIN).</li> 
<li><b>Spalte</b>: Wird verwendet, um die Spalte zu wählen, für die die mathematische Funktion angewendet werden soll. Nur numerische Spalten werden angezeigt.</li> 
</ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_DOWNLOAD',
  p_message_language => 'de-ch',
  p_message_text => 'Ermöglicht das Herunterladen der aktuellen Ergebnismenge. Die Downloadformate variieren je nach Installation und Berichtsdefinition, können jedoch CSV, HTML, E-Mail, XLS, PDF oder RTF umfassen.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SAVE_REPORT',
  p_message_language => 'de-ch',
  p_message_text => '<p>Speichert den angepassten Bericht für die spätere Verwendung. Sie können einen Namen und eine optionale Beschreibung eingeben und den Bericht öffentlich zugänglich machen (bzw. den Zugriff allen Benutzern ermöglichen, die auf den primären Standardbericht zugreifen können). Es können vier Arten interaktiver Berichte gespeichert werden:</p> 
<ul> 
<li><strong>Primärstandard</strong> (nur Entwickler). Primärstandardberichte werden ursprünglich angezeigt. Solche Berichte können weder umbenannt noch gelöscht werden.</li> 
<li><strong>Alternativer Bericht</strong> (Nur Entwickler). Erlaubt Entwicklern die Erstellung von Berichten mit mehreren Layouts. Nur Entwickler können alternative Berichte sichern, umbenennen oder löschen.</li> 
<li><strong>Öffentliche Berichte</strong> (Endbenutzer). Diese Berichte können von dem erstellenden Endbenutzer gespeichert, umbenannt und gelöscht werden. Andere Benutzer können das Layout als weiteren Bericht anzeigen und speichern.</li> 
<li><strong>Private Berichte</strong> (Endbenutzer). Diese Berichte kann nur der erstellende Endbenutzer anzeigen, speichern, umbenennen oder löschen.</li> 
</ul> 
<p>Wenn Sie angepasste Berichte speichern, dann wird die Berichtsauswahl in der Suchleiste links der Spaltenauswahl angezeigt (falls diese Funktion aktiviert ist).</p> 
');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHTS',
  p_message_language => 'de-ch',
  p_message_text => 'Markierungen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHT_CONDITION',
  p_message_language => 'de-ch',
  p_message_text => 'Markierungsbedingung');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MAX_X',
  p_message_language => 'de-ch',
  p_message_text => 'Maximum %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MEDIAN_X',
  p_message_language => 'de-ch',
  p_message_text => 'Mittelwert %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_X_HOURS',
  p_message_language => 'de-ch',
  p_message_text => 'Nächste %0 Stunden');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NUMERIC_FLASHBACK_TIME',
  p_message_language => 'de-ch',
  p_message_text => 'Flashback-Zeit muss numerisch sein.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PRIMARY',
  p_message_language => 'de-ch',
  p_message_text => 'Primary');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW',
  p_message_language => 'de-ch',
  p_message_text => 'Zeile');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_DEFAULT_CONFIRM',
  p_message_language => 'de-ch',
  p_message_text => 'Die aktuellen Berichteinstellungen werden als Standard für alle Benutzer verwendet.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNIQUE_COLUMN_HEADING',
  p_message_language => 'de-ch',
  p_message_text => 'Spaltenüberschrift muss eindeutig sein.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_WEEK',
  p_message_language => 'de-ch',
  p_message_text => 'Woche');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.YES_NO.YES_LABEL',
  p_message_language => 'de-ch',
  p_message_text => 'Ja');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_REPORTS',
  p_message_language => 'de-ch',
  p_message_text => 'In <li><b>Berichte</b> werden verschiedene Standardwerte und gespeicherte private oder öffentliche Berichte angezeigt.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NOT_VALID_EMAIL',
  p_message_language => 'de-ch',
  p_message_text => 'Keine gültige E-Mail-Adresse.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_AGG_NOT_ON_ROW_COL',
  p_message_language => 'de-ch',
  p_message_text => 'Sie können keine Spalte aggregieren, die als Zeilenspalte gewählt ist.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_COLUMN_NOT_NULL',
  p_message_language => 'de-ch',
  p_message_text => 'Pivot-Spalte muss angegeben werden.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_FILTER',
  p_message_language => 'de-ch',
  p_message_text => 'Zeilenfilter');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_GROUP_BY_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => '- Gruppe nach Spalte wählen -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_FUNCTION',
  p_message_language => 'de-ch',
  p_message_text => 'Funktion hinzufügen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALL_ROWS',
  p_message_language => 'de-ch',
  p_message_text => 'Alle Zeilen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHECK_ALL',
  p_message_language => 'de-ch',
  p_message_text => 'Alle auswählen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT',
  p_message_language => 'de-ch',
  p_message_text => 'Bearbeiten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_SUBJECT_REQUIRED',
  p_message_language => 'de-ch',
  p_message_text => 'Der Betreff der E-Mail muss angegeben werden.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_ROW_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => '- Zeilenspalte wählen -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TABLE_SUMMARY',
  p_message_language => 'de-ch',
  p_message_text => 'Region = %0, Bericht = %1, Ansicht = %2, Angezeigte Zeilen Start = %3, Angezeigte Zeilen Ende = %4, Zeilen insgesamt = %5');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_DAYS',
  p_message_language => 'de-ch',
  p_message_text => '%0 Tage');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.CLOSE_NOTIFICATION',
  p_message_language => 'de-ch',
  p_message_text => 'Benachrichtigung schließen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SAVED_REPORTS.PRIMARY.DEFAULT',
  p_message_language => 'de-ch',
  p_message_text => 'Primärer Standardwert');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_MONTH',
  p_message_language => 'de-ch',
  p_message_text => 'Nächster Monat');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RESET',
  p_message_language => 'de-ch',
  p_message_text => 'Zurücksetzen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_INVALID',
  p_message_language => 'de-ch',
  p_message_text => '#LABEL# stimmt nicht mit Format %0 überein.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'PAGINATION.NEXT',
  p_message_language => 'de-ch',
  p_message_text => 'Weiter');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALTERNATIVE',
  p_message_language => 'de-ch',
  p_message_text => 'Alternative');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AS_OF',
  p_message_language => 'de-ch',
  p_message_text => 'Ab %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART_INITIALIZING',
  p_message_language => 'de-ch',
  p_message_text => 'Initialisierung erfolgt...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMNS',
  p_message_language => 'de-ch',
  p_message_text => 'Spalten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION',
  p_message_language => 'de-ch',
  p_message_text => 'Berechnung');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CONTROL_BREAK',
  p_message_language => 'de-ch',
  p_message_text => 'Kontrollgruppenwechsel');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DAY',
  p_message_language => 'de-ch',
  p_message_text => 'Tag');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DESCENDING',
  p_message_language => 'de-ch',
  p_message_text => 'Absteigend');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISPLAY',
  p_message_language => 'de-ch',
  p_message_text => 'Anzeigen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISPLAYED',
  p_message_language => 'de-ch',
  p_message_text => 'Angezeigt');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DOWNLOAD',
  p_message_language => 'de-ch',
  p_message_text => 'Herunterladen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_ALTERNATIVE_DEFAULT',
  p_message_language => 'de-ch',
  p_message_text => 'Alternativen Standardwert bearbeiten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_CHART2',
  p_message_language => 'de-ch',
  p_message_text => 'Diagramm bearbeiten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXAMPLES',
  p_message_language => 'de-ch',
  p_message_text => 'Beispiele');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXPAND_COLLAPSE_ALT',
  p_message_language => 'de-ch',
  p_message_text => 'Einblenden/Ausblenden');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FILTER',
  p_message_language => 'de-ch',
  p_message_text => 'Filter');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FORMAT_MASK',
  p_message_language => 'de-ch',
  p_message_text => 'Formatmaske %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FUNCTIONS_OPERATORS',
  p_message_language => 'de-ch',
  p_message_text => 'Funktionen/Operatoren');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_REPORT_SETTINGS',
  p_message_language => 'de-ch',
  p_message_text => 'Wenn Sie einen interaktiven Bericht anpassen, werden die Berichtseinstellungen unter der Suchleiste und über dem Bericht angezeigt. Dieser Bereich kann mit dem Symbol auf der linken Seite aus- und eingeblendet werden. 
<p/> 
Sie können für Berichtseinstellungen folgende Aktionen durchführen: 
</p><ul> 
<li>Bearbeiten</b>, indem Sie auf den Namen klicken.</li> 
<li><b>Deaktivieren/Aktivieren</b>, indem Sie das Kontrollkästchen "Aktivieren/Deaktivieren" wählen oder die Wahl aufheben. Diese Option wird verwendet, um die Einstellung vorübergehend ein- und auszuschalten.</li> 
<li><b>Entfernen</b>, indem Sie auf das Symbol für Entfernen klicken.</li></ul> 
<p/> 
Wenn Sie ein Diagramm, "Gruppieren nach" oder "Pivot" erstellt haben, können Sie mit den Links "Berichtsansicht", "Diagrammansicht" und "Nach Ansicht gruppieren" und "Pivot-Ansicht"  
auf der rechten Seite zwischen den Diagrammen und dem Basisbericht umschalten. Im Diagramm oder der Ansicht "Gruppieren nach" können Sie auch den Link "Bearbeiten" verwenden, um die Einstellungen zu bearbeiten.</p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_KEYPAD',
  p_message_language => 'de-ch',
  p_message_text => 'Zehnertastatur');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEW_COMPUTATION',
  p_message_language => 'de-ch',
  p_message_text => 'Neue Berechnung');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_OPERATOR',
  p_message_language => 'de-ch',
  p_message_text => 'Operator');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PERCENT_TOTAL_SUM',
  p_message_language => 'de-ch',
  p_message_text => 'Prozentsatz der Gesamtsumme');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_AGGREGATE',
  p_message_language => 'de-ch',
  p_message_text => 'Aggregat entfernen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_FLASHBACK',
  p_message_language => 'de-ch',
  p_message_text => 'Flashback entfernen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_OF',
  p_message_language => 'de-ch',
  p_message_text => 'Zeile %0 von %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_ORDER',
  p_message_language => 'de-ch',
  p_message_text => 'Zeilenreihenfolge');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROWS_PER_PAGE',
  p_message_language => 'de-ch',
  p_message_text => 'Zeilen pro Seite');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_SORT_COLUMN',
  p_message_language => 'de-ch',
  p_message_text => '- Sortierspalte wählen -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SUBSCRIPTION_STARTING_FROM',
  p_message_language => 'de-ch',
  p_message_text => 'Ab');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_HOURS',
  p_message_language => 'de-ch',
  p_message_text => 'Stunden');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALUE',
  p_message_language => 'de-ch',
  p_message_text => 'Wert');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VCOLUMN',
  p_message_language => 'de-ch',
  p_message_text => 'Vertikale Spalte');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_WEEKS',
  p_message_language => 'de-ch',
  p_message_text => '%0 Wochen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_YES',
  p_message_language => 'de-ch',
  p_message_text => 'Ja');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.POPUP_LOV.ICON_TEXT',
  p_message_language => 'de-ch',
  p_message_text => 'Popup-Werteliste: %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_COL_NOT_NULL',
  p_message_language => 'de-ch',
  p_message_text => '"Nach Spalte gruppieren" muss angegeben werden.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_FORMAT',
  p_message_language => 'de-ch',
  p_message_text => '<p>Mit der Option "Formatieren" können Sie das Erscheinungsbild des Berichts anpassen. 
"Formatieren" enthält das folgende Untermenü:</p> 
<ul><li>Sortieren</li> 
<li>Kontrollgruppenwechsel</li> 
<li>Markieren</li> 
<li>Berechnen</li> 
<li>Aggregieren</li> 
<li>Diagramm</li> 
<li>Gruppieren nach</li> 
<li>Pivot</li> 
</ul> 
');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_VIEW',
  p_message_language => 'de-ch',
  p_message_text => '<li><b>Ansicht-Symbole</b> wechselt zwischen Symbol-, Berichts-, Detail-, Diagramm-, Gruppieren nach- und Pivot-Ansicht des Berichts, sofern definiert.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RENAME_DEFAULT_REPORT',
  p_message_language => 'de-ch',
  p_message_text => 'Standardbericht umbenennen');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_ALIAS_DOES_NOT_EXIST',
  p_message_language => 'de-ch',
  p_message_text => 'Gespeicherter interaktiver Bericht mit Alias %0 ist nicht vorhanden.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_N',
  p_message_language => 'de-ch',
  p_message_text => 'Spalte %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_EXPRESSION',
  p_message_language => 'de-ch',
  p_message_text => 'Berechnungsausdruck');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CONTROL_BREAK_COLUMNS',
  p_message_language => 'de-ch',
  p_message_text => 'Gruppenwechselspalten');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_REQUIRED',
  p_message_language => 'de-ch',
  p_message_text => 'Die E-Mail-Adresse muss angegeben werden.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_YEAR',
  p_message_language => 'de-ch',
  p_message_text => 'Jahr');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WELCOME_USER',
  p_message_language => 'de-ch',
  p_message_text => 'Willkommen: %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SESSION.EXPIRED.NEW_SESSION',
  p_message_language => 'de-ch',
  p_message_text => 'Klicken Sie <a href="%0">hier</a>, um eine neue Session zu erstellen.');



COMMIT; END;
/