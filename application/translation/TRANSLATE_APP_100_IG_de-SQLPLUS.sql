rollback;
/*
The MIT License (MIT)

Copyright (c) 2018 Pretius Sp. z o.o. sk.
Żwirki i Wigury 16a
02-092 Warsaw, Poland
www.pretius.com
www.translate-apex.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

SET DEFINE OFF
ALTER SESSION SET NLS_LANGUAGE = AMERICAN;

    
DECLARE
  v_workspace_name VARCHAR2(100) := 'STPV'; -- APEX Workspace Name
  v_app_id NUMBER := 100; -- APEX Application ID
  v_session_id NUMBER := 1; -- APEX Session ID (doesn't matter)

  v_workspace_id apex_workspaces.workspace_id%type;

BEGIN
-- Get APEX workspace ID by name
  select 
    workspace_id
  into 
    v_workspace_id
  from 
    apex_workspaces
  where 
    upper(workspace) = upper(v_workspace_name);

-- Set APEX workspace ID
  apex_util.set_security_group_id(v_workspace_id);

-- Set APEX application ID
  apex_application.g_flow_id := v_app_id; 

-- Set APEX session ID
  apex_application.g_instance := v_session_id; 


wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.DIALOG.CANCEL',
				  p_message_language => 'de-ch',
				  p_message_text => 'Abbrechen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.DIALOG.CLOSE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Schließen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.DIALOG.HELP',
				  p_message_language => 'de-ch',
				  p_message_text => 'Hilfe');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.DIALOG.SAVE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Speichern');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.GV.NEXT_PAGE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Weiter');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.GV.PAGE_RANGE_XYZ',
				  p_message_language => 'de-ch',
				  p_message_text => '%0 - %1 von %2');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.GV.PREV_PAGE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Zurück');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.GV.SORT_ASCENDING',
				  p_message_language => 'de-ch',
				  p_message_text => 'Aufsteigend sortieren');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.GV.SORT_DESCENDING',
				  p_message_language => 'de-ch',
				  p_message_text => 'Absteigend sortieren');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.ACTIONS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Aktionen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.ADD',
				  p_message_language => 'de-ch',
				  p_message_text => 'Hinzufügen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.AGGREGATE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Aggregat');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.AGGREGATION',
				  p_message_language => 'de-ch',
				  p_message_text => 'Aggregation');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.ALL',
				  p_message_language => 'de-ch',
				  p_message_text => 'Alle');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.ALTERNATIVE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Alternative');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.AND',
				  p_message_language => 'de-ch',
				  p_message_text => 'und');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.ASCENDING',
				  p_message_language => 'de-ch',
				  p_message_text => 'Aufsteigend');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.AVG',
				  p_message_language => 'de-ch',
				  p_message_text => 'Durchschnitt');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.BACKGROUND_COLOR',
				  p_message_language => 'de-ch',
				  p_message_text => 'Hintergrundfarbe');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.BETWEEN',
				  p_message_language => 'de-ch',
				  p_message_text => 'zwischen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.CANCEL',
				  p_message_language => 'de-ch',
				  p_message_text => 'Abbrechen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.CHART',
				  p_message_language => 'de-ch',
				  p_message_text => 'Diagramm');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.CLOSE_COLUMN',
				  p_message_language => 'de-ch',
				  p_message_text => 'Schließen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.COLUMN',
				  p_message_language => 'de-ch',
				  p_message_text => 'Spalte');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.COLUMN_CONTEXT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Spalte %0');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.COLUMNS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Spalten');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.COMPUTE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Berechnen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.CONTAINS',
				  p_message_language => 'de-ch',
				  p_message_text => 'enthält');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.CONTROL_BREAK',
				  p_message_language => 'de-ch',
				  p_message_text => 'Kontrollgruppenwechsel');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.COUNT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Anzahl');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.COUNT_DISTINCT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Nur eindeutige Reihen zählen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.DATE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Datum');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.DAYS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Tage');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.DELETE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Löschen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.DESCENDING',
				  p_message_language => 'de-ch',
				  p_message_text => 'Absteigend');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.DIRECTION',
				  p_message_language => 'de-ch',
				  p_message_text => 'Richtung %0');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.DISABLED',
				  p_message_language => 'de-ch',
				  p_message_text => 'Deaktiviert');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.DOES_NOT_CONTAIN',
				  p_message_language => 'de-ch',
				  p_message_text => 'enthält nicht');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.DOWNLOAD',
				  p_message_language => 'de-ch',
				  p_message_text => 'Herunterladen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.EDIT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Bearbeiten');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.EDIT_CHART',
				  p_message_language => 'de-ch',
				  p_message_text => 'Diagramm bearbeiten');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.EDIT_GROUP_BY',
				  p_message_language => 'de-ch',
				  p_message_text => '"Gruppieren nach" bearbeiten');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.EMAIL_SUBJECT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Betreff');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.ENABLED',
				  p_message_language => 'de-ch',
				  p_message_text => 'Aktiviert');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.EXPRESSION',
				  p_message_language => 'de-ch',
				  p_message_text => 'Ausdruck');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.FILTER',
				  p_message_language => 'de-ch',
				  p_message_text => 'Filter');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.FILTERS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Filter');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.FILTER_WITH_DOTS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Filtern ...');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.FLASHBACK',
				  p_message_language => 'de-ch',
				  p_message_text => 'Flashback');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.FORMATMASK',
				  p_message_language => 'de-ch',
				  p_message_text => 'Formatieren');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.GO',
				  p_message_language => 'de-ch',
				  p_message_text => 'Los');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.GROUP_BY',
				  p_message_language => 'de-ch',
				  p_message_text => 'Gruppieren nach');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.HELP',
				  p_message_language => 'de-ch',
				  p_message_text => 'Hilfe');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.HIGHLIGHT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Markierung');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.HOURS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Stunden');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.IN',
				  p_message_language => 'de-ch',
				  p_message_text => 'in');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.LABEL',
				  p_message_language => 'de-ch',
				  p_message_text => 'Beschriftung %0');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.LABEL_COLUMN',
				  p_message_language => 'de-ch',
				  p_message_text => 'Beschriftung %0');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.LAST.DAY',
				  p_message_language => 'de-ch',
				  p_message_text => 'Letzter Tag');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.LAST.HOUR',
				  p_message_language => 'de-ch',
				  p_message_text => 'Letzte Stunde');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.LAST.MONTH',
				  p_message_language => 'de-ch',
				  p_message_text => 'Letzter Monat');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.LAST.WEEK',
				  p_message_language => 'de-ch',
				  p_message_text => 'Letzte Woche');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.LAST.X_DAYS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Letzte %0 Tage');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.LAST.X_HOURS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Letzte %0 Stunden');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.LAST.X_YEARS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Letzte %0 Jahre');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.LAST.YEAR',
				  p_message_language => 'de-ch',
				  p_message_text => 'Letztes Jahr');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.LINE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Zeile');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.MATCHES_REGULAR_EXPRESSION',
				  p_message_language => 'de-ch',
				  p_message_text => 'entspricht regulärem Ausdruck');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.MAX',
				  p_message_language => 'de-ch',
				  p_message_text => 'Maximal');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.MEDIAN',
				  p_message_language => 'de-ch',
				  p_message_text => 'Medianwert');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.MIN',
				  p_message_language => 'de-ch',
				  p_message_text => 'Minimal');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.MINUTES',
				  p_message_language => 'de-ch',
				  p_message_text => 'Minuten');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.MONTHS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Monaten');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.NAME',
				  p_message_language => 'de-ch',
				  p_message_text => 'Name');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.NEXT.DAY',
				  p_message_language => 'de-ch',
				  p_message_text => 'Nächster Tag');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.NEXT.HOUR',
				  p_message_language => 'de-ch',
				  p_message_text => 'Nächste Stunde');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.NEXT.MONTH',
				  p_message_language => 'de-ch',
				  p_message_text => 'Nächster Monat');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.NEXT.WEEK',
				  p_message_language => 'de-ch',
				  p_message_text => 'Nächste Woche');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.NEXT.X_DAYS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Nächste %0 Tage');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.NEXT.X_HOURS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Nächste %0 Stunden');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.NEXT.X_YEARS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Nächste %0 Jahre');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.NEXT.YEAR',
				  p_message_language => 'de-ch',
				  p_message_text => 'Nächstes Jahr');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.NOT_IN',
				  p_message_language => 'de-ch',
				  p_message_text => 'nicht in');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.OPERATOR',
				  p_message_language => 'de-ch',
				  p_message_text => 'Operator');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.PIE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Kreis');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.PIVOT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Pivot');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.PRIMARY',
				  p_message_language => 'de-ch',
				  p_message_text => 'Primary');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.PRIMARY_DEFAULT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Primärer Standardwert');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.PRIMARY_REPORT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Hauptbericht');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.REPORT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Bericht');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.REPORT_SETTINGS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Berichteinstellungen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.REPORT_VIEW',
				  p_message_language => 'de-ch',
				  p_message_text => 'Berichtsansicht');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.RESET',
				  p_message_language => 'de-ch',
				  p_message_text => 'Zurücksetzen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.ROW',
				  p_message_language => 'de-ch',
				  p_message_text => 'Zeile');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.ROWS_PER_PAGE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Zeilen pro Seite');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.SAVE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Speichern');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.SAVED_REPORT_DEFAULT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Standard');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.SAVED_REPORT_PRIVATE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Privat');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.SAVED_REPORT_PUBLIC',
				  p_message_language => 'de-ch',
				  p_message_text => 'Öffentlich');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.SEARCH',
				  p_message_language => 'de-ch',
				  p_message_text => 'Suche');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.SEARCH.COLUMN',
				  p_message_language => 'de-ch',
				  p_message_text => 'Suchen: %0');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.SELECT_COLUMNS_TO_SEARCH',
				  p_message_language => 'de-ch',
				  p_message_text => 'Zu suchende Spalten wählen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.SINGLE_ROW_VIEW',
				  p_message_language => 'de-ch',
				  p_message_text => 'Single Row-Ansicht');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.SORT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Sortierung');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.SUBSCRIPTION',
				  p_message_language => 'de-ch',
				  p_message_text => 'Subscription');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.SUM',
				  p_message_language => 'de-ch',
				  p_message_text => 'Summe %0');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.TEXT_COLOR',
				  p_message_language => 'de-ch',
				  p_message_text => 'Textfarbe');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.TOGGLE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Ein-/ausschalten');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.VALUE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Wert');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.VALUE_COLUMN',
				  p_message_language => 'de-ch',
				  p_message_text => 'Wert');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.VISIBLE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Angezeigt');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.WEEKS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Wochen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.X.MINUTES_AGO',
				  p_message_language => 'de-ch',
				  p_message_text => 'vor %0 Minuten');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.IG.YEARS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Jahren');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.RV.DELETE',
				  p_message_language => 'de-ch',
				  p_message_text => 'Löschen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.RV.EXCLUDE_HIDDEN',
				  p_message_language => 'de-ch',
				  p_message_text => 'Angezeigte Spalten');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.RV.EXCLUDE_NULL',
				  p_message_language => 'de-ch',
				  p_message_text => 'Nullwerte ausschließen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.RV.INSERT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Hinzufügen');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.RV.NEXT_RECORD',
				  p_message_language => 'de-ch',
				  p_message_text => 'Weiter');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.RV.PREV_RECORD',
				  p_message_language => 'de-ch',
				  p_message_text => 'Zurück');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.RV.REC_XY',
				  p_message_language => 'de-ch',
				  p_message_text => 'Zeile %0 von %1');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.TABS.NEXT',
				  p_message_language => 'de-ch',
				  p_message_text => 'Weiter');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.TABS.PREVIOUS',
				  p_message_language => 'de-ch',
				  p_message_text => 'Zurück');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'APEX.VALUE_REQUIRED',
				  p_message_language => 'de-ch',
				  p_message_text => 'Wert erforderlich');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'LAYOUT.CHART',
				  p_message_language => 'de-ch',
				  p_message_text => 'Diagramm');

wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => 'REGION_NAME.NATIVE_JET_CHART',
				  p_message_language => 'de-ch',
				  p_message_text => 'Diagramm');

COMMIT; 
END;
/
