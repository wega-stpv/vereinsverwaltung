set serveroutput on size unlimited
create or replace function
	get_translation(p_text varchar2, p_lang varchar2)
	return varchar2 is
		l_text varchar(32000);
	begin
		select distinct de_innen.message_text into l_text
	from APEX_190200.wwv_flow_messages$ en_innen join APEX_190200.wwv_flow_messages$ de_innen 
	on en_innen.name=de_innen.name
	and en_innen.message_language='en'
	and de_innen.message_language = p_lang
	and en_innen.message_text = p_text;
	return l_text;
	exception 
	when others then
		return null;
	end;
/

declare 
    l_command varchar2(8000);
    --l_lang varchar2(5):= 'fr-ch';
	
begin
 l_command := 'wwv_flow_api.create_message (
				  p_id => null, -- to get unique ID from sequence
				  p_flow_id => v_app_id,
				  p_name => ''<name>'',
				  p_message_language => ''<lang>'',
				  p_message_text => ''<tranlsation>'');' ||chr(10);
  
 for p_trans in (select distinct name, message_text,  trans_message_text from (
				select en.name name, en.message_text message_text
				, replace(get_translation(en.message_text, 'fr-ch'), '''', '''''') trans_message_text
                from  APEX_190200.wwv_flow_messages$ en left join  APEX_190200.wwv_flow_messages$ de 
				on en.message_language='en' and de.message_language = 'fr-ch' and  en.name=de.name
				where 1=1
				and de.name is null
                and en.flow_id = 100
                and en.message_language='en'
                and not exists (select 1 from APEX_190200.wwv_flow_messages$ where name = en.name and message_language = 'fr-ch')
				) where trans_message_text is not null
                order by name
                ) loop
				--dbms_output.put_line(replace(replace(replace(l_command, '<name>',p_trans.name), '<lang>', 'fr-ch' ), '<tranlsation>', p_trans.de_message_text));
				dbms_output.put_line(replace(replace(replace(l_command, '<name>',p_trans.name), '<lang>', 'fr-ch'), '<tranlsation>', p_trans.trans_message_text));
 end loop;
 end;
/

drop function get_translation
/
