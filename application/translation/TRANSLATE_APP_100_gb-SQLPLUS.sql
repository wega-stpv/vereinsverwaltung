/*
The MIT License (MIT)

Copyright (c) 2018 Pretius Sp. z o.o. sk.
Żwirki i Wigury 16a
02-092 Warsaw, Poland
www.pretius.com
www.translate-apex.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

SET DEFINE OFF
ALTER SESSION SET NLS_LANGUAGE = AMERICAN;

    
DECLARE
  v_workspace_name VARCHAR2(100) := 'STPV'; -- APEX Workspace Name
  v_app_id NUMBER := 100; -- APEX Application ID
  v_session_id NUMBER := 1; -- APEX Session ID (doesn't matter)

  v_workspace_id apex_workspaces.workspace_id%type;

BEGIN
-- Get APEX workspace ID by name
  select 
    workspace_id
  into 
    v_workspace_id
  from 
    apex_workspaces
  where 
    upper(workspace) = upper(v_workspace_name);

-- Set APEX workspace ID
  apex_util.set_security_group_id(v_workspace_id);

-- Set APEX application ID
  apex_application.g_flow_id := v_app_id; 

-- Set APEX session ID
  apex_application.g_instance := v_session_id; 

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_PIVOT',
  p_message_language => 'en',
  p_message_text => 'You can define one Pivot view per saved report. Once defined, you can switch between the pivot and report views using view icons on the Search bar. To create a Pivot view, you select:  
<p></p> 
<ul> 
<li>the columns on which to pivot</li> 
<li>the columns to display as rows</li> 
<li>the columns to aggregate along with the function to be performed (average, sum, count, etc.)</li> 
</ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.YES_NO.INVALID_VALUE',
  p_message_language => 'en',
  p_message_text => '#LABEL# must match the values %0 and %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_GROUP_BY_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Add Group By Column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_FUNCTION',
  p_message_language => 'en',
  p_message_text => 'Add Function');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_FILTER',
  p_message_language => 'en',
  p_message_text => 'Row Filter');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER.ICON_TEXT',
  p_message_language => 'en',
  p_message_text => 'Popup Calendar: %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_ROW_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Add Row Column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TOGGLE',
  p_message_language => 'en',
  p_message_text => 'Toggle');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_SETTING',
  p_message_language => 'en',
  p_message_text => '1 invalid setting');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_FILTER_QUERY',
  p_message_language => 'en',
  p_message_text => 'Invalid filter query');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NOT_VALID_EMAIL',
  p_message_language => 'en',
  p_message_text => 'Not a valid email address.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_COLUMN_N',
  p_message_language => 'en',
  p_message_text => 'Row Column %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_COLUMNS',
  p_message_language => 'en',
  p_message_text => 'Pivot Columns');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.DUP_REC_ID',
  p_message_language => 'en',
  p_message_text => 'Duplicate identity');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.NEXT_PAGE',
  p_message_language => 'en',
  p_message_text => 'Next');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.SELECTION_COUNT',
  p_message_language => 'en',
  p_message_text => '%0 rows selected');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.SORT_DESCENDING',
  p_message_language => 'en',
  p_message_text => 'Sort Descending');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.AGGREGATE',
  p_message_language => 'en',
  p_message_text => 'Aggregate');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.APPROX_COUNT_DISTINCT',
  p_message_language => 'en',
  p_message_text => 'Approx. Count Distinct');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.AREA',
  p_message_language => 'en',
  p_message_text => 'Area');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.AUTHORIZATION',
  p_message_language => 'en',
  p_message_text => 'Authorization');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.AXIS_VALUE_TITLE',
  p_message_language => 'en',
  p_message_text => 'Value Axis Title');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.CHART',
  p_message_language => 'en',
  p_message_text => 'Chart');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.CLOSE_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Close');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COLORS',
  p_message_language => 'en',
  p_message_text => 'Colors');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DONUT',
  p_message_language => 'en',
  p_message_text => 'Donut');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DUPLICATE_AGGREGATION',
  p_message_language => 'en',
  p_message_text => 'Duplicate Aggregation');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DUPLICATE_ROW',
  p_message_language => 'en',
  p_message_text => 'Duplicate Row');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.EDIT_GROUP_BY',
  p_message_language => 'en',
  p_message_text => 'Edit Group By');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.EMAIL_BCC',
  p_message_language => 'en',
  p_message_text => 'Blind Copy (bcc)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.EMAIL_SUBJECT',
  p_message_language => 'en',
  p_message_text => 'Subject');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.FD_TYPE',
  p_message_language => 'en',
  p_message_text => 'Type');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.FILTERS',
  p_message_language => 'en',
  p_message_text => 'Filters');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.FORMATMASK',
  p_message_language => 'en',
  p_message_text => 'Format Mask');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.FUNCTIONS_AND_OPERATORS',
  p_message_language => 'en',
  p_message_text => 'Functions and Operators');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.GO',
  p_message_language => 'en',
  p_message_text => 'Go');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_CUSTOMIZE.T_REMOVE_STYLE',
  p_message_language => 'en',
  p_message_text => 'Use Application Default Style');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'UPGRADE_CANDLESTICK_CHART',
  p_message_language => 'en',
  p_message_text => 'After upgrading, ensure the series attribute Label Column is mapped to a Date / Timestamp column.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.CORRECT_ERRORS',
  p_message_language => 'en',
  p_message_text => 'Correct errors before saving.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_DATA_EXPORT.HIGHLIGHT_COLUMN_IDX_NOT_EXIST',
  p_message_language => 'en',
  p_message_text => 'The column index referenced in the highlight %0 does not exist.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_DATA_EXPORT.AGG_COLUMN_IDX_NOT_EXIST',
  p_message_language => 'en',
  p_message_text => 'The column index referenced in the aggregate %0 does not exist.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'LAYOUT.CHART',
  p_message_language => 'en',
  p_message_text => 'Chart');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.DOWNLOAD_TITLE',
  p_message_language => 'en',
  p_message_text => 'Download Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HIDE',
  p_message_language => 'en',
  p_message_text => 'Hide');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ICON_VIEW',
  p_message_language => 'en',
  p_message_text => 'Icon View');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LABEL_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Label');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAST.MONTH',
  p_message_language => 'en',
  p_message_text => 'Last Month');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAST.X_MONTHS',
  p_message_language => 'en',
  p_message_text => 'Last %0 Months');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAST.X_YEARS',
  p_message_language => 'en',
  p_message_text => 'Last %0 Years');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LINE',
  p_message_language => 'en',
  p_message_text => 'Line');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LOW_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Low');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.MEDIAN',
  p_message_language => 'en',
  p_message_text => 'Median');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.MORE_DATA_FOUND',
  p_message_language => 'en',
  p_message_text => 'The data contains more than %0 rows which exceeds the maximum allowed. Please apply additional filters in order to view the results.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NEXT.MONTH',
  p_message_language => 'en',
  p_message_text => 'Next Month');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NOT_IN',
  p_message_language => 'en',
  p_message_text => 'not in');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NUMBER',
  p_message_language => 'en',
  p_message_text => 'Numeric');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.OPERATOR',
  p_message_language => 'en',
  p_message_text => 'Operator');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.PLACEHOLDER_INVALUES',
  p_message_language => 'en',
  p_message_text => 'Separate values with "%0"');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.POLAR',
  p_message_language => 'en',
  p_message_text => 'Polar');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.POSITION_CENTER',
  p_message_language => 'en',
  p_message_text => 'Center');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REPORT_DATA_AS_OF_ONE_MINUTE_AGO',
  p_message_language => 'en',
  p_message_text => 'Report data as of 1 minute ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_CHART',
  p_message_language => 'en',
  p_message_text => 'View Chart');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_ICONS',
  p_message_language => 'en',
  p_message_text => 'View Icons');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_YEARS',
  p_message_language => 'en',
  p_message_text => '%0 years');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_HOURS_FROM_NOW',
  p_message_language => 'en',
  p_message_text => '%0 hours from now');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.X_Y_OF_Z',
  p_message_language => 'en',
  p_message_text => 'row(s)%0 - %1 of %2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CELL',
  p_message_language => 'en',
  p_message_text => 'Cell');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART_TYPE',
  p_message_language => 'en',
  p_message_text => 'Chart Type');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_HEADING_MENU',
  p_message_language => 'en',
  p_message_text => 'Column Heading Menu');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_INFO',
  p_message_language => 'en',
  p_message_text => 'Column Information');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IS_NOT_NULL',
  p_message_language => 'en',
  p_message_text => 'is not null');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_LIKE',
  p_message_language => 'en',
  p_message_text => 'like');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_FOOTER_E3',
  p_message_language => 'en',
  p_message_text => 'CASE WHEN A = 10 THEN B + C ELSE B END');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTE',
  p_message_language => 'en',
  p_message_text => 'Compute');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DAY',
  p_message_language => 'en',
  p_message_text => 'Day');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DESCENDING',
  p_message_language => 'en',
  p_message_text => 'Descending');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DESCRIPTION',
  p_message_language => 'en',
  p_message_text => 'Description');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISPLAY',
  p_message_language => 'en',
  p_message_text => 'Display');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_CHART2',
  p_message_language => 'en',
  p_message_text => 'Edit Chart');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_GROUP_BY',
  p_message_language => 'en',
  p_message_text => 'Edit Group By');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXCLUDE_NULL',
  p_message_language => 'en',
  p_message_text => 'Exclude Null Values');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_SUBSCRIPTION',
  p_message_language => 'en',
  p_message_text => 'Add Subscription');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HCOLUMN',
  p_message_language => 'en',
  p_message_text => 'Horizontal Column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHTS',
  p_message_language => 'en',
  p_message_text => 'Highlights');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INTERACTIVE_REPORT_HELP',
  p_message_language => 'en',
  p_message_text => 'Interactive Report Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_HOUR',
  p_message_language => 'en',
  p_message_text => 'Last Hour');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_X_YEARS',
  p_message_language => 'en',
  p_message_text => 'Last %0 Years');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MEDIAN_X',
  p_message_language => 'en',
  p_message_text => 'Median %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MIN_AGO',
  p_message_language => 'en',
  p_message_text => '%0 minutes ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_DAY',
  p_message_language => 'en',
  p_message_text => 'Next Day');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_MONTH',
  p_message_language => 'en',
  p_message_text => 'Next Month');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NUMERIC_SEQUENCE',
  p_message_language => 'en',
  p_message_text => 'Sequence must be numeric.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RED',
  p_message_language => 'en',
  p_message_text => 'red');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE',
  p_message_language => 'en',
  p_message_text => 'Remove');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_FILTER',
  p_message_language => 'en',
  p_message_text => 'Remove Filter');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_FLASHBACK',
  p_message_language => 'en',
  p_message_text => 'Remove Flashback');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_HIGHLIGHT',
  p_message_language => 'en',
  p_message_text => 'Remove Highlight');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_OF',
  p_message_language => 'en',
  p_message_text => 'Row %0 of %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEARCH',
  p_message_language => 'en',
  p_message_text => 'Search');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_COLUMN_HEADING_MENU',
  p_message_language => 'en',
  p_message_text => 'Clicking on any column heading exposes a column heading menu.<p/><ul><li><b>Sort Ascending icon</b> sorts the report by the column in ascending order.</li><li><b>Sort Descending icon</b> sorts the report by the column in descending order.</li><li><b>Hide Column</b> hides the column.</li><li><b>Break Column</b> creates a break group on the column. This pulls the column out of the report as a master record.</li><li><b>Column Information</b> displays help text about the column, if available.</li><li><b>Text Area</b> is used to enter case insensitive search criteria (no need for wild cards). Entering a value will reduce the list of values at the bottom of the menu. You can then select a value from the bottom and the selected value will be created as a filter using ''''='''' (e.g. column = ''''ABC''''). Alternatively, you can click the flashlight icon and the entered value will be created as a filter with the ''''LIKE'''' modifier (e.g. column LIKE ''''%ABC%'''').<li><b>List of Unique Values</b> contains the first 500 unique values that meet your filters. If the column is a date, a list of date ranges is displayed instead. If you select a value, a filter will be created using ''''='''' (e.g. column = ''''ABC'''').</li></ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEQUENCE',
  p_message_language => 'en',
  p_message_text => 'Sequence');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.GRID',
  p_message_language => 'en',
  p_message_text => 'Grid');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_NEXT.Y.MONTHS',
  p_message_language => 'en',
  p_message_text => '%0 in the next %1 months');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_NEXT_MONTH',
  p_message_language => 'en',
  p_message_text => '%0 in the next month');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IS_NULL',
  p_message_language => 'en',
  p_message_text => '%0 is empty');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_BETWEEN.Y.AND.Z',
  p_message_language => 'en',
  p_message_text => '%0 not between %1 and %2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_LAST.Y.MINUTES',
  p_message_language => 'en',
  p_message_text => '%0 not in the last %1 minutes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_LAST_HOUR',
  p_message_language => 'en',
  p_message_text => '%0 not in the last hour');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_LAST_WEEK',
  p_message_language => 'en',
  p_message_text => '%0 not in the last week');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_NEXT_HOUR',
  p_message_language => 'en',
  p_message_text => '%0 not in the next hour');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_NEXT_MINUTE',
  p_message_language => 'en',
  p_message_text => '%0 not in the next minute');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_NEXT_WEEK',
  p_message_language => 'en',
  p_message_text => '%0 not in the next week');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_NEXT_YEAR',
  p_message_language => 'en',
  p_message_text => '%0 not in the next year');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DIALOG.CLOSE',
  p_message_language => 'en',
  p_message_text => 'Close');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.UI.BACK_TO_TOP',
  p_message_language => 'en',
  p_message_text => 'Start of page');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.YEARS_AGO',
  p_message_language => 'en',
  p_message_text => '%0y');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.WEEKS_AGO',
  p_message_language => 'en',
  p_message_text => '%0w');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.THEMES.INVALID_THEME_NUMBER',
  p_message_language => 'en',
  p_message_text => 'Theme number is invalid or theme is not a current UI theme.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SAVE_REPORT_SETTINGS',
  p_message_language => 'en',
  p_message_text => 'Save Report Settings');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SHOW_OVERALL_VALUE',
  p_message_language => 'en',
  p_message_text => 'Show Overall Value');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SORT_BY',
  p_message_language => 'en',
  p_message_text => 'Sort By');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SUMMARY',
  p_message_language => 'en',
  p_message_text => 'Interactive Grid. Report: %0, View: %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.TOOLTIP',
  p_message_language => 'en',
  p_message_text => 'Tooltip');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.VIEW',
  p_message_language => 'en',
  p_message_text => 'View');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_LAST_MONTH',
  p_message_language => 'en',
  p_message_text => '%0 in the last month');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_LAST_WEEK',
  p_message_language => 'en',
  p_message_text => '%0 in the last week');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'MAXIMIZE',
  p_message_language => 'en',
  p_message_text => 'Maximize');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.AUTHENTICATION.LOGIN_THROTTLE.COUNTER',
  p_message_language => 'en',
  p_message_text => 'Please wait <span id="apex_login_throttle_sec">%0</span> seconds to login again.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_ID_DOES_NOT_EXIST',
  p_message_language => 'en',
  p_message_text => 'Saved Interactive Report ID %0 does not exist.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_GROUP_BY_COLUMN',
  p_message_language => 'en',
  p_message_text => '- Select Group By Column -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FUNCTION_N',
  p_message_language => 'en',
  p_message_text => 'Function %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_DISPLAY_COLUMN_LIMIT_REACHED',
  p_message_language => 'en',
  p_message_text => 'The number of display columns in the report reached the limit.  Please click Select Columns under Actions menu to minimize the report display column list.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_AGG_NOT_ON_ROW_COL',
  p_message_language => 'en',
  p_message_text => 'You cannot aggregate on a column selected to as row column.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INACTIVE_SETTINGS',
  p_message_language => 'en',
  p_message_text => '%0 inactive settings');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_ROW',
  p_message_language => 'en',
  p_message_text => 'Select Row');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART_MAX_ROW_CNT',
  p_message_language => 'en',
  p_message_text => 'The maximum row count for a Chart query limits the number of rows in the base query, not the number of rows displayed.  Your base query exceeds the maximum row count of %0.  Please apply a filter to reduce the number of records in your base query.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.PAGE_RANGE_XY',
  p_message_language => 'en',
  p_message_text => '%0 - %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.ROW_CHANGED',
  p_message_language => 'en',
  p_message_text => 'Changed');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ACC_LABEL',
  p_message_language => 'en',
  p_message_text => 'Interactive Grid %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ALL',
  p_message_language => 'en',
  p_message_text => 'All');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ALL_TEXT_COLUMNS',
  p_message_language => 'en',
  p_message_text => 'All Text Columns');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ALTERNATIVE',
  p_message_language => 'en',
  p_message_text => 'Alternative');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.CASE_SENSITIVE_WITH_BRACKETS',
  p_message_language => 'en',
  p_message_text => '(Case Sensitive)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.CHANGES_SAVED',
  p_message_language => 'en',
  p_message_text => 'Changes saved');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.CHART_VIEW',
  p_message_language => 'en',
  p_message_text => 'Chart View');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DEFAULT_TYPE',
  p_message_language => 'en',
  p_message_text => 'Default Type');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DELETE_ROWS',
  p_message_language => 'en',
  p_message_text => 'Delete Rows');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DIRECTION',
  p_message_language => 'en',
  p_message_text => 'Direction');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.EDIT_CHART',
  p_message_language => 'en',
  p_message_text => 'Edit Chart');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.EMAIL_BODY',
  p_message_language => 'en',
  p_message_text => 'Message');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.FILTER_WITH_DOTS',
  p_message_language => 'en',
  p_message_text => 'Filter...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.FUNNEL',
  p_message_language => 'en',
  p_message_text => 'Funnel');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ERROR.PAGE_NOT_AVAILABLE',
  p_message_language => 'en',
  p_message_text => 'Sorry, this page isn''t available');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM.NOT_FOUND',
  p_message_language => 'en',
  p_message_text => 'Item %0 not found.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.AGGREGATE_TITLE',
  p_message_language => 'en',
  p_message_text => 'Aggregation Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.COMPUTE',
  p_message_language => 'en',
  p_message_text => '<p>Use this dialog to define additional columns based on mathematical and functional computations performed against existing columns.</p>

<p><strong>Computation List</strong><br>
The Computation list displays defined computations. Disable an existing computation by deselecting it.<br>
Click Add ( &plus; ) to add a new computation, or Delete ( &minus; ) to remove an existing computation.</p>

<p><strong>Computation Settings</strong><br>
Use the form to define the computation.<br>
Enter the column details such as heading, label, and select alignment settings.<br> 
Use the Expression textarea to enter the column(s) and associated functions for the computation.<br>
Select the appropriate data type, and optionally a format mask, for the new column.</p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.FILTER',
  p_message_language => 'en',
  p_message_text => '<p>Use this dialog to configure data filters which limit the rows returned.</p>

<p><strong>Filter List</strong><br>
The Filter list displays defined filters. Disable an existing filter by deselecting it.<br>
Click Add ( &plus; ) to create a new filter, or Delete ( &minus; ) to remove an existing filter.</p>

<p><strong>Filter Settings</strong><br>
Use the form to define the filter properties.<br>
Select the appropriate filter type:<br>
&nbsp;&nbsp;&nbsp;Row - filter for a term in any filterable column.<br>
&nbsp;&nbsp;&nbsp;Column - filter a specific column with a specified operator and value.</p>

<p><em>Note: When viewing the interactive grid, you can define row filters by typing directly into the Search field. Click Select Columns to Search to limit the search to a specific column. Alternately, open a Column Heading menu and select a value to create a column filter.</em></p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.REPORT',
  p_message_language => 'en',
  p_message_text => '<p>Use this dialog to save changes you have made to the current grid layout and configuration.<br>
Application developers can define multiple alternate report layouts. Where permissible, you and other end users can save a report as Public, which makes the report available to all other users of the grid. You can also save a report as a Private report that only you can view.</p>
<p>Select from the available types and enter a name for the saved report.</p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HOURS',
  p_message_language => 'en',
  p_message_text => 'hours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAST.MINUTE',
  p_message_language => 'en',
  p_message_text => 'Last Minute');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LESS_THAN_OR_EQUALS',
  p_message_language => 'en',
  p_message_text => 'less than or equals');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.MEDIAN_OVERALL',
  p_message_language => 'en',
  p_message_text => 'Overall Median');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NEXT.WEEK',
  p_message_language => 'en',
  p_message_text => 'Next Week');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NEXT.X_HOURS',
  p_message_language => 'en',
  p_message_text => 'Next %0 Hours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NEXT.X_MONTHS',
  p_message_language => 'en',
  p_message_text => 'Next %0 Months');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ORIENTATION',
  p_message_language => 'en',
  p_message_text => 'Orientation');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.PIVOT',
  p_message_language => 'en',
  p_message_text => 'Pivot');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.PRIMARY_REPORT',
  p_message_language => 'en',
  p_message_text => 'Primary Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.RADAR',
  p_message_language => 'en',
  p_message_text => 'Radar');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REPORT_SAVE_AS',
  p_message_language => 'en',
  p_message_text => 'Report - Save As');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REVERT_CHANGES',
  p_message_language => 'en',
  p_message_text => 'Revert Changes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_YEARS',
  p_message_language => 'en',
  p_message_text => 'years');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNAUTHORIZED',
  p_message_language => 'en',
  p_message_text => 'Unauthorized');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNSUPPORTED_DATA_TYPE',
  p_message_language => 'en',
  p_message_text => 'unsupported data type');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_DETAIL',
  p_message_language => 'en',
  p_message_text => 'View Detail');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_HOURS',
  p_message_language => 'en',
  p_message_text => '%0 hours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'IR_AS_NAMED_REPORT',
  p_message_language => 'en',
  p_message_text => 'As Named Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'OUT_OF_RANGE',
  p_message_language => 'en',
  p_message_text => 'Invalid set of rows requested, the source data of the report has been modified.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'PAGINATION.NEXT',
  p_message_language => 'en',
  p_message_text => 'Next');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_WEEKS_FROM_NOW',
  p_message_language => 'en',
  p_message_text => '%0 weeks from now');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'TOTAL',
  p_message_language => 'en',
  p_message_text => 'Total');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.X_Y_OF_MORE_THAN_Z',
  p_message_language => 'en',
  p_message_text => 'row(s) %0 - %1 of more than %2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_LESS_MIN_VALUE',
  p_message_language => 'en',
  p_message_text => '#LABEL# is less than specified minimum %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_NOT_BETWEEN_MIN_MAX',
  p_message_language => 'en',
  p_message_text => '#LABEL# is not between the valid range of %0 and %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGGREGATION',
  p_message_language => 'en',
  p_message_text => 'Aggregation');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_NOT_IN',
  p_message_language => 'en',
  p_message_text => 'not in');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COUNT_DISTINCT_X',
  p_message_language => 'en',
  p_message_text => 'Count Distinct');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DEFAULT_REPORT_TYPE',
  p_message_language => 'en',
  p_message_text => 'Default Report Type');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_CONFIRM',
  p_message_language => 'en',
  p_message_text => 'Would you like to delete these report settings?');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_CONFIRM_JS_DIALOG',
  p_message_language => 'en',
  p_message_text => 'Would you like to perform this delete action?');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ENABLED',
  p_message_language => 'en',
  p_message_text => 'Enabled');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXAMPLES_WITH_COLON',
  p_message_language => 'en',
  p_message_text => 'Examples:');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXPAND_COLLAPSE_ALT',
  p_message_language => 'en',
  p_message_text => 'Expand/Collapse');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ACTIONS',
  p_message_language => 'en',
  p_message_text => 'Actions');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ACTIONS_MENU',
  p_message_language => 'en',
  p_message_text => 'Actions Menu');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GREEN',
  p_message_language => 'en',
  p_message_text => 'green');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LABEL',
  p_message_language => 'en',
  p_message_text => 'Label');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_DAY',
  p_message_language => 'en',
  p_message_text => 'Last Day');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_MONTH',
  p_message_language => 'en',
  p_message_text => 'Last Month');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_YEAR',
  p_message_language => 'en',
  p_message_text => 'Last Year');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MAX_QUERY_COST',
  p_message_language => 'en',
  p_message_text => 'The query is estimated to exceed the maximum allowed resources. Please modify your report settings and try again.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MONTHLY',
  p_message_language => 'en',
  p_message_text => 'Monthly');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEW_CATEGORY',
  p_message_language => 'en',
  p_message_text => '- New Category -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_WEEK',
  p_message_language => 'en',
  p_message_text => 'Next Week');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_ALL',
  p_message_language => 'en',
  p_message_text => 'Remove All');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RESET_CONFIRM',
  p_message_language => 'en',
  p_message_text => 'Restore report to the default settings.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_TEXT_CONTAINS',
  p_message_language => 'en',
  p_message_text => 'Row text contains');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEARCH_BAR',
  p_message_language => 'en',
  p_message_text => 'Search Bar');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_CATEGORY',
  p_message_language => 'en',
  p_message_text => '- Select Category -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_CONTROL_BREAK',
  p_message_language => 'en',
  p_message_text => 'Used to create a break group on one or several columns. This pulls the columns out of the Interactive Report and displays them as a master record.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_DETAIL_VIEW',
  p_message_language => 'en',
  p_message_text => 'To view the details of a single row at a time, click the single row view icon on the row you want to view. If available, the single row view will always be the first column. Depending on the customization of the Interactive Report, the single row view may be the standard view or a custom page that may allow update.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_DOWNLOAD',
  p_message_language => 'en',
  p_message_text => 'Allows the current result set to be downloaded. The download formats will differ depending upon your installation and report definition but may include CSV, XLS, PDF, or RTF.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HD_TYPE',
  p_message_language => 'en',
  p_message_text => 'Condition Type');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.ACTIONS.INTRO_HEADING',
  p_message_language => 'en',
  p_message_text => 'Overview');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IS_NOT_NULL',
  p_message_language => 'en',
  p_message_text => '%0 is not empty');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_LAST.Y.HOURS',
  p_message_language => 'en',
  p_message_text => '%0 not in the last %1 hours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_LAST_YEAR',
  p_message_language => 'en',
  p_message_text => '%0 not in the last year');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_NEXT.Y.DAYS',
  p_message_language => 'en',
  p_message_text => '%0 not in the next %1 days');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.Y_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Y');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG_FORMAT_SAMPLE_1',
  p_message_language => 'en',
  p_message_text => 'Monday, 12 January, 2016');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.RV.PREV_RECORD',
  p_message_language => 'en',
  p_message_text => 'Previous');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.RV.DELETE',
  p_message_language => 'en',
  p_message_text => 'Delete');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ACTIONS.TOGGLE',
  p_message_language => 'en',
  p_message_text => 'Toggle %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.WEEKS_FROM_NOW',
  p_message_language => 'en',
  p_message_text => 'in %0w');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.HOURS_FROM_NOW',
  p_message_language => 'en',
  p_message_text => 'in %0h');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.MONTHS_FROM_NOW',
  p_message_language => 'en',
  p_message_text => 'in %0mo');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.YEARS_FROM_NOW',
  p_message_language => 'en',
  p_message_text => 'in %0y');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SAVED_REPORT_PUBLIC',
  p_message_language => 'en',
  p_message_text => 'Public');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SCATTER',
  p_message_language => 'en',
  p_message_text => 'Scatter');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SERIES_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Series');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SORT',
  p_message_language => 'en',
  p_message_text => 'Sort');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.TYPE',
  p_message_language => 'en',
  p_message_text => 'Type');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.UNIT',
  p_message_language => 'en',
  p_message_text => 'Unit');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.VALUE_REQUIRED',
  p_message_language => 'en',
  p_message_text => 'A value is required.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.WEEKS',
  p_message_language => 'en',
  p_message_text => 'weeks');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_LAST.Y.DAYS',
  p_message_language => 'en',
  p_message_text => '%0 in the last %1 days');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_LAST.Y.MONTHS',
  p_message_language => 'en',
  p_message_text => '%0 in the last %1 months');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_LAST_HOUR',
  p_message_language => 'en',
  p_message_text => '%0 in the last hour');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SESSION.EXPIRED',
  p_message_language => 'en',
  p_message_text => 'Your session has expired');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.SLIDER.VALUE_NOT_BETWEEN_MIN_MAX',
  p_message_language => 'en',
  p_message_text => '#LABEL# is not between the valid range of %0 and %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_SORT',
  p_message_language => 'en',
  p_message_text => 'Group By Sort');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_COLUMN_NOT_NULL',
  p_message_language => 'en',
  p_message_text => 'Pivot column must be specified.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CONTROL_BREAK_COLUMNS',
  p_message_language => 'en',
  p_message_text => 'Control Break Columns');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_GROUP_BY',
  p_message_language => 'en',
  p_message_text => 'Remove Group By');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TABLE_SUMMARY',
  p_message_language => 'en',
  p_message_text => 'Region = %0, Report = %1, View = %2, Displayed Rows Start = %3, Displayed Rows End = %4, Total Rows = %5');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RENAME_DEFAULT_REPORT',
  p_message_language => 'en',
  p_message_text => 'Rename Default Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_REQUIRED',
  p_message_language => 'en',
  p_message_text => 'Email Address must be specified.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SUBSCRIPTION',
  p_message_language => 'en',
  p_message_text => 'Subscription');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_FORMAT',
  p_message_language => 'en',
  p_message_text => '<p>Format enable you to customize the display of the report. 
Format contains the following submenu:</p> 
<ul><li>Sort</li> 
<li>Control Break</li> 
<li>Highlight</li> 
<li>Compute</li> 
<li>Aggregate</li> 
<li>Chart</li> 
<li>Group By</li> 
<li>Pivot</li> 
</ul> 
');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_FINDER',
  p_message_language => 'en',
  p_message_text => '<li><b>Select columns icon</b> enables you to identify which column to search (or all).</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.ROW_DELETED',
  p_message_language => 'en',
  p_message_text => 'Deleted');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.SORT_ASCENDING_ORDER',
  p_message_language => 'en',
  p_message_text => 'Sort Ascending %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.SORT_DESCENDING_ORDER',
  p_message_language => 'en',
  p_message_text => 'Sort Descending %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.SORT_OFF',
  p_message_language => 'en',
  p_message_text => 'Don''t Sort');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ADD_ROW',
  p_message_language => 'en',
  p_message_text => 'Add Row');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.AUTO',
  p_message_language => 'en',
  p_message_text => 'Auto');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.AVG',
  p_message_language => 'en',
  p_message_text => 'Average');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.AXIS_LABEL_TITLE',
  p_message_language => 'en',
  p_message_text => 'Label Axis Title');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COUNT_DISTINCT_OVERALL',
  p_message_language => 'en',
  p_message_text => 'Overall Count Distinct');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DATA',
  p_message_language => 'en',
  p_message_text => 'Data');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DATE',
  p_message_language => 'en',
  p_message_text => 'Date');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DESCENDING',
  p_message_language => 'en',
  p_message_text => 'Descending');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DUPLICATE_ROWS',
  p_message_language => 'en',
  p_message_text => 'Duplicate Rows');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.FIRST',
  p_message_language => 'en',
  p_message_text => 'First');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.FORMAT_CSV',
  p_message_language => 'en',
  p_message_text => 'CSV');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.GREATER_THAN_OR_EQUALS',
  p_message_language => 'en',
  p_message_text => 'greater than or equals');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.MENU.PROCESSING',
  p_message_language => 'en',
  p_message_text => 'Loading');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_DATA_EXPORT.PARENT_GROUP_IDX_NOT_EXIST',
  p_message_language => 'en',
  p_message_text => 'The parent group index referenced in %0 does not exist.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_DATA_EXPORT.COLUMN_GROUP_IDX_NOT_EXIST',
  p_message_language => 'en',
  p_message_text => 'The column group index referenced in %0 does not exist.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'REGION_NAME.NATIVE_JET_CHART',
  p_message_language => 'en',
  p_message_text => 'Chart');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.COMPUTE_TITLE',
  p_message_language => 'en',
  p_message_text => 'Compute Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.FILTER_TITLE',
  p_message_language => 'en',
  p_message_text => 'Filter Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.IN_THE_LAST',
  p_message_language => 'en',
  p_message_text => 'in the last');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.INVISIBLE',
  p_message_language => 'en',
  p_message_text => 'Not Displayed');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.IS_NULL',
  p_message_language => 'en',
  p_message_text => 'is empty');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAST',
  p_message_language => 'en',
  p_message_text => 'Last');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAST.HOUR',
  p_message_language => 'en',
  p_message_text => 'Last Hour');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAST.X_MINUTES',
  p_message_language => 'en',
  p_message_text => 'Last %0 Minutes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.MAILADRESSES_COMMASEP',
  p_message_language => 'en',
  p_message_text => 'Separate multiple Adresses with commas');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NAMED_REPORT',
  p_message_language => 'en',
  p_message_text => 'Named Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NEXT.HOUR',
  p_message_language => 'en',
  p_message_text => 'Next Hour');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.OPEN_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Open');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.PRIMARY_DEFAULT',
  p_message_language => 'en',
  p_message_text => 'Primary Default');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REPORT.SAVED.DEFAULT',
  p_message_language => 'en',
  p_message_text => 'Default report saved for all users');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT_ASCENDING',
  p_message_language => 'en',
  p_message_text => 'Sort Ascending');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_WEEK',
  p_message_language => 'en',
  p_message_text => 'Week');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'IR_STAR',
  p_message_language => 'en',
  p_message_text => 'Only displayed for developers');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'REPORT',
  p_message_language => 'en',
  p_message_text => 'Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'REPORT_TOTAL',
  p_message_language => 'en',
  p_message_text => 'report total');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_DAYS_AGO',
  p_message_language => 'en',
  p_message_text => '%0 days ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_HOURS_AGO',
  p_message_language => 'en',
  p_message_text => '%0 hours ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_NOW',
  p_message_language => 'en',
  p_message_text => 'Now');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_YEARS_FROM_NOW',
  p_message_language => 'en',
  p_message_text => '%0 years from now');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SAVE_REPORT',
  p_message_language => 'en',
  p_message_text => 'Saves the customized report for future use. You provide a name and an optional description.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_INVALID',
  p_message_language => 'en',
  p_message_text => '#LABEL# does not match format %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_LESS_MIN_DATE',
  p_message_language => 'en',
  p_message_text => '#LABEL# is less than specified minimum date %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_NOT_IN_YEAR_RANGE',
  p_message_language => 'en',
  p_message_text => '#LABEL# is not within valid year range of %0 and %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.FILE_BROWSE.DOWNLOAD_LINK_TEXT',
  p_message_language => 'en',
  p_message_text => 'Download.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_INVALID',
  p_message_language => 'en',
  p_message_text => '#LABEL# must be Numeric.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALTERNATIVE_DEFAULT_NAME',
  p_message_language => 'en',
  p_message_text => 'Alternative Default: %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CLEAR',
  p_message_language => 'en',
  p_message_text => 'clear');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IS_IN_NEXT',
  p_message_language => 'en',
  p_message_text => 'is in the next');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_ISNOT_IN_NEXT',
  p_message_language => 'en',
  p_message_text => 'is not in the next');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COUNT_DISTINCT',
  p_message_language => 'en',
  p_message_text => 'Count Distinct');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COUNT_X',
  p_message_language => 'en',
  p_message_text => 'Count %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DEFAULT',
  p_message_language => 'en',
  p_message_text => 'Default');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISABLED',
  p_message_language => 'en',
  p_message_text => 'Disabled');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISPLAYED',
  p_message_language => 'en',
  p_message_text => 'Displayed');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISPLAYED_COLUMNS',
  p_message_language => 'en',
  p_message_text => 'Displayed Columns');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_FILTER',
  p_message_language => 'en',
  p_message_text => 'Edit Filter');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_HIGHLIGHT',
  p_message_language => 'en',
  p_message_text => 'Edit Highlight');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_BODY',
  p_message_language => 'en',
  p_message_text => 'Body');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_CC',
  p_message_language => 'en',
  p_message_text => 'Cc');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_SEE_ATTACHED',
  p_message_language => 'en',
  p_message_text => 'See attached.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_SUBJECT',
  p_message_language => 'en',
  p_message_text => 'Subject');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_AVG',
  p_message_language => 'en',
  p_message_text => 'Average');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FILTER',
  p_message_language => 'en',
  p_message_text => 'Filter');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHT_CONDITION',
  p_message_language => 'en',
  p_message_text => 'Highlight Condition');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_X_DAYS',
  p_message_language => 'en',
  p_message_text => 'Last %0 Days');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_X_HOURS',
  p_message_language => 'en',
  p_message_text => 'Last %0 Hours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LINE',
  p_message_language => 'en',
  p_message_text => 'Line');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO_COLUMN_INFO',
  p_message_language => 'en',
  p_message_text => 'No column information available.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NULLS_ALWAYS_FIRST',
  p_message_language => 'en',
  p_message_text => 'Nulls Always First');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NUMERIC_FLASHBACK_TIME',
  p_message_language => 'en',
  p_message_text => 'Flashback time must be numeric.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_OPERATOR',
  p_message_language => 'en',
  p_message_text => 'Operator');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PERCENT_TOTAL_COUNT',
  p_message_language => 'en',
  p_message_text => 'Percent of Total Count');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PERCENT_TOTAL_SUM',
  p_message_language => 'en',
  p_message_text => 'Percent of Total Sum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_DOES_NOT_EXIST',
  p_message_language => 'en',
  p_message_text => 'Report does not exist.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROWS',
  p_message_language => 'en',
  p_message_text => 'Rows');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_AS_DEFAULT',
  p_message_language => 'en',
  p_message_text => 'Save as Default');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVED_REPORT',
  p_message_language => 'en',
  p_message_text => 'Saved Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_COLUMN',
  p_message_language => 'en',
  p_message_text => '- Select Column -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_GREATER_MAX_DATE',
  p_message_language => 'en',
  p_message_text => '#LABEL# is greater than specified maximum date %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_LAST.Y.YEARS',
  p_message_language => 'en',
  p_message_text => '%0 not in the last %1 years');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_NEXT.Y.MINUTES',
  p_message_language => 'en',
  p_message_text => '%0 not in the next %1 minutes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG_FORMAT_SAMPLE_3',
  p_message_language => 'en',
  p_message_text => '16 hours ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEW_CATEGORY_LABEL',
  p_message_language => 'en',
  p_message_text => 'New Category');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MULTIIR_PAGE_REGION_STATIC_ID_REQUIRED',
  p_message_language => 'en',
  p_message_text => 'Region Static ID must be specified as the page contains multiple interactive reports.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DATA',
  p_message_language => 'en',
  p_message_text => 'Data');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM.HELP_TEXT',
  p_message_language => 'en',
  p_message_text => 'Help Text: %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.WAIT_UNTIL_PAGE_LOADED',
  p_message_language => 'en',
  p_message_text => 'Please wait until the page is fully loaded and try again.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.TABS.PREVIOUS',
  p_message_language => 'en',
  p_message_text => 'Previous');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SAVED_REPORT_DEFAULT',
  p_message_language => 'en',
  p_message_text => 'Default');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SELECT_COLUMNS_TO_SEARCH',
  p_message_language => 'en',
  p_message_text => 'Select columns to search');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SEND_AS_EMAIL',
  p_message_language => 'en',
  p_message_text => 'Send as Email');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SORT_ONLY_ONE_PER_COLUMN',
  p_message_language => 'en',
  p_message_text => 'You can define only one sort per column.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SRV_CHANGE_MENU',
  p_message_language => 'en',
  p_message_text => 'Change Menu');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.TEXT_COLOR',
  p_message_language => 'en',
  p_message_text => 'Text Color');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.UNSAVED_CHANGES_CONTINUE_CONFIRM',
  p_message_language => 'en',
  p_message_text => 'There are unsaved changes. Do you want to continue?');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.VERTICAL',
  p_message_language => 'en',
  p_message_text => 'Vertical');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.BETWEEN.Y.AND.Z',
  p_message_language => 'en',
  p_message_text => '%0 between %1 and %2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN.Y',
  p_message_language => 'en',
  p_message_text => '%0 in %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_LAST_DAY',
  p_message_language => 'en',
  p_message_text => '%0 in the last day');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_LAST_MINUTE',
  p_message_language => 'en',
  p_message_text => '%0 in the last minute');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_NEXT.Y.HOURS',
  p_message_language => 'en',
  p_message_text => '%0 in the next %1 hours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.YES_NO.NO_LABEL',
  p_message_language => 'en',
  p_message_text => 'No');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LABEL_PREFIX',
  p_message_language => 'en',
  p_message_text => 'Label Prefix');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT',
  p_message_language => 'en',
  p_message_text => 'Edit');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECTED_COLUMNS',
  p_message_language => 'en',
  p_message_text => 'Selected Columns');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_GROUP_BY',
  p_message_language => 'en',
  p_message_text => 'You can define one Group By view per saved 
report. Once defined, you can switch between the group by and report 
views using view icons on the Search bar. To create a Group By view, 
you select: 
<p></p><ul> 
<li>the columns on which to group</li> 
<li>the columns to aggregate along with the function to be performed (average, sum, count, etc.)</li> 
</ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_VIEW',
  p_message_language => 'en',
  p_message_text => '<li><b>View Icons</b> switches between the icon, report, detail, chart, group by, and pivot views of the report if they are defined.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_COLUMN_N',
  p_message_language => 'en',
  p_message_text => 'Pivot Column %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_ROW_COLUMN',
  p_message_language => 'en',
  p_message_text => '- Select Row Column -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.PAGE_RANGE_XYZ',
  p_message_language => 'en',
  p_message_text => '%0 - %1 of %2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.PREV_PAGE',
  p_message_language => 'en',
  p_message_text => 'Previous');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ASCENDING',
  p_message_language => 'en',
  p_message_text => 'Ascending');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.BACKGROUND_COLOR',
  p_message_language => 'en',
  p_message_text => 'Background Color');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.BOTH',
  p_message_language => 'en',
  p_message_text => 'Both');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.CANCEL',
  p_message_language => 'en',
  p_message_text => 'Cancel');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COLOR_RED',
  p_message_language => 'en',
  p_message_text => 'Red');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COLUMN',
  p_message_language => 'en',
  p_message_text => 'Column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COMPLEX',
  p_message_language => 'en',
  p_message_text => 'Complex');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.CONTROL_BREAK',
  p_message_language => 'en',
  p_message_text => 'Control Break');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COUNT',
  p_message_language => 'en',
  p_message_text => 'Count');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COUNT_DISTINCT',
  p_message_language => 'en',
  p_message_text => 'Count Distinct');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DELETE_ROW',
  p_message_language => 'en',
  p_message_text => 'Delete Row');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DUPLICATE_CONTROLBREAK',
  p_message_language => 'en',
  p_message_text => 'Duplicate Control Break');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.EQUALS',
  p_message_language => 'en',
  p_message_text => 'equals');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.FLASHBACK',
  p_message_language => 'en',
  p_message_text => 'Flashback');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.FORMAT_HTML',
  p_message_language => 'en',
  p_message_text => 'HTML');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_CUSTOMIZE.T_THEME_STYLE',
  p_message_language => 'en',
  p_message_text => 'Appearance');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DIALOG.OK',
  p_message_language => 'en',
  p_message_text => 'OK');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.CHART',
  p_message_language => 'en',
  p_message_text => '<p>Use this dialog to define a chart which displays as a separate data view.<br> 
Select a chart Type, configure the chart settings, and click <strong>Save.</strong></p>

<p><strong>Chart Settings</strong></br>
The chart attributes that display vary depending on the chart type. A number of attributes can be entered to define the chart. Attributes marked with a red asterisk are mandatory.</p>

<p>Below are all available attributes across the different chart types (in alphabetical order):
<ul>
  <li>Aggregation - Select how to aggregate the associated chart values.</li> 
  <li>Close - Select the column that contains the daily stock close price (Stock chart only).</li> 
  <li>Decimal Places - Enter the number of decimal places to which the values are rounded.</li> 
  <li>Direction -  In relation to the Sort By attribute, specify whether the data is sorted in ascending or descending values.</li> 
  <li>High - Select the column that contains the high value (Range and Stock charts only).</li> 
  <li>Label - Select the column that contains the text for each data point.</li> 
  <li>Label Axis Title - Enter the title that displays on the label axis.</li> 
  <li>Low - Select the column that contains the low value (Range and Stock charts only).</li> 
  <li>Nulls - In relation to the Sort By attribute, specify how you want records with null values to be sorted in relation to records with non null values.</li> 
  <li>Open - Select the column that contains the daily stock openning price (Stock chart only).</li> 
  <li>Orientation - Select whether the chart elements, such as bars, display vertically or horizontally.</li> 
  <li>Series - Select the column used for defining your multi-series dynamic query.</li> 
  <li>Stack - Specify whether the data items are stacked.</li> 
  <li>Sort By - Select whether the chart is sorted by the label or the value(s).</li> 
  <li>Target - Select the column to be used for defining the target value on this chart. When set, the Value attribute defines the filled area within the slice and the Target represents the value of the whole slice (Funnel chart only).</li> 
  <li>Value - Select the column that contains the data to be plotted.</li> 
  <li>Value Axis Title - Enter the title that displays on the value axis.</li> 
  <li>Volume - Select the column that contains the daily stock volume (Stock chart only).</li> 
  <li>X - Select the column that contains the x-axis value for this chart (Bubble and Scatter charts only).</li> 
  <li>Y - Select the column that contains the y-axis value for this chart (Bubble and Scatter charts only).</li> 
  <li>Z - Select the column that contains the bar width or bubble radius (Bar, Bubble, and Range charts only)</li> 
</p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.CHART_TITLE',
  p_message_language => 'en',
  p_message_text => 'Chart Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.FLASHBACK_TITLE',
  p_message_language => 'en',
  p_message_text => 'Flashback Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.HIGHLIGHT_TITLE',
  p_message_language => 'en',
  p_message_text => 'Highlight Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ICON',
  p_message_language => 'en',
  p_message_text => 'Icon');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.INACTIVE_SETTINGS',
  p_message_language => 'en',
  p_message_text => 'Inactive Settings');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.INVALID_SETTING',
  p_message_language => 'en',
  p_message_text => 'Invalid Setting');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAST.X_HOURS',
  p_message_language => 'en',
  p_message_text => 'Last %0 Hours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAST.YEAR',
  p_message_language => 'en',
  p_message_text => 'Last Year');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.MAX_OVERALL',
  p_message_language => 'en',
  p_message_text => 'Overall Maximum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.MINUTES_AGO',
  p_message_language => 'en',
  p_message_text => 'Minutes ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NEXT.X_DAYS',
  p_message_language => 'en',
  p_message_text => 'Next %0 Days');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NEXT.X_MINUTES',
  p_message_language => 'en',
  p_message_text => 'Next %0 Minutes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NEXT.YEAR',
  p_message_language => 'en',
  p_message_text => 'Next Year');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NOT_EXIST',
  p_message_language => 'en',
  p_message_text => 'Region with ID %0 is not an Interactive Grid region or does not exist in application %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.OFF',
  p_message_language => 'en',
  p_message_text => 'Off');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.OPEN_COLORPICKER',
  p_message_language => 'en',
  p_message_text => 'Open Color Picker: %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REPORT_STATIC_ID_DOES_NOT_EXIST',
  p_message_language => 'en',
  p_message_text => 'Saved Interactive Grid with static ID %0 does not exist.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_MINS',
  p_message_language => 'en',
  p_message_text => '%0 minutes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'FLOW.SINGLE_VALIDATION_ERROR',
  p_message_language => 'en',
  p_message_text => '1 error has occurred');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'IR_AS_DEFAULT_REPORT_SETTING',
  p_message_language => 'en',
  p_message_text => 'As Default Report Settings');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'PAGINATION.PREVIOUS_SET',
  p_message_language => 'en',
  p_message_text => 'Previous Set');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'RESET',
  p_message_language => 'en',
  p_message_text => 'Reset Pagination');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_DAYS_FROM_NOW',
  p_message_language => 'en',
  p_message_text => '%0 days from now');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_MINUTES_FROM_NOW',
  p_message_language => 'en',
  p_message_text => '%0 minutes from now');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_UTILITIES.CLOSE',
  p_message_language => 'en',
  p_message_text => 'Close');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.FOUND_BUT_NOT_DISPLAYED',
  p_message_language => 'en',
  p_message_text => 'Minimum row requested: %0, rows found but not displayed: %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_REPORT_SETTINGS',
  p_message_language => 'en',
  p_message_text => 'If you have customized your Interactive Report, the report settings will be displayed below the Search Bar and above the report. If you have saved customized reports, they will be shown as tabs. You can access your alternate views by clicking the tabs. Below the tabs are the report settings for the current report. This area can be collapsed and expanded using the icon on the left.<p/>For each report setting, you can:<ul><li><b>Edit</b> by clicking the name.</li><li><b>Disable/Enable</b> by unchecking or checking the Enable/Disable check box. This is used to temporarily turn off and on the setting.</li><li><b>Remove</b> by click the Remove icon. This permanently removes the setting.</li></ul><p/>If you have created a chart, you can toggle between the report and chart using the Report View and Chart View links shown on the right. If you are viewing the chart, you can also use the Edit Chart link to edit the chart settings.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALTERNATIVE',
  p_message_language => 'en',
  p_message_text => 'Alternative');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AND',
  p_message_language => 'en',
  p_message_text => 'and');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_DOESNOT_CONTAIN',
  p_message_language => 'en',
  p_message_text => 'does not contain');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IS_IN_LAST',
  p_message_language => 'en',
  p_message_text => 'is in the last');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION',
  p_message_language => 'en',
  p_message_text => 'Computation');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_FOOTER_E1',
  p_message_language => 'en',
  p_message_text => '(B+C)*100');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISABLE',
  p_message_language => 'en',
  p_message_text => 'Disable');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DO_NOT_AGGREGATE',
  p_message_language => 'en',
  p_message_text => '- Do not aggregate -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DOWNLOAD',
  p_message_language => 'en',
  p_message_text => 'Download');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_ALTERNATIVE_DEFAULT',
  p_message_language => 'en',
  p_message_text => 'Edit Alternative Default');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_CHART',
  p_message_language => 'en',
  p_message_text => 'Edit Chart Settings');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL',
  p_message_language => 'en',
  p_message_text => 'Email');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_NOT_CONFIGURED',
  p_message_language => 'en',
  p_message_text => 'Email has not been configured for this application. Please contact your administrator.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FILTER_EXPRESSION',
  p_message_language => 'en',
  p_message_text => 'Filter Expression');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FILTERS',
  p_message_language => 'en',
  p_message_text => 'Filters');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FUNCTION',
  p_message_language => 'en',
  p_message_text => 'Function');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FUNCTIONS_OPERATORS',
  p_message_language => 'en',
  p_message_text => 'Functions / Operators');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GO',
  p_message_language => 'en',
  p_message_text => 'Go');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY',
  p_message_language => 'en',
  p_message_text => 'Group By');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHT_TYPE',
  p_message_language => 'en',
  p_message_text => 'Highlight Type');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHT_WHEN',
  p_message_language => 'en',
  p_message_text => 'Highlight When');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID',
  p_message_language => 'en',
  p_message_text => 'Invalid');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_IS_NOT_IN_THE_NEXT',
  p_message_language => 'en',
  p_message_text => '%0 is not in the next %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LAST_WEEK',
  p_message_language => 'en',
  p_message_text => 'Last Week');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MAX_ROW_CNT',
  p_message_language => 'en',
  p_message_text => 'This query returns more then %0 rows, please filter your data to ensure complete results.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MOVE',
  p_message_language => 'en',
  p_message_text => 'Move');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_HOUR',
  p_message_language => 'en',
  p_message_text => 'Next Hour');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO',
  p_message_language => 'en',
  p_message_text => 'No');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NULL_SORTING',
  p_message_language => 'en',
  p_message_text => 'Null Sorting');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NULLS_ALWAYS_LAST',
  p_message_language => 'en',
  p_message_text => 'Nulls Always Last');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIE',
  p_message_language => 'en',
  p_message_text => 'Pie');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PRIMARY',
  p_message_language => 'en',
  p_message_text => 'Primary');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PRIVATE',
  p_message_language => 'en',
  p_message_text => 'Private');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PUBLIC',
  p_message_language => 'en',
  p_message_text => 'Public');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_AGGREGATE',
  p_message_language => 'en',
  p_message_text => 'Remove Aggregate');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RENAME_REPORT',
  p_message_language => 'en',
  p_message_text => 'Rename Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_SETTINGS',
  p_message_language => 'en',
  p_message_text => 'Report Settings');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROWS_PER_PAGE',
  p_message_language => 'en',
  p_message_text => 'Rows Per Page');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_ACTIONS_MENU',
  p_message_language => 'en',
  p_message_text => 'The actions menu is used to customize the display of your Interactive Report.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_AGGREGATE',
  p_message_language => 'en',
  p_message_text => 'Aggregates are mathematical computations performed against a column. Aggregates are displayed after each control break and at the end of the report within the column they are defined.<p/><ul><li><b>aggregation</b> allows you to select a previously defined aggregation to edit.</li><li><b>Function</b> is the function to be performed (e.g. SUM, MIN).</li><li><b>Column</b> is used to select the column to apply the mathematical function to. Only numeric columns will be displayed.</li></ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_FILTER',
  p_message_language => 'en',
  p_message_text => 'Used to add or modify the where clause on the query. You first select a column (it does not need to be one that is displayed), select from a list of standard Oracle operators (=, !=, not in, between), and enter an expression to compare against. The expression is case sensitive and you can use % as a wildcard (for example, STATE_NAME like A%).');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_HIGHLIGHT',
  p_message_language => 'en',
  p_message_text => 'Highlighting allows you to define a filter. The rows that meet the filter are highlighted using the characteristics associated with the filter.<p/><ul><li><b>Name</b> is used only for display.</li><li><b>Sequence</b> identifies the sequence in which the rules will be evaluated.</li><li><b>Enabled</b> identifies if the rule is enabled or disabled.</li><li><b>Highlight Type</b> identifies whether the Row or Cell should be highlighted. If Cell is selected, the column referenced in the Highlight Condition is highlighted.</li><li><b>Background Color</b> is the new color for the background of the highlighted area.</li><li><b>Text Color</b> is the new color for the text in the highlighted area.</li><li><b>Highlight Condition</b> defines your filter condition.</li></ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_VALUE',
  p_message_language => 'en',
  p_message_text => 'Select Value');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.GROUP',
  p_message_language => 'en',
  p_message_text => 'Group');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HEADING',
  p_message_language => 'en',
  p_message_text => 'Heading');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.ACTIONS.REPORTING',
  p_message_language => 'en',
  p_message_text => '<p>You can customize the interactive grid to display data in various different ways using the built-in capabilities.</p>

<p>Use the Column Heading menus or the Actions menu to determine which columns to display, in what sequence, and freeze columns. You can also define various data filters and sort the data returned.</p>

<p>Use the View button (adjacent to the Search field) to access other data views that may have been defined by the application developer. You can also create a chart or view an existing chart.</p>  

<p><em>Note: Click <strong>Help</strong> in the interactive grid dialogs to obtain more detailed information on the selected function.</em></p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.ACTIONS.REPORTING_HEADING',
  p_message_language => 'en',
  p_message_text => 'Reporting Capabilities');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_NEXT_DAY',
  p_message_language => 'en',
  p_message_text => '%0 in the next day');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_LAST.Y.WEEKS',
  p_message_language => 'en',
  p_message_text => '%0 not in the last %1 weeks');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_NEXT.Y.HOURS',
  p_message_language => 'en',
  p_message_text => '%0 not in the next %1 hours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_NEXT.Y.YEARS',
  p_message_language => 'en',
  p_message_text => '%0 not in the next %1 years');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_NEXT_DAY',
  p_message_language => 'en',
  p_message_text => '%0 not in the next day');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_LIKE.Y',
  p_message_language => 'en',
  p_message_text => '%0 not like %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.RV.EXCLUDE_HIDDEN',
  p_message_language => 'en',
  p_message_text => 'Displayed Columns');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.RV.EXCLUDE_NULL',
  p_message_language => 'en',
  p_message_text => 'Exclude Null Values');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_REPORT',
  p_message_language => 'en',
  p_message_text => 'Remove Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REGION_STATIC_ID_DOES_NOT_EXIST',
  p_message_language => 'en',
  p_message_text => 'Region Static ID %0 does not exist.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_ROW_COLUMN_INVALID',
  p_message_language => 'en',
  p_message_text => 'Please select different row column. The HTML expression or link in the row column contains column defined as pivot or aggregate column.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FILTER_EXPR_TOO_LONG',
  p_message_language => 'en',
  p_message_text => 'The filter expression is too long.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_WEB_SERVICES.UNSUPPORTED_OAUTH_TOKEN',
  p_message_language => 'en',
  p_message_text => 'Server responded with unsupported OAuth token type.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_WEB_SERVICES.AUTHENTICATION_FAILED',
  p_message_language => 'en',
  p_message_text => 'Authentication failed.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.TEMPLATE.EXPAND_COLLAPSE_SIDE_COL_LABEL',
  p_message_language => 'en',
  p_message_text => 'Expand / Collapse Side Column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.TABS.NEXT',
  p_message_language => 'en',
  p_message_text => 'Next');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.MINUTES_FROM_NOW',
  p_message_language => 'en',
  p_message_text => 'in %0m');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.HOURS_AGO',
  p_message_language => 'en',
  p_message_text => '%0h');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SAVE',
  p_message_language => 'en',
  p_message_text => 'Save');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SAVE_AS',
  p_message_language => 'en',
  p_message_text => 'Save As');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.STARTS_WITH',
  p_message_language => 'en',
  p_message_text => 'starts with');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.UNFREEZE',
  p_message_language => 'en',
  p_message_text => 'Unfreeze');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_LAST.Y.MINUTES',
  p_message_language => 'en',
  p_message_text => '%0 in the last %1 minutes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_LAST.Y.WEEKS',
  p_message_language => 'en',
  p_message_text => '%0 in the last %1 weeks');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_NEXT.Y.DAYS',
  p_message_language => 'en',
  p_message_text => '%0 in the next %1 days');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'RESTORE',
  p_message_language => 'en',
  p_message_text => 'Restore');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SESSION_STATE.SSP_VIOLATION',
  p_message_language => 'en',
  p_message_text => 'Session state protection violation: This may be caused by manual alteration of a URL containing a checksum or by using a link with an incorrect or missing checksum. If you are unsure what caused this error, please contact the application administrator for assistance.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_MAX_ROW_CNT',
  p_message_language => 'en',
  p_message_text => 'The maximum row count for a Pivot query limits the number of rows in the base query, not the number of rows displayed. Your base query exceeds the maximum row count of %0. Please apply a filter to reduce the number of records in your base query.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.REGION.JQM_LIST_VIEW.LOAD_MORE',
  p_message_language => 'en',
  p_message_text => 'Load more...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RPT_DISP_COL_EXCEED',
  p_message_language => 'en',
  p_message_text => 'The number of display columns in the report reached the limit.  Please click Select Columns under Actions menu to minimize the report display column list.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.REGION.JQM_LIST_VIEW.SEARCH',
  p_message_language => 'en',
  p_message_text => 'Search...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_FILTER',
  p_message_language => 'en',
  p_message_text => 'Invalid filter expression. %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD_PIVOT_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Add Pivot Column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_COL_DIFF_FROM_PIVOT_COL',
  p_message_language => 'en',
  p_message_text => 'Row column must be different from the pivot column.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_SORT_ORDER',
  p_message_language => 'en',
  p_message_text => 'Group By Sort Order');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_FILTER',
  p_message_language => 'en',
  p_message_text => 'Filter...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHECK_ALL',
  p_message_language => 'en',
  p_message_text => 'Check All');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_TEXTBOX',
  p_message_language => 'en',
  p_message_text => '<li><b>Text area</b> enables you to enter case insensitive search criteria (wild card characters are implied).</li> 
<li><b>Go button</b> executes the search. Hitting the enter key will also execute the search when the cursor is in the search text area.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.BREAK_EXPAND',
  p_message_language => 'en',
  p_message_text => 'Expand control break');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.LAST_PAGE',
  p_message_language => 'en',
  p_message_text => 'Last');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.BAR',
  p_message_language => 'en',
  p_message_text => 'Bar');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COLOR_ORANGE',
  p_message_language => 'en',
  p_message_text => 'Orange');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COLOR_YELLOW',
  p_message_language => 'en',
  p_message_text => 'Yellow');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COLUMN_TYPE',
  p_message_language => 'en',
  p_message_text => 'Column Purpose');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COLUMNS',
  p_message_language => 'en',
  p_message_text => 'Columns');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DELETE',
  p_message_language => 'en',
  p_message_text => 'Delete');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DISABLED',
  p_message_language => 'en',
  p_message_text => 'Disabled');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.EMAIL_CC',
  p_message_language => 'en',
  p_message_text => 'Copy (cc)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ENABLED',
  p_message_language => 'en',
  p_message_text => 'Enabled');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.FREEZE',
  p_message_language => 'en',
  p_message_text => 'Freeze');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.VALUE_REQUIRED',
  p_message_language => 'en',
  p_message_text => 'Value Required');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.COLUMNS',
  p_message_language => 'en',
  p_message_text => '<p>Use this dialog to choose which columns display and in what order.</p>

<p>Hide a column by deselecting it.<br>
Reorder columns by clicking Move Up ( &uarr; ) or Move Down ( &darr; ).<br>
Use the drop down selector to list All columns, Displayed columns, or Not Displayed columns.</p>

<p>Optionally, use the form to specify the minimum width of a column in pixels.</p>

<p><em>Note: You can also reorder displayed columns by clicking the drag handle (at the start of the column heading) and dragging the column left or right. You can also change the column width of displayed columns by selecting the column separator, between headings, and moving it left or right.</em</p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.SORT',
  p_message_language => 'en',
  p_message_text => '<p>Use this dialog to set the display order.</p>

<p><strong>Sort List</strong><br>
The Sort dialog displays a list of configured sorting rules.<br>
Click Add ( &plus; ) to create a sort column, or Delete ( &minus; ) to remove a sort column.<br>
Click Move Up ( &uarr; ) and Move Down ( &darr; ) to move the selected sort column up and down relative to the other sort columns.</p>

<p><strong>Sort Settings</strong><br>
Select a sort column, the sort direction, and how to order null columns (columns with no value).</p>

<p><em>Note: Data can be sorted by columns which are not displayed; however, not all columns may be sortable.</em><br>
<em>Displayed columns can be sorted by pressing the up (ascending) or down (descending) arrows at the end of the column headings. To add a subsequent column to an existing sort, hold the Shift key and click the up or down arrow.</em></p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.IN',
  p_message_language => 'en',
  p_message_text => 'in');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.INVALID_SETTINGS',
  p_message_language => 'en',
  p_message_text => 'Invalid Settings');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAYOUT_USEGROUPFOR',
  p_message_language => 'en',
  p_message_text => 'Use Group For');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LESS_THAN',
  p_message_language => 'en',
  p_message_text => 'less than');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.MIN_OVERALL',
  p_message_language => 'en',
  p_message_text => 'Overall Minimum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NOT_EQUALS',
  p_message_language => 'en',
  p_message_text => 'not equals');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.PIVOT_VIEW',
  p_message_language => 'en',
  p_message_text => 'Pivot View');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REFRESH_ROWS',
  p_message_language => 'en',
  p_message_text => 'Refresh Rows');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REPORT.DELETED',
  p_message_language => 'en',
  p_message_text => 'Report deleted');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALID_FORMAT_MASK',
  p_message_language => 'en',
  p_message_text => 'Please enter a valid format mask.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_REPORT',
  p_message_language => 'en',
  p_message_text => 'View Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_MONTHS',
  p_message_language => 'en',
  p_message_text => '%0 months');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'PAGINATION.PREVIOUS',
  p_message_language => 'en',
  p_message_text => 'Previous');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SAVED_REPORTS.PRIMARY.DEFAULT',
  p_message_language => 'en',
  p_message_text => 'Primary Default');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.UNSAVED_DATA',
  p_message_language => 'en',
  p_message_text => 'This form contains unsaved changes. Press Ok to proceed without saving your changes.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_GREATER_MAX_VALUE',
  p_message_language => 'en',
  p_message_text => '#LABEL# is greater than specified maximum %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_MAX',
  p_message_language => 'en',
  p_message_text => 'Maximum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_MEDIAN',
  p_message_language => 'en',
  p_message_text => 'Median');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_MODE',
  p_message_language => 'en',
  p_message_text => 'Mode');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_APPLY',
  p_message_language => 'en',
  p_message_text => 'Apply');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CANCEL',
  p_message_language => 'en',
  p_message_text => 'Cancel');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART_INITIALIZING',
  p_message_language => 'en',
  p_message_text => 'Initializing...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_HEADING',
  p_message_language => 'en',
  p_message_text => 'Column Heading');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_ISNOT_IN_LAST',
  p_message_language => 'en',
  p_message_text => 'is not in the last');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_REGEXP_LIKE',
  p_message_language => 'en',
  p_message_text => 'matches regular expression');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_FOOTER_E2',
  p_message_language => 'en',
  p_message_text => 'INITCAP(B)||'''', ''''||INITCAP(C)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CONTROL_BREAKS',
  p_message_language => 'en',
  p_message_text => 'Control Breaks');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DATE',
  p_message_language => 'en',
  p_message_text => 'Date');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DO_NOT_DISPLAY',
  p_message_language => 'en',
  p_message_text => 'Do Not Display');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DOWN',
  p_message_language => 'en',
  p_message_text => 'Down');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXAMPLES',
  p_message_language => 'en',
  p_message_text => 'Examples');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EXPRESSION',
  p_message_language => 'en',
  p_message_text => 'Expression');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FINDER_ALT',
  p_message_language => 'en',
  p_message_text => 'Select columns to search');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SORT',
  p_message_language => 'en',
  p_message_text => 'Used to change the column(s) to sort on and whether to sort ascending or descending. You can also specify how to handle nulls (use the default setting, always display them last or always display them first). The resulting sorting is displayed to the right of column headings in the report.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEW_COMPUTATION',
  p_message_language => 'en',
  p_message_text => 'New Computation');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT',
  p_message_language => 'en',
  p_message_text => '&gt;');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NONE',
  p_message_language => 'en',
  p_message_text => '- None -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PERCENT_OF_TOTAL_COUNT_X',
  p_message_language => 'en',
  p_message_text => 'Percent of Total Count %0 (%)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PRIMARY_REPORT',
  p_message_language => 'en',
  p_message_text => 'Primary Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT',
  p_message_language => 'en',
  p_message_text => 'Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_REPORT',
  p_message_language => 'en',
  p_message_text => 'Save Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_CHART',
  p_message_language => 'en',
  p_message_text => 'You can include one chart per Interactive Report. Once defined, you can switch between the chart and report views using links below the search bar.<p/><ul><li><b>Chart Type</b> identifies the chart type to include. Select from horizontal bar, vertical bar, pie or line.</li><li><b>Label</b> allows you to select the column to be used as the label.</li><li><b>Axis Title for Label</b> is the title that will display on the axis associated with the column selected for Label. This is not available for pie chart.</li><li><b>Value</b> allows you to select the column to be used as the value. If your function is a COUNT, a Value does not need to be selected.</li><li><b>Axis Title for Value</b> is the title that will display on the axis associated with the column selected for Value. This is not available for pie chart.</li><li><b>Function</b> is an optional function to be performed on the column selected for Value.</li></ul>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT',
  p_message_language => 'en',
  p_message_text => 'Sort');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.GROUP_BY',
  p_message_language => 'en',
  p_message_text => 'Group By');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.GROUP_BY_VIEW',
  p_message_language => 'en',
  p_message_text => 'Group By View');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HEADING_ALIGN',
  p_message_language => 'en',
  p_message_text => 'Heading Alignment');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP',
  p_message_language => 'en',
  p_message_text => 'Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.ACTIONS.EDITING_HEADING',
  p_message_language => 'en',
  p_message_text => 'Editing Capabilities');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ROW_ACTIONS',
  p_message_language => 'en',
  p_message_text => 'Row Actions');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ROW_COLUMN_CONTEXT',
  p_message_language => 'en',
  p_message_text => 'Row %0 Column %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ROW_CONTEXT',
  p_message_language => 'en',
  p_message_text => 'Row %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_NEXT.Y.YEARS',
  p_message_language => 'en',
  p_message_text => '%0 in the next %1 years');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_NEXT_MINUTE',
  p_message_language => 'en',
  p_message_text => '%0 in the next minute');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.LESS_THAN_OR_EQUALS.Y',
  p_message_language => 'en',
  p_message_text => '%0 less than or equal to %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.MINUTES_AGO',
  p_message_language => 'en',
  p_message_text => '%0 minutes ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_LAST.Y.MONTHS',
  p_message_language => 'en',
  p_message_text => '%0 not in the last %1 months');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_LAST_MINUTE',
  p_message_language => 'en',
  p_message_text => '%0 not in the last minute');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_NEXT.Y.WEEKS',
  p_message_language => 'en',
  p_message_text => '%0 not in the next %1 weeks');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.RV.REC_XY',
  p_message_language => 'en',
  p_message_text => 'Row %0 of %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.RV.SETTINGS_MENU',
  p_message_language => 'en',
  p_message_text => 'Settings Menu');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ACTIVE_STATE',
  p_message_language => 'en',
  p_message_text => '(Active)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.RICH_TEXT_EDITOR.ACCESSIBLE_LABEL',
  p_message_language => 'en',
  p_message_text => '%0, rich text editor');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.MENU.OVERFLOW_LABEL',
  p_message_language => 'en',
  p_message_text => 'More...');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SUCCESS_MESSAGE_HEADING',
  p_message_language => 'en',
  p_message_text => 'Success Message');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_WEB_SERVICES.NO_VALID_OAUTH_TOKEN',
  p_message_language => 'en',
  p_message_text => 'OAuth access token not available or expired.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.MONTHS_AGO',
  p_message_language => 'en',
  p_message_text => '%0mo');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.MINUTES_AGO',
  p_message_language => 'en',
  p_message_text => '%0m');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.DAYS_AGO',
  p_message_language => 'en',
  p_message_text => '%0d');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ROWS_PER_PAGE',
  p_message_language => 'en',
  p_message_text => 'Rows Per Page');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SEARCH',
  p_message_language => 'en',
  p_message_text => 'Search');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SEARCH.ALL_COLUMNS',
  p_message_language => 'en',
  p_message_text => 'Search: All Text Columns');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SEARCH.COLUMN',
  p_message_language => 'en',
  p_message_text => 'Search: %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.STACK',
  p_message_language => 'en',
  p_message_text => 'Stack');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.TOGGLE',
  p_message_language => 'en',
  p_message_text => 'Toggle');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.VALUE',
  p_message_language => 'en',
  p_message_text => 'Value');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.CONTAINS.Y',
  p_message_language => 'en',
  p_message_text => '%0 contains %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.EQUALS.Y',
  p_message_language => 'en',
  p_message_text => '%0 equals %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.CLOSE_NOTIFICATION',
  p_message_language => 'en',
  p_message_text => 'Close Notification');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.AUTHENTICATION.LOGIN_THROTTLE.ERROR',
  p_message_language => 'en',
  p_message_text => 'The login attempt has been blocked.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALL_ROWS',
  p_message_language => 'en',
  p_message_text => 'All Rows');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.SLIDER.VALUE_NOT_MULTIPLE_OF_STEP',
  p_message_language => 'en',
  p_message_text => '#LABEL# is not a multiple of %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMN_N',
  p_message_language => 'en',
  p_message_text => 'Column %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_PIVOT',
  p_message_language => 'en',
  p_message_text => 'View Pivot');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_PIVOT_COLUMN',
  p_message_language => 'en',
  p_message_text => '- Select Pivot Column -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_COLUMNS',
  p_message_language => 'en',
  p_message_text => 'Row Columns');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_COLUMN_NOT_NULL',
  p_message_language => 'en',
  p_message_text => 'Row column must be specified.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_AGG_NOT_NULL',
  p_message_language => 'en',
  p_message_text => 'Aggregate must be specified.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_CHART',
  p_message_language => 'en',
  p_message_text => 'Remove Chart');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_REPORT_DEFAULT',
  p_message_language => 'en',
  p_message_text => 'Save Report *');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNGROUPED_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Ungrouped Column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_DEFAULT_REPORT',
  p_message_language => 'en',
  p_message_text => 'Save Default Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_EXPRESSION',
  p_message_language => 'en',
  p_message_text => 'Computation Expression');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_MAX_ROW_CNT',
  p_message_language => 'en',
  p_message_text => 'The maximum row count for a Group By query limits the number of rows in the base query, not the number of rows displayed.  Your base query exceeds the maximum row count of %0.  Please apply a filter to reduce the number of records in your base query.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.LOAD_MORE',
  p_message_language => 'en',
  p_message_text => 'Load More Rows');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.ROW_ADDED',
  p_message_language => 'en',
  p_message_text => 'Added');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.SORT_ASCENDING',
  p_message_language => 'en',
  p_message_text => 'Sort Ascending');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.AGGREGATION',
  p_message_language => 'en',
  p_message_text => 'Aggregation');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.CASE_SENSITIVE',
  p_message_language => 'en',
  p_message_text => 'Case Sensitive');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.CHANGE_VIEW',
  p_message_language => 'en',
  p_message_text => 'Change View');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COLOR_GREEN',
  p_message_language => 'en',
  p_message_text => 'Green');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.CONTAINS',
  p_message_language => 'en',
  p_message_text => 'contains');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DATA_TYPE',
  p_message_language => 'en',
  p_message_text => 'Data Type');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DEFAULT_SETTINGS',
  p_message_language => 'en',
  p_message_text => 'Default Settings');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DETAIL_VIEW',
  p_message_language => 'en',
  p_message_text => 'Detail View');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.EMAIL_SENT',
  p_message_language => 'en',
  p_message_text => 'Email sent.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.EXAMPLE',
  p_message_language => 'en',
  p_message_text => 'Example');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.EXPRESSION',
  p_message_language => 'en',
  p_message_text => 'Expression');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.FILTER',
  p_message_language => 'en',
  p_message_text => 'Filter');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_CUSTOMIZE.T_MESSAGE3',
  p_message_language => 'en',
  p_message_text => 'You can personalize the appearance of this application by changing the Theme Style. Please select a Theme Style from the list below and click on Apply Changes.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DIALOG.SAVE',
  p_message_language => 'en',
  p_message_text => 'Save');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.MENU.CURRENT_MENU',
  p_message_language => 'en',
  p_message_text => 'current');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_DATA_EXPORT.COLUMN_BREAK_MUST_BE_IN_THE_BEGGINING',
  p_message_language => 'en',
  p_message_text => 'The column break needs to be in the beggining of the columns array.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.CONTROL_BREAK',
  p_message_language => 'en',
  p_message_text => '<p>Use this dialog to define a control break on one or more columns.</p>

<p><strong>Control Break List</strong><br>
The Control Break list displays defined control breaks. Disable an existing control break column by deselecting it.<br>
Click Add ( &plus; ) to include a new column in the control break, or Delete ( &minus; ) to remove an existing column from the control break.<br>
To reorder columns, click Move Up ( &uarr; ) or Move Down ( &darr; ) to move the selected column up and down relative to other columns.</p>

<p><strong>Control Break Settings</strong><br>
Use the form to define the control break column.<br>
Selected a control break column, the sort direction, and how to order null columns (columns with no value).</p>

<p><em>Note: When viewing the interactive grid, you can define a control break by clicking a Column Heading and selecting the control break icon.</em></p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.DOWNLOAD',
  p_message_language => 'en',
  p_message_text => '<p>Use this dialog to download all the current rows to an external file. The file will contain only the currently displayed columns, using any filters and sorts applied to the data.</p>

<p>Select the file format and click Download.<br>
Note: CSV will not include text formatting such as aggregates and control breaks.</p>

<p>To email the file, select Send as Email and enter the email details (Recipient, Subject and Message).</p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.GROUP_BY_TITLE',
  p_message_language => 'en',
  p_message_text => 'Group By Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.SORT_TITLE',
  p_message_language => 'en',
  p_message_text => 'Sort Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HIGH_COLUMN',
  p_message_language => 'en',
  p_message_text => 'High');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.INACTIVE_SETTING',
  p_message_language => 'en',
  p_message_text => 'Inactive Setting');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.INTERNAL_ERROR',
  p_message_language => 'en',
  p_message_text => 'An internal error has occurred while processing the Interactive Grid request.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.INVALID_DATE_FORMAT',
  p_message_language => 'en',
  p_message_text => 'Invalid Date Format');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.INVALID_VALUE',
  p_message_language => 'en',
  p_message_text => 'Invalid Value');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LINE_WITH_AREA',
  p_message_language => 'en',
  p_message_text => 'Line with Area');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.MATCHES_REGULAR_EXPRESSION',
  p_message_language => 'en',
  p_message_text => 'matches regular expression');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.MAX',
  p_message_language => 'en',
  p_message_text => 'Maximum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.MONTHS',
  p_message_language => 'en',
  p_message_text => 'months');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NEXT.DAY',
  p_message_language => 'en',
  p_message_text => 'Next Day');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NEXT.X_YEARS',
  p_message_language => 'en',
  p_message_text => 'Next %0 Years');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NOT_IN_THE_NEXT',
  p_message_language => 'en',
  p_message_text => 'not in the next');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ON',
  p_message_language => 'en',
  p_message_text => 'On');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.PIE',
  p_message_language => 'en',
  p_message_text => 'Pie');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.POSITION_START',
  p_message_language => 'en',
  p_message_text => 'Start');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.PRIMARY',
  p_message_language => 'en',
  p_message_text => 'Primary');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.RANGE',
  p_message_language => 'en',
  p_message_text => 'Range');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REFRESH_ROW',
  p_message_language => 'en',
  p_message_text => 'Refresh Row');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REPORT.SAVED.ALTERNATIVE',
  p_message_language => 'en',
  p_message_text => 'Alternative report saved for all users');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REPORT_SETTINGS',
  p_message_language => 'en',
  p_message_text => 'Report Settings');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT_DESCENDING',
  p_message_language => 'en',
  p_message_text => 'Sort Descending');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SPACE_AS_IN_ONE_EMPTY_STRING',
  p_message_language => 'en',
  p_message_text => 'space');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SUBSCRIPTION_ENDING',
  p_message_language => 'en',
  p_message_text => 'Ending');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SUBSCRIPTION_STARTING_FROM',
  p_message_language => 'en',
  p_message_text => 'Starting From');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SUM_X',
  p_message_language => 'en',
  p_message_text => 'Sum %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNIQUE_COLUMN_HEADING',
  p_message_language => 'en',
  p_message_text => 'Column Heading must be unique.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UP',
  p_message_language => 'en',
  p_message_text => 'Up');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VIEW_GROUP_BY',
  p_message_language => 'en',
  p_message_text => 'View Group By');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_WEEKS',
  p_message_language => 'en',
  p_message_text => '%0 weeks');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_YEAR',
  p_message_language => 'en',
  p_message_text => 'Year');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_YELLOW',
  p_message_language => 'en',
  p_message_text => 'yellow');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'INVALID_CREDENTIALS',
  p_message_language => 'en',
  p_message_text => 'Invalid Login Credentials');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.SORT_BY_THIS_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Sort by this column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SELECT_COLUMNS',
  p_message_language => 'en',
  p_message_text => 'Used to modify the columns displayed. The columns on the right are displayed. The columns on the left are hidden. You can reorder the displayed columns using the arrows on the far right. Computed columns are prefixed with <b>**</b>.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.NUMBER_FIELD.VALUE_INVALID2',
  p_message_language => 'en',
  p_message_text => '#LABEL# does not match number format %0 (For example, %1).');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGGREGATE',
  p_message_language => 'en',
  p_message_text => 'Aggregate');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALL',
  p_message_language => 'en',
  p_message_text => 'All');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_BETWEEN',
  p_message_language => 'en',
  p_message_text => 'between');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_BGCOLOR',
  p_message_language => 'en',
  p_message_text => 'Background Color');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CALENDAR',
  p_message_language => 'en',
  p_message_text => 'Calendar');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART',
  p_message_language => 'en',
  p_message_text => 'Chart');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHOOSE_DOWNLOAD_FORMAT',
  p_message_language => 'en',
  p_message_text => 'Choose report download format');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_CONTAINS',
  p_message_language => 'en',
  p_message_text => 'contains');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_NOT_LIKE',
  p_message_language => 'en',
  p_message_text => 'not like');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPUTATION_FOOTER',
  p_message_language => 'en',
  p_message_text => 'Create a computation using column aliases.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CONTROL_BREAK',
  p_message_language => 'en',
  p_message_text => 'Control Break');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DATA_AS_OF',
  p_message_language => 'en',
  p_message_text => 'Report data as of %0 minutes ago.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE',
  p_message_language => 'en',
  p_message_text => 'Delete');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DISPLAY_IN_REPORT',
  p_message_language => 'en',
  p_message_text => 'Display in Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_ADDRESS',
  p_message_language => 'en',
  p_message_text => 'Email Address');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_FREQUENCY',
  p_message_language => 'en',
  p_message_text => 'Frequency');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ENABLE_DISABLE_ALT',
  p_message_language => 'en',
  p_message_text => 'Enable/Disable');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.PAGE_ITEM_IS_REQUIRED',
  p_message_language => 'en',
  p_message_text => '#LABEL# must have some value.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_3D',
  p_message_language => 'en',
  p_message_text => '3D');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FORMAT',
  p_message_language => 'en',
  p_message_text => 'Format Mask');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP',
  p_message_language => 'en',
  p_message_text => 'Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIDE_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Hide Column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_IS_IN_THE_NEXT',
  p_message_language => 'en',
  p_message_text => '%0 is in the next %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MAX_X',
  p_message_language => 'en',
  p_message_text => 'Maximum %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO_END_DATE',
  p_message_language => 'en',
  p_message_text => '- No End Date -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ORANGE',
  p_message_language => 'en',
  p_message_text => 'orange');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_VIEW',
  p_message_language => 'en',
  p_message_text => '&lt; Report View');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_RESET',
  p_message_language => 'en',
  p_message_text => 'Reset');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVED_REPORT_MSG',
  p_message_language => 'en',
  p_message_text => 'Saved Report = "%0"');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEARCH_REPORT',
  p_message_language => 'en',
  p_message_text => 'Search Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_SORT_COLUMN',
  p_message_language => 'en',
  p_message_text => '- Select Sort Column -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.GRID_VIEW',
  p_message_language => 'en',
  p_message_text => 'Grid View');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.ACTIONS.INTRO',
  p_message_language => 'en',
  p_message_text => '<p>An interactive grid presents a set of data in a searchable, customizable report. You can perform numerous operations to limit the records returned, and change the way the data is displayed.</p>

<p>Use the Search field to filter the records returned. Click Actions to access numerous options for modifying the report layout, or use the Column Heading menus on displayed columns.</p>

<p>Use Report Settings to save your customizations to a report. You can also download the data from the report to an external file or email the data to yourself or others.</p>

<p>To learn more, see Using Interactive Grids" in <em>Oracle Application Express End User''s Guide</em>."');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.ACTIONS_TITLE',
  p_message_language => 'en',
  p_message_text => 'Interactive Grid Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REVERT_ROWS',
  p_message_language => 'en',
  p_message_text => 'Revert Rows');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.LESS_THAN.Y',
  p_message_language => 'en',
  p_message_text => '%0 less than %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_EQUALS.Y',
  p_message_language => 'en',
  p_message_text => '%0 not equals %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.YEARS',
  p_message_language => 'en',
  p_message_text => 'years');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.RV.NOT_GROUPED_LABEL',
  p_message_language => 'en',
  p_message_text => 'Other Columns');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.RV.REC_X',
  p_message_language => 'en',
  p_message_text => 'Row %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SHOW_ALL',
  p_message_language => 'en',
  p_message_text => 'Show All');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.COMBOBOX.SHOW_ALL_VALUES',
  p_message_language => 'en',
  p_message_text => 'Open list for: %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SEARCH.ORACLE_TEXT',
  p_message_language => 'en',
  p_message_text => 'Search: Full Text');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SUM_OVERALL',
  p_message_language => 'en',
  p_message_text => 'Overall Sum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.VARCHAR2',
  p_message_language => 'en',
  p_message_text => 'Text');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.DOES_NOT_CONTAIN.Y',
  p_message_language => 'en',
  p_message_text => '%0 does not contain %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.GREATER_THAN_OR_EQUALS.Y',
  p_message_language => 'en',
  p_message_text => '%0 greater than or equal to %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_LAST.Y.HOURS',
  p_message_language => 'en',
  p_message_text => '%0 in the last %1 hours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_LAST_YEAR',
  p_message_language => 'en',
  p_message_text => '%0 in the last year');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_NEXT.Y.MINUTES',
  p_message_language => 'en',
  p_message_text => '%0 in the next %1 minutes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEND',
  p_message_language => 'en',
  p_message_text => 'Send');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORT_ALIAS_DOES_NOT_EXIST',
  p_message_language => 'en',
  p_message_text => 'Saved Interactive report with alias %0 does not exist.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EDIT_PIVOT',
  p_message_language => 'en',
  p_message_text => 'Edit Pivot');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_COL_NOT_NULL',
  p_message_language => 'en',
  p_message_text => 'Group by column must be specified.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT_ORDER',
  p_message_language => 'en',
  p_message_text => 'Sort Order');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INACTIVE_SETTING',
  p_message_language => 'en',
  p_message_text => '1 inactive setting');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SEARCH_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Search: %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SUBSCRIPTION',
  p_message_language => 'en',
  p_message_text => 'When you add a subscription, you provide an email address (or multiple email addresses, separated by commas), email subject, frequency, and start and end dates. The resulting emails include an HTML version of the interactive report containing the current data using the report setting that were present when the subscription was added.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_REPORTS',
  p_message_language => 'en',
  p_message_text => '<li><b>Reports</b> displays alternate default and saved private or public reports.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.DELETED_COUNT',
  p_message_language => 'en',
  p_message_text => '%0 rows deleted');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.FIRST_PAGE',
  p_message_language => 'en',
  p_message_text => 'First');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.SELECT_PAGE_N',
  p_message_language => 'en',
  p_message_text => 'Page %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.TOTAL_PAGES',
  p_message_language => 'en',
  p_message_text => 'Total %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ACTIONS',
  p_message_language => 'en',
  p_message_text => 'Actions');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ADD',
  p_message_language => 'en',
  p_message_text => 'Add');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.AVG_OVERALL',
  p_message_language => 'en',
  p_message_text => 'Overall Average');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.BETWEEN',
  p_message_language => 'en',
  p_message_text => 'between');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COLOR_BLUE',
  p_message_language => 'en',
  p_message_text => 'Blue');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COLUMN_CONTEXT',
  p_message_language => 'en',
  p_message_text => 'Column %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COUNT_OVERALL',
  p_message_language => 'en',
  p_message_text => 'Overall Count');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.CREATE_X',
  p_message_language => 'en',
  p_message_text => 'Create %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DAYS',
  p_message_language => 'en',
  p_message_text => 'days');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DELETE_REPORT_CONFIRM',
  p_message_language => 'en',
  p_message_text => 'Are you sure you would like to delete this report?');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DETAIL',
  p_message_language => 'en',
  p_message_text => 'Detail');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.EDIT',
  p_message_language => 'en',
  p_message_text => 'Edit');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.FLASHBACK',
  p_message_language => 'en',
  p_message_text => '<p>Use this dialog to view the data as it existed at a previous point in time.</p>
<p>Enter the number of minutes in the past to execute the flashback query.</p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.HIGHLIGHT',
  p_message_language => 'en',
  p_message_text => '<p>Use this dialog to highlight rows or columns of data based on the condition entered.</p>

<p><strong>Highlight List</strong><br>
The Highlight list displays defined highlights. Disable an existing highlight by deselecting it.<br>
Click Add ( &plus; ) to create a new highlight, or Delete ( &minus; ) to remove an existing highlight.</p>

<p><strong>Highlight Settings</strong><br>
Use the form to define the highlight properties.<br>
Enter the name, select Row or Column, and select the HTML color codes for the background and text.<br>
Select the appropriate <strong>Condition Type</strong> to highlight specific data:<br>
&nbsp;&nbsp;&nbsp;Row - highlight the term in any column.<br>
&nbsp;&nbsp;&nbsp;Column - highlight within a specific column based on the specified operator and value.</p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.SUBSCRIPTION_TITLE',
  p_message_language => 'en',
  p_message_text => 'Subscription Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HORIZONTAL',
  p_message_language => 'en',
  p_message_text => 'Horizontal');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LABEL',
  p_message_language => 'en',
  p_message_text => 'Label');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LISTAGG',
  p_message_language => 'en',
  p_message_text => 'Listagg');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NAME',
  p_message_language => 'en',
  p_message_text => 'Name');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NO_DATA_FOUND',
  p_message_language => 'en',
  p_message_text => 'No data found');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NULLS',
  p_message_language => 'en',
  p_message_text => 'Nulls');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ONE_MINUTE_AGO',
  p_message_language => 'en',
  p_message_text => '1 minute ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.POSITION_END',
  p_message_language => 'en',
  p_message_text => 'End');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REFRESH',
  p_message_language => 'en',
  p_message_text => 'Refresh');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REMOVE_CONTROL',
  p_message_language => 'en',
  p_message_text => 'Remove %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REPORT.SAVED.PUBLIC',
  p_message_language => 'en',
  p_message_text => 'Public report saved for all users');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REPORT_DATA_AS_OF.X.MINUTES_AGO',
  p_message_language => 'en',
  p_message_text => 'Report data as of %0 minutes ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REPORT_VIEW',
  p_message_language => 'en',
  p_message_text => 'Report View');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.RESET',
  p_message_language => 'en',
  p_message_text => 'Reset');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SORT_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Sort Column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_STATUS',
  p_message_language => 'en',
  p_message_text => 'Status');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TEXT_COLOR',
  p_message_language => 'en',
  p_message_text => 'Text Color');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_HOURS',
  p_message_language => 'en',
  p_message_text => 'hours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_MONTHS',
  p_message_language => 'en',
  p_message_text => 'months');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_WEEKS',
  p_message_language => 'en',
  p_message_text => 'weeks');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TOP',
  p_message_language => 'en',
  p_message_text => 'Top');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_UNIQUE_HIGHLIGHT_NAME',
  p_message_language => 'en',
  p_message_text => 'Highlight Name must be unique.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALUE',
  p_message_language => 'en',
  p_message_text => 'Value');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALUE_REQUIRED',
  p_message_language => 'en',
  p_message_text => 'Value Required');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_WEEKLY',
  p_message_language => 'en',
  p_message_text => 'Weekly');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_MONTHS_FROM_NOW',
  p_message_language => 'en',
  p_message_text => '%0 months from now');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_SECONDS_FROM_NOW',
  p_message_language => 'en',
  p_message_text => '%0 seconds from now');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_UTILITIES.CAL',
  p_message_language => 'en',
  p_message_text => 'Calendar');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_UTILITIES.OK',
  p_message_language => 'en',
  p_message_text => 'Ok');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_RENDER_REPORT3.X_Y_OF_Z_2',
  p_message_language => 'en',
  p_message_text => '%0 - %1 of %2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR',
  p_message_language => 'en',
  p_message_text => 'At the top of each report page is a search region. The region provides the following features:<p/><ul><li><b>Select columns icon</b> allows you to identify which column to search (or all).</li><li><b>Text area</b> allows for case insensitive search criteria (no need for wild cards).</li><li><b>Rows</b> selects the number of records to display per page.</li><li><b>[Go] button</b> executes the search.</li><li><b>Actions Menu icon</b> displays the actions menu (discussed next).</li></ul><p/>Please note that all features may not be available for each report.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GO_TO_ERROR',
  p_message_language => 'en',
  p_message_text => 'Go to error.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_MIN',
  p_message_language => 'en',
  p_message_text => 'Minimum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_SUM',
  p_message_language => 'en',
  p_message_text => 'Sum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGGREGATE_DESCRIPTION',
  p_message_language => 'en',
  p_message_text => 'Aggregates are displayed after each control break and at the end of the report.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AS_OF',
  p_message_language => 'en',
  p_message_text => 'As of %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AVERAGE_X',
  p_message_language => 'en',
  p_message_text => 'Average %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_BLUE',
  p_message_language => 'en',
  p_message_text => 'blue');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_BOTTOM',
  p_message_language => 'en',
  p_message_text => 'Bottom');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COLUMNS',
  p_message_language => 'en',
  p_message_text => 'Columns');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IS_NULL',
  p_message_language => 'en',
  p_message_text => 'is null');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ENABLE',
  p_message_language => 'en',
  p_message_text => 'Enable');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ADD',
  p_message_language => 'en',
  p_message_text => 'Add');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_AGG_COUNT',
  p_message_language => 'en',
  p_message_text => 'Count');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FLASHBACK',
  p_message_language => 'en',
  p_message_text => 'Flashback');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FLASHBACK_DESCRIPTION',
  p_message_language => 'en',
  p_message_text => 'A flashback query enables you to view the data as it existed at a previous point in time.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FLASHBACK_ERROR_MSG',
  p_message_language => 'en',
  p_message_text => 'Unable to perform flashback request');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_GROUP_BY_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Group By Column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_COMPUTATION',
  p_message_language => 'en',
  p_message_text => 'Invalid computation expression. %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_IS_IN_THE_LAST',
  p_message_language => 'en',
  p_message_text => '%0 is in the last %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_IS_NOT_IN_THE_LAST',
  p_message_language => 'en',
  p_message_text => '%0 is not in the last %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_KEYPAD',
  p_message_language => 'en',
  p_message_text => 'Keypad');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MIN_X',
  p_message_language => 'en',
  p_message_text => 'Minimum %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MOVE_ALL',
  p_message_language => 'en',
  p_message_text => 'Move All');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_X_DAYS',
  p_message_language => 'en',
  p_message_text => 'Next %0 Days');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_X_HOURS',
  p_message_language => 'en',
  p_message_text => 'Next %0 Hours');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_X_YEARS',
  p_message_language => 'en',
  p_message_text => 'Next %0 Years');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEXT_YEAR',
  p_message_language => 'en',
  p_message_text => 'Next Year');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO_COMPUTATION_DEFINED',
  p_message_language => 'en',
  p_message_text => 'No computation defined.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_CONTROL_BREAK',
  p_message_language => 'en',
  p_message_text => 'Remove Control Break');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW_ORDER',
  p_message_language => 'en',
  p_message_text => 'Row Order');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_COLUMNS',
  p_message_language => 'en',
  p_message_text => 'Select Columns');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_01',
  p_message_language => 'en',
  p_message_text => 'An Interactive Report displays a predetermined set of columns. The report may be further customized with an initial filter clause, a default sort order, control breaks, highlighting, computations, aggregates and a chart. Each Interactive Report can then be further customized and the results can be viewed, or downloaded, and the report definition can be stored for later use. <p/> An Interactive Report can be customized in three ways: the search bar, actions menu and column heading menu.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_COMPUTE',
  p_message_language => 'en',
  p_message_text => 'Computations allow you to add computed columns to your report. These can be mathematical computations (e.g. NBR_HOURS/24) or standard Oracle functions applied to existing columns (some have been displayed for example, others, like TO_DATE, can also be used).<p/><ul><li><b>Computation</b> allows you to select a previously defined computation to edit.</li><li><b>Column Heading</b> is the column heading for the new column.</li><li><b>Format Mask</b> is an Oracle format mask to be applied against the column (e.g. S9999).</li><li><b>Format Mask</b> is an Oracle format mask to be applied against the column (e.g. S9999).</li><li><b>Computation</b> is the computation to be performed. Within the computation, columns are referenced using the aliases displayed.</li></ul><p/>Below computation, the columns in your query are displayed with their associated alias. Clicking on the column name or alias will write them into the Computation. Next to Columns is a Keypad. These are simply shortcuts of commonly used keys. On the far right are Functions.<p/>An example computation to display Total Compensation is:<p/><pre>CASE WHEN A = ''''sales'''' THEN B + C ELSE B END</pre>(where A is ORGANIZATION, B is SALARY and C is COMMISSION)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_FLASHBACK',
  p_message_language => 'en',
  p_message_text => 'Performs a flashback query to allow you to view the data as it existed at a previous point in time. The default amount of time that you can flashback is 3 hours (or 180 minutes) but the actual amount will differ per database.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.ACTIONS.EDITING',
  p_message_language => 'en',
  p_message_text => '<p>You can insert, update, and delete data directly within this interactive grid.</p>

<p>Insert a new row by clicking the Add Row button.</p>

<p>Edit existing data by double-clicking a specific cell. For larger editing work, click Edit to enter editing mode. In editing mode, you can single-click or use the keyboard to edit specific cells.</p>

<p>Use the Change menu to duplicate and delete rows. To enable the Change menu, use the check boxes to select one or more rows.</p>

<p>Duplicate a selected row by clicking the Change menu and selecting Duplicate Rows. Delete a selected row by clicking the Change menu and selecting Delete Row.</p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_NEXT.Y.WEEKS',
  p_message_language => 'en',
  p_message_text => '%0 in the next %1 weeks');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.LIKE.Y',
  p_message_language => 'en',
  p_message_text => '%0 like %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN.Y',
  p_message_language => 'en',
  p_message_text => '%0 not in %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_LAST_DAY',
  p_message_language => 'en',
  p_message_text => '%0 not in the last day');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_LAST_MONTH',
  p_message_language => 'en',
  p_message_text => '%0 not in the last month');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_NEXT.Y.MONTHS',
  p_message_language => 'en',
  p_message_text => '%0 not in the next %1 months ');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.STARTS_WITH.Y',
  p_message_language => 'en',
  p_message_text => '%0 starts with %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG_FORMAT_SAMPLE_2',
  p_message_language => 'en',
  p_message_text => 'January');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.RV.NEXT_RECORD',
  p_message_language => 'en',
  p_message_text => 'Next');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.RV.INSERT',
  p_message_language => 'en',
  p_message_text => 'Add');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MAP_IT',
  p_message_language => 'en',
  p_message_text => 'Map it');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ERROR_MESSAGE_HEADING',
  p_message_language => 'en',
  p_message_text => 'Error Message');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'LAYOUT.T_#EXPAND_COLLAPSE_SIDE_COL_LABEL#',
  p_message_language => 'en',
  p_message_text => 'Label for Expand / Collapse Side Column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.TEMPLATE.EXPAND_COLLAPSE_NAV_LABEL',
  p_message_language => 'en',
  p_message_text => 'Expand / Collapse Navigation');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.COMBOBOX.LIST_OF_VALUES',
  p_message_language => 'en',
  p_message_text => 'List of Values');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.DAYS_FROM_NOW',
  p_message_language => 'en',
  p_message_text => 'in %0d');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.SECONDS_AGO',
  p_message_language => 'en',
  p_message_text => '%0s');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SAVED_REPORT_PRIVATE',
  p_message_language => 'en',
  p_message_text => 'Private');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SAVED_REPORTS',
  p_message_language => 'en',
  p_message_text => 'Saved Reports');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SEL_ACTIONS',
  p_message_language => 'en',
  p_message_text => 'Selection Actions');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SELECT',
  p_message_language => 'en',
  p_message_text => '- Select -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SUBSCRIPTION',
  p_message_language => 'en',
  p_message_text => 'Subscription');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SUM',
  p_message_language => 'en',
  p_message_text => 'Sum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.TARGET_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Target');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.VISIBLE',
  p_message_language => 'en',
  p_message_text => 'Displayed');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.VOLUME_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Volume');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.WIDTH',
  p_message_language => 'en',
  p_message_text => 'Minimum Column Width (Pixel)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.GREATER_THAN.Y',
  p_message_language => 'en',
  p_message_text => '%0 greater than %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_LAST.Y.YEARS',
  p_message_language => 'en',
  p_message_text => '%0 in the last %1 years');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SESSION.EXPIRED.NEW_SESSION',
  p_message_language => 'en',
  p_message_text => 'Click <a href="%0">here</a> to create a new session.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.ITEM_TYPE.YES_NO.YES_LABEL',
  p_message_language => 'en',
  p_message_text => 'Yes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_END_DATE',
  p_message_language => 'en',
  p_message_text => 'The end date must be greater than the start date.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.POPUP_LOV.ICON_TEXT',
  p_message_language => 'en',
  p_message_text => 'Popup List of Values: %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT',
  p_message_language => 'en',
  p_message_text => 'Pivot');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PIVOT_SORT',
  p_message_language => 'en',
  p_message_text => 'Pivot Sort');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CHART_LABEL_NOT_NULL',
  p_message_language => 'en',
  p_message_text => 'Chart label must be specified.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DUPLICATE_PIVOT_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Duplicate pivot column.  Pivot column list must be unique.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_INVALID_SETTINGS',
  p_message_language => 'en',
  p_message_text => '%0 invalid settings');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REMOVE_PIVOT',
  p_message_language => 'en',
  p_message_text => 'Remove Pivot');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_DEFAULT_REPORT',
  p_message_language => 'en',
  p_message_text => 'Delete Default Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_SUBJECT_REQUIRED',
  p_message_language => 'en',
  p_message_text => 'Email Subject must be specified.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_ROWS_PER_PAGE',
  p_message_language => 'en',
  p_message_text => 'Sets the number of records to display per page.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_ROWS',
  p_message_language => 'en',
  p_message_text => '<li><b>Rows</b> sets the number of records to display per page.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_SEARCH_BAR_ACTIONS_MENU',
  p_message_language => 'en',
  p_message_text => '<li><b>Actions Menu</b> enables you to customize a report. See the sections that follow.</li>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.GV.BREAK_COLLAPSE',
  p_message_language => 'en',
  p_message_text => 'Collapse control break');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.AND',
  p_message_language => 'en',
  p_message_text => 'and');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.APPROX_COUNT_DISTINCT_OVERALL',
  p_message_language => 'en',
  p_message_text => 'Overal Approx. Count Distinct');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.AXIS_VALUE_DECIMAL',
  p_message_language => 'en',
  p_message_text => 'Decimal Places');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.BUBBLE',
  p_message_language => 'en',
  p_message_text => 'Bubble');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.COMPUTE',
  p_message_language => 'en',
  p_message_text => 'Compute');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DOES_NOT_CONTAIN',
  p_message_language => 'en',
  p_message_text => 'does not contain');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DOES_NOT_START_WITH',
  p_message_language => 'en',
  p_message_text => 'does not start with');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DOWNLOAD',
  p_message_language => 'en',
  p_message_text => 'Download');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.DOWNLOAD_FORMAT',
  p_message_language => 'en',
  p_message_text => 'Choose Format');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.EMAIL_TO',
  p_message_language => 'en',
  p_message_text => 'Recipient (to)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.FORMAT',
  p_message_language => 'en',
  p_message_text => 'Format');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.GREATER_THAN',
  p_message_language => 'en',
  p_message_text => 'greater than');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_CUSTOMIZE.T_REGION_DISP',
  p_message_language => 'en',
  p_message_text => 'Region Display');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DIALOG.CANCEL',
  p_message_language => 'en',
  p_message_text => 'Cancel');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'WWV_FLOW_CREATE_MODEL_APP.CREATE_IG',
  p_message_language => 'en',
  p_message_text => 'Unable to create interactive grid page. %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'CHART_SERIES_ERROR',
  p_message_language => 'en',
  p_message_text => 'Chart series error %0 for %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.COLUMNS_TITLE',
  p_message_language => 'en',
  p_message_text => 'Columns Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.CONTROL_BREAK_TITLE',
  p_message_language => 'en',
  p_message_text => 'Control Break Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.REPORT_TITLE',
  p_message_language => 'en',
  p_message_text => 'Report Help
');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HIGHLIGHT',
  p_message_language => 'en',
  p_message_text => 'Highlight');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.IN_THE_NEXT',
  p_message_language => 'en',
  p_message_text => 'in the next');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.INVALID_SORT_BY',
  p_message_language => 'en',
  p_message_text => 'Sort By has been set to %0, but no column as been selected for %0.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.IS_NOT_NULL',
  p_message_language => 'en',
  p_message_text => 'is not empty');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAST.DAY',
  p_message_language => 'en',
  p_message_text => 'Last Day');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAST.WEEK',
  p_message_language => 'en',
  p_message_text => 'Last Week');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAST.X_DAYS',
  p_message_language => 'en',
  p_message_text => 'Last %0 Days');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAST.X_WEEKS',
  p_message_language => 'en',
  p_message_text => 'Last %0 Weeks');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.LAYOUT_ALIGN',
  p_message_language => 'en',
  p_message_text => 'Cell Alignment');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.MIN',
  p_message_language => 'en',
  p_message_text => 'Minimum');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.MINUTES',
  p_message_language => 'en',
  p_message_text => 'minutes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NEXT.MINUTE',
  p_message_language => 'en',
  p_message_text => 'Next Minute');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NEXT.X_WEEKS',
  p_message_language => 'en',
  p_message_text => 'Next %0 Weeks');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NOT_BETWEEN',
  p_message_language => 'en',
  p_message_text => 'not between');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.NOT_IN_THE_LAST',
  p_message_language => 'en',
  p_message_text => 'not in the last');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REPORT',
  p_message_language => 'en',
  p_message_text => 'Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REPORT.SAVED.PRIVATE',
  p_message_language => 'en',
  p_message_text => 'Private report saved');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.REPORT_EDIT',
  p_message_language => 'en',
  p_message_text => 'Report - Edit');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_DAYS',
  p_message_language => 'en',
  p_message_text => 'days');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_TIME_MINS',
  p_message_language => 'en',
  p_message_text => 'minutes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALID_COLOR',
  p_message_language => 'en',
  p_message_text => 'Please enter a valid color.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VALUE_AXIS_TITLE',
  p_message_language => 'en',
  p_message_text => 'Axis Title for Value');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_VCOLUMN',
  p_message_language => 'en',
  p_message_text => 'Vertical Column');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_WORKING_REPORT',
  p_message_language => 'en',
  p_message_text => 'Working Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_X_DAYS',
  p_message_language => 'en',
  p_message_text => '%0 days');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_YES',
  p_message_language => 'en',
  p_message_text => 'Yes');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'FLOW.VALIDATION_ERROR',
  p_message_language => 'en',
  p_message_text => '%0 errors have occurred');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'LAYOUT.T_CONDITION_EXPR2',
  p_message_language => 'en',
  p_message_text => 'Expression 2');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'PAGINATION.NEXT_SET',
  p_message_language => 'en',
  p_message_text => 'Next Set');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'REPORTING_PERIOD',
  p_message_language => 'en',
  p_message_text => 'Reporting Period');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_MINUTES_AGO',
  p_message_language => 'en',
  p_message_text => '%0 minutes ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_MONTHS_AGO',
  p_message_language => 'en',
  p_message_text => '%0 months ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_SECONDS_AGO',
  p_message_language => 'en',
  p_message_text => '%0 seconds ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_WEEKS_AGO',
  p_message_language => 'en',
  p_message_text => '%0 weeks ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'SINCE_YEARS_AGO',
  p_message_language => 'en',
  p_message_text => '%0 years ago');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HELP_RESET',
  p_message_language => 'en',
  p_message_text => 'Resets the report back to the default settings, removing any customizations that you have made.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATEPICKER_VALUE_NOT_BETWEEN_MIN_MAX',
  p_message_language => 'en',
  p_message_text => '#LABEL# is not between the valid range of %0 and %1.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ALL_COLUMNS',
  p_message_language => 'en',
  p_message_text => 'All Columns');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ASCENDING',
  p_message_language => 'en',
  p_message_text => 'Ascending');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_CATEGORY',
  p_message_language => 'en',
  p_message_text => 'Category');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_COMPARISON_IN',
  p_message_language => 'en',
  p_message_text => 'in');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DAILY',
  p_message_language => 'en',
  p_message_text => 'Daily');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_CHECKED',
  p_message_language => 'en',
  p_message_text => 'Delete Checked');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DELETE_REPORT',
  p_message_language => 'en',
  p_message_text => 'Delete Report');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DETAIL_VIEW',
  p_message_language => 'en',
  p_message_text => 'Single Row View');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_DIRECTION',
  p_message_language => 'en',
  p_message_text => 'Direction');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_BCC',
  p_message_language => 'en',
  p_message_text => 'Bcc');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_EMAIL_TO',
  p_message_language => 'en',
  p_message_text => 'To');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ERROR',
  p_message_language => 'en',
  p_message_text => 'Error');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FILTER_TYPE',
  p_message_language => 'en',
  p_message_text => 'Filter Type');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FORMAT_MASK',
  p_message_language => 'en',
  p_message_text => 'Format');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_FUNCTIONS',
  p_message_language => 'en',
  p_message_text => 'Functions');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_HIGHLIGHT',
  p_message_language => 'en',
  p_message_text => 'Highlight');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_LABEL_AXIS_TITLE',
  p_message_language => 'en',
  p_message_text => 'Axis Title for Label');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MONTH',
  p_message_language => 'en',
  p_message_text => 'Month');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_MORE_DATA',
  p_message_language => 'en',
  p_message_text => 'More Data');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NAME',
  p_message_language => 'en',
  p_message_text => 'Name');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NEW_AGGREGATION',
  p_message_language => 'en',
  p_message_text => 'New Aggregation');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_NO_AGGREGATION_DEFINED',
  p_message_language => 'en',
  p_message_text => 'No aggregation defined.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_OTHER',
  p_message_language => 'en',
  p_message_text => 'Other');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PERCENT_OF_TOTAL_SUM_X',
  p_message_language => 'en',
  p_message_text => 'Percent of Total Sum %0 (%)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_PREVIOUS',
  p_message_language => 'en',
  p_message_text => '&lt;');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_REPORTS',
  p_message_language => 'en',
  p_message_text => 'Reports');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_ROW',
  p_message_language => 'en',
  p_message_text => 'Row');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE',
  p_message_language => 'en',
  p_message_text => 'Save');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SAVE_DEFAULT_CONFIRM',
  p_message_language => 'en',
  p_message_text => 'The current report settings will be used as the default for all users.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_COLUMNS_FOOTER',
  p_message_language => 'en',
  p_message_text => 'Computed columns are prefixed with **.');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => '4150_COLUMN_NUMBER',
  p_message_language => 'en',
  p_message_text => 'Column %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DATA_HAS_CHANGED',
  p_message_language => 'en',
  p_message_text => 'Current version of data in database has changed since user initiated update process. current checksum = "%0" application checksum = "%1"');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEXIR_SELECT_FUNCTION',
  p_message_language => 'en',
  p_message_text => '- Select Function -');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.HELP.AGGREGATE',
  p_message_language => 'en',
  p_message_text => '<p>Use this dialog to aggregate columns. Aggregated values display at the bottom of the data, or if Control Breaks are defined, at the bottom of each break.</p>

<p><strong>Aggregation List</strong><br>
The Aggregation list displays defined aggregations. Disable an existing aggregation by deselecting it.<br>
Click Add ( &plus; ) to create a new aggregation, or Delete ( &minus; ) to remove an existing aggregation.</p>

<p><strong>Aggregation Settings</strong><br>
Use the form on the right to define the aggregation.<br>
Select the Column name and Aggregation type.<br>
Optionally, enter a tooltip for the aggregation.<br>
If you have defined a Control Break, selecting <strong>Show Overall Value</strong> displays the overall average, total, or similar value at the bottom of the data.</p>

<p><em>Note: Access the Aggregation dialog in the Actions menu or by clicking the column heading and sum ( &sum; ).</em></p>');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ROW',
  p_message_language => 'en',
  p_message_text => 'Row');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.ROW_ACTIONS_FOR',
  p_message_language => 'en',
  p_message_text => 'Actions for row %0');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_NEXT_HOUR',
  p_message_language => 'en',
  p_message_text => '%0 in the next hour');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_NEXT_WEEK',
  p_message_language => 'en',
  p_message_text => '%0 in the next week');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.IN_THE_NEXT_YEAR',
  p_message_language => 'en',
  p_message_text => '%0 in the next year');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.MATCHES_REGULAR_EXPRESSION.Y',
  p_message_language => 'en',
  p_message_text => '%0 matches regular expression %1');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_LAST.Y.DAYS',
  p_message_language => 'en',
  p_message_text => '%0 not in the last %1 days');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X.NOT_IN_THE_NEXT_MONTH',
  p_message_language => 'en',
  p_message_text => '%0 not in the next month');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.X_COLUMN',
  p_message_language => 'en',
  p_message_text => 'X');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.Z_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Z');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG_FORMAT_SAMPLE_4',
  p_message_language => 'en',
  p_message_text => 'in 16h');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.COMPLETED_STATE',
  p_message_language => 'en',
  p_message_text => '(Completed)');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.PROCESSING',
  p_message_language => 'en',
  p_message_text => 'Processing');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'LAYOUT.T_#EXPAND_COLLAPSE_NAV_LABEL#',
  p_message_language => 'en',
  p_message_text => 'Label for Expand / Collapse Navigation');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.DIALOG.HELP',
  p_message_language => 'en',
  p_message_text => 'Help');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.SINCE.SHORT.SECONDS_FROM_NOW',
  p_message_language => 'en',
  p_message_text => 'in %0s');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.TB.TOOLBAR',
  p_message_language => 'en',
  p_message_text => 'Toolbar');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SEARCH_FOR.X',
  p_message_language => 'en',
  p_message_text => 'Search for ''%0''');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SELECT_1_ROW_IN_MASTER',
  p_message_language => 'en',
  p_message_text => 'Select 1 row in the master region');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.SINGLE_ROW_VIEW',
  p_message_language => 'en',
  p_message_text => 'Single Row View');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.STOCK',
  p_message_language => 'en',
  p_message_text => 'Stock');

wwv_flow_api.create_message (
  p_id => null, -- to get unique ID from sequence
  p_flow_id => v_app_id,
  p_name => 'APEX.IG.VALUE_COLUMN',
  p_message_language => 'en',
  p_message_text => 'Value');



COMMIT; END;
/