prompt --application/set_environment
set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_190200 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2019.10.04'
,p_release=>'19.2.0.00.18'
,p_default_workspace_id=>1500476828438366
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'STPV'
);
end;
/
 
prompt APPLICATION 100 - Vereins- und Verbandsadministration (VVA)
--
-- Application Export:
--   Application:     100
--   Name:            Vereins- und Verbandsadministration (VVA)
--   Date and Time:   12:55 Friday November 13, 2020
--   Exported By:     MBL
--   Flashback:       0
--   Export Type:     Page Export
--   Manifest
--     PAGE: 17
--   Manifest End
--   Version:         19.2.0.00.18
--   Instance ID:     200175196925559
--

begin
null;
end;
/
prompt --application/pages/delete_00017
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>17);
end;
/
prompt --application/pages/page_00017
begin
wwv_flow_api.create_page(
 p_id=>17
,p_user_interface_id=>wwv_flow_api.id(2440735227545058)
,p_name=>'Auswertung Mitglieder'
,p_step_title=>'Auswertung Mitglieder'
,p_autocomplete_on_off=>'OFF'
,p_group_id=>wwv_flow_api.id(11431597980941074)
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'MBL'
,p_last_upd_yyyymmddhh24miss=>'20201113125438'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(11403567314926959)
,p_plug_name=>'Auswertung Mitglieder fr-ch'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(2357587072544996)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select PERS_ID,',
'       PERS_USER_ID,',
'       PERS_NAME, ',
'       PERS_VORNAME,',
'       PERS_GEBURTSDATUM,',
'       --floor(months_between(TRUNC(sysdate), PERS_GEBURTSDATUM)/12) Pers_Alter,',
'       Pers_Alter,',
'       PERS_PHOTO,',
'       PERS_ANREDE,',
'       PERS_GESCHLECHT,',
'       PERS_STRASSE,',
'       PERS_PLZ,',
'       PERS_ORT,',
'       PERS_LAND_ID,',
'       PERS_TELEFON_NR,',
'       PERS_EMAIL,',
'       --PERS_AHV_NR,',
'       --PERS_IBAN,',
'       PERS_BANKVERBINDUNG_IMPORT,',
'       PERS_STERBE_DATUM,',
'       PERS_STATUS,',
'       PERS_PASSWORD,',
'       PERS_KOMMENTAR,',
'       PERS_PERSONENSEQ,',
'       PERS_CREATION_USER,',
'       PERS_CREATION_DATE,',
'       PERS_MODIFICATION_USER,',
'       PERS_MODIFICATION_DATE,',
'       PEVE_ID,',
'       PEVE_PERS_ID,',
'       PEVE_VERE_ID,',
'       PEVE_VEREIN_EINTRITT,',
'       floor(months_between(TRUNC(sysdate), PEVE_VEREIN_EINTRITT)/12) Mitgliedsjahre,',
'       PEVE_VEREIN_AUSTRITT,',
'       PEVE_MITGLIEDERBEITRAG,',
'       PEVE_MITGLIEDERBEITRAG_IMPORT,',
'       PEVE_VEREIN_EHRENMITGLIED,',
'       PEVE_VEREIN_KEINE_EHRUNG_ERWUENSCHT,',
'       PEVE_VEREIN_VETERAN_SEIT,',
'       floor(months_between(TRUNC(sysdate), PEVE_VEREIN_VETERAN_SEIT)/12) Veteranenjahre,',
'       --PEVE_VEREIN_GOLDENE_EHRENNADEL,',
'       PEVE_CREATION_USER,',
'       PEVE_CREATION_DATE,',
'       PEVE_MODIFICATION_USER,',
'       PEVE_MODIFICATION_DATE,',
'       PERS_MILITAER_MUSIKER, ',
'       PERS_MILITAER_STATUS, ',
'       PERS_MILITAER_EINTEILUNG, ',
'       PERS_MILITAER_GRAD, ',
'       PERS_MILITAER_FUNKTION,',
'       VERE_ID,',
'       VERE_VERE_ID,',
'       VERE_VETY_ID,',
'       VERE_NAME,',
'       VERE_STRASSE,',
'       VERE_PLZ,',
'       VERE_ORT,',
'       VERE_LAND_ID,',
'       VERE_HOMEPAGE,',
'       VERE_EMAIL,',
'       VERE_SUISA,',
'       VERE_GRUENDUNGSJAHR,',
'       VERE_STATUS,',
'       VERE_BEITRITTSJAHR,',
'       VERE_AUSTRITTSJAHR,',
'       VERE_COMMENT,',
'       VERE_IBAN,',
'       VERE_VEREINID,',
'       VERE_CREATION_USER,',
'       VERE_CREATION_DATE,',
'       VERE_MODIFICATION_USER,',
'       VERE_MODIFICATION_DATE,',
'       MITGLIED_FUNKTION_IDS,',
'       MITGLIED_FUNKTION,',
'       VEREIN_FUNKTION_IDS,',
'       VEREIN_FUNKTION ',
'       , JUROR',
'       ,VEREIN_INSTRUMENT',
'from stpv_v_person_verein peve   ',
'    where vere_id in ',
'               (select vere_id ',
'                    from stpv_v_vereins_befugnis',
'                    where pers_id = :APP_PERS_ID',
'                )',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_ajax_items_to_submit=>'APP_USER'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_plug_display_when_condition=>'P17_LANGUAGE'
,p_plug_display_when_cond2=>'fr-ch'
,p_plug_caching=>'SESSION'
,p_plug_caching_max_age_in_sec=>21600
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(11403691342926959)
,p_name=>'Auswertung Mitglieder'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_save_rpt_public=>'Y'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_download_formats=>'CSV:HTML:EMAIL:XLS:PDF:RTF'
,p_csv_output_separator=>';'
,p_supplemental_text=>'SEP=;'
,p_owner=>'MBL'
,p_internal_uid=>11403691342926959
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11404028779926968)
,p_db_column_name=>'PERS_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Benutzer'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11404437669926972)
,p_db_column_name=>'PERS_USER_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'User ID'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11404813899926972)
,p_db_column_name=>'PERS_NAME'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Name'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11405223878926972)
,p_db_column_name=>'PERS_VORNAME'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Vorname'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11405679548926973)
,p_db_column_name=>'PERS_GEBURTSDATUM'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Geburtsdatum'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11406047178926973)
,p_db_column_name=>'PERS_ANREDE'
,p_display_order=>7
,p_column_identifier=>'F'
,p_column_label=>'Anrede'
,p_column_type=>'STRING'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'LEFT'
,p_rpt_named_lov=>wwv_flow_api.id(18618454684400478)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11406491035926973)
,p_db_column_name=>'PERS_GESCHLECHT'
,p_display_order=>8
,p_column_identifier=>'G'
,p_column_label=>'Geschlecht'
,p_column_type=>'STRING'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'LEFT'
,p_rpt_named_lov=>wwv_flow_api.id(10498836357258523)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11406878389926973)
,p_db_column_name=>'PERS_STRASSE'
,p_display_order=>9
,p_column_identifier=>'H'
,p_column_label=>'Strasse'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11407215107926973)
,p_db_column_name=>'PERS_PLZ'
,p_display_order=>10
,p_column_identifier=>'I'
,p_column_label=>'PLZ'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11407689570926974)
,p_db_column_name=>'PERS_ORT'
,p_display_order=>11
,p_column_identifier=>'J'
,p_column_label=>'Ort'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11408008037926974)
,p_db_column_name=>'PERS_LAND_ID'
,p_display_order=>12
,p_column_identifier=>'K'
,p_column_label=>'Land'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(8501317391437132)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11408495726926974)
,p_db_column_name=>'PERS_TELEFON_NR'
,p_display_order=>13
,p_column_identifier=>'L'
,p_column_label=>'Telefon Nr'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11408869362926974)
,p_db_column_name=>'PERS_EMAIL'
,p_display_order=>14
,p_column_identifier=>'M'
,p_column_label=>'E-Mail'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11410030463926975)
,p_db_column_name=>'PERS_BANKVERBINDUNG_IMPORT'
,p_display_order=>17
,p_column_identifier=>'P'
,p_column_label=>'Bankverbindung Import'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11410471600926975)
,p_db_column_name=>'PERS_STERBE_DATUM'
,p_display_order=>18
,p_column_identifier=>'Q'
,p_column_label=>'Sterbedatum'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11410833074926975)
,p_db_column_name=>'PERS_STATUS'
,p_display_order=>19
,p_column_identifier=>'R'
,p_column_label=>'Mitgliedstatus'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(10306642221552010)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11413295186926976)
,p_db_column_name=>'PERS_PASSWORD'
,p_display_order=>79
,p_column_identifier=>'X'
,p_column_label=>'change Password'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11413652810926977)
,p_db_column_name=>'PERS_KOMMENTAR'
,p_display_order=>89
,p_column_identifier=>'Y'
,p_column_label=>'Kommentar'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11414068482926977)
,p_db_column_name=>'PERS_PERSONENSEQ'
,p_display_order=>99
,p_column_identifier=>'Z'
,p_column_label=>'Personenseq'
,p_column_type=>'NUMBER'
,p_display_text_as=>'STRIP_HTML_ESCAPE_SC'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11414455115926977)
,p_db_column_name=>'PERS_CREATION_USER'
,p_display_order=>109
,p_column_identifier=>'AA'
,p_column_label=>'angelegt von'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11414822978926977)
,p_db_column_name=>'PERS_CREATION_DATE'
,p_display_order=>119
,p_column_identifier=>'AB'
,p_column_label=>'angelegt am'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'DD-MON-YYYY HH24:MI:SS'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11415209610926977)
,p_db_column_name=>'PERS_MODIFICATION_USER'
,p_display_order=>129
,p_column_identifier=>'AC'
,p_column_label=>unistr('zuletzt ge\00E4ndert von')
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11415636251926978)
,p_db_column_name=>'PERS_MODIFICATION_DATE'
,p_display_order=>139
,p_column_identifier=>'AD'
,p_column_label=>unistr('zuletzt ge\00E4ndert am')
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'DD-MON-YYYY HH24:MI:SS'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11416028952926978)
,p_db_column_name=>'PEVE_VEREIN_EINTRITT'
,p_display_order=>149
,p_column_identifier=>'AE'
,p_column_label=>'Eintritt in den Verein'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11416434891926978)
,p_db_column_name=>'PEVE_VEREIN_AUSTRITT'
,p_display_order=>159
,p_column_identifier=>'AF'
,p_column_label=>'Austritt aus dem Verein'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11416827888926978)
,p_db_column_name=>'PEVE_MITGLIEDERBEITRAG'
,p_display_order=>169
,p_column_identifier=>'AG'
,p_column_label=>'Mitgliederbeitrag'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11417271451926978)
,p_db_column_name=>'PEVE_MITGLIEDERBEITRAG_IMPORT'
,p_display_order=>179
,p_column_identifier=>'AH'
,p_column_label=>'Mitgliederbeitrag Import'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11417641375926979)
,p_db_column_name=>'PEVE_VEREIN_EHRENMITGLIED'
,p_display_order=>189
,p_column_identifier=>'AI'
,p_column_label=>'Ehrenmitglied'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(10306322714547407)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11418093530926979)
,p_db_column_name=>'PEVE_VEREIN_KEINE_EHRUNG_ERWUENSCHT'
,p_display_order=>199
,p_column_identifier=>'AJ'
,p_column_label=>'Ehrung'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_rpt_named_lov=>wwv_flow_api.id(11908128219661205)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11418439644926980)
,p_db_column_name=>'PEVE_VEREIN_VETERAN_SEIT'
,p_display_order=>209
,p_column_identifier=>'AK'
,p_column_label=>'Veteran Seit'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11419201166926980)
,p_db_column_name=>'PEVE_CREATION_USER'
,p_display_order=>229
,p_column_identifier=>'AM'
,p_column_label=>'angelegt von'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11419625328926980)
,p_db_column_name=>'PEVE_CREATION_DATE'
,p_display_order=>239
,p_column_identifier=>'AN'
,p_column_label=>'angelegt am'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy hh24:mi:ss'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11420045940926980)
,p_db_column_name=>'PEVE_MODIFICATION_USER'
,p_display_order=>249
,p_column_identifier=>'AO'
,p_column_label=>unistr('zuletzt ge\00E4ndert von')
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11420447256926981)
,p_db_column_name=>'PEVE_MODIFICATION_DATE'
,p_display_order=>259
,p_column_identifier=>'AP'
,p_column_label=>unistr('zuletzt ge\00E4ndert am')
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy hh24:mi:ss'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11420895170926981)
,p_db_column_name=>'VERE_ID'
,p_display_order=>269
,p_column_identifier=>'AQ'
,p_column_label=>'Verein'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_rpt_named_lov=>wwv_flow_api.id(2617845640687854)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11421200569926981)
,p_db_column_name=>'VERE_VERE_ID'
,p_display_order=>279
,p_column_identifier=>'AR'
,p_column_label=>'Verband'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(2617845640687854)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11421679005926981)
,p_db_column_name=>'VERE_VETY_ID'
,p_display_order=>289
,p_column_identifier=>'AS'
,p_column_label=>'Vereinstyp'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(10305275962496790)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11422097680926982)
,p_db_column_name=>'VERE_NAME'
,p_display_order=>299
,p_column_identifier=>'AT'
,p_column_label=>'Verein'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11422484968926982)
,p_db_column_name=>'VERE_STRASSE'
,p_display_order=>309
,p_column_identifier=>'AU'
,p_column_label=>'Strasse'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11422811912926982)
,p_db_column_name=>'VERE_PLZ'
,p_display_order=>319
,p_column_identifier=>'AV'
,p_column_label=>'PLZ'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11423264318926982)
,p_db_column_name=>'VERE_ORT'
,p_display_order=>329
,p_column_identifier=>'AW'
,p_column_label=>'Ort'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11423658278926982)
,p_db_column_name=>'VERE_LAND_ID'
,p_display_order=>339
,p_column_identifier=>'AX'
,p_column_label=>'Land'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(8501317391437132)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11424032210926983)
,p_db_column_name=>'VERE_HOMEPAGE'
,p_display_order=>349
,p_column_identifier=>'AY'
,p_column_label=>'Homepage'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11424481317926983)
,p_db_column_name=>'VERE_EMAIL'
,p_display_order=>359
,p_column_identifier=>'AZ'
,p_column_label=>'E-Mail'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11425237416926983)
,p_db_column_name=>'VERE_GRUENDUNGSJAHR'
,p_display_order=>379
,p_column_identifier=>'BB'
,p_column_label=>unistr('Gr\00FCndungsjahr')
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11425684046926984)
,p_db_column_name=>'VERE_STATUS'
,p_display_order=>389
,p_column_identifier=>'BC'
,p_column_label=>'Vereinstatus'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(10307420855561276)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11426045193926984)
,p_db_column_name=>'VERE_BEITRITTSJAHR'
,p_display_order=>399
,p_column_identifier=>'BD'
,p_column_label=>'Beitrittsjahr des Vereins in den Verband'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11426469660926984)
,p_db_column_name=>'VERE_AUSTRITTSJAHR'
,p_display_order=>409
,p_column_identifier=>'BE'
,p_column_label=>'Austrittsjahr des Vereins aus dem Verband'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11426874151926984)
,p_db_column_name=>'VERE_COMMENT'
,p_display_order=>419
,p_column_identifier=>'BF'
,p_column_label=>'Kommentar'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11427270607926984)
,p_db_column_name=>'VERE_IBAN'
,p_display_order=>429
,p_column_identifier=>'BG'
,p_column_label=>'IBAN'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11427651803926985)
,p_db_column_name=>'VERE_VEREINID'
,p_display_order=>439
,p_column_identifier=>'BH'
,p_column_label=>'Verband'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11428005093926985)
,p_db_column_name=>'VERE_CREATION_USER'
,p_display_order=>449
,p_column_identifier=>'BI'
,p_column_label=>'angelegt durch'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11428485126926985)
,p_db_column_name=>'VERE_CREATION_DATE'
,p_display_order=>459
,p_column_identifier=>'BJ'
,p_column_label=>'angelegt am'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy hh24:mi:ss'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11428862096926985)
,p_db_column_name=>'VERE_MODIFICATION_USER'
,p_display_order=>469
,p_column_identifier=>'BK'
,p_column_label=>unistr('zuletzt ge\00E4ndert durch')
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(11429249249926985)
,p_db_column_name=>'VERE_MODIFICATION_DATE'
,p_display_order=>479
,p_column_identifier=>'BL'
,p_column_label=>unistr('zuletzt ge\00E4ndert am')
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy hh24:mi:ss'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10980918263291605)
,p_db_column_name=>'PERS_PHOTO'
,p_display_order=>509
,p_column_identifier=>'BO'
,p_column_label=>'Pers Photo'
,p_column_type=>'OTHER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10981013738291606)
,p_db_column_name=>'PEVE_ID'
,p_display_order=>519
,p_column_identifier=>'BP'
,p_column_label=>'Peve Id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10981176521291607)
,p_db_column_name=>'PEVE_PERS_ID'
,p_display_order=>529
,p_column_identifier=>'BQ'
,p_column_label=>'Peve Pers Id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10981237536291608)
,p_db_column_name=>'PEVE_VERE_ID'
,p_display_order=>539
,p_column_identifier=>'BR'
,p_column_label=>'Peve Vere Id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(12071368868123521)
,p_db_column_name=>'MITGLIED_FUNKTION_IDS'
,p_display_order=>549
,p_column_identifier=>'BV'
,p_column_label=>'Mitglied Funktion Ids'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(12071437046123522)
,p_db_column_name=>'MITGLIED_FUNKTION'
,p_display_order=>559
,p_column_identifier=>'BW'
,p_column_label=>'Funktionen im Verein'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(12071510315123523)
,p_db_column_name=>'VEREIN_FUNKTION_IDS'
,p_display_order=>569
,p_column_identifier=>'BX'
,p_column_label=>'Verein Funktion Ids'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(12071675050123524)
,p_db_column_name=>'VEREIN_FUNKTION'
,p_display_order=>579
,p_column_identifier=>'BY'
,p_column_label=>'Rolle im Verein'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(24613824494506404)
,p_db_column_name=>'VERE_SUISA'
,p_display_order=>589
,p_column_identifier=>'BZ'
,p_column_label=>'Suisa'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(26706492899702102)
,p_db_column_name=>'PERS_MILITAER_MUSIKER'
,p_display_order=>599
,p_column_identifier=>'CA'
,p_column_label=>unistr('Milit\00E4r Musiker')
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_column_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(10306322714547407)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(26706512201702103)
,p_db_column_name=>'PERS_MILITAER_STATUS'
,p_display_order=>609
,p_column_identifier=>'CB'
,p_column_label=>unistr('Milit\00E4r Status')
,p_column_type=>'STRING'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_rpt_named_lov=>wwv_flow_api.id(11675580962118848)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(26706671688702104)
,p_db_column_name=>'PERS_MILITAER_EINTEILUNG'
,p_display_order=>619
,p_column_identifier=>'CC'
,p_column_label=>unistr('Milit\00E4r Einteilung')
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(26706757983702105)
,p_db_column_name=>'PERS_MILITAER_GRAD'
,p_display_order=>629
,p_column_identifier=>'CD'
,p_column_label=>unistr('Milit\00E4r Grad')
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(26706833416702106)
,p_db_column_name=>'PERS_MILITAER_FUNKTION'
,p_display_order=>639
,p_column_identifier=>'CE'
,p_column_label=>unistr('Milit\00E4r Funktion')
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(26706932723702107)
,p_db_column_name=>'VETERANENJAHRE'
,p_display_order=>649
,p_column_identifier=>'CF'
,p_column_label=>'Veteranenjahre'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(26707065930702108)
,p_db_column_name=>'PERS_ALTER'
,p_display_order=>659
,p_column_identifier=>'CG'
,p_column_label=>'Alter'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(26707186942702109)
,p_db_column_name=>'MITGLIEDSJAHRE'
,p_display_order=>669
,p_column_identifier=>'CH'
,p_column_label=>'Mitgliedsjahre'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(26707645023702114)
,p_db_column_name=>'JUROR'
,p_display_order=>679
,p_column_identifier=>'CI'
,p_column_label=>'Juror'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(26707716140702115)
,p_db_column_name=>'VEREIN_INSTRUMENT'
,p_display_order=>689
,p_column_identifier=>'CJ'
,p_column_label=>'Instrument'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(11430511748927420)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'114306'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_ANREDE:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GESCHLECHT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_STATUS:PERS_KOMMENTAR:PERS_MILITAER_MUSIKER:PERS_MILITAER_STATUS:PERS_MILITAER_EINTEILUNG:PERS_MILI'
||'TAER_GRAD:PERS_MILITAER_FUNKTION:PEVE_MITGLIEDERBEITRAG:PEVE_VEREIN_EHRENMITGLIED:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL:VERE_STATUS:VERE_COMMENT:MITGLIEDSJAHRE:PEVE_VEREIN_EINTRITT:PE'
||'VE_VEREIN_AUSTRITT:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:VERE_GRUENDUNGSJAHR:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VEREIN_FUNKTION:MITGLIED_FUNKTION:JUROR:VEREIN_INSTRUMENT'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39752709821807470)
,p_report_id=>wwv_flow_api.id(11430511748927420)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'membre actif'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''membre actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(11456502639169909)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('Pr\00E9sidents')
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'114566'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_VERE_ID:VERE_NAME:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GEBURTSDATUM:PERS_ANREDE:PERS_GESCHLECHT:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_BANKVERBINDUNG_IMPORT:PERS_STERBE_DATUM:PERS_STATUS:PERS_KOMMENTAR:PE'
||'VE_VEREIN_EINTRITT:PEVE_VEREIN_AUSTRITT:PEVE_MITGLIEDERBEITRAG:PEVE_VEREIN_EHRENMITGLIED:PEVE_VEREIN_VETERAN_SEIT:VERE_VETY_ID:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL:VERE_GRUENDUNGSJAHR:VERE_STATUS:VERE_BEITRITTSJAHR:VER'
||'E_AUSTRITTSJAHR:VERE_COMMENT:VERE_IBAN:MITGLIED_FUNKTION:VEREIN_FUNKTION'
,p_sort_column_1=>'VERE_ID'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_NAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_VORNAME'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39835190203381197)
,p_report_id=>wwv_flow_api.id(11456502639169909)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'membre actif'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''membre actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39835511482381197)
,p_report_id=>wwv_flow_api.id(11456502639169909)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VEREIN_FUNKTION'
,p_operator=>'contains'
,p_expr=>unistr('Pr\00E9sident')
,p_condition_sql=>'upper("VEREIN_FUNKTION") like ''%''||upper(#APXWS_EXPR#)||''%'''
,p_condition_display=>unistr('#APXWS_COL_NAME# #APXWS_OP_NAME# ''Pr\00E9sident''  ')
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39835931785381197)
,p_report_id=>wwv_flow_api.id(11456502639169909)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VERE_STATUS'
,p_operator=>'='
,p_expr=>'actif'
,p_condition_sql=>'"VERE_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(11459828899223533)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('Nombres de membres par soci\00E9t\00E9')
,p_report_seq=>10
,p_report_type=>'GROUP_BY'
,p_report_alias=>'114599'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GEBURTSDATUM:PERS_ANREDE:PERS_GESCHLECHT:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_BANKVERBINDUNG_IMPORT:PERS_STERBE_DATUM:PERS_STATUS:PERS_KOMMENTAR:PERS_PERSONENSEQ:PEVE_VER'
||'EIN_EINTRITT:PEVE_VEREIN_AUSTRITT:PEVE_MITGLIEDERBEITRAG:PEVE_MITGLIEDERBEITRAG_IMPORT:PEVE_VEREIN_EHRENMITGLIED:PEVE_VEREIN_VETERAN_SEIT:VERE_ID:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL'
||':VERE_GRUENDUNGSJAHR:VERE_STATUS:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VERE_COMMENT:VERE_IBAN:VERE_VEREINID'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39849973344420032)
,p_report_id=>wwv_flow_api.id(11459828899223533)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'membre actif'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''membre actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39850358978420032)
,p_report_id=>wwv_flow_api.id(11459828899223533)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VERE_STATUS'
,p_operator=>'='
,p_expr=>'actif'
,p_condition_sql=>'"VERE_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_group_by(
 p_id=>wwv_flow_api.id(39850759286420033)
,p_report_id=>wwv_flow_api.id(11459828899223533)
,p_group_by_columns=>'VERE_NAME'
,p_function_01=>'COUNT'
,p_function_column_01=>'VERE_NAME'
,p_function_db_column_name_01=>'APXWS_GBFC_01'
,p_function_label_01=>'Anzahl Mitglieder'
,p_function_format_mask_01=>'999G999G999G999G990'
,p_function_sum_01=>'N'
,p_sort_column_01=>'VERE_ID'
,p_sort_direction_01=>'ASC'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(11461588551245918)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('Nombre hommes/femmes par soci\00E9t\00E9')
,p_report_seq=>10
,p_report_type=>'GROUP_BY'
,p_report_alias=>'114616'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>100000
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GEBURTSDATUM:PERS_ANREDE:PERS_GESCHLECHT:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_BANKVERBINDUNG_IMPORT:PERS_STERBE_DATUM:PERS_STATUS:PERS_KOMMENTAR:PERS_PERSONENSEQ:PEVE_VER'
||'EIN_EINTRITT:PEVE_VEREIN_AUSTRITT:PEVE_MITGLIEDERBEITRAG:PEVE_MITGLIEDERBEITRAG_IMPORT:PEVE_VEREIN_EHRENMITGLIED:PEVE_VEREIN_VETERAN_SEIT:VERE_ID:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL'
||':VERE_GRUENDUNGSJAHR:VERE_STATUS:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VERE_COMMENT:VERE_IBAN:VERE_VEREINID'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
end;
/
begin
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39848570811418044)
,p_report_id=>wwv_flow_api.id(11461588551245918)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'membre actif'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''membre actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_group_by(
 p_id=>wwv_flow_api.id(39848968817418045)
,p_report_id=>wwv_flow_api.id(11461588551245918)
,p_group_by_columns=>'VERE_NAME:PERS_GESCHLECHT'
,p_function_01=>'COUNT'
,p_function_column_01=>'PERS_GESCHLECHT'
,p_function_db_column_name_01=>'APXWS_GBFC_01'
,p_function_label_01=>'Anzahl'
,p_function_format_mask_01=>'999G999G999G999G990'
,p_function_sum_01=>'N'
,p_sort_column_01=>'VERE_ID'
,p_sort_direction_01=>'ASC'
,p_sort_column_02=>'PERS_GESCHLECHT'
,p_sort_direction_02=>'ASC'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(15349691907582620)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('Comit\00E9s')
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'153497'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_VERE_ID:VERE_NAME:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GEBURTSDATUM:PERS_ANREDE:PERS_GESCHLECHT:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_BANKVERBINDUNG_IMPORT:PERS_STERBE_DATUM:PERS_STATUS:PERS_KOMMENTAR:PE'
||'VE_VEREIN_EINTRITT:PEVE_VEREIN_AUSTRITT:PEVE_MITGLIEDERBEITRAG:PEVE_VEREIN_EHRENMITGLIED:PEVE_VEREIN_VETERAN_SEIT:VERE_VETY_ID:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL:VERE_GRUENDUNGSJAHR:VERE_STATUS:VERE_BEITRITTSJAHR:VER'
||'E_AUSTRITTSJAHR:VERE_COMMENT:VERE_IBAN:MITGLIED_FUNKTION:VEREIN_FUNKTION'
,p_sort_column_1=>'VERE_ID'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_NAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_VORNAME'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39837463108395072)
,p_report_id=>wwv_flow_api.id(15349691907582620)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'membre actif'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''membre actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39837864811395073)
,p_report_id=>wwv_flow_api.id(15349691907582620)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VEREIN_FUNKTION'
,p_operator=>'contains'
,p_expr=>unistr('Comit\00E9')
,p_condition_sql=>'upper("VEREIN_FUNKTION") like ''%''||upper(#APXWS_EXPR#)||''%'''
,p_condition_display=>unistr('#APXWS_COL_NAME# #APXWS_OP_NAME# ''Comit\00E9''  ')
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39838256062395073)
,p_report_id=>wwv_flow_api.id(15349691907582620)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VERE_STATUS'
,p_operator=>'='
,p_expr=>'actif'
,p_condition_sql=>'"VERE_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(27765369713107352)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Liste d''adresses'
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'277654'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GESCHLECHT:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39840389483398874)
,p_report_id=>wwv_flow_api.id(27765369713107352)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'membre actif'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''membre actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(27767686521141414)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Militaire'
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'277677'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_ANREDE:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GESCHLECHT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_STATUS:PERS_KOMMENTAR:PERS_MILITAER_MUSIKER:PERS_MILITAER_STATUS:PERS_MILITAER_EINTEILUNG:PERS_MILI'
||'TAER_GRAD:PERS_MILITAER_FUNKTION:PEVE_MITGLIEDERBEITRAG:PEVE_VEREIN_EHRENMITGLIED:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL:VERE_STATUS:VERE_COMMENT:MITGLIEDSJAHRE:PEVE_VEREIN_EINTRITT:PE'
||'VE_VEREIN_AUSTRITT:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:VERE_GRUENDUNGSJAHR:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VEREIN_FUNKTION:MITGLIED_FUNKTION'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39847670051416067)
,p_report_id=>wwv_flow_api.id(27767686521141414)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_MILITAER_STATUS'
,p_operator=>'='
,p_expr=>'militaire'
,p_condition_sql=>'"PERS_MILITAER_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''militaire''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(27770824979150308)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('Membres par soci\00E9t\00E9')
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'277709'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GEBURTSDATUM:PERS_ANREDE:PERS_GESCHLECHT:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_BANKVERBINDUNG_IMPORT:PERS_STERBE_DATUM:PERS_STATUS:PERS_KOMMENTAR:PERS_PERSONENSEQ:PEVE_VER'
||'EIN_EINTRITT:PEVE_VEREIN_AUSTRITT:PEVE_MITGLIEDERBEITRAG:PEVE_MITGLIEDERBEITRAG_IMPORT:PEVE_VEREIN_EHRENMITGLIED:PEVE_VEREIN_VETERAN_SEIT:VERE_ID:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL'
||':VERE_GRUENDUNGSJAHR:VERE_STATUS:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VERE_COMMENT:VERE_IBAN:VERE_VEREINID'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
,p_break_on=>'VERE_NAME:0:0:0:0:0'
,p_break_enabled_on=>'VERE_NAME:0:0:0:0:0'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39846443230413126)
,p_report_id=>wwv_flow_api.id(27770824979150308)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'membre actif'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''membre actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39846829916413127)
,p_report_id=>wwv_flow_api.id(27770824979150308)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VERE_STATUS'
,p_operator=>'='
,p_expr=>'actif'
,p_condition_sql=>'"VERE_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(27773320304202846)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Liste des responsables'
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'277734'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VEREIN_FUNKTION:MITGLIED_FUNKTION:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'VERE_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_NAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_VORNAME'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39845147835410447)
,p_report_id=>wwv_flow_api.id(27773320304202846)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'membre actif'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''membre actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39845503424410447)
,p_report_id=>wwv_flow_api.id(27773320304202846)
,p_name=>'Row Filter'
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_expr_type=>'ROW'
,p_expr=>'BW IS not null or  BY is not null'
,p_condition_sql=>'"MITGLIED_FUNKTION" IS not null or  "VEREIN_FUNKTION" is not null'
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(27775486191214865)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Liste des moniteurs'
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'277755'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_ANREDE:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GESCHLECHT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_STATUS:PERS_KOMMENTAR:PERS_MILITAER_MUSIKER:PERS_MILITAER_STATUS:PERS_MILITAER_EINTEILUNG:PERS_MILI'
||'TAER_GRAD:PERS_MILITAER_FUNKTION:PEVE_MITGLIEDERBEITRAG:PEVE_VEREIN_EHRENMITGLIED:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL:VERE_STATUS:VERE_COMMENT:MITGLIEDSJAHRE:PEVE_VEREIN_EINTRITT:PE'
||'VE_VEREIN_AUSTRITT:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:VERE_GRUENDUNGSJAHR:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VEREIN_FUNKTION:MITGLIED_FUNKTION'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39843879856408043)
,p_report_id=>wwv_flow_api.id(27775486191214865)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'MITGLIED_FUNKTION'
,p_operator=>'contains'
,p_expr=>'Moniteur'
,p_condition_sql=>'upper("MITGLIED_FUNKTION") like ''%''||upper(#APXWS_EXPR#)||''%'''
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# ''Moniteur''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39844269047408043)
,p_report_id=>wwv_flow_api.id(27775486191214865)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'membre actif'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''membre actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(27782932260257093)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Liste des caissiers'
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'277830'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VEREIN_FUNKTION:MITGLIED_FUNKTION:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'VERE_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_NAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_VORNAME'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39841225771403436)
,p_report_id=>wwv_flow_api.id(27782932260257093)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'membre actif'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''membre actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39841655737403436)
,p_report_id=>wwv_flow_api.id(27782932260257093)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VEREIN_FUNKTION'
,p_operator=>'='
,p_expr=>'Caissier'
,p_condition_sql=>'"VEREIN_FUNKTION" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Caissier''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(27784868932266007)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Liste des jeunes musiciens'
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'277849'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:PERS_ALTER:VEREIN_FUNKTION:MITGLIED_FUNKTION:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'VERE_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_NAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_VORNAME'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39842573942405278)
,p_report_id=>wwv_flow_api.id(27784868932266007)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_ALTER'
,p_operator=>'<='
,p_expr=>'16'
,p_condition_sql=>'"PERS_ALTER" <= to_number(#APXWS_EXPR#)'
,p_condition_display=>'#APXWS_COL_NAME# <= #APXWS_EXPR_NUMBER#  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39842987011405279)
,p_report_id=>wwv_flow_api.id(27784868932266007)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'membre actif'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''membre actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(27786937190293069)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('V\00E9teran 20 ans')
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'277870'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39830317564369322)
,p_report_id=>wwv_flow_api.id(27786937190293069)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VETERANENJAHRE'
,p_operator=>'between'
,p_expr=>'20'
,p_expr2=>'29'
,p_condition_sql=>'"VETERANENJAHRE" between to_number(#APXWS_EXPR#) and to_number(#APXWS_EXPR2#)'
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# #APXWS_EXPR_NUMBER# #APXWS_AND# #APXWS_EXPR2_NUMBER#'
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(27788457077297061)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('V\00E9teran 30 ans')
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'277885'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39831215287370706)
,p_report_id=>wwv_flow_api.id(27788457077297061)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VETERANENJAHRE'
,p_operator=>'between'
,p_expr=>'30'
,p_expr2=>'39'
,p_condition_sql=>'"VETERANENJAHRE" between to_number(#APXWS_EXPR#) and to_number(#APXWS_EXPR2#)'
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# #APXWS_EXPR_NUMBER# #APXWS_AND# #APXWS_EXPR2_NUMBER#'
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(27790692497310321)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('V\00E9teran 50 ans')
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'277907'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39833028114373205)
,p_report_id=>wwv_flow_api.id(27790692497310321)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VETERANENJAHRE'
,p_operator=>'between'
,p_expr=>'50'
,p_expr2=>'59'
,p_condition_sql=>'"VETERANENJAHRE" between to_number(#APXWS_EXPR#) and to_number(#APXWS_EXPR2#)'
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# #APXWS_EXPR_NUMBER# #APXWS_AND# #APXWS_EXPR2_NUMBER#'
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(27792119696312874)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('V\00E9teran 60 ans et plus')
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'277922'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39833990281374430)
,p_report_id=>wwv_flow_api.id(27792119696312874)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VETERANENJAHRE'
,p_operator=>'>='
,p_expr=>'60'
,p_condition_sql=>'"VETERANENJAHRE" >= to_number(#APXWS_EXPR#)'
,p_condition_display=>'#APXWS_COL_NAME# >= #APXWS_EXPR_NUMBER#  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(27798341186341973)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('V\00E9teran 40 ans')
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'277984'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39832154737371957)
,p_report_id=>wwv_flow_api.id(27798341186341973)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VETERANENJAHRE'
,p_operator=>'between'
,p_expr=>'40'
,p_expr2=>'49'
,p_condition_sql=>'"VETERANENJAHRE" between to_number(#APXWS_EXPR#) and to_number(#APXWS_EXPR2#)'
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# #APXWS_EXPR_NUMBER# #APXWS_AND# #APXWS_EXPR2_NUMBER#'
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(27920825490759320)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('Jur\00E9s')
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'279209'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_ANREDE:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GESCHLECHT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_STATUS:PERS_KOMMENTAR:PERS_MILITAER_MUSIKER:PERS_MILITAER_STATUS:PERS_MILITAER_EINTEILUNG:PERS_MILI'
||'TAER_GRAD:PERS_MILITAER_FUNKTION:PEVE_MITGLIEDERBEITRAG:PEVE_VEREIN_EHRENMITGLIED:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL:VERE_STATUS:VERE_COMMENT:MITGLIEDSJAHRE:PEVE_VEREIN_EINTRITT:PE'
||'VE_VEREIN_AUSTRITT:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:VERE_GRUENDUNGSJAHR:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VEREIN_FUNKTION:MITGLIED_FUNKTION:JUROR:VEREIN_INSTRUMENT'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39839152749397085)
,p_report_id=>wwv_flow_api.id(27920825490759320)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'JUROR'
,p_operator=>'is not null'
,p_condition_sql=>'"JUROR" is not null'
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME#'
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39839559479397085)
,p_report_id=>wwv_flow_api.id(27920825490759320)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'membre actif'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''membre actif''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(39339715845819011)
,p_plug_name=>'Auswertung Mitglieder'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(2357587072544996)
,p_plug_display_sequence=>30
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select PERS_ID,',
'       PERS_USER_ID,',
'       PERS_NAME, ',
'       PERS_VORNAME,',
'       PERS_GEBURTSDATUM,',
'       --floor(months_between(TRUNC(sysdate), PERS_GEBURTSDATUM)/12) Pers_Alter,',
'       Pers_Alter,',
'       PERS_PHOTO,',
'       PERS_ANREDE,',
'       PERS_GESCHLECHT,',
'       PERS_STRASSE,',
'       PERS_PLZ,',
'       PERS_ORT,',
'       PERS_LAND_ID,',
'       PERS_TELEFON_NR,',
'       PERS_EMAIL,',
'       --PERS_AHV_NR,',
'       --PERS_IBAN,',
'       PERS_BANKVERBINDUNG_IMPORT,',
'       PERS_STERBE_DATUM,',
'       PERS_STATUS,',
'       PERS_PASSWORD,',
'       PERS_KOMMENTAR,',
'       PERS_PERSONENSEQ,',
'       PERS_CREATION_USER,',
'       PERS_CREATION_DATE,',
'       PERS_MODIFICATION_USER,',
'       PERS_MODIFICATION_DATE,',
'       PEVE_ID,',
'       PEVE_PERS_ID,',
'       PEVE_VERE_ID,',
'       PEVE_VEREIN_EINTRITT,',
'       floor(months_between(TRUNC(sysdate), PEVE_VEREIN_EINTRITT)/12) Mitgliedsjahre,',
'       PEVE_VEREIN_AUSTRITT,',
'       PEVE_MITGLIEDERBEITRAG,',
'       PEVE_MITGLIEDERBEITRAG_IMPORT,',
'       PEVE_VEREIN_EHRENMITGLIED,',
'       PEVE_VEREIN_KEINE_EHRUNG_ERWUENSCHT,',
'       PEVE_VEREIN_VETERAN_SEIT,',
'       floor(months_between(TRUNC(sysdate), PEVE_VEREIN_VETERAN_SEIT)/12) Veteranenjahre,',
'       --PEVE_VEREIN_GOLDENE_EHRENNADEL,',
'       PEVE_CREATION_USER,',
'       PEVE_CREATION_DATE,',
'       PEVE_MODIFICATION_USER,',
'       PEVE_MODIFICATION_DATE,',
'       PERS_MILITAER_MUSIKER, ',
'       PERS_MILITAER_STATUS, ',
'       PERS_MILITAER_EINTEILUNG, ',
'       PERS_MILITAER_GRAD, ',
'       PERS_MILITAER_FUNKTION,',
'       VERE_ID,',
'       VERE_VERE_ID,',
'       VERE_VETY_ID,',
'       VERE_NAME,',
'       VERE_STRASSE,',
'       VERE_PLZ,',
'       VERE_ORT,',
'       VERE_LAND_ID,',
'       VERE_HOMEPAGE,',
'       VERE_EMAIL,',
'       VERE_SUISA,',
'       VERE_GRUENDUNGSJAHR,',
'       VERE_STATUS,',
'       VERE_BEITRITTSJAHR,',
'       VERE_AUSTRITTSJAHR,',
'       VERE_COMMENT,',
'       VERE_IBAN,',
'       VERE_VEREINID,',
'       VERE_CREATION_USER,',
'       VERE_CREATION_DATE,',
'       VERE_MODIFICATION_USER,',
'       VERE_MODIFICATION_DATE,',
'       MITGLIED_FUNKTION_IDS,',
'       MITGLIED_FUNKTION,',
'       VEREIN_FUNKTION_IDS,',
'       VEREIN_FUNKTION ',
'       , JUROR',
'       ,VEREIN_INSTRUMENT',
'from stpv_v_person_verein peve   ',
'    where vere_id in ',
'               (select vere_id ',
'                    from stpv_v_vereins_befugnis',
'                    where pers_id = :APP_PERS_ID',
'                )',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_ajax_items_to_submit=>'APP_USER'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_plug_display_when_condition=>'P17_LANGUAGE'
,p_plug_display_when_cond2=>'de-ch'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_plug_caching=>'SESSION'
,p_plug_caching_max_age_in_sec=>21600
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(39339922921819013)
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_save_rpt_public=>'Y'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_download_formats=>'CSV:HTML:EMAIL:XLS:PDF:RTF'
,p_csv_output_separator=>';'
,p_supplemental_text=>'SEP=;'
,p_owner=>'MBL'
,p_internal_uid=>39339922921819013
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39340025821819014)
,p_db_column_name=>'PERS_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Benutzer'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39340104209819015)
,p_db_column_name=>'PERS_CREATION_USER'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'angelegt von'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39340262662819016)
,p_db_column_name=>'PERS_CREATION_DATE'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'angelegt am'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'DD-MON-YYYY HH24:MI:SS'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39340340813819017)
,p_db_column_name=>'PERS_MODIFICATION_USER'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>unistr('zuletzt ge\00E4ndert von')
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39340432789819018)
,p_db_column_name=>'PERS_MODIFICATION_DATE'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>unistr('zuletzt ge\00E4ndert am')
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'DD-MON-YYYY HH24:MI:SS'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39340512826819019)
,p_db_column_name=>'PEVE_VEREIN_EINTRITT'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Eintritt in den Verein'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39340666800819020)
,p_db_column_name=>'PEVE_VEREIN_AUSTRITT'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'Austritt aus dem Verein'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39340715885819021)
,p_db_column_name=>'PEVE_MITGLIEDERBEITRAG'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Mitgliederbeitrag'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39340836136819022)
,p_db_column_name=>'PEVE_MITGLIEDERBEITRAG_IMPORT'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'Mitgliederbeitrag Import'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39340960902819023)
,p_db_column_name=>'PEVE_VEREIN_EHRENMITGLIED'
,p_display_order=>100
,p_column_identifier=>'J'
,p_column_label=>'Ehrenmitglied'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(10306322714547407)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39341038769819024)
,p_db_column_name=>'PEVE_VEREIN_KEINE_EHRUNG_ERWUENSCHT'
,p_display_order=>110
,p_column_identifier=>'K'
,p_column_label=>'Ehrung'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_rpt_named_lov=>wwv_flow_api.id(11908128219661205)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39341151740819025)
,p_db_column_name=>'PEVE_VEREIN_VETERAN_SEIT'
,p_display_order=>120
,p_column_identifier=>'L'
,p_column_label=>'Veteran Seit'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy'
,p_tz_dependent=>'N'
);
end;
/
begin
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39341266794819026)
,p_db_column_name=>'PEVE_CREATION_USER'
,p_display_order=>130
,p_column_identifier=>'M'
,p_column_label=>'angelegt von'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39341334461819027)
,p_db_column_name=>'PEVE_CREATION_DATE'
,p_display_order=>140
,p_column_identifier=>'N'
,p_column_label=>'angelegt am'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy hh24:mi:ss'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39341459582819028)
,p_db_column_name=>'PEVE_MODIFICATION_USER'
,p_display_order=>150
,p_column_identifier=>'O'
,p_column_label=>unistr('zuletzt ge\00E4ndert von')
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39341532761819029)
,p_db_column_name=>'PEVE_MODIFICATION_DATE'
,p_display_order=>160
,p_column_identifier=>'P'
,p_column_label=>unistr('zuletzt ge\00E4ndert am')
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy hh24:mi:ss'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39341691094819030)
,p_db_column_name=>'VERE_ID'
,p_display_order=>170
,p_column_identifier=>'Q'
,p_column_label=>'Verein'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_rpt_named_lov=>wwv_flow_api.id(2617845640687854)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39341717171819031)
,p_db_column_name=>'VERE_VERE_ID'
,p_display_order=>180
,p_column_identifier=>'R'
,p_column_label=>'Verband'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(2617845640687854)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39341805081819032)
,p_db_column_name=>'VERE_VETY_ID'
,p_display_order=>190
,p_column_identifier=>'S'
,p_column_label=>'Vereinstyp'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(10305275962496790)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39341925087819033)
,p_db_column_name=>'VERE_NAME'
,p_display_order=>200
,p_column_identifier=>'T'
,p_column_label=>'Verein'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39342073441819034)
,p_db_column_name=>'VERE_STRASSE'
,p_display_order=>210
,p_column_identifier=>'U'
,p_column_label=>'Strasse'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39342191559819035)
,p_db_column_name=>'VERE_PLZ'
,p_display_order=>220
,p_column_identifier=>'V'
,p_column_label=>'PLZ'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39342216423819036)
,p_db_column_name=>'VERE_ORT'
,p_display_order=>230
,p_column_identifier=>'W'
,p_column_label=>'Ort'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39342367728819037)
,p_db_column_name=>'VERE_LAND_ID'
,p_display_order=>240
,p_column_identifier=>'X'
,p_column_label=>'Land'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(8501317391437132)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39342453165819038)
,p_db_column_name=>'VERE_HOMEPAGE'
,p_display_order=>250
,p_column_identifier=>'Y'
,p_column_label=>'Homepage'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39342593341819039)
,p_db_column_name=>'VERE_EMAIL'
,p_display_order=>260
,p_column_identifier=>'Z'
,p_column_label=>'E-Mail'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39342694446819040)
,p_db_column_name=>'PERS_USER_ID'
,p_display_order=>270
,p_column_identifier=>'AA'
,p_column_label=>'User ID'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39342794070819041)
,p_db_column_name=>'VERE_GRUENDUNGSJAHR'
,p_display_order=>280
,p_column_identifier=>'AB'
,p_column_label=>unistr('Gr\00FCndungsjahr')
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39342820569819042)
,p_db_column_name=>'VERE_STATUS'
,p_display_order=>290
,p_column_identifier=>'AC'
,p_column_label=>'Vereinstatus'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(10307420855561276)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39342904632819043)
,p_db_column_name=>'VERE_BEITRITTSJAHR'
,p_display_order=>300
,p_column_identifier=>'AD'
,p_column_label=>'Beitrittsjahr des Vereins in den Verband'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39343000256819044)
,p_db_column_name=>'VERE_AUSTRITTSJAHR'
,p_display_order=>310
,p_column_identifier=>'AE'
,p_column_label=>'Austrittsjahr des Vereins aus dem Verband'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39343166476819045)
,p_db_column_name=>'VERE_COMMENT'
,p_display_order=>320
,p_column_identifier=>'AF'
,p_column_label=>'Kommentar'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39343231109819046)
,p_db_column_name=>'VERE_IBAN'
,p_display_order=>330
,p_column_identifier=>'AG'
,p_column_label=>'IBAN'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39343332019819047)
,p_db_column_name=>'VERE_VEREINID'
,p_display_order=>340
,p_column_identifier=>'AH'
,p_column_label=>'Verband'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39343479120819048)
,p_db_column_name=>'VERE_CREATION_USER'
,p_display_order=>350
,p_column_identifier=>'AI'
,p_column_label=>'angelegt durch'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39343522585819049)
,p_db_column_name=>'VERE_CREATION_DATE'
,p_display_order=>360
,p_column_identifier=>'AJ'
,p_column_label=>'angelegt am'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy hh24:mi:ss'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39343657834819050)
,p_db_column_name=>'VERE_MODIFICATION_USER'
,p_display_order=>370
,p_column_identifier=>'AK'
,p_column_label=>unistr('zuletzt ge\00E4ndert durch')
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39524252616198101)
,p_db_column_name=>'VERE_MODIFICATION_DATE'
,p_display_order=>380
,p_column_identifier=>'AL'
,p_column_label=>unistr('zuletzt ge\00E4ndert am')
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy hh24:mi:ss'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39524397173198102)
,p_db_column_name=>'PERS_PHOTO'
,p_display_order=>390
,p_column_identifier=>'AM'
,p_column_label=>'Pers Photo'
,p_column_type=>'OTHER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39524469469198103)
,p_db_column_name=>'PEVE_ID'
,p_display_order=>400
,p_column_identifier=>'AN'
,p_column_label=>'Peve Id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39524503283198104)
,p_db_column_name=>'PEVE_PERS_ID'
,p_display_order=>410
,p_column_identifier=>'AO'
,p_column_label=>'Peve Pers Id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39524664938198105)
,p_db_column_name=>'PEVE_VERE_ID'
,p_display_order=>420
,p_column_identifier=>'AP'
,p_column_label=>'Peve Vere Id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39524779186198106)
,p_db_column_name=>'MITGLIED_FUNKTION_IDS'
,p_display_order=>430
,p_column_identifier=>'AQ'
,p_column_label=>'Mitglied Funktion Ids'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39524858735198107)
,p_db_column_name=>'MITGLIED_FUNKTION'
,p_display_order=>440
,p_column_identifier=>'AR'
,p_column_label=>'Funktionen im Verein'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39524997641198108)
,p_db_column_name=>'VEREIN_FUNKTION_IDS'
,p_display_order=>450
,p_column_identifier=>'AS'
,p_column_label=>'Verein Funktion Ids'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39525069711198109)
,p_db_column_name=>'VEREIN_FUNKTION'
,p_display_order=>460
,p_column_identifier=>'AT'
,p_column_label=>'Rolle im Verein'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39525127642198110)
,p_db_column_name=>'VERE_SUISA'
,p_display_order=>470
,p_column_identifier=>'AU'
,p_column_label=>'Suisa'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39525288096198111)
,p_db_column_name=>'PERS_NAME'
,p_display_order=>480
,p_column_identifier=>'AV'
,p_column_label=>'Name'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39525395340198112)
,p_db_column_name=>'PERS_MILITAER_MUSIKER'
,p_display_order=>490
,p_column_identifier=>'AW'
,p_column_label=>unistr('Milit\00E4r Musiker')
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_column_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(10306322714547407)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39525456791198113)
,p_db_column_name=>'PERS_MILITAER_STATUS'
,p_display_order=>500
,p_column_identifier=>'AX'
,p_column_label=>unistr('Milit\00E4r Status')
,p_column_type=>'STRING'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_rpt_named_lov=>wwv_flow_api.id(11675580962118848)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39525567229198114)
,p_db_column_name=>'PERS_MILITAER_EINTEILUNG'
,p_display_order=>510
,p_column_identifier=>'AY'
,p_column_label=>unistr('Milit\00E4r Einteilung')
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39525671605198115)
,p_db_column_name=>'PERS_MILITAER_GRAD'
,p_display_order=>520
,p_column_identifier=>'AZ'
,p_column_label=>unistr('Milit\00E4r Grad')
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39525768797198116)
,p_db_column_name=>'PERS_MILITAER_FUNKTION'
,p_display_order=>530
,p_column_identifier=>'BA'
,p_column_label=>unistr('Milit\00E4r Funktion')
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39525831453198117)
,p_db_column_name=>'VETERANENJAHRE'
,p_display_order=>540
,p_column_identifier=>'BB'
,p_column_label=>'Veteranenjahre'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39525952265198118)
,p_db_column_name=>'PERS_ALTER'
,p_display_order=>550
,p_column_identifier=>'BC'
,p_column_label=>'Alter'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39526062908198119)
,p_db_column_name=>'MITGLIEDSJAHRE'
,p_display_order=>560
,p_column_identifier=>'BD'
,p_column_label=>'Mitgliedsjahre'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39526152831198120)
,p_db_column_name=>'JUROR'
,p_display_order=>570
,p_column_identifier=>'BE'
,p_column_label=>'Juror'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39526248557198121)
,p_db_column_name=>'VEREIN_INSTRUMENT'
,p_display_order=>580
,p_column_identifier=>'BF'
,p_column_label=>'Instrument'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39526334391198122)
,p_db_column_name=>'PERS_VORNAME'
,p_display_order=>590
,p_column_identifier=>'BG'
,p_column_label=>'Vorname'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39526432639198123)
,p_db_column_name=>'PERS_GEBURTSDATUM'
,p_display_order=>600
,p_column_identifier=>'BH'
,p_column_label=>'Geburtsdatum'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39526566861198124)
,p_db_column_name=>'PERS_ANREDE'
,p_display_order=>610
,p_column_identifier=>'BI'
,p_column_label=>'Anrede'
,p_column_type=>'STRING'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'LEFT'
,p_rpt_named_lov=>wwv_flow_api.id(18618454684400478)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39526680013198125)
,p_db_column_name=>'PERS_GESCHLECHT'
,p_display_order=>620
,p_column_identifier=>'BJ'
,p_column_label=>'Geschlecht'
,p_column_type=>'STRING'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'LEFT'
,p_rpt_named_lov=>wwv_flow_api.id(10498836357258523)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39526726210198126)
,p_db_column_name=>'PERS_STRASSE'
,p_display_order=>630
,p_column_identifier=>'BK'
,p_column_label=>'Strasse'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39526896553198127)
,p_db_column_name=>'PERS_PLZ'
,p_display_order=>640
,p_column_identifier=>'BL'
,p_column_label=>'PLZ'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39526976683198128)
,p_db_column_name=>'PERS_ORT'
,p_display_order=>650
,p_column_identifier=>'BM'
,p_column_label=>'Ort'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39527048971198129)
,p_db_column_name=>'PERS_LAND_ID'
,p_display_order=>660
,p_column_identifier=>'BN'
,p_column_label=>'Land'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(8501317391437132)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39527149310198130)
,p_db_column_name=>'PERS_TELEFON_NR'
,p_display_order=>670
,p_column_identifier=>'BO'
,p_column_label=>'Telefon Nr'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39527277309198131)
,p_db_column_name=>'PERS_EMAIL'
,p_display_order=>680
,p_column_identifier=>'BP'
,p_column_label=>'E-Mail'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39527328769198132)
,p_db_column_name=>'PERS_BANKVERBINDUNG_IMPORT'
,p_display_order=>690
,p_column_identifier=>'BQ'
,p_column_label=>'Bankverbindung Import'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39527468953198133)
,p_db_column_name=>'PERS_STERBE_DATUM'
,p_display_order=>700
,p_column_identifier=>'BR'
,p_column_label=>'Sterbedatum'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_format_mask=>'dd.mm.yyyy'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39527593031198134)
,p_db_column_name=>'PERS_STATUS'
,p_display_order=>710
,p_column_identifier=>'BS'
,p_column_label=>'Mitgliedstatus'
,p_column_type=>'NUMBER'
,p_display_text_as=>'LOV_ESCAPE_SC'
,p_heading_alignment=>'RIGHT'
,p_rpt_named_lov=>wwv_flow_api.id(10306642221552010)
,p_rpt_show_filter_lov=>'1'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39527688546198135)
,p_db_column_name=>'PERS_PASSWORD'
,p_display_order=>720
,p_column_identifier=>'BT'
,p_column_label=>'change Password'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39527747631198136)
,p_db_column_name=>'PERS_KOMMENTAR'
,p_display_order=>730
,p_column_identifier=>'BU'
,p_column_label=>'Kommentar'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39527809037198137)
,p_db_column_name=>'PERS_PERSONENSEQ'
,p_display_order=>740
,p_column_identifier=>'BV'
,p_column_label=>'Personenseq'
,p_column_type=>'NUMBER'
,p_display_text_as=>'STRIP_HTML_ESCAPE_SC'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39552120710200165)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'395522'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_ANREDE:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GESCHLECHT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_STATUS:PERS_KOMMENTAR:PERS_MILITAER_MUSIKER:PERS_MILITAER_STATUS:PERS_MILITAER_EINTEILUNG:PERS_MILI'
||'TAER_GRAD:PERS_MILITAER_FUNKTION:PEVE_MITGLIEDERBEITRAG:PEVE_VEREIN_EHRENMITGLIED:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL:VERE_STATUS:VERE_COMMENT:MITGLIEDSJAHRE:PEVE_VEREIN_EINTRITT:PE'
||'VE_VEREIN_AUSTRITT:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:VERE_GRUENDUNGSJAHR:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VEREIN_FUNKTION:MITGLIED_FUNKTION:JUROR:VEREIN_INSTRUMENT'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(27920017183745964)
,p_report_id=>wwv_flow_api.id(39552120710200165)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'Aktivmitglied'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aktivmitglied''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39555386159412196)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Adressliste'
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'395554'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GESCHLECHT:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(27766939168124116)
,p_report_id=>wwv_flow_api.id(39555386159412196)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'Aktivmitglied'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aktivmitglied''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39555891873417972)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('Anzahl M\00E4nner/Frauen pro Verein')
,p_report_seq=>10
,p_report_type=>'GROUP_BY'
,p_report_alias=>'395559'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>100000
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GEBURTSDATUM:PERS_ANREDE:PERS_GESCHLECHT:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_BANKVERBINDUNG_IMPORT:PERS_STERBE_DATUM:PERS_STATUS:PERS_KOMMENTAR:PERS_PERSONENSEQ:PEVE_VER'
||'EIN_EINTRITT:PEVE_VEREIN_AUSTRITT:PEVE_MITGLIEDERBEITRAG:PEVE_MITGLIEDERBEITRAG_IMPORT:PEVE_VEREIN_EHRENMITGLIED:PEVE_VEREIN_VETERAN_SEIT:VERE_ID:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL'
||':VERE_GRUENDUNGSJAHR:VERE_STATUS:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VERE_COMMENT:VERE_IBAN:VERE_VEREINID'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(27260504131854225)
,p_report_id=>wwv_flow_api.id(39555891873417972)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'Aktivmitglied'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aktivmitglied''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_group_by(
 p_id=>wwv_flow_api.id(27260992227854225)
,p_report_id=>wwv_flow_api.id(39555891873417972)
,p_group_by_columns=>'VERE_NAME:PERS_GESCHLECHT'
,p_function_01=>'COUNT'
,p_function_column_01=>'PERS_GESCHLECHT'
,p_function_db_column_name_01=>'APXWS_GBFC_01'
,p_function_label_01=>'Anzahl'
,p_function_format_mask_01=>'999G999G999G999G990'
,p_function_sum_01=>'N'
,p_sort_column_01=>'VERE_ID'
,p_sort_direction_01=>'ASC'
,p_sort_column_02=>'PERS_GESCHLECHT'
,p_sort_direction_02=>'ASC'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39611572382857283)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Anzahl Mitglieder pro Verein'
,p_report_seq=>10
,p_report_type=>'GROUP_BY'
,p_report_alias=>'396116'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GEBURTSDATUM:PERS_ANREDE:PERS_GESCHLECHT:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_BANKVERBINDUNG_IMPORT:PERS_STERBE_DATUM:PERS_STATUS:PERS_KOMMENTAR:PERS_PERSONENSEQ:PEVE_VER'
||'EIN_EINTRITT:PEVE_VEREIN_AUSTRITT:PEVE_MITGLIEDERBEITRAG:PEVE_MITGLIEDERBEITRAG_IMPORT:PEVE_VEREIN_EHRENMITGLIED:PEVE_VEREIN_VETERAN_SEIT:VERE_ID:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL'
||':VERE_GRUENDUNGSJAHR:VERE_STATUS:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VERE_COMMENT:VERE_IBAN:VERE_VEREINID'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(27258829415847815)
,p_report_id=>wwv_flow_api.id(39611572382857283)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'Aktivmitglied'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aktivmitglied''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(27259259324847816)
,p_report_id=>wwv_flow_api.id(39611572382857283)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VERE_STATUS'
,p_operator=>'='
,p_expr=>'aktiv'
,p_condition_sql=>'"VERE_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''aktiv''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_group_by(
 p_id=>wwv_flow_api.id(27259637112847816)
,p_report_id=>wwv_flow_api.id(39611572382857283)
,p_group_by_columns=>'VERE_NAME'
,p_function_01=>'COUNT'
,p_function_column_01=>'VERE_NAME'
,p_function_db_column_name_01=>'APXWS_GBFC_01'
,p_function_label_01=>'Anzahl Mitglieder'
,p_function_format_mask_01=>'999G999G999G999G990'
,p_function_sum_01=>'N'
,p_sort_column_01=>'VERE_ID'
,p_sort_direction_01=>'ASC'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39612834392861283)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Juroren'
,p_report_seq=>10
,p_report_alias=>'396129'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_ANREDE:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GESCHLECHT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_STATUS:PERS_KOMMENTAR:PERS_MILITAER_MUSIKER:PERS_MILITAER_STATUS:PERS_MILITAER_EINTEILUNG:PERS_MILI'
||'TAER_GRAD:PERS_MILITAER_FUNKTION:PEVE_MITGLIEDERBEITRAG:PEVE_VEREIN_EHRENMITGLIED:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL:VERE_STATUS:VERE_COMMENT:MITGLIEDSJAHRE:PEVE_VEREIN_EINTRITT:PE'
||'VE_VEREIN_AUSTRITT:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:VERE_GRUENDUNGSJAHR:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VEREIN_FUNKTION:MITGLIED_FUNKTION:JUROR:VEREIN_INSTRUMENT'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(30404912413455181)
,p_report_id=>wwv_flow_api.id(39612834392861283)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'JUROR'
,p_operator=>'is not null'
,p_condition_sql=>'"JUROR" is not null'
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME#'
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(30405351558455182)
,p_report_id=>wwv_flow_api.id(39612834392861283)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'Aktivmitglied'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aktivmitglied''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39614500675869273)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('Liste der Funktionstr\00E4ger')
,p_report_seq=>10
,p_report_alias=>'396146'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VEREIN_FUNKTION:MITGLIED_FUNKTION:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'VERE_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_NAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_VORNAME'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(27773799877202849)
,p_report_id=>wwv_flow_api.id(39614500675869273)
,p_name=>'Row Filter'
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_expr_type=>'ROW'
,p_expr=>'BW IS not null or  BY is not null'
,p_condition_sql=>'"MITGLIED_FUNKTION" IS not null or  "VEREIN_FUNKTION" is not null'
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(27774179561202850)
,p_report_id=>wwv_flow_api.id(39614500675869273)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'Aktivmitglied'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aktivmitglied''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39615810033871502)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Liste der Jungmusiker'
,p_report_seq=>10
,p_report_alias=>'396159'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_ANREDE:PERS_NAME:PERS_VORNAME:PERS_GESCHLECHT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_STATUS:PERS_KOMMENTAR:PERS_MILITAER_MUSIKER:PERS_MILITAER_STATUS:PERS_MILITAER_EINTEILUNG:PERS_MILITAER_GRAD:PER'
||'S_MILITAER_FUNKTION:PEVE_MITGLIEDERBEITRAG:PEVE_VEREIN_EHRENMITGLIED:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL:VERE_STATUS:VERE_COMMENT:MITGLIEDSJAHRE:PEVE_VEREIN_EINTRITT:PEVE_VEREIN_AUS'
||'TRITT:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:VERE_GRUENDUNGSJAHR:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VEREIN_FUNKTION:MITGLIED_FUNKTION:JUROR:VEREIN_INSTRUMENT:'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39852714147433169)
,p_report_id=>wwv_flow_api.id(39615810033871502)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_ALTER'
,p_operator=>'<='
,p_expr=>'16'
,p_condition_sql=>'"PERS_ALTER" <= to_number(#APXWS_EXPR#)'
,p_condition_display=>'#APXWS_COL_NAME# <= #APXWS_EXPR_NUMBER#  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39853145861433169)
,p_report_id=>wwv_flow_api.id(39615810033871502)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'Aktivmitglied'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aktivmitglied''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39617134791893095)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Liste der Kassier'
,p_report_seq=>10
,p_report_alias=>'396172'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VEREIN_FUNKTION:MITGLIED_FUNKTION:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'VERE_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_NAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_VORNAME'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39854060932435632)
,p_report_id=>wwv_flow_api.id(39617134791893095)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'Aktivmitglied'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aktivmitglied''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39854411467435632)
,p_report_id=>wwv_flow_api.id(39617134791893095)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VEREIN_FUNKTION'
,p_operator=>'contains'
,p_expr=>'Kassier'
,p_condition_sql=>'upper("VEREIN_FUNKTION") like ''%''||upper(#APXWS_EXPR#)||''%'''
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# ''Kassier''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39619469090898877)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('Milit\00E4r')
,p_report_seq=>10
,p_report_alias=>'396195'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_ANREDE:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GESCHLECHT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_STATUS:PERS_KOMMENTAR:PERS_MILITAER_MUSIKER:PERS_MILITAER_STATUS:PERS_MILITAER_EINTEILUNG:PERS_MILI'
||'TAER_GRAD:PERS_MILITAER_FUNKTION:PEVE_MITGLIEDERBEITRAG:PEVE_VEREIN_EHRENMITGLIED:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL:VERE_STATUS:VERE_COMMENT:MITGLIEDSJAHRE:PEVE_VEREIN_EINTRITT:PE'
||'VE_VEREIN_AUSTRITT:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:VERE_GRUENDUNGSJAHR:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VEREIN_FUNKTION:MITGLIED_FUNKTION'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
end;
/
begin
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39821288332352167)
,p_report_id=>wwv_flow_api.id(39619469090898877)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_MILITAER_STATUS'
,p_operator=>'='
,p_expr=>'Military'
,p_condition_sql=>'"PERS_MILITAER_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Military''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39821636909352169)
,p_report_id=>wwv_flow_api.id(39619469090898877)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'Aktivmitglied'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aktivmitglied''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39620781717904155)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Mitglieder nach Verein'
,p_report_seq=>10
,p_report_alias=>'396208'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GEBURTSDATUM:PERS_ANREDE:PERS_GESCHLECHT:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_BANKVERBINDUNG_IMPORT:PERS_STERBE_DATUM:PERS_STATUS:PERS_KOMMENTAR:PERS_PERSONENSEQ:PEVE_VER'
||'EIN_EINTRITT:PEVE_VEREIN_AUSTRITT:PEVE_MITGLIEDERBEITRAG:PEVE_MITGLIEDERBEITRAG_IMPORT:PEVE_VEREIN_EHRENMITGLIED:PEVE_VEREIN_VETERAN_SEIT:VERE_ID:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL'
||':VERE_GRUENDUNGSJAHR:VERE_STATUS:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VERE_COMMENT:VERE_IBAN:VERE_VEREINID'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
,p_break_on=>'VERE_NAME:0:0:0:0:0'
,p_break_enabled_on=>'VERE_NAME:0:0:0:0:0'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(27771618125150312)
,p_report_id=>wwv_flow_api.id(39620781717904155)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'Aktivmitglied'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aktivmitglied''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39621168924904155)
,p_report_id=>wwv_flow_api.id(39620781717904155)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VERE_STATUS'
,p_operator=>'='
,p_expr=>'aktiv'
,p_condition_sql=>'"VERE_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''aktiv''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39622129697906809)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('Pr\00E4sidenten')
,p_report_seq=>10
,p_report_alias=>'396222'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_VERE_ID:VERE_NAME:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GEBURTSDATUM:PERS_ANREDE:PERS_GESCHLECHT:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_BANKVERBINDUNG_IMPORT:PERS_STERBE_DATUM:PERS_STATUS:PERS_KOMMENTAR:PE'
||'VE_VEREIN_EINTRITT:PEVE_VEREIN_AUSTRITT:PEVE_MITGLIEDERBEITRAG:PEVE_VEREIN_EHRENMITGLIED:PEVE_VEREIN_VETERAN_SEIT:VERE_VETY_ID:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL:VERE_GRUENDUNGSJAHR:VERE_STATUS:VERE_BEITRITTSJAHR:VER'
||'E_AUSTRITTSJAHR:VERE_COMMENT:VERE_IBAN:MITGLIED_FUNKTION:VEREIN_FUNKTION'
,p_sort_column_1=>'VERE_ID'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_NAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_VORNAME'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39822959905356032)
,p_report_id=>wwv_flow_api.id(39622129697906809)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'Aktivmitglied'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aktivmitglied''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39823348741356032)
,p_report_id=>wwv_flow_api.id(39622129697906809)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VEREIN_FUNKTION'
,p_operator=>'contains'
,p_expr=>unistr('Pr\00E4sident')
,p_condition_sql=>'upper("VEREIN_FUNKTION") like ''%''||upper(#APXWS_EXPR#)||''%'''
,p_condition_display=>unistr('#APXWS_COL_NAME# #APXWS_OP_NAME# ''Pr\00E4sident''  ')
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39823729314356032)
,p_report_id=>wwv_flow_api.id(39622129697906809)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VERE_STATUS'
,p_operator=>'='
,p_expr=>'aktiv'
,p_condition_sql=>'"VERE_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''aktiv''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39623176965916304)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Liste der Leiter'
,p_report_seq=>10
,p_report_alias=>'396232'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'PERS_ANREDE:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GESCHLECHT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_STATUS:PERS_KOMMENTAR:PERS_MILITAER_MUSIKER:PERS_MILITAER_STATUS:PERS_MILITAER_EINTEILUNG:PERS_MILI'
||'TAER_GRAD:PERS_MILITAER_FUNKTION:PEVE_MITGLIEDERBEITRAG:PEVE_VEREIN_EHRENMITGLIED:VERE_VERE_ID:VERE_VETY_ID:VERE_NAME:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL:VERE_STATUS:VERE_COMMENT:MITGLIEDSJAHRE:PEVE_VEREIN_EINTRITT:PE'
||'VE_VEREIN_AUSTRITT:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:VERE_GRUENDUNGSJAHR:VERE_BEITRITTSJAHR:VERE_AUSTRITTSJAHR:VEREIN_FUNKTION:MITGLIED_FUNKTION'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(27777690466222100)
,p_report_id=>wwv_flow_api.id(39623176965916304)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'MITGLIED_FUNKTION'
,p_operator=>'contains'
,p_expr=>'Leiter'
,p_condition_sql=>'upper("MITGLIED_FUNKTION") like ''%''||upper(#APXWS_EXPR#)||''%'''
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# ''Leiter''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(277772412062220100)
,p_report_id=>wwv_flow_api.id(39623176965916304)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'Aktivmitglied'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aktivmitglied''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39624618882920148)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('Veteran 60 Jahre und dar\00FCber')
,p_report_seq=>10
,p_report_alias=>'396247'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39827048681361909)
,p_report_id=>wwv_flow_api.id(39624618882920148)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VETERANENJAHRE'
,p_operator=>'>='
,p_expr=>'60'
,p_condition_sql=>'"VETERANENJAHRE" >= to_number(#APXWS_EXPR#)'
,p_condition_display=>'#APXWS_COL_NAME# >= #APXWS_EXPR_NUMBER#  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39626128478922765)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Veteran 20 Jahre'
,p_report_seq=>10
,p_report_alias=>'396262'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39824687306358256)
,p_report_id=>wwv_flow_api.id(39626128478922765)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VETERANENJAHRE'
,p_operator=>'between'
,p_expr=>'20'
,p_expr2=>'29'
,p_condition_sql=>'"VETERANENJAHRE" between to_number(#APXWS_EXPR#) and to_number(#APXWS_EXPR2#)'
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# #APXWS_EXPR_NUMBER# #APXWS_AND# #APXWS_EXPR2_NUMBER#'
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39627652799927145)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Veteran 30 Jahre'
,p_report_seq=>10
,p_report_alias=>'396277'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39825503127359537)
,p_report_id=>wwv_flow_api.id(39627652799927145)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VETERANENJAHRE'
,p_operator=>'between'
,p_expr=>'30'
,p_expr2=>'39'
,p_condition_sql=>'"VETERANENJAHRE" between to_number(#APXWS_EXPR#) and to_number(#APXWS_EXPR2#)'
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# #APXWS_EXPR_NUMBER# #APXWS_AND# #APXWS_EXPR2_NUMBER#'
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39629177742929904)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Veteran 40 Jahre'
,p_report_seq=>10
,p_report_alias=>'396292'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39826439769360721)
,p_report_id=>wwv_flow_api.id(39629177742929904)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VETERANENJAHRE'
,p_operator=>'between'
,p_expr=>'40'
,p_expr2=>'49'
,p_condition_sql=>'"VETERANENJAHRE" between to_number(#APXWS_EXPR#) and to_number(#APXWS_EXPR2#)'
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# #APXWS_EXPR_NUMBER# #APXWS_AND# #APXWS_EXPR2_NUMBER#'
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39630653666932721)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Veteran 50 Jahre'
,p_report_seq=>10
,p_report_alias=>'396307'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_NAME:PERS_ANREDE:PERS_NAME:PERS_VORNAME:VETERANENJAHRE:PEVE_VEREIN_VETERAN_SEIT:PERS_ALTER:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL'
,p_sort_column_1=>'PERS_NAME'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_VORNAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_GEBURTSDATUM'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39827970436363598)
,p_report_id=>wwv_flow_api.id(39630653666932721)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VETERANENJAHRE'
,p_operator=>'between'
,p_expr=>'50'
,p_expr2=>'59'
,p_condition_sql=>'"VETERANENJAHRE" between to_number(#APXWS_EXPR#) and to_number(#APXWS_EXPR2#)'
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# #APXWS_EXPR_NUMBER# #APXWS_AND# #APXWS_EXPR2_NUMBER#'
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(39632496739940021)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>unistr('Vorst\00E4nde')
,p_report_seq=>10
,p_report_alias=>'396325'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_view_mode=>'REPORT'
,p_report_columns=>'VERE_VERE_ID:VERE_NAME:PERS_NAME:PERS_VORNAME:PERS_USER_ID:PERS_GEBURTSDATUM:PERS_ANREDE:PERS_GESCHLECHT:PERS_STRASSE:PERS_PLZ:PERS_ORT:PERS_LAND_ID:PERS_TELEFON_NR:PERS_EMAIL:PERS_BANKVERBINDUNG_IMPORT:PERS_STERBE_DATUM:PERS_STATUS:PERS_KOMMENTAR:PE'
||'VE_VEREIN_EINTRITT:PEVE_VEREIN_AUSTRITT:PEVE_MITGLIEDERBEITRAG:PEVE_VEREIN_EHRENMITGLIED:PEVE_VEREIN_VETERAN_SEIT:VERE_VETY_ID:VERE_STRASSE:VERE_PLZ:VERE_ORT:VERE_LAND_ID:VERE_HOMEPAGE:VERE_EMAIL:VERE_GRUENDUNGSJAHR:VERE_STATUS:VERE_BEITRITTSJAHR:VER'
||'E_AUSTRITTSJAHR:VERE_COMMENT:VERE_IBAN:MITGLIED_FUNKTION:VEREIN_FUNKTION'
,p_sort_column_1=>'VERE_ID'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'PERS_NAME'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'PERS_VORNAME'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39828973597366262)
,p_report_id=>wwv_flow_api.id(39632496739940021)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PERS_STATUS'
,p_operator=>'='
,p_expr=>'Aktivmitglied'
,p_condition_sql=>'"PERS_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aktivmitglied''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39829315258366262)
,p_report_id=>wwv_flow_api.id(39632496739940021)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VEREIN_FUNKTION'
,p_operator=>'contains'
,p_expr=>'Vorstand'
,p_condition_sql=>'upper("VEREIN_FUNKTION") like ''%''||upper(#APXWS_EXPR#)||''%'''
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# ''Vorstand''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(39829786366366263)
,p_report_id=>wwv_flow_api.id(39632496739940021)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'VERE_STATUS'
,p_operator=>'='
,p_expr=>'aktiv'
,p_condition_sql=>'"VERE_STATUS" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''aktiv''  '
,p_enabled=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(39528595144198144)
,p_plug_name=>'New'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(2330118715544985)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(26005886868610333)
,p_name=>'P17_APP_USER'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(11403567314926959)
,p_use_cache_before_default=>'NO'
,p_source=>'lower(:APP_USER)'
,p_source_type=>'FUNCTION'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(39339851393819012)
,p_name=>'P17_APP_USER_1'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(39339715845819011)
,p_use_cache_before_default=>'NO'
,p_source=>'lower(:APP_USER)'
,p_source_type=>'FUNCTION'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(39528637694198145)
,p_name=>'P17_LANGUAGE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(39528595144198144)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_computation(
 p_id=>wwv_flow_api.id(39528762685198146)
,p_computation_sequence=>10
,p_computation_item=>'P17_LANGUAGE'
,p_computation_point=>'BEFORE_HEADER'
,p_computation_type=>'FUNCTION_BODY'
,p_computation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :MY_LANGUAGE is  null then',
'    return APEX_UTIL.gET_SESSION_LANG;',
'else',
'     if :MY_LANGUAGE = ''deutsch'' then',
'         return ''de-ch'';',
'     else',
'         return ''fr-ch'';',
'     end if;',
'end if;'))
);
end;
/
prompt --application/end_environment
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
