exec dbms_stats.gather_schema_stats ( 
     ownname          => 'STPV', 
     options          => 'GATHER', 
     estimate_percent => dbms_stats.auto_sample_size, 
     method_opt       => 'for all columns size auto', 
     cascade          => true, 
     degree           => 5 
)
/