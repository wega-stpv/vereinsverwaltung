create or replace view
stpv_v_peve_instrument
as
select * from stpv_pers_vere join stpv_peve_inst on peve_id = pein_peve_id
join stpv_instrument on inst_id = pein_inst_id
/