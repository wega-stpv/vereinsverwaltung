create or replace view
stpv_v_person_verein
as
select pers.* , peve.*, vere.*,
       person_pkg.get_funktion_ids(pers_id, vere_id, i_funk_Groups=>'MITGLIED') mitglied_funktion_ids,
	   person_pkg.get_funktionnames(pers_id, vere_id, i_funk_Groups=>'MITGLIED') mitglied_funktion,
       person_pkg.get_funktion_ids(pers_id, vere_id, i_funk_Groups=>'VEREIN') verein_funktion_ids,
	   person_pkg.get_funktionnames(pers_id, vere_id, i_funk_Groups=>'VEREIN') verein_funktion,
	   person_pkg.get_funktionnames(pers_id, vere_id, i_funk_Groups=>'MITGLIED:VEREIN') mitglied_verein_funktion,
	   person_pkg.get_instrumentnames(pers_id, vere_id) verein_instrument,
	   person_pkg.get_funktionnames(pers_id, vere_id, i_funk_Groups=>'MITGLIED:VEREIN')  || '</BR>' ||
	   person_pkg.get_instrumentnames(pers_id, vere_id) verein_verein_funktion_instrument,
	   person_pkg.get_funktionnames(pers_id, vere_id, i_funk_Groups=>'JUROR_BK:JUROR_WETTSPIELE:JUROR_TK') juror
	   , floor(months_between(TRUNC(sysdate), PERS_GEBURTSDATUM)/12) Pers_Alter
	   from 
stpv_person pers join stpv_pers_vere peve on pers_id = peve_pers_id
join stpv_verein vere on peve_vere_id = vere_id
/