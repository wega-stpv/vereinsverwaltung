create or replace view
stpv_v_accessitem_funktion
as
select acit.* , acfu.*, funk.*
--,       person_pkg.get_funktion_ids(pers_id, vere_id, i_funk_Groups=>'VEREIN') access_item_funktion_ids
--,	   person_pkg.get_funktionnames(pers_id, vere_id, i_funk_Groups=>'VEREIN') access_item_funktion
	   from 
stpv_access_item acit 
   join stpv_acit_funk acfu on acit_id = acfu_acit_id
   join stpv_funktion funk on acfu_funk_id =funk_id
/

/*
select a.acit_item, max(a.acfu_access_code), pers_user_id, peve_vere_id from stpv_v_access a join stpv_v_peve_funk f on acfu_funk_id = a.funk_id
join stpv_person o on pers_id = peve_pers_id
where acit_item = 'PERS_NAME' and pers_user_id='mbl'
group by a.acit_item, pers_user_id, peve_vere_id ;
*/

/*
with ac as (select a.acit_item
--, max(a.acfu_access_code)
,acfu_access_code
, pers_user_id
, peve_vere_id 
--,vere_name
--,level
from stpv_v_access a join stpv_v_peve_funk f on acfu_funk_id = a.funk_id
join stpv_person o on pers_id = peve_pers_id)

select * from ac, 
(select vere_id
,vere_name
,level
from 
stpv_verein v 
   start with vere_id = (select distinct peve_vere_id from ac where pers_user_id = 'mbl')
   CONNECT BY PRIOR vere_id = vere_vere_id ) v
where ac.pers_user_id = 'mbl'
;
*/