create or replace view stpv_v_all_Data
as
select PERS_ID,
       PERS_USER_ID,
       PERS_NAME,
       PERS_VORNAME,
       PERS_GEBURTSDATUM,
       PERS_PHOTO,
       PERS_ANREDE,
       PERS_GESCHLECHT,
       PERS_STRASSE,
       PERS_PLZ,
       PERS_ORT,
       PERS_LAND_ID,
       PERS_TELEFON_NR,
       PERS_EMAIL,
       --PERS_AHV_NR,
       --PERS_IBAN,
       PERS_BANKVERBINDUNG_IMPORT,
       PERS_STERBE_DATUM,
       PERS_STATUS,
       /*PERS_MILITAER_MUSIKER,
       PERS_MILITAER_STATUS, 
       PERS_MILITAER_EINTEILUNG,
       PERS_MILITAER_GRAD,
       PERS_MILITAER_FUNKTION,*/
       decode(nvl(access_code_MILITAER_MUSIKER, 'NO'), 'NO', 'n/a', PERS_MILITAER_MUSIKER) PERS_MILITAER_MUSIKER, 
       decode(nvl(access_code_MILITAER_STATUS, 'NO'), 'NO', 'n/a', PERS_MILITAER_STATUS) PERS_MILITAER_STATUS, 
       decode(nvl(access_code_MILITAER_EINTEILUNG, 'NO'), 'NO', 'n/a', PERS_MILITAER_EINTEILUNG) PERS_MILITAER_EINTEILUNG, 
       decode(nvl(access_code_MILITAER_GRAD, 'NO'), 'NO', 'n/a', PERS_MILITAER_GRAD) PERS_MILITAER_GRAD, 
       decode(nvl(access_code_MILITAER_FUNKTION, 'NO'), 'NO', 'n/a', PERS_MILITAER_FUNKTION) PERS_MILITAER_FUNKTION,

       PERS_PASSWORD,
       PERS_KOMMENTAR,
       PERS_PERSONENSEQ,
       PERS_CREATION_USER,
       PERS_CREATION_DATE,
       PERS_MODIFICATION_USER,
       PERS_MODIFICATION_DATE,
       PEVE_ID,
       PEVE_PERS_ID,
       PEVE_VERE_ID,
       PEVE_VEREIN_EINTRITT,
       PEVE_VEREIN_AUSTRITT,
       PEVE_MITGLIEDERBEITRAG,
       PEVE_MITGLIEDERBEITRAG_IMPORT,
       PEVE_VEREIN_EHRENMITGLIED,
       PEVE_VEREIN_KEINE_EHRUNG_ERWUENSCHT,
       PEVE_VEREIN_VETERAN_SEIT,
       --PEVE_VEREIN_GOLDENE_EHRENNADEL,
       PEVE_CREATION_USER,
       PEVE_CREATION_DATE,
       PEVE_MODIFICATION_USER,
       PEVE_MODIFICATION_DATE,
       VERE_ID,
       VERE_VERE_ID,
       VERE_VETY_ID,
       VERE_NAME,
       VERE_STRASSE,
       VERE_PLZ,
       VERE_ORT,
       VERE_LAND_ID,
       VERE_HOMEPAGE,
       VERE_EMAIL,
       VERE_SUISA,
       VERE_GRUENDUNGSJAHR,
       VERE_STATUS,
       VERE_BEITRITTSJAHR,
       VERE_AUSTRITTSJAHR,
       VERE_COMMENT,
       VERE_IBAN,
       VERE_VEREINID,
       VERE_CREATION_USER,
       VERE_CREATION_DATE,
       VERE_MODIFICATION_USER,
       VERE_MODIFICATION_DATE,
       MITGLIED_FUNKTION_IDS,
       MITGLIED_FUNKTION,
       VEREIN_FUNKTION_IDS,
       VEREIN_FUNKTION
from (
      with ac as (
      select 
        access_pers.pers_user_id
       , access_pers.pers_id
       , peve_vere_id vere_id
       , acit_item
       , stpv_acit_funk.acfu_access_code access_code
      from stpv_v_vereins_befugnis 
          join stpv_acit_funk on stpv_acit_funk.acfu_funk_id = stpv_v_vereins_befugnis.funk_id
          join stpv_access_item on acit_id = acfu_acit_id
          join stpv_person access_pers on access_pers.pers_id = stpv_v_vereins_befugnis.pers_id 
          join stpv_pers_vere peve on peve_vere_id = stpv_v_vereins_befugnis.vere_id 
          where access_pers.pers_user_id = :P17_APP_USER
      ) 
     select 
     (select max( access_code) 
     from ac 
     where  ac.vere_id = peve.vere_id
      and ac.acit_item='PERS_MILITAER_MUSIKER' )access_code_MILITAER_MUSIKER,
     (select max( access_code) 
     from ac 
     where  ac.vere_id = peve.vere_id  
      and ac.acit_item='PERS_MILITAER_STATUS' )access_code_MILITAER_STATUS,
     (select max( access_code) 
     from ac 
     where  ac.vere_id = peve.vere_id  
      and ac.acit_item='PERS_MILITAER_EINTEILUNG' )access_code_MILITAER_EINTEILUNG,
       (select max( access_code) 
     from ac 
     where  ac.vere_id = peve.vere_id  
      and ac.acit_item='PERS_MILITAER_GRAD' )access_code_MILITAER_GRAD,
       (select max( access_code) 
     from ac 
     where  ac.vere_id = peve.vere_id  
      and ac.acit_item='PERS_MILITAER_FUNKTION' )access_code_MILITAER_FUNKTION,
      peve.* 
    from stpv_v_person_verein peve   
    where vere_id in 
               (select vere_id 
                    from stpv_v_vereins_befugnis
                    where pers_id = (select pers_id from stpv_person where pers_user_id = :P17_APP_USER )
                )
 )    