create or replace view
stpv_v_vereins_befugnis
as 
with vereins_befugnis
as (select peve_pers_id, pefu_funk_id, (select listagg(vere_id, person_pkg.get_id_separator()) vere_ids from 
	stpv_verein
	   start with vere_id = peve_vere_id
	   CONNECT BY PRIOR vere_id = vere_vere_id) vere_ids
from stpv_pers_vere peve join stpv_peve_funk pefu on peve_id = pefu_peve_id
      join stpv_funktion on funk_id = pefu_funk_id and funk_is_role = 1
)
select pers_id, vere_id, funk_id, vere_name
from stpv_verein , stpv_person p, stpv_funktion f
where vere_id in 
	(select column_value from table(apex_string.split(
		(select listagg(vere_ids, person_pkg.get_id_separator()) 
		 from vereins_befugnis
		 where peve_pers_id = p.pers_id--(select pers_id from stpv_person where pers_user_id = 'mbl')
			   and pefu_funk_id = f.funk_id
		), person_pkg.get_id_separator()) 
		)
	)
/