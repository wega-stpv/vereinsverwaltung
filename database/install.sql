@dblink/create_stpv_kawaikini.sql

@sequence/stpv_seq.sql

@package/ddl_pkg.sql
@package/ddl_pkg_body.sql

@table/cm_message_log.sql

@table/cm_parameter.sql

@package/cm_message_log_pkg.sql
@package/cm_message_log_pkg_body.sql
/*@package/USER_PKG.sql
@package/USER_PKG_BODY.sql*/

@table/cm_list_parameter.sql


@table/stpv_land.sql
@table/stpv_verein_typ.sql
@table/stpv_person.sql
@table/stpv_funktion.sql
@table/stpv_verein.sql
@table/stpv_pers_vere.sql
@table/stpv_peve_funk.sql
@table/stpv_audit_trail.sql
@table/stpv_access_item.sql
@table/stpv_acit_funk.sql
@table/stpv_instrument.sql
@table/stpv_peve_inst.sql
@table/stpv_ausbildung.sql
@table/stpv_wettspiel.sql

@table/stpv_suisa_meldung.sql
@table/stpv_komposition.sql
@table/stpv_sume_komp.sql

@package/cm_parameter_pkg.sql
@package/cm_parameter_pkg_body.sql
@package/COMMON_PKG.sql
@package/COMMON_PKG_BODY.sql
@package/PERSON_PKG.sql
@package/PERSON_PKG_BODY.sql

@trigger/create_triggers.sql
exec ddl_pkg.generate_jn_trigger;

@view/all_views.sql 
set serverout on size unlimited
@data/import_land.sql
@data/import_verein_typ.sql
@data/import_verein.sql
@data/import_person.sql
@data/merge_person.sql

@data/import_funktion.sql
@data/import_pers_funk.sql
@data/import_lipa.sql
@data/insert_access_item.sql
@data/import_instrument.sql
--set serveroutput on size unlimited
@data/import_pers_inst.sql
@data/import_komposition.sql
@data/import_suisa_meldung.sql
@data/import_pers_ausb.sql



set serveroutput on size unlimited