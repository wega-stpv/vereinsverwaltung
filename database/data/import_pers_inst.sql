set serveroutput on size unlimited 
/*prompt importing funktion mitglieder

merge into stpv_peve_funk i
using (select peve.*, funk_id from stpv_pers_vere peve, stpv_funktion funk  where funk_code = 'MITGLIED') d
on (i.pefu_peve_id = d.peve_id and i.pefu_funk_id = d.funk_id)
when not matched then insert (i.pefu_peve_id, i.pefu_funk_id) 
values (d.peve_id, d.funk_id)
/
*/

prompt importing Instrumente from personen@stpv_old
begin  
   for l in (
	    select pers_id, (select vere_id from stpv_verein where vere_vereinid=vereinid) vere_id,
						 (select inst_id from stpv_instrument where inst_code = instrumentid) inst_id
				from personen@stpv_old p --join personenverein@stpv_old pv on p.personenseq = pv.personenseq
				 join stpv_person np on np.pers_personenseq = p.personenseq  
				 where nvl(instrumentid, 'I-NONE') <> 'I-NONE'
		union 
		select pers_id, (select vere_id from stpv_verein where vere_vereinid=vereinid) vere_id,
							 (select inst_id from stpv_instrument where inst_code = instrument2id)
					from personen@stpv_old p --join personenverein@stpv_old pv on p.personenseq = pv.personenseq
					 join stpv_person np on np.pers_personenseq = p.personenseq  
					 where nvl(instrument2id, 'I-NONE') <> 'I-NONE' 
		) loop
--		dbms_output.put_line('pers_id/vere_id/inst_id:' || l.pers_id ||'/'|| l.vere_id||'/'|| l.inst_id);
        person_pkg.merge_person_verein_instrument(i_pers_id=>l.pers_id, i_vere_id=>l.vere_id, i_inst_id=>l.inst_id);
    end loop;
end;
/


prompt importing Instrumente from personenverein@stpv_old
begin  
   for l in (
	    select pers_id, (select vere_id from stpv_verein where vere_vereinid=vereinid) vere_id,
						 (select inst_id from stpv_instrument where inst_code = instrumentid) inst_id
				from personenverein@stpv_old p --join personenverein@stpv_old pv on p.personenseq = pv.personenseq
				 join stpv_person np on np.pers_personenseq = p.personenseq  
				 where nvl(instrumentid, 'I-NONE') <> 'I-NONE'
		union 
		select pers_id, (select vere_id from stpv_verein where vere_vereinid=vereinid) vere_id,
							 (select inst_id from stpv_instrument where inst_code = instrument2id)
					from personenverein@stpv_old p --join personenverein@stpv_old pv on p.personenseq = pv.personenseq
					 join stpv_person np on np.pers_personenseq = p.personenseq  
					 where nvl(instrument2id, 'I-NONE') <> 'I-NONE'
		) loop
--		dbms_output.put_line('pers_id/vere_id/inst_id:' || l.pers_id ||'/'|| l.vere_id||'/'|| l.inst_id);
        person_pkg.merge_person_verein_instrument(i_pers_id=>l.pers_id, i_vere_id=>l.vere_id, i_inst_id=>l.inst_id);
    end loop;
end;
/
commit
/