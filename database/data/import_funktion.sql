
/*
select distinct rolle_zentralvorstand_STPV from personenverein@stpv_old;

regexp ersetezen
suche: \h*(\w*),
ersetzend durch: select '\U$1\E', '$1', 'MITGLIED', 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union


*/
select * from stpv_funktion;
merge into stpv_funktion i using 
(
select 'MITGLIED' funk_code, 'Mitglied' funk_name, 'NONE' funk_group, 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from dual union 

select 'BK_NOTEN_LEITER', 'Leiter BK Noten', 'BK_NOTEN'           , 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'BBK_NOTEN_MITGLIED', 'Mitglied  BK Noten', 'BK_NOTEN'     , 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
                                                               
select 'TK_KLAKO_LEITER', 'Leiter Klako BK', 'TK_KLAKO'           , 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'TK_KLAKO_MITGLIED', 'Mitglied Klako BK', 'TK_KLAKO'       , 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
                                                               
select 'BK_LEITER', 'Leiter Bläserkommission', 'MK'               , 1 FUNK_STPV, 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'BK_MITGLIED', 'Mitglied Bläserkommission', 'MK'           , 1 FUNK_STPV, 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'BK_NOTEN', 'Notenkommision', 'MK'                       , 1 FUNK_STPV, 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'BK_STV_LEITER', 'Stv. Leiter Bläserkommission', 'MK'    , 1 FUNK_STPV, 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'CHEF_PFEIFER_BLAESER', 'Pfeifer-Bläserchef', 'MK'         , 0 FUNK_STPV, 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'EXPERT', 'Experte', 'MK'                                  , 1 FUNK_STPV, 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'KOMPONIST_B', 'Komponist Bläser/Pfeifer', 'MK'          , 1 FUNK_STPV, 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'KOMPONIST_T', 'Komponist Tambouren', 'MK'               , 1 FUNK_STPV, 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'TK_LEITER', 'Leiter Tambourenkommission', 'MK'            , 1 FUNK_STPV, 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'TK_MITGLIED', 'Mitglied Tambourenkommission', 'MK'        , 1 FUNK_STPV, 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'TK_OBMANN', 'TK-Obmann', 'MK', 1 FUNK_STPV              , 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'MK_CHEF', 'Chef Musikkommision', 'VEREIN', 0 FUNK_STPV              , 1 FUNK_RV, 0 FUNK_VEREIN, 1 FUNK_IS_ROLE from  dual union
--select 'TK_STV_LEITER', 'Stv. Leiter Tambourenkommission', ' MK', 1 FUNK_STPV, 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
                                                               
select 'JUROR_TAMBOUR', 'Jury Tambour', 'JUROR_TK'           , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union

select 'JUROR_CLAIRON', 'Jury Clairon', 'JUROR_BK'           , 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'JUROR_NATWAERISCH', 'Jury Natwärisch', 'JUROR_BK'    , 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'JUROR_PICOLO', 'Jury Piccolo', 'JUROR_BK'            , 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union

select 'JUROR_TC', 'Jury TC', 'JUROR_WETTSPIELE'             , 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'JUROR_TN', 'Jury TN', 'JUROR_WETTSPIELE'             , 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'JUROR_TP', 'Jury TP', 'JUROR_WETTSPIELE'             , 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'JUROR_TPER', 'Jury TPer', 'JUROR_WETTSPIELE'         , 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union

--select 'CHEFREDAKTOR_TM', 'Chefredaktor TM', 'MITGLIED'                      , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'FAEHNRICH', 'Fähnrich, Bannerträger', 'MITGLIED'                       , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'JUNGPFEIFER', 'Jungpfeifer', 'MITGLIED'                              , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'JUNGTAMBOURENLEITER', 'Leiter Jungtambouren', 'MITGLIED'               , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'INTERNET', 'Internet', 'MITGLIED'                                    , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'JURYKURS', 'Jurykurs', 'MITGLIED'                                    , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'LEITER_DRUCK', 'Leiter Druck', 'MITGLIED'                            , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'LEITER_RPK_STV', 'Leiter RPK_STV', 'MITGLIED'                        , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'LEITERKURS', 'Leiterkurs', 'MITGLIED'                                , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'MITGLIED_RPK_STV', 'Mitglied RPK_STV', 'MITGLIED'                    , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'PFEIFER', 'Pfeifer', 'MITGLIED'                                      , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'REDAKTION_TM', 'Redaktion TM', 'MITGLIED'                            , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'SPIELLEITER', 'Sektionsleiter / musikalische Gesamtleitung', 'MITGLIED', 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'TAMBOURENLEITER', 'Leiter Tambouren', 'MITGLIED'                       , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'TECHNISCHERLEITER', 'Material-/Instrumentenverwalter', 'MITGLIED'      , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'WEBMASTER', 'Webmaster', 'MITGLIED'                                  , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'JUNGPFEIFFERLEITER', 'Leiter Jungpfeifer/Jungbläser', 'MITGLIED'       , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'PEIFFERLEITER', 'Leiter Pfeifer / Bläser', 'MITGLIED'                  , 0 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union

select 'ADMIN', 'Administrator', 'VEREIN'                      , 1 FUNK_STPV, 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'BK_SEKRETAER', 'Sekretär Bläserkommission', 'VEREIN'   , 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'KASSIER', 'Kassier', 'VEREIN'                          , 1 FUNK_STPV, 1 FUNK_RV, 1 FUNK_VEREIN, 1 FUNK_IS_ROLE from  dual union
select 'MILITAER', 'Militärverantwortlicher', 'VEREIN'         , 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
--select 'MUTATIONSFUEHRER', 'Mutationsführer', 'VEREIN'       , 1 FUNK_STPV, 1 FUNK_RV, 1 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'PRAESIDENT', 'Präsident', 'VEREIN'                     , 1 FUNK_STPV, 1 FUNK_RV, 1 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'RECHNUNGSPRUEFER', 'Rechnungsprüfer/GPK', 'VEREIN'     , 1 FUNK_STPV, 1 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'SEKRETAER', 'Sekretär/Aktuar', 'VEREIN'                , 1 FUNK_STPV, 1 FUNK_RV, 1 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'TK_SEKRETAER', 'Sekretär Tambourenkommission', 'VEREIN', 1 FUNK_STPV, 0 FUNK_RV, 0 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union
select 'VIZE', 'Vizepräsident', 'VEREIN'                       , 1 FUNK_STPV, 1 FUNK_RV, 1 FUNK_VEREIN, 0 FUNK_IS_ROLE from  dual union 
select 'VORSTAND', 'Rolle Vorstand', 'NONE'                      , 0 FUNK_STPV, 0 FUNK_RV, 1 FUNK_VEREIN, 1 FUNK_IS_ROLE from  dual union 
select 'EXTENDED_VORSTAND', 'Vorstand', 'VEREIN'   , 0 FUNK_STPV, 0 FUNK_RV, 1 FUNK_VEREIN, 0 FUNK_IS_ROLE from dual 
)
 d
on (i.funk_code = d.funk_code)
when matched then update set 
  i.funk_name = d.funk_name,
  i.funk_group = d.funk_group,
  i.FUNK_STPV = d.FUNK_STPV,
  i.FUNK_RV = d.FUNK_RV,
  i.FUNK_VEREIN = d.FUNK_VEREIN,
  i.funk_is_role = d.funk_is_role
when not matched then insert (i.funk_code, i.funk_name, i.funk_group,i.FUNK_STPV , i.FUNK_RV, i.FUNK_VEREIN, i.FUNK_IS_ROLE)
values (d.funk_code, d.funk_name, d.funk_group, d.FUNK_STPV, d.FUNK_RV, d.FUNK_VEREIN, d.FUNK_IS_ROLE)
/

update stpv_funktion set funk_is_role = 1 where funk_code in (
'VORSTAND',
'ADMIN',
'SEKRETAER',
'MILITAER',
'TK_SEKRETAER',
'KASSIER',
'BK_SEKRETAER')
/

commit
/


