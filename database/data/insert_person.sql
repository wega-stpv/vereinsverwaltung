prompt importing stpv_person 
insert into stpv_person
(
PERS_NAME, 
PERS_VORNAME, 
PERS_USER_ID, 
PERS_GEBURTSDATUM, 
PERS_ANREDE, 
PERS_GESCHLECHT,
pers_status)
select 
'Blattner', 'Mario', 'mbl', to_date('01.01.2020', 'dd.mm.yyyy'), 'Herr', 'm' , 1 from dual 
/

update stpV_person set pers_password = 'mbl' where pers_user_id = 'mbl'
/

commit
/

insert into stpv_pers_vere (peve_pers_id, peve_vere_id) select 
(select pers_id from stpv_person where pers_user_id ='mbl')
, (select vere_id from stpv_verein where vere_name like 'Schweizerischer Tam%')
from dual
/


insert into STPV_PEVE_FUNK (pefu_peve_id, pefu_funk_id)
select 
(select peve_id from stpv_pers_vere
where peve_pers_id = (select pers_id from stpv_person where pers_user_id ='mbl')
and peve_vere_id = (select vere_id from stpv_verein where vere_name like 'Schweizerischer Tam%')
)
, 
(select funk_id from stpv_funktion where funk_code = 'ADMIN')
from dual 
/

commit
/

select vere_id from stpv_verein
where vere_name like 'Schweiz%';

select * from stpv_person where pers_user_id = 'mbl';