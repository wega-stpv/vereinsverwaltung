set serveroutput on size unlimited 
/*prompt importing funktion mitglieder

merge into stpv_peve_funk i
using (select peve.*, funk_id from stpv_pers_vere peve, stpv_funktion funk  where funk_code = 'MITGLIED') d
on (i.pefu_peve_id = d.peve_id and i.pefu_funk_id = d.funk_id)
when not matched then insert (i.pefu_peve_id, i.pefu_funk_id) 
values (d.peve_id, d.funk_id)
/
*/

prompt importing LEITERKurse from personen@stpv_old

merge into stpv_ausbildung d 
using (select pers_id, 
						 replace(LEITERKURS, 'LEITERKURS', 'LEITERKURS_') kurs
                         ,max(datum_letzter_kurs_intrumental) AUSB_DATUM
				from (  select personenseq, vereinid, LEITERKURS, datum_letzter_kurs_intrumental from personen@stpv_old 
                        union
                        select personenseq, vereinid, LEITERKURS, datum_letzter_kurs_intrumental from personenverein@stpv_old 
                     ) p --join personenverein@stpv_old pv on p.personenseq = pv.personenseq
				 join stpv_person np on np.pers_personenseq = p.personenseq  
				 where nvl(LEITERKURS, 'NONE') in ('LEITERKURS3', 'LEITERKURS2', 'LEITERKURS1')
				 group by pers_id, LEITERKURS
) s
on (d.ausb_pers_id = s.pers_id and d.ausb_kurs = s.kurs)
when matched then 
update
set d.AUSB_DATUM=s.AUSB_DATUM
when not matched then 
insert (ausb_pers_id, ausb_kurs, d.AUSB_DATUM)
values (pers_id, kurs, s.AUSB_DATUM)
/

prompt importing JURYKURS from personen@stpv_old

merge into stpv_ausbildung d 
using (select pers_id, JURYKURS kurs,  max(datum_letzter_kurs_intrumental) ausb_datum
				from (  select personenseq, vereinid, JURYKURS, datum_letzter_kurs_intrumental from personen@stpv_old 
                        union
                        select personenseq, vereinid, JURYKURS, datum_letzter_kurs_intrumental from personenverein@stpv_old 
                     ) p --join personenverein@stpv_old pv on p.personenseq = pv.personenseq
				 join stpv_person np on np.pers_personenseq = p.personenseq  
				 where nvl(JURYKURS, 'NONE') in ('Jury')
				 group by pers_id, JURYKURS
) s
on (d.ausb_pers_id = s.pers_id and d.ausb_kurs = s.kurs)
when matched then 
update
set d.ausb_datum=s.ausb_datum
when not matched then 
insert (ausb_pers_id, ausb_kurs, d.ausb_datum)
values (pers_id, kurs, s.ausb_datum)
/



prompt importing INSTRUMENTALKURS from personen@stpv_old

merge into stpv_ausbildung d 
using (select  pers_id, upper(INSTRUMENTALKURS) kurs, max(datum_letzter_kurs_intrumental) ausb_datum
				from (  select personenseq, vereinid, INSTRUMENTALKURS, datum_letzter_kurs_intrumental from personen@stpv_old 
                        union
                        select personenseq, vereinid, INSTRUMENTALKURS, datum_letzter_kurs_intrumental from personenverein@stpv_old 
                     ) p --join personenverein@stpv_old pv on p.personenseq = pv.personenseq
				 join stpv_person np on np.pers_personenseq = p.personenseq  
				 where upper(nvl(INSTRUMENTALKURS, 'NONE')) in ('OBERSTUFE', 'MITTELSTUFE', 'UNTERSTUFE')
				 group by pers_id, INSTRUMENTALKURS 
) s
on (d.ausb_pers_id = s.pers_id and d.ausb_kurs = s.kurs)
when matched then 
update
set d.ausb_datum=s.ausb_datum
when not matched then 
insert (ausb_pers_id, ausb_kurs, d.ausb_datum)
values (pers_id, kurs, s.ausb_datum)
/

commit
/