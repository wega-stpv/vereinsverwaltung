
prompt merge Veteran STPV from personen into stpv_pers_vere
merge into stpv_pers_vere i using (
select pers_id, vere_id
, nvl("EIDG._VETERAN_STPV_SEIT", VETERAN_SEIT) VETERAN_SEIT
from stpv_person join personen@stpv_old p on pers_personenseq = personenseq 
  join vereinstammdaten@stpv_old vs on vs.vereinid = (select vereinid from vereinstammdaten@stpv_old 
						where rvid='STPV')
  join stpv_verein v on v.vere_vereinid = vs.vereinid
  where VETERAN_SEIT is not null or "EIDG._VETERAN_STPV_SEIT" is not null
  ) d
  on (d.pers_id = i.peve_pers_id  and d.vere_id = i.peve_vere_id)
  when not matched then
    insert (
	peve_pers_id
	, peve_vere_id
    , PEVE_VEREIN_VETERAN_SEIT  )
    values (
    pers_id
	, vere_id
	, VETERAN_SEIT
    )
   when matched then update set                   
   PEVE_VEREIN_VETERAN_SEIT                      = VETERAN_SEIT
/

prompt merge Veteran RV from personen into stpv_pers_vere
merge into stpv_pers_vere i using (
select pers_id, vere_id
, VETERAN_RV_SEIT - interval '20' year veteran_seit
--, p.vereinid
--, vs.rvid
from stpv_person join personen@stpv_old p on pers_personenseq = personenseq 
  join vereinstammdaten@stpv_old vs on vs.vereinid = p.vereinid
  join stpv_verein v on v.vere_vereinid = vs.rvid
  where VETERAN_RV_SEIT is not null
  ) d
  on (d.pers_id = i.peve_pers_id  and d.vere_id = i.peve_vere_id)
  when not matched then
    insert (
	peve_pers_id
	, peve_vere_id
    , PEVE_VEREIN_VETERAN_SEIT  )
    values (
    pers_id
	, vere_id
	, VETERAN_SEIT
    )
   when matched then update set                   
   PEVE_VEREIN_VETERAN_SEIT                      = VETERAN_SEIT
/

prompt merge Veteran STPV from personenverein into stpv_pers_vere
merge into stpv_pers_vere i using (
select pers_id, vere_id
, VETERAN_SEIT
from stpv_person join personenverein@stpv_old p on pers_personenseq = personenseq 
  join vereinstammdaten@stpv_old vs on vs.vereinid = (select vereinid from vereinstammdaten@stpv_old 
						where rvid='STPV')
  join stpv_verein v on v.vere_vereinid = vs.vereinid
  where VETERAN_SEIT is not null
  ) d
  on (d.pers_id = i.peve_pers_id  and d.vere_id = i.peve_vere_id)
  when not matched then
    insert (
	peve_pers_id
	, peve_vere_id
    , PEVE_VEREIN_VETERAN_SEIT  )
    values (
    pers_id
	, vere_id
	, VETERAN_SEIT
    )
   when matched then update set                   
   PEVE_VEREIN_VETERAN_SEIT                      = VETERAN_SEIT
/

prompt merge Veteran RV from personenverein into stpv_pers_vere
merge into stpv_pers_vere i using (
select pers_id, vere_id
, VETERAN_RV_SEIT - interval '20' year veteran_seit
--, p.vereinid
--, vs.rvid
from stpv_person join personenverein@stpv_old p on pers_personenseq = personenseq 
  join vereinstammdaten@stpv_old vs on vs.vereinid = p.vereinid
  join stpv_verein v on v.vere_vereinid = vs.rvid
  where VETERAN_RV_SEIT is not null
  ) d
  on (d.pers_id = i.peve_pers_id  and d.vere_id = i.peve_vere_id)
  when not matched then
    insert (
	peve_pers_id
	, peve_vere_id
    , PEVE_VEREIN_VETERAN_SEIT  )
    values (
    pers_id
	, vere_id
	, VETERAN_SEIT
    )
   when matched then update set                   
   PEVE_VEREIN_VETERAN_SEIT                      = VETERAN_SEIT
/

prompt updating goldene Ehrennadel
update stpv_pers_vere set peve_verein_ehrenmitglied = 3 --Träger der goldenen Ehrennadel
where peve_verein_goldene_ehrennadel is not null
/

prompt importing from personenverein into stpv_pers_vere
insert into stpv_pers_vere (peve_pers_id, peve_vere_id)
select distinct p.pers_id, v.vere_id from personenverein@stpv_old pv 
join stpv_person p on p.pers_personenseq = pv.personenseq
join stpv_verein v on v.vere_vereinid = vereinid
where not exists (select 1 from stpv_pers_vere where peve_pers_id = p.pers_id and peve_vere_id = v.vere_id)
/

prompt updating possible user_ids
update stpv_person set pers_user_id = trim(pers_email)
where nvl(pers_email, 'hi') not in (
select nvl(trim(pers_email), 'rotz')
from stpv_person
group by trim(pers_email)
having count(*) > 1)
/

commit
/


prompt importing personen.ehrenmitglied
merge into stpv_pers_vere dst 
using
( select (select pers_id from stpv_person where pers_personenseq = personenseq) pers_id
, (select vere_id from stpv_verein where vere_vereinid = vereinid) vere_id
, decode (nvl(ehrenmitglied , 'EHRENM-NO'), 'EHRENM-YES', 1, 0) ehrenmitglied 
from personen@stpv_old
where decode (nvl(ehrenmitglied , 'EHRENM-NO'), 'EHRENM-YES', 1, 0) = 1
--and vereinid='CrazyDrummersLinden' 
--and personenseq = 16395
and exists (select pers_id from stpv_person where pers_personenseq = personenseq)
order by personenseq
) src
on (dst.peve_pers_id = src.pers_id and dst.peve_vere_id = src.vere_id)
when matched then 
	update 
	set 
	  dst.PEVE_VEREIN_EHRENMITGLIED = src.ehrenmitglied
when not matched then 
	insert (
		dst.peve_pers_id, dst.peve_vere_id, dst.PEVE_VEREIN_EHRENMITGLIED)
	values(
		src.pers_id, src.vere_id,  src.ehrenmitglied )
/

commit
/
