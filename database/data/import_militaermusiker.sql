--Dachverband
insert into stpv_verein (
--VERE_VERE_ID,
VERE_VETY_ID,
VERE_NAME, 
VERE_STRASSE, 
	VERE_PLZ, 
	VERE_ORT, 
	VERE_LAND_ID, 
	VERE_HOMEPAGE,
	VERE_SUISA , 
	VERE_GRUENDUNGSJAHR ,
    VERE_COMMENT,
    VERE_EMAIL,
	VERE_STATUS,
    VERE_BEITRITTSJAHR,
	VERE_AUSTRITTSJAHR,
	VERE_IBAN,
	VERE_VEREINID
)
select --(select vere_id from stpv_verein where vere_name = rv.verbandsname) , 
(select vety_id from stpv_verein_typ where vety_code = vtid),
verbandsname, v.strasse, v.plz, v.ort, (select land_id from stpv_land where land_Code = v.landid), v.homepage, 
decode(nvl(suisa, 'na') , 'YES', 1, 'NO', 0, 'na' ), 
rv.gruendungsjahr, rv.beschreibung,
rv.email, 
decode(v.statusid, 'AKTIV', 1, 0),
v.beitrittsjahr, v.austrittsjahr, v.bankverbindung
, vereinid
from vereinstammdaten@stpv_old v join regionalverband@stpv_old rv  on v.rvid = rv.rvid
where rv.verbandstyp in ('STV')
/

/*
insert into stpv_verein (VERE_NAME, 
VERE_STRASSE, 
	VERE_PLZ, 
	VERE_ORT, 
	VERE_LAND_ID , 
	VERE_HOMEPAGE,
	--VERE_SUISA VARCHAR2(40 BYTE), 
	VERE_GRUENDUNGSJAHR ,
    VERE_COMMENT,
    VERE_EMAIL,
	VERE_STATUS
    --, VERE_BEITRITTSJAHR
)
select verbandsname, strasse, plz, ort, (select land_id from stpv_land where land_Code = landid), homepage, gruendungsjahr, beschreibung, email, 1 
from regionalverband@stpv_old
where verbandstyp = 'STV'
/
*/

--Regionalverbände
insert into stpv_verein (
VERE_VERE_ID,
VERE_VETY_ID,
VERE_NAME, 
VERE_STRASSE, 
	VERE_PLZ, 
	VERE_ORT, 
	VERE_LAND_ID, 
	VERE_HOMEPAGE,
	VERE_SUISA , 
	VERE_GRUENDUNGSJAHR ,
    VERE_COMMENT,
    VERE_EMAIL,
	VERE_STATUS,
    VERE_BEITRITTSJAHR,
	VERE_AUSTRITTSJAHR,
	VERE_IBAN,
	VERE_VEREINID
)
select (select vere_id from stpv_verein where vere_vereinid = '00') , 
(select vety_id from stpv_verein_typ where vety_code = vtid),
verbandsname, v.strasse, v.plz, v.ort, (select land_id from stpv_land where land_Code = v.landid), v.homepage, suisa, 
rv.gruendungsjahr, rv.beschreibung,
rv.email, 
decode(v.statusid, 'AKTIV', 1, 0),
v.beitrittsjahr, v.austrittsjahr, v.bankverbindung
, vereinid
from vereinstammdaten@stpv_old v join regionalverband@stpv_old rv  on v.rvid = rv.rvid
where v.vtid in ('RV')
and rv.verbandstyp not in ('STV')
/

/*
insert into stpv_verein (
VERE_VERE_ID,
VERE_VETY_ID,
VERE_NAME, 
VERE_STRASSE, 
	VERE_PLZ, 
	VERE_ORT, 
	VERE_LAND_ID, 
	VERE_HOMEPAGE,
	--VERE_SUISA VARCHAR2(40 BYTE), 
	VERE_GRUENDUNGSJAHR ,
    VERE_COMMENT,
    VERE_EMAIL,
	VERE_STATUS
    --, VERE_BEITRITTSJAHR
)
select 
(select VERE_ID from stpv_verein where vere_name ='Schweizerischer Tambouren- und Pfeiferverband'),
(select vety_id from stpv_verein_typ where vety_code = 'VT'),
verbandsname, strasse, plz, ort, (select land_id from stpv_land where land_Code = landid), homepage,  gruendungsjahr, beschreibung, email, 1
from regionalverband@stpv_old
where verbandstyp = 'RV'
/
*/

--Vereine
insert into stpv_verein (
VERE_VERE_ID,
VERE_VETY_ID,
VERE_NAME, 
VERE_STRASSE, 
	VERE_PLZ, 
	VERE_ORT, 
	VERE_LAND_ID, 
	VERE_HOMEPAGE,
	VERE_SUISA , 
	VERE_GRUENDUNGSJAHR ,
    VERE_COMMENT,
    --VERE_EMAIL,
	VERE_STATUS,
    VERE_BEITRITTSJAHR,
	VERE_AUSTRITTSJAHR,
	VERE_IBAN,
	VERE_VEREINID
)
select (select vere_id from stpv_verein where vere_name = rv.verbandsname and vere_vereinid <> 'Testverein') , 
(select vety_id from stpv_verein_typ where vety_code = vtid),
vereinname, v.strasse, v.plz, v.ort, (select land_id from stpv_land where land_Code = v.landid), v.homepage, suisa, v.gruendungsjahr, v.comments,
--v.email, 
decode(v.statusid, 'AKTIV', 1, 0),
v.beitrittsjahr, v.austrittsjahr, v.bankverbindung
, vereinid
from vereinstammdaten@stpv_old v join regionalverband@stpv_old rv  on v.rvid = rv.rvid
where v.vtid not in ('RV')
and rv.verbandstyp not in ('STV')
;


commit
/