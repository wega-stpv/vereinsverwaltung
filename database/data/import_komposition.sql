
delete from stpv_komposition
/

insert into stpv_komposition (
KOMP_WERK
, KOMP_KOMPONIST
, KOMP_ARRANGEUR
, KOMP_KOMPONIST2
, KOMP_BEZUGSQUELLE
, KOMP_KLASSE
, KOMP_TYP
, KOMP_FUERWETTSPIELEZUGELASSEN
, KOMP_CREATION_DATE
, KOMP_CREATION_USER
, KOMP_MODIFICATION_USER
, KOMP_kompositionseq
)
select werk, komponist
, arrangeur
, komponist2 
, bezugsquelle
, klasse
, kt.bezeichnung 
, decode(nvl(k.fuerwettspielezugelassen, 'Nein'), 'Ja', 1, 0 )
, decode(datum, null, null, to_date(datum, 'dd.mm.yyyy') )
 , k.hinzugefuegtdurch
 , k.userid
 , kompositionseq
from kompositionen@stpv_old k left join kompositionstyp@stpv_old kt on k.typseq=kt.typseq
/

commit
/