
insert into stpv_VEREIN_TYP (VETY_code, VETY_name)
select VTid, VTname
from VEREINSTYP@stpv_old
/

merge into stpv_VEREIN_TYP i 
 using (
	select 'DV' VETY_CODE, 'Dachverband' VETY_NAME
	from dual
  ) d
  on (d.VETY_CODE = i.VETY_CODE)
  when not matched then
    insert (
	VETY_CODE 
	, VETY_NAME
     )
    values (
    d.VETY_CODE
	, d.VETY_NAME
    )
   when matched then update set                   
	VETY_NAME = d.VETY_NAME
/

commit
/