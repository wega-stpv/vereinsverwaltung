prompt importing stpv_person 
insert into stpv_person
(
pers_personenseq,
--PERS_USER_ID, 
PERS_NAME, 
PERS_VORNAME, 
PERS_GEBURTSDATUM, 
PERS_ANREDE, 
PERS_GESCHLECHT, 
PERS_STRASSE, 
PERS_PLZ, 
PERS_ORT, 
PERS_LAND_ID, 
PERS_TELEFON_NR,
PERS_EMAIL, 
PERS_AHV_NR,
PERS_BANKVERBINDUNG_IMPORT, 
PERS_STERBE_DATUM, 
PERS_STATUS,  
PERS_MILITAER_MUSIKER, 
PERS_MILITAER_STATUS, 
PERS_KOMMENTAR
)
select 
personenseq, /*email, */name, vorname,geburtsdatum,
anrede, decode (geschlecht, 'Masculine', 'm', 'Feminine', 'w', '?'),
strasse, plz, ort,  (select land_id from stpv_land where land_Code = landid) , telp, email, ahv_nr, 
bankverbindung, sterbedatum,
decode (STPV_status, 'ACTIV', 1, 'NOACTIVE', 0, -1),
decode(nvl(militaermusiker, 'Nein'), 'Ja', 1, 'Nein', 0, -1),
decode(MILLITAERSTATUS, 'ex_military', 'ex', 'military','m', 'no_military', 'no', 'na'),
comments
from personen@stpv_old 
/

prompt importing from personen into stpv_pers_vere
insert into stpv_pers_vere(peve_pers_id, peve_vere_id
, PEVE_MITGLIEDERBEITRAG_IMPORT
, PEVE_VEREIN_EINTRITT 
, PEVE_VEREIN_AUSTRITT 
, PEVE_VEREIN_EHRENMITGLIED 
, PEVE_VEREIN_KEINE_EHRUNG_ERWUENSCHT 
--, PEVE_VEREIN_VETERAN_SEIT 
, PEVE_VEREIN_GOLDENE_EHRENNADEL 
)
select pers_id, vere_id
, mitgliederbeitrag
, p.eintrittinverein, p.AUSTRITTSDATUM
, decode(EHRENMITGLIED, 'EHRENM-NO', 0, 'EHRENM-YES', 1, -1)
, decode(KEINE_EHRUNG_ERWUENSCHT, 'N', 0, 'Y', 1, -1)
--, VETERAN_SEIT
, GOLDENE_EHRENNADEL
from stpv_person join personen@stpv_old p on pers_personenseq = personenseq 
  join vereinstammdaten@stpv_old vs on vs.vereinid = p.vereinid
  join stpv_verein v on v.vere_vereinid = vs.vereinid
/

update stpv_person set pers_anrede = 'Frau'
where pers_anrede in ( 'Madame', 'Mlle')
/

update stpv_person set pers_anrede = 'Herr'
where pers_anrede = 'Monsieur'
/

commit
/