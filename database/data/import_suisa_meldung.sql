prompt insert suisa_meldung
merge into stpv_suisa_meldung i using 
(
select distinct 
(select vere_id from stpv_verein where vere_vereinid = s.vereinid) vere_id
, (select jahr from Jahre@stpv_old j where j.jahrseq= s.jahrseq) jahr
from suisa@stpv_old s
)  d
on (i.sume_vere_id = d.vere_id and to_number(to_char(i.sume_jahr, 'yyyy')) = d.jahr)
when not matched then insert (i.sume_vere_id, i.sume_jahr, i.sume_abgeschlossen)
values (d.vere_id, to_date(d.jahr,'yyyy'), 1 )
/

commit
/

prompt import suisa_meldung_komposition

merge into stpv_sume_komp i using 
(
select 
 (select komp_id from stpv_komposition where komp_kompositionseq= kompositionseq) komp_id
, (select sume_id 
from stpv_suisa_meldung sume join stpv_verein vere on vere_id = sume_vere_id
where vere_vereinid = s.vereinid 
  and to_number(to_char(sume_jahr, 'yyyy')) = j.jahr)  sume_id
, auffuehrungen
from suisa@stpv_old s join jahre@stpv_old j on s.jahrseq=j.jahrseq
)  d
on (i.suko_komp_id = d.komp_id and i.suko_sume_id = d.sume_id)

when not matched then insert (i.suko_komp_id, i.suko_sume_id, i.suko_auffuehrungen)
values (d.komp_id, d.sume_id, d.auffuehrungen )
/

update stpv_suisa_meldung
set sume_abgeschlossen = 1
where sume_jahr <= to_date('2019','yyyy')
/

commit
/