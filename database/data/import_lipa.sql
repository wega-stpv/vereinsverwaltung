merge into cm_list_parameter l
using (
	select 'ACCESS' LIPA_MODULE, 'ACCESS_RIGHT' LIPA_NAME, 'RO' LIPA_VALUE, 'RO' LIPA_DISP_VALUE  from dual
	union 
	select 'ACCESS' LIPA_MODULE, 'ACCESS_RIGHT' LIPA_NAME, 'RW' LIPA_VALUE, 'RW' LIPA_DISP_VALUE  from dual
	) d
on (d.LIPA_MODULE= l.LIPA_MODULE and d.LIPA_NAME = l.LIPA_NAME and d.LIPA_VALUE = l.LIPA_VALUE)
WHEN NOT MATCHED THEN INSERT (l.LIPA_MODULE, l.LIPA_NAME, l.LIPA_TYPE, l.LIPA_DISP_VALUE,l. LIPA_VALUE)
values (D.LIPA_MODULE, D.LIPA_NAME, 'CHR', D.LIPA_DISP_VALUE, d.LIPA_VALUE)
/*WHEN MATCHED then update
   set r.ROLE_NAME  = D.ROLE_NAME*/
/  

prompt Vereinstatus
merge into cm_list_parameter l
using (
select 'VEREIN' LIPA_MODULE, 'STATUS' LIPA_NAME, '1' LIPA_VALUE, 'aktiv' LIPA_DISP_VALUE  from dual union
select 'VEREIN' LIPA_MODULE, 'STATUS' LIPA_NAME, '0' LIPA_VALUE, 'inaktiv' LIPA_DISP_VALUE  from dual 
) d
on (d.LIPA_MODULE= l.LIPA_MODULE and d.LIPA_NAME = l.LIPA_NAME and d.LIPA_VALUE = l.LIPA_VALUE)
WHEN NOT MATCHED THEN INSERT (l.LIPA_MODULE, l.LIPA_NAME, l.LIPA_TYPE, l.LIPA_DISP_VALUE,l. LIPA_VALUE)
values (D.LIPA_MODULE, D.LIPA_NAME, 'CHR', D.LIPA_DISP_VALUE, d.LIPA_VALUE)
when matched then update 
   set  l.LIPA_DISP_VALUE=d.LIPA_DISP_VALUE
/  

delete from cm_list_parameter
where LIPA_MODULE = 'MITGLIED'
and LIPA_NAME = 'STATUS'
/

prompt Mitgliederstatus
merge into cm_list_parameter l
using (
select 'MITGLIED' LIPA_MODULE, 'STATUS' LIPA_NAME, '0' LIPA_VALUE, 'ausgetreten' LIPA_DISP_VALUE, '' LIPA_DESCRIPTION  from dual union
select 'MITGLIED' LIPA_MODULE, 'STATUS' LIPA_NAME, '1' LIPA_VALUE, 'Aktivmitglied' LIPA_DISP_VALUE, ''  from dual 
 union
select 'MITGLIED' LIPA_MODULE, 'STATUS' LIPA_NAME, '2' LIPA_VALUE, 'nicht aktiv, in Ausbildung' LIPA_DISP_VALUE, 'Mitglieder in diesem Status dürfen gelöscht werden!'  from dual 
 union
select 'MITGLIED' LIPA_MODULE, 'STATUS' LIPA_NAME, '3' LIPA_VALUE, 'nicht aktiv, sonstige Funktion' LIPA_DISP_VALUE, '' LIPA_DESCRIPTION from dual 


) d
on (d.LIPA_MODULE= l.LIPA_MODULE and d.LIPA_NAME = l.LIPA_NAME and d.LIPA_VALUE = l.LIPA_VALUE)
WHEN NOT MATCHED THEN INSERT (l.LIPA_MODULE, l.LIPA_NAME, l.LIPA_TYPE, l.LIPA_DISP_VALUE,l.LIPA_VALUE, l.LIPA_DESCRIPTION)
values (D.LIPA_MODULE, D.LIPA_NAME, 'CHR', D.LIPA_DISP_VALUE, d.LIPA_VALUE, d.LIPA_DESCRIPTION)
/  

prompt JaNein
merge into cm_list_parameter l
using (
select 'COMMON' LIPA_MODULE, 'JaNein' LIPA_NAME, '0' LIPA_VALUE, 'Nein' LIPA_DISP_VALUE  from dual union
select 'COMMON' LIPA_MODULE, 'JaNein' LIPA_NAME, '1' LIPA_VALUE, 'Ja' LIPA_DISP_VALUE  from dual 
) d
on (d.LIPA_MODULE= l.LIPA_MODULE and d.LIPA_NAME = l.LIPA_NAME and d.LIPA_VALUE = l.LIPA_VALUE)
WHEN NOT MATCHED THEN INSERT (l.LIPA_MODULE, l.LIPA_NAME, l.LIPA_TYPE, l.LIPA_DISP_VALUE,l. LIPA_VALUE)
values (D.LIPA_MODULE, D.LIPA_NAME, 'CHR', D.LIPA_DISP_VALUE, d.LIPA_VALUE)
/  

prompt geschlecht
merge into cm_list_parameter l
using (
select 'MITGLIED' LIPA_MODULE, 'GESCHLECHT' LIPA_NAME, 'm' LIPA_VALUE, 'männlich' LIPA_DISP_VALUE  from dual union
select 'MITGLIED' LIPA_MODULE, 'GESCHLECHT' LIPA_NAME, 'w' LIPA_VALUE, 'weiblich' LIPA_DISP_VALUE  from dual union
select 'MITGLIED' LIPA_MODULE, 'GESCHLECHT' LIPA_NAME, 'd' LIPA_VALUE, 'divers' LIPA_DISP_VALUE  from dual 
) d
on (d.LIPA_MODULE= l.LIPA_MODULE and d.LIPA_NAME = l.LIPA_NAME and d.LIPA_VALUE = l.LIPA_VALUE)
WHEN NOT MATCHED THEN INSERT (l.LIPA_MODULE, l.LIPA_NAME, l.LIPA_TYPE, l.LIPA_DISP_VALUE,l. LIPA_VALUE)
values (D.LIPA_MODULE, D.LIPA_NAME, 'CHR', D.LIPA_DISP_VALUE, d.LIPA_VALUE)
/  

prompt SUISA
merge into cm_list_parameter l
using (
select 'VEREIN' LIPA_MODULE, 'SUISA' LIPA_NAME, 'YES' LIPA_VALUE, 'Ja' LIPA_DISP_VALUE  from dual union
select 'VEREIN' LIPA_MODULE, 'SUISA' LIPA_NAME, 'NO' LIPA_VALUE, 'Nein' LIPA_DISP_VALUE  from dual 
) d
on (d.LIPA_MODULE= l.LIPA_MODULE and d.LIPA_NAME = l.LIPA_NAME and d.LIPA_VALUE = l.LIPA_VALUE)
WHEN NOT MATCHED THEN INSERT (l.LIPA_MODULE, l.LIPA_NAME, l.LIPA_TYPE, l.LIPA_DISP_VALUE,l. LIPA_VALUE)
values (D.LIPA_MODULE, D.LIPA_NAME, 'CHR', D.LIPA_DISP_VALUE, d.LIPA_VALUE)
/  

prompt Militärstatus

merge into cm_list_parameter l
using (
select 'MILITAER' LIPA_MODULE, 'STATUS' LIPA_NAME, 'ex' LIPA_VALUE, 'Ex-Military' LIPA_DISP_VALUE  from dual union
select 'MILITAER' LIPA_MODULE, 'STATUS' LIPA_NAME, 'm' LIPA_VALUE, 'Military' LIPA_DISP_VALUE  from dual  union
select 'MILITAER' LIPA_MODULE, 'STATUS' LIPA_NAME, 'no' LIPA_VALUE, 'No Military' LIPA_DISP_VALUE  from dual  union
select 'MILITAER' LIPA_MODULE, 'STATUS' LIPA_NAME, 'na' LIPA_VALUE, 'n/a' LIPA_DISP_VALUE  from dual 
) d
on (d.LIPA_MODULE= l.LIPA_MODULE and d.LIPA_NAME = l.LIPA_NAME and d.LIPA_VALUE = l.LIPA_VALUE)
WHEN NOT MATCHED THEN INSERT (l.LIPA_MODULE, l.LIPA_NAME, l.LIPA_TYPE, l.LIPA_DISP_VALUE,l. LIPA_VALUE)
values (D.LIPA_MODULE, D.LIPA_NAME, 'CHR', D.LIPA_DISP_VALUE, d.LIPA_VALUE)
/  


prompt Ehrung 

merge into cm_list_parameter l
using (
select 'MITGLIED' LIPA_MODULE, 'EHRUNG' LIPA_NAME, '1' LIPA_VALUE, 'keine Ehrung erwünscht' LIPA_DISP_VALUE  from dual union
select 'MITGLIED' LIPA_MODULE, 'EHRUNG' LIPA_NAME, '0' LIPA_VALUE, 'Ehrung erwünscht' LIPA_DISP_VALUE  from dual  
) d
on (d.LIPA_MODULE= l.LIPA_MODULE and d.LIPA_NAME = l.LIPA_NAME and d.LIPA_VALUE = l.LIPA_VALUE)
WHEN NOT MATCHED THEN INSERT (l.LIPA_MODULE, l.LIPA_NAME, l.LIPA_TYPE, l.LIPA_DISP_VALUE,l. LIPA_VALUE)
values (D.LIPA_MODULE, D.LIPA_NAME, 'CHR', D.LIPA_DISP_VALUE, d.LIPA_VALUE)
/  

prompt EhrenMitglied

merge into cm_list_parameter l
using (
select 'MITGLIED' LIPA_MODULE, 'EHRENMITGLIED' LIPA_NAME, '1' LIPA_VALUE, 'Ehrenmitglied' LIPA_DISP_VALUE  from dual  union
select 'MITGLIED' LIPA_MODULE, 'EHRENMITGLIED' LIPA_NAME, '2' LIPA_VALUE, 'Freimitglied' LIPA_DISP_VALUE  from dual union
select 'MITGLIED' LIPA_MODULE, 'EHRENMITGLIED' LIPA_NAME, '3' LIPA_VALUE, 'Träger der goldenen Ehrennadel' LIPA_DISP_VALUE  from dual  union
select 'MITGLIED' LIPA_MODULE, 'EHRENMITGLIED' LIPA_NAME, '0' LIPA_VALUE, 'Nein' LIPA_DISP_VALUE  from dual   
) d
on (d.LIPA_MODULE= l.LIPA_MODULE and d.LIPA_NAME = l.LIPA_NAME and d.LIPA_VALUE = l.LIPA_VALUE)
WHEN NOT MATCHED THEN INSERT (l.LIPA_MODULE, l.LIPA_NAME, l.LIPA_TYPE, l.LIPA_DISP_VALUE,l. LIPA_VALUE)
values (D.LIPA_MODULE, D.LIPA_NAME, 'CHR', D.LIPA_DISP_VALUE, d.LIPA_VALUE)
/  

prompt merge Anrede
delete from cm_list_parameter where LIPA_MODULE = 'MITGLIED' and LIPA_NAME ='ANREDE'
/

merge into cm_list_parameter l
using (
select distinct 'MITGLIED' LIPA_MODULE, 'ANREDE' LIPA_NAME,  anrede LIPA_VALUE , anrede LIPA_DISP_VALUE from personen@stpv_old
) d
on (d.LIPA_MODULE= l.LIPA_MODULE and d.LIPA_NAME = l.LIPA_NAME and d.LIPA_VALUE = l.LIPA_VALUE)
WHEN NOT MATCHED THEN INSERT (l.LIPA_MODULE, l.LIPA_NAME, l.LIPA_TYPE, l.LIPA_DISP_VALUE,l. LIPA_VALUE)
values (D.LIPA_MODULE, D.LIPA_NAME, 'CHR', D.LIPA_DISP_VALUE, d.LIPA_VALUE)
/  

delete from cm_list_parameter
where lipa_name in ('ANREDE')
and lipa_value in ('Monsieur', 'Madame', 'Mlle')
/

commit
/

prompt merge Ausbildung
delete from cm_list_parameter where LIPA_MODULE = 'MITGLIED' and LIPA_NAME ='AUSBILDUNG'
/
merge into cm_list_parameter l
using (

select distinct 'AUSBILDUNG' LIPA_MODULE, 'MODUL' LIPA_NAME,  'LEITER' LIPA_VALUE , 'Leiterkurs' LIPA_DISP_VALUE, 1 LIPA_SEQ from dual union
select distinct 'AUSBILDUNG' LIPA_MODULE, 'MODUL' LIPA_NAME,  'J+M' LIPA_VALUE , 'Jugend+Musik' LIPA_DISP_VALUE, 2 LIPA_SEQ from dual union
select distinct 'AUSBILDUNG' LIPA_MODULE, 'MODUL' LIPA_NAME,  'JURY' LIPA_VALUE , 'Jury' LIPA_DISP_VALUE, 3 LIPA_SEQ from dual union
select distinct 'AUSBILDUNG' LIPA_MODULE, 'MODUL' LIPA_NAME,  'INSTRUMENTAL' LIPA_VALUE , 'Instrumentalkurs' LIPA_DISP_VALUE, 4 LIPA_SEQ from dual union


select distinct 'AUSBILDUNG' LIPA_MODULE, 'LEITER' LIPA_NAME,  'LEITERKURS_1' LIPA_VALUE , 'Leiterkurs 1' LIPA_DISP_VALUE, 1 LIPA_SEQ from dual union
select distinct 'AUSBILDUNG' LIPA_MODULE, 'LEITER' LIPA_NAME,  'LEITERKURS_2' LIPA_VALUE , 'Leiterkurs 2' LIPA_DISP_VALUE, 2 LIPA_SEQ from dual union
select distinct 'AUSBILDUNG' LIPA_MODULE, 'LEITER' LIPA_NAME,  'LEITERKURS_3' LIPA_VALUE , 'Leiterkurs 3' LIPA_DISP_VALUE, 3 LIPA_SEQ from dual union
select distinct 'AUSBILDUNG' LIPA_MODULE, 'LEITER' LIPA_NAME,  'LEITER_JUNG' LIPA_VALUE , 'Leiter Jungtambour/-pfeifer' LIPA_DISP_VALUE, 4 LIPA_SEQ from dual union

select distinct 'AUSBILDUNG' LIPA_MODULE, 'J+M' LIPA_NAME,  'J+M_LEITER' LIPA_VALUE , 'J+M-Leiter' LIPA_DISP_VALUE, 1 LIPA_SEQ from dual union
select distinct 'AUSBILDUNG' LIPA_MODULE, 'J+M' LIPA_NAME,  'J+M_AUSBILDNER' LIPA_VALUE , 'J+M-Ausbildner' LIPA_DISP_VALUE, 2 LIPA_SEQ from dual union
select distinct 'AUSBILDUNG' LIPA_MODULE, 'J+M' LIPA_NAME,  'J+M_EXPERTE' LIPA_VALUE , 'J+M-Experte' LIPA_DISP_VALUE, 3 LIPA_SEQ from dual union

select distinct 'AUSBILDUNG' LIPA_MODULE, 'JURY' LIPA_NAME,  'Jury' LIPA_VALUE , 'Jurygrundkurs' LIPA_DISP_VALUE, 1 LIPA_SEQ from dual union
--select distinct 'AUSBILDUNG' LIPA_MODULE, 'JURY' LIPA_NAME,  'TPer' LIPA_VALUE , 'TPer' LIPA_DISP_VALUE, 2 LIPA_SEQ from dual union
--select distinct 'AUSBILDUNG' LIPA_MODULE, 'JURY' LIPA_NAME,  'Mixed' LIPA_VALUE , 'Mixed' LIPA_DISP_VALUE, 3 LIPA_SEQ from dual union
--select distinct 'AUSBILDUNG' LIPA_MODULE, 'JURY' LIPA_NAME,  'Weiterbildung' LIPA_VALUE , 'Weiterbildung' LIPA_DISP_VALUE, 4 LIPA_SEQ from dual union
select distinct 'AUSBILDUNG' LIPA_MODULE, 'INSTRUMENTAL' LIPA_NAME,  'UNTERSTUFE' LIPA_VALUE , 'Unterstufe' LIPA_DISP_VALUE, 1 LIPA_SEQ from dual union
select distinct 'AUSBILDUNG' LIPA_MODULE, 'INSTRUMENTAL' LIPA_NAME,  'MITTELSTUFE' LIPA_VALUE , 'Mittelstufe' LIPA_DISP_VALUE, 2 LIPA_SEQ from dual union
select distinct 'AUSBILDUNG' LIPA_MODULE, 'INSTRUMENTAL' LIPA_NAME,  'OBERSTUFE' LIPA_VALUE , 'Oberstufe' LIPA_DISP_VALUE, 3 LIPA_SEQ from dual
) d
on (d.LIPA_MODULE= l.LIPA_MODULE and d.LIPA_NAME = l.LIPA_NAME and d.LIPA_VALUE = l.LIPA_VALUE)
WHEN NOT MATCHED THEN INSERT (l.LIPA_MODULE, l.LIPA_NAME, l.LIPA_TYPE, l.LIPA_DISP_VALUE,l. LIPA_VALUE, LIPA_SEQ)
values (D.LIPA_MODULE, D.LIPA_NAME, 'CHR', D.LIPA_DISP_VALUE, d.LIPA_VALUE, d.LIPA_SEQ)
when matched then update set 
  l.LIPA_DISP_VALUE = d.LIPA_DISP_VALUE
/  

commit
/

prompt kompositionstyp
--select 'select ''KOMPOSITION'' LIPA_MODULE, ''KOMPOSIITONSTYP'' LIPA_NAME,  ''' || typseq || ''' LIPA_VALUE , '''|| bezeichnung  ||''' LIPA_DISP_VALUE, 1 LIPA_SEQ from dual union' from kompositionstyp@stpv_old; 
merge into cm_list_parameter l
using (
select 'KOMPOSITION' LIPA_MODULE, 'KOMPOSIITONSTYP' LIPA_NAME,  '1' LIPA_VALUE , 'Trommel-Komposition' LIPA_DISP_VALUE, 1 LIPA_SEQ from dual union
select 'KOMPOSITION' LIPA_MODULE, 'KOMPOSIITONSTYP' LIPA_NAME,  '2' LIPA_VALUE , 'Trommel-Marsch' LIPA_DISP_VALUE, 1 LIPA_SEQ from dual union
select 'KOMPOSITION' LIPA_MODULE, 'KOMPOSIITONSTYP' LIPA_NAME,  '0' LIPA_VALUE , 'Keine' LIPA_DISP_VALUE, 1 LIPA_SEQ from dual union
select 'KOMPOSITION' LIPA_MODULE, 'KOMPOSIITONSTYP' LIPA_NAME,  '3' LIPA_VALUE , 'BM' LIPA_DISP_VALUE, 1 LIPA_SEQ from dual  
) d
on (d.LIPA_MODULE= l.LIPA_MODULE and d.LIPA_NAME = l.LIPA_NAME and d.LIPA_VALUE = l.LIPA_VALUE)
WHEN NOT MATCHED THEN INSERT (l.LIPA_MODULE, l.LIPA_NAME, l.LIPA_TYPE, l.LIPA_DISP_VALUE,l. LIPA_VALUE, LIPA_SEQ)
values (D.LIPA_MODULE, D.LIPA_NAME, 'CHR', D.LIPA_DISP_VALUE, d.LIPA_VALUE, d.LIPA_SEQ)
when matched then update set 
  l.LIPA_DISP_VALUE = d.LIPA_DISP_VALUE
/  

commit
/