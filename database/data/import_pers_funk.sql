set serveroutput on size unlimited 
/*prompt importing funktion mitglieder

merge into stpv_peve_funk i
using (select peve.*, funk_id from stpv_pers_vere peve, stpv_funktion funk  where funk_code = 'MITGLIED') d
on (i.pefu_peve_id = d.peve_id and i.pefu_funk_id = d.funk_id)
when not matched then insert (i.pefu_peve_id, i.pefu_funk_id) 
values (d.peve_id, d.funk_id)
/
*/


prompt importing Zentralvorstandsfunktionen from personen@stpv_old
begin  
   for l in (with f as
			(select pers_id, (select vere_id from stpv_verein where vere_vereinid='00') vere_id,
					decode(
					   p.rolle_zentralvorstand_stpv
						 , 'Zentralsekretaer_d', (select funk_id from stpv_funktion where funk_code = 'SEKRETAER')
						 , 'Vizepraesident'    , (select funk_id from stpv_funktion where funk_code = 'VIZE')
						 , 'Zentralpraesident' , (select funk_id from stpv_funktion where funk_code = 'PRAESIDENT')
						 , null) funk_id
			from personen@stpv_old p --join personenverein@stpv_old pv on p.personenseq = pv.personenseq
			 join stpv_person np on np.pers_personenseq = p.personenseq 
			where p.rolle_zentralvorstand_stpv is not null )
	     select * from f where funk_id is not null
		) loop
        merge into stpv_pers_vere i
         using (select l.pers_id pers_id , l.vere_id vere_id from dual) d
         on (i.peve_pers_id = d.pers_id and i.peve_vere_id = d.vere_id)
         when not matched then insert (i.peve_pers_id, i.peve_vere_id)
		values (d.pers_id, d.vere_id);
        
        merge into stpv_peve_funk i 
        using (select peve_id, l.funk_id funk_id from  stpv_pers_vere 
            where peve_pers_id = l.pers_id and peve_vere_id = l.vere_id ) d 
        on (i.pefu_peve_id = d.peve_id and i.pefu_funk_id = d.funk_id)
        when not matched then insert (i.pefu_peve_id, i.pefu_funk_id)
           values (d.peve_id, d.funk_id);
	
    end loop;
end;
/

prompt importing Zentralvorstandsfunktion from personenverein@stpv_old
begin  
   for l in (with f as
			(select pers_id, (select vere_id from stpv_verein where vere_vereinid='00') vere_id,
					decode(
					   pv.rolle_zentralvorstand_stpv
						 , 'Zentralsekretaer_d', (select funk_id from stpv_funktion where funk_code = 'SEKRETAER')
						 , 'Vizepraesident'    , (select funk_id from stpv_funktion where funk_code = 'VIZE')
						 , 'Zentralpraesident' , (select funk_id from stpv_funktion where funk_code = 'PRAESIDENT')
						 , 'Zentralsekretaer_f', (select funk_id from stpv_funktion where funk_code = 'SEKRETAER')
						 , 'Mitglied_RPK_STV' , (select funk_id from stpv_funktion where funk_code = 'RECHNUNGSPRUEFER')
						 , 'Leiter_RPK_STV'   , (select funk_id from stpv_funktion where funk_code = 'RECHNUNGSPRUEFER')
						 , 'Zentralkassier'   , (select funk_id from stpv_funktion where funk_code = 'KASSIER')
						 , null) funk_id
			from personenverein@stpv_old pv join stpv_person np on np.pers_personenseq = pv.personenseq 
			where pv.rolle_zentralvorstand_stpv is not null )
	     select * from f where funk_id is not null and exists (select  1 from stpv_person where pers_id = f.pers_id and pers_status = 1)
		) loop        
        merge into stpv_pers_vere i
         using (select l.pers_id pers_id , l.vere_id vere_id from dual) d
         on (i.peve_pers_id = d.pers_id and i.peve_vere_id = d.vere_id)
         when not matched then insert (i.peve_pers_id, i.peve_vere_id)
		values (d.pers_id, d.vere_id);
        
        merge into stpv_peve_funk i 
        using (select peve_id, l.funk_id funk_id from  stpv_pers_vere 
            where peve_pers_id = l.pers_id and peve_vere_id = l.vere_id ) d 
        on (i.pefu_peve_id = d.peve_id and i.pefu_funk_id = d.funk_id)
        when not matched then insert (i.pefu_peve_id, i.pefu_funk_id)
           values (d.peve_id, d.funk_id);
	
    end loop;
end;
/

commit
/


prompt importing Regoinalverbanddsfunktionen from personen@stpv_old
begin  
   for l in (with f as
			(select pers_id, (select vere_id from stpv_verein where vere_vereinid=rv.vereinid) vere_id,
					decode(
					   p.ROLLE_RV
						 , 'Faehnrich', (select funk_id from stpv_funktion where funk_code = 'FAEHNRICH')
						 , 'Faehnrich_RV', (select funk_id from stpv_funktion where funk_code = 'FAEHNRICH')
						 , 'Kassier'    , (select funk_id from stpv_funktion where funk_code = 'KASSIER')
						 , 'Mutationsfuehrer_RV' , (select funk_id from stpv_funktion where funk_code = 'MUTATIONSFUEHRER')
                         , 'Mutfuehrer', (select funk_id from stpv_funktion where funk_code = 'MUTATIONSFUEHRER')
						 , 'Sekretaer'    , (select funk_id from stpv_funktion where funk_code = 'SEKRETAER')
						 , 'Vizepraesident' , (select funk_id from stpv_funktion where funk_code = 'VIZE')
						 , 'Vorstandsmitglied' , (select funk_id from stpv_funktion where funk_code = 'EXTENDED_VORSTAND')
						 , null) funk_id
			from personen@stpv_old p --join personenverein@stpv_old pv on p.personenseq = pv.personenseq
			 join stpv_person np on np.pers_personenseq = p.personenseq 
			 join vereinstammdaten@stpv_old v on v.vereinid = p.vereinid
             join vereinstammdaten@stpv_old rv on v.rvid=rv.vereinid
			where p.ROLLE_RV is not null )
	     select * from f where funk_id is not null
		) loop
        merge into stpv_pers_vere i
         using (select l.pers_id pers_id , l.vere_id vere_id from dual) d
         on (i.peve_pers_id = d.pers_id and i.peve_vere_id = d.vere_id)
         when not matched then insert (i.peve_pers_id, i.peve_vere_id)
		values (d.pers_id, d.vere_id);
        
        merge into stpv_peve_funk i 
        using (select peve_id, l.funk_id funk_id from  stpv_pers_vere 
            where peve_pers_id = l.pers_id and peve_vere_id = l.vere_id ) d 
        on (i.pefu_peve_id = d.peve_id and i.pefu_funk_id = d.funk_id)
        when not matched then insert (i.pefu_peve_id, i.pefu_funk_id)
           values (d.peve_id, d.funk_id);
	
    end loop;
end;
/

prompt importing Regoinalverbanddsfunktionen from personenverein@stpv_old
begin  
   for l in (with f as
			(select pers_id, (select vere_id from stpv_verein where vere_vereinid=rv.vereinid) vere_id,
					decode(
					   p.ROLLE_RV
						 , 'Faehnrich', (select funk_id from stpv_funktion where funk_code = 'FAEHNRICH')
						 , 'Faehnrich_RV', (select funk_id from stpv_funktion where funk_code = 'FAEHNRICH')
						 , 'Kassier'    , (select funk_id from stpv_funktion where funk_code = 'KASSIER')
						 , 'Mutationsfuehrer_RV' , (select funk_id from stpv_funktion where funk_code = 'MUTATIONSFUEHRER')
                         , 'Mutfuehrer', (select funk_id from stpv_funktion where funk_code = 'MUTATIONSFUEHRER')
						 , 'Sekretaer'    , (select funk_id from stpv_funktion where funk_code = 'SEKRETAER')
						 , 'Vizepraesident' , (select funk_id from stpv_funktion where funk_code = 'VIZE')
						 , 'Vorstandsmitglied' , (select funk_id from stpv_funktion where funk_code = 'EXTENDED_VORSTAND')
						 , null) funk_id
			from personenverein@stpv_old p --join personenverein@stpv_old pv on p.personenseq = pv.personenseq
			 join stpv_person np on np.pers_personenseq = p.personenseq 
			 join vereinstammdaten@stpv_old v on v.vereinid = p.vereinid
             join vereinstammdaten@stpv_old rv on v.rvid=rv.vereinid
			where p.ROLLE_RV is not null )
	     select * from f where funk_id is not null
		) loop
        merge into stpv_pers_vere i
         using (select l.pers_id pers_id , l.vere_id vere_id from dual) d
         on (i.peve_pers_id = d.pers_id and i.peve_vere_id = d.vere_id)
         when not matched then insert (i.peve_pers_id, i.peve_vere_id)
		values (d.pers_id, d.vere_id);
        
        merge into stpv_peve_funk i 
        using (select peve_id, l.funk_id funk_id from  stpv_pers_vere 
            where peve_pers_id = l.pers_id and peve_vere_id = l.vere_id ) d 
        on (i.pefu_peve_id = d.peve_id and i.pefu_funk_id = d.funk_id)
        when not matched then insert (i.pefu_peve_id, i.pefu_funk_id)
           values (d.peve_id, d.funk_id);
	
    end loop;
end;
/

prompt import funktionen
declare
   sql_statment varchar2(4000) := 'merge into stpv_peve_funk dst
using
(
  select peve_id 
  , (select funk_id from stpv_funktion where funk_code = ''<FUNKTION>'') funk_id
  from stpv_pers_vere join (
	select (select pers_id from stpv_person where pers_personenseq = personenseq) pers_id
	, (select vere_id from stpv_verein where vere_vereinid = vereinid) vere_id
	, decode(nvl(<FUNKTION_OLD>, ''<NO_VALUE>''), ''<YES_VALUE>'' , 1, ''<NO_VALUE>'', 0, -1) <FUNKTION>
	from <tablename>@stpv_old p
    --where exists (select 1 from PERSONEN@stpv_old pa where mitgliederstatus=''Aktiv'' and p.personenseq=pa.personenseq)
	) po on peve_pers_id = po.pers_id and peve_vere_id = po.vere_id
	where po.<FUNKTION> > 0
) src
on (dst.pefu_peve_id = src.peve_id and dst.pefu_funk_id = src.funk_id)
when not matched then 
insert (pefu_peve_id, pefu_funk_id)
values (src.peve_id, src.funk_id)';
begin
   for t in (select 'PERSONEN' tablename from dual
             union  select 'PERSONENVEREIN' from dual ) 
   loop
	   for f in ( select funk, nvl(funk_old, funk) funk_old, yes_value, 'Nein' no_value from 
					(
	                 select 'SEKRETAER' funk, 'AKTUAR' funk_old, 'Ja' yes_value, 'Nein' no_value from dual 
			   union select 'FAEHNRICH' funk, null, 'Ja' yes_value, 'Nein' no_value  from dual 
			   union select 'JUNGPFEIFER', 'JUNGPFEIFFER', 'Ja' yes_value, 'Nein' no_value  from dual
			   union select 'JUNGTAMBOURENLEITER', null, 'Ja' yes_value, 'Nein' no_value  from dual
			   union select 'KASSIER', null, 'Ja' yes_value, 'Nein' no_value  from dual
			   union select 'MITGLIED', null, 'Ja' yes_value, 'Nein' no_value  from dual
			   union select 'PFEIFER', 'PFEIFFER', 'Ja' yes_value, 'Nein' no_value  from dual
			   union select 'PRAESIDENT', null, 'Ja' yes_value, 'Nein' no_value  from dual
			   union select 'SPIELLEITER', null, 'Ja' yes_value, 'Nein' no_value  from dual
			   union select 'TAMBOURENLEITER', null, 'Ja' yes_value, 'Nein' no_value  from dual
			   union select 'TECHNISCHERLEITER', null, 'Ja' yes_value, 'Nein' no_value  from dual	
			   
               union select 'REDAKTION_TM', null, 'JA' yes_value, 'NONE' no_value  from dual
			   union select 'LEITER_DRUCK', null, 'JA' yes_value, 'Nein' no_value  from dual	
			   union select 'INTERNET', null, 'JA' yes_value, 'Nein' no_value  from dual	
			   union select 'WEBMASTER', null, 'JA' yes_value, 'Nein' no_value  from dual	
			   union select 'CHEFREDAKTOR_TM', null, 'JA' yes_value, 'Nein' no_value  from dual	
			   
			   union select 'JUROR_CLAIRON', 'JUROR_BLAESER', 'Clairon' yes_value, 'NONE' no_value  from dual
			   union select 'JUROR_NATWAERISCH', 'JUROR_BLAESER', 'Natwaerisch' yes_value, 'NONE' no_value  from dual
			   union select 'JUROR_PICOLO', 'JUROR_BLAESER', 'Piccolo' yes_value, 'NONE' no_value  from dual
			   
			   union select 'JUROR_TC', 'JUROR_WETTSPIELE', 'TC' yes_value, 'NONE' no_value  from dual
			   union select 'JUROR_TPER', 'JUROR_WETTSPIELE', 'Tper' yes_value, 'NONE' no_value  from dual
			   union select 'JUROR_TN', 'JUROR_WETTSPIELE', 'TN' yes_value, 'NONE' no_value  from dual
			   union select 'JUROR_TP', 'JUROR_WETTSPIELE', 'TP' yes_value, 'NONE' no_value  from dual
			   
			   union select 'JUROR_TAMBOUR', null, 'Jurymitglied_Tambouren' yes_value, 'NONE' no_value  from dual

			   union select 'KOMPONIST_T', 'KOMPONIST', 'Komponist_Tambouren' yes_value, 'NONE' no_value  from dual
			   union select 'KOMPONIST_B', 'KOMPONIST', 'Komponist_Pfeifer-Blaeser' yes_value, 'NONE' no_value  from dual
			   
			   union select 'EXTENDED_VORSTAND', 'VORSTAND', 'Ja' yes_value, 'Nein' no_value  from dual
			   union select 'BK_NOTEN', 'ROLLE_BK', 'Notenkomission' yes_value, 'NONE' no_value  from dual
			   union select 'BK_MITGLIED', 'ROLLE_BK', 'Mitglied_Blaeserkomission' yes_value, 'NONE' no_value  from dual
			   union select 'BK_STV_LEITER', 'ROLLE_BK', 'Stv_Leiter_Blaeserkomission' yes_value, 'NONE' no_value  from dual
			   union select 'BK_LEITER', 'ROLLE_BK', 'Leiter_BK' yes_value, 'NONE' no_value  from dual
			   union select 'BK_SEKRETAER', 'ROLLE_BK', 'Sekretaer_Blaeserkomission' yes_value, 'NONE' no_value  from dual
			  
			   union select 'TK_SEKRETAER', 'ROLLE_TK', 'Sekretaer_Tambourenkomission' yes_value, 'NONE' no_value  from dual
			   union select 'TK_MITGLIED', 'ROLLE_TK', 'Mitglied_Tambourenkomission' yes_value, 'NONE' no_value  from dual
			   union select 'TK_STV_LEITER', 'ROLLE_TK', 'Stv_Leiter_Tambourenkomission' yes_value, 'NONE' no_value  from dual
			   union select 'TK_LEITER', 'ROLLE_TK', 'Leiter_Tambourenkomission' yes_value, 'NONE' no_value  from dual
			 
 			   union select 'BK_KLAKO_LEITER', 'ROLLE_KLAKO_BK', 'LeiterKlakoBK' yes_value, 'NONE' no_value  from dual
 			   union select 'BK_KLAKO_MITGLIED', 'ROLLE_KLAKO_BK', 'MitgliedKlakoBK' yes_value, 'NONE' no_value  from dual
			   union select 'TK_KLAKO_LEITER', 'ROLLE_KLAKO_TK', 'Leiter_Klako_TV' yes_value, 'NONE' no_value  from dual
 			   union select 'TK_KLAKO_MITGLIED', 'ROLLE_KLAKO_TK', 'Mitglied_Klako_TV' yes_value, 'NONE' no_value  from dual

			   union select 'TK_MITGLIED', 'ROLLE_TK_RV', 'Mitglied_TK_RV' yes_value, 'NONE' no_value  from dual
			   union select 'CHEF_PFEIFER_BLAESER', 'ROLLE_TK_RV', 'Pfeifer-Blaeserchef_RV' yes_value, 'NONE' no_value  from dual
			   union select 'MK_CHEF', 'ROLLE_TK_RV', 'TK-Obmann_RV' yes_value, 'NONE' no_value  from dual

			   union select 'MITGLIED', null, 'Ja' yes_value, 'NONE' no_value  from dual
			   
					)
				)
		loop
		   --dbms_output.put_line(replace(sql_statment, '<FUNKTION>', f.funk));
			dbms_output.put_line('merging Funktion ' || t.tablename || '.<' || f.funk || '>...') ;
			dbms_output.put_line(replace(replace(replace(replace(replace(sql_statment, '<FUNKTION>', f.funk),'<FUNKTION_OLD>', f.funk_old), '<YES_VALUE>', f.yes_value), '<NO_VALUE>', f.no_value), '<tablename>', t.tablename));
		   execute immediate replace(replace(replace(replace(replace(sql_statment, '<FUNKTION>', f.funk), '<FUNKTION_OLD>', f.funk_old),'<YES_VALUE>', f.yes_value), '<NO_VALUE>', f.no_value), '<tablename>', t.tablename);
		end loop;
	end loop;
end;
/
   
/*   
prompt importing JUROR_BLAESER
merge into stpv_peve_funk dst
using
(
  select peve_id 
  , (select funk_id from stpv_funktion where funk_code = 'JUROR_BLAESER') funk_id
  from stpv_pers_vere join (
	select (select pers_id from stpv_person where pers_personenseq = personenseq) pers_id
	, (select vere_id from stpv_verein where vere_vereinid = vereinid) vere_id
	, decode(nvl(JUROR_BLAESER, 0), 'Clairon' , 1, 'NONE', 0, -1) JUROR_BLAESER
	from personen@stpv_old
	) po on peve_pers_id = po.pers_id and peve_vere_id = po.vere_id
	where po.JUROR_BLAESER > 0
	) src
	on (dst.pefu_peve_id = src.peve_id and dst.pefu_funk_id = src.funk_id)
	when not matched then 
	insert (pefu_peve_id, pefu_funk_id)
	values (src.peve_id, src.funk_id)
/
*/

commit
/

prompt check if all person with funktion in a club are actually a member. Result shoul be 0
select * from (
select pv.pers_name, pv.pers_vorname, pv.vere_name, listagg(funk_name, ', ') WITHIN GROUP (ORDER BY funk_name) funktionen
from stpv_v_person_verein pv 
   join stpv_v_peve_funktion
      on pv.peve_id = pefu_peve_id
   join (select pers_id, vere_id, count(*) from stpv_v_person_verein pv join
        stpv_v_peve_funktion on pv.peve_id = pefu_peve_id
        group by pers_id, vere_id
        having count(*) > 1) sq 
      on sq.pers_id = pv.pers_id and sq.vere_id = pv.vere_id 
group by pv.pers_name, pv.pers_vorname, pv.vere_name
order by 1, 2, 3, 4)
where instr(funktionen, 'Mitglied')<0 
/

prompt moving TK un BK Sekretäre to STPV

declare 
 l_vere_id number;
begin
for s in (select * from stpv_v_person_verein join stpv_funktion on instr(stpv_v_person_verein.verein_funktion_ids, to_char(funk_id)) > 0
where funk_code like '%K_SEKRETAER') loop
   select vere_id into l_vere_id from stpv_verein where vere_name = 'Schweizerischer Tambouren- und Pfeiferverband';
   person_pkg.merge_person_verein_funktion(s.pers_id, l_vere_id , s.funk_id );
   if s.peve_vere_id <> l_vere_id then
   person_pkg.merge_person_verein_funktionen(s.vere_id , s.pers_id, 
       replace(person_pkg.get_funktion_ids(s.pers_id, s.vere_id, s.funk_group ), s.funk_id), s.funk_group );
   end if;
end loop;
end;
/

/*
prompt mapping Old vorstand to the new role
update stpv_peve_funk 
set pefu_funk_id = (select funk_id from stpv_funktion where funk_Code ='EXTENDED_VORSTAND') 
where pefu_funk_id = (select funk_id from stpv_funktion where funk_Code ='VORSTAND')
/
*/

prompt assign VORSTAND to 'PRAESIDENT', 'SEKRETAER', 'KASSIER', 'EXTENDED_VORSTAND' in Vereine
declare
  l_id number;
begin
select funk_id into l_id from stpv_funktion where funk_code='VORSTAND';

for p in (select * from stpv_v_peve_funktion 
                   where funk_code in ('EXTENDED_VORSTAND') 
                        )loop
    person_pkg.merge_person_verein_funktion(i_pers_id => p.peve_pers_id, i_vere_id => p.peve_vere_id, i_funk_id=>l_id);
end loop;

for p in (select * from stpv_v_peve_funktion 
                   where peve_vere_id in (
                        select vere_id from stpv_verein where vere_vety_id not in 
                            (select vety_id from stpv_verein_typ where vety_code in ('DV', 'RV') )
                            )
                        and funk_code in ('PRAESIDENT', 'SEKRETAER', 'KASSIER') 
                        )loop
    person_pkg.merge_person_verein_funktion(i_pers_id => p.peve_pers_id, i_vere_id => p.peve_vere_id, i_funk_id=>l_id);
end loop;

for p in (select * from stpv_v_peve_funktion 
                   where peve_vere_id in (
                        select vere_id from stpv_verein where vere_vety_id not in 
                            (select vety_id from stpv_verein_typ where vety_code in ('DV', 'RV') )
                            )
                        and funk_code in ('PRAESIDENT', 'SEKRETAER', 'KASSIER', 'EXTENDED_VORSTAND') 
                        )loop
	/*delete from stpv_peve_funk 
		where pefu_funk_id = p.funk_id
			and pefu_peve_id = p.peve_id;*/
    person_pkg.merge_person_verein_funktion(i_pers_id => p.peve_pers_id, i_vere_id => p.peve_vere_id, i_funk_id=>l_id);

end loop;
end;
/

commit;
prompt todo bitte die funktion vom alten Verein löschen

commit
/