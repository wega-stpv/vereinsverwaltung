 update stpv_person set pers_user_id = null where pers_id not in (
    select peve_pers_id  from stpv_v_peve_funktion where funk_code = 'PRAESIDENT'
    union select pers_id from stpv_person where pers_password is not null
    )
    and pers_user_id is not null;
	
    select pers_name, pers_vorname, pers_user_id, pers_email from stpv_person where  pers_id  in (;
	
    update stpv_person set pers_password = 'eA9BQGVG8jMLPVK' where pers_password is null
    and pers_user_id is not null;
	
	Präsidenten ohne email bzw. user_id:
	select peve_pers_id  from stpv_v_peve_funktion where funk_code = 'PRAESIDENT'
    union select pers_id from stpv_person where pers_password is not null
    ) and pers_user_id is null
    ;
    
	
--Bereinigung der Musikinstrument von Mitglieder, ohne Verein	
declare 
  child_exists EXCEPTION;
  PRAGMA EXCEPTION_INIT(child_exists, -2292); /* raises ORA-02292 */
  
  begin
  for v in (select peve_id from stpv_pers_vere
                where 1=1
                --and peve_pers_id = :P56_pers_id
                and peve_vere_id  in (3223)
                and nvl(PEVE_VEREIN_EHRENMITGLIED, 0) = 0
                and PEVE_VEREIN_VETERAN_SEIT is null
                ) loop
        begin
            delete from stpv_peve_inst where pein_peve_id = v.peve_id;
            delete from stpv_pers_vere where peve_id = v.peve_id;
        exception
            when child_exists then
                 null;
        end;
   end loop;
   end;
/   
commit;


--select * from stpv_verein where vere_name = 'Corps des Fifres et Tambours des Collèges de Lausanne';

begin
for n in (select peve_pers_id pers_id, peve_vere_id vere_id from stpv_pers_vere join stpv_verein on vere_id=peve_vere_id
join stpv_peve_funk on pefu_peve_id = peve_id
where vere_vety_id in (
select vety_id  from stpv_verein_typ where vety_code not in ('RV', 'DV')
)
and pefu_funk_id = (select funk_id from stpv_funktion where funk_code='PRAESIDENT')
) loop
PERSON_PKG.merge_person_verein_funktion(n.pers_id , n.vere_id ,183833);
end loop;
end;
/


--aktive mitglieder korrigieren
declare
funk_mitglied_id number;
begin
 select funk_id into funk_mitglied_id from stpv_funktion where funk_code='MITGLIED';
for n in (select pers_id, vere_id 
            from stpv_v_person_verein
            where 1=1
			and vere_name = 'Merula - Société de Tambours et Fifres - Lausanne' 
			and pers_status = 1
            and nvl(peve_verein_ehrenmitglied, 0) = 0
            and peve_verein_veteran_seit is null
			and peve_verein_austritt is null
        ) loop
PERSON_PKG.merge_person_verein_funktion(n.pers_id , n.vere_id ,funk_mitglied_id);
end loop;
end;
/
commit;


--wer hat das Passwort gewechselt
select pers_name, pers_vorname, Pers_user_id, 
pers_password, person_pkg.hash_password(pers_user_id, 'eA9BQGVG8jMLPVK') from stpv_person
where 1=1
--and pers_password is not null
--and pers_user_id = lower('stamm@opti-mischte.ch')
and pers_password <> person_pkg.hash_password(pers_user_id, 'eA9BQGVG8jMLPVK') 
;


select --c.*, 
'ALTER TABLE '|| table_NAME || ' modify ( ' ||column_name  || ' varchar2(100 BYTE));' s
 from user_tab_cols c 
 where 1=1
 and (column_name like '%MODIFICATION_USER'
    or column_name like '%CREATION_USER')
    and table_name not like 'STPV_V%'
	and c.data_length <> 100
order by table_name, column_name;