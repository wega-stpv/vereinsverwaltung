begin
-- common_pkg.insert_access_item('PERS_NAME', 'RW', 'ADMIN');
-- common_pkg.insert_access_item('PERS_NAME', 'RO', 'KASSIER');
-- common_pkg.insert_access_item('PERS_NAME', 'RO', 'MILITAER');

-- common_pkg.insert_access_item('PERS_VORNAME', 'RW', 'ADMIN');
-- common_pkg.insert_access_item('PERS_VORNAME', 'RO', 'KASSIER');
-- common_pkg.insert_access_item('PERS_VORNAME', 'RO', 'MILITAER');

dbms_output.put_line('MITGLIED_BASIC');
common_pkg.insert_access_item('MITGLIED_BASIC', 'RO', 'KASSIER', 'Zugriff auf folgende Atribute des Mitglieds: STATUS, ANREDE, NAME, VORNAME, GEBURTSDATUM');
common_pkg.insert_access_item('MITGLIED_BASIC', 'RO', 'SEKRETAER');
common_pkg.insert_access_item('MITGLIED_BASIC', 'RO', 'MILITAER');
common_pkg.insert_access_item('MITGLIED_BASIC', 'RO', 'BK_SEKRETAER');
common_pkg.insert_access_item('MITGLIED_BASIC', 'RO', 'TK_SEKRETAER');
common_pkg.insert_access_item('MITGLIED_BASIC', 'RO', 'MK_CHEF');
common_pkg.insert_access_item('MITGLIED_BASIC', 'RW', 'VORSTAND');

common_pkg.insert_access_item('MITGLIED_ADVANCED', 'RW', 'VORSTAND', 'Zugriff auf folgende Atribute des Mitglieds: GESCHLECHT, AHV, BANKVERBINDUNG_IMPORT, KOMMENTAR');
common_pkg.insert_access_item('MITGLIED_ADRESSE', 'RW', 'VORSTAND', 'Zugriff auf folgende Atribute des Mitglieds: STRASSE, PLZ, ORT, LAND, TELEFON_NR');
--common_pkg.insert_access_item('PERS_AHV_NR', 'RW', 'VORSTAND');
common_pkg.insert_access_item('ADRESSE_SEKRETAERE', 'RO', 'SEKRETAER', 'Sichtbarkeit Adresse der Sekretäre');
common_pkg.insert_access_item('ADRESSE_SEKRETAERE', 'RO', 'BK_SEKRETAER');
common_pkg.insert_access_item('ADRESSE_SEKRETAERE', 'RO', 'TK_SEKRETAER');
common_pkg.insert_access_item('ADRESSE_SEKRETAERE', 'RO', 'MK_CHEF');
common_pkg.insert_access_item('ADRESSE_KASSIERS', 'RO', 'SEKRETAER', 'Sichtbarkeit Adresse der Kassiers');
common_pkg.insert_access_item('ADRESSE_KASSIERS', 'RO', 'BK_SEKRETAER');
common_pkg.insert_access_item('ADRESSE_KASSIERS', 'RO', 'TK_SEKRETAER');
common_pkg.insert_access_item('ADRESSE_KASSIERS', 'RO', 'MK_CHEF');
common_pkg.insert_access_item('ADRESSE_KASSIERS', 'RO', 'KASSIER');
common_pkg.insert_access_item('ADRESSE_PRAESIDENTEN', 'RO', 'SEKRETAER', 'Sichtbarkeit Adresse der Präsidenten');
common_pkg.insert_access_item('ADRESSE_PRAESIDENTEN', 'RO', 'BK_SEKRETAER');
common_pkg.insert_access_item('ADRESSE_PRAESIDENTEN', 'RO', 'TK_SEKRETAER');
common_pkg.insert_access_item('ADRESSE_PRAESIDENTEN', 'RO', 'MK_CHEF');
common_pkg.insert_access_item('ADRESSE_PRAESIDENTEN', 'RO', 'KASSIER');
common_pkg.insert_access_item('ADRESSE_VETERANEN', 'RW', 'SEKRETAER', 'Sichtbarkeit Adresse der Veteranen');

common_pkg.insert_access_item('PERS_STERBE_DATUM', 'RW', 'VORSTAND', 'Zugriff auf Mitgliedsattribut Sterbedatm');

--Zuweisen einer Vereinszugehörigkeit
common_pkg.insert_access_item('PERS_VEREINSMITGLIED', 'RW', 'SEKRETAER', 'Recht ein Mitglied einem Verein zuzuweisen');
common_pkg.insert_access_item('PERS_VEREINSMITGLIED', 'RW', 'ADMIN');
common_pkg.insert_access_item('PERS_VEREINSMITGLIED', 'RW', 'VORSTAND');
--Attribute der Prson auf Vereinsebene
common_pkg.insert_access_item('PERS_VEREIN', 'RW', 'VORSTAND', 'Zugriff auf die vereinsspezifischen Mitgliederattribute: Ein-, Austrittsdatum, Mitgliedsbeitrag, Mitgliedbeitrag Text');
common_pkg.insert_access_item('PERS_VEREIN', 'RW', 'ADMIN');

common_pkg.insert_access_item('PERS_USER_ID', 'RW', 'ADMIN', 'Zugriff auf Mitgliedsattribut User-Id');
common_pkg.insert_access_item('PERS_USER_ID', 'RW', 'VORSTAND');
common_pkg.insert_access_item('PERS_PASSWORD', 'RW', 'ADMIN', 'Zugriff auf Mitgliedsattribut Passwort');
common_pkg.insert_access_item('PERS_PASSWORD', 'RW', 'VORSTAND');

common_pkg.insert_access_item('PERS_PHOTO', 'RO', 'SEKRETAER', 'Zugriff auf Mitgliedsattribut Photo');
common_pkg.insert_access_item('PERS_EMAIL', 'RO', 'SEKRETAER', 'Zugriff auf Mitgliedsattribut E-Mail');
common_pkg.insert_access_item('PERS_IBAN', 'RO', 'KASSIER', 'Zugriff auf Mitgliedsattribut IBAN');
common_pkg.insert_access_item('PERS_PHOTO', 'RW', 'VORSTAND');
common_pkg.insert_access_item('PERS_EMAIL', 'RW', 'VORSTAND');
common_pkg.insert_access_item('PERS_IBAN', 'RW', 'VORSTAND');
common_pkg.insert_access_item('PERS_MILITAER_MUSIKER', 'RO', 'SEKRETAER', 'Zugriff auf Mitgliedsattribut Militärmusiker');
common_pkg.insert_access_item('PERS_MILITAER_STATUS', 'RO', 'SEKRETAER', 'Zugriff auf Mitgliedsattribut Militärstatus');

common_pkg.insert_access_item('PERS_MILITAER_MUSIKER', 'RW', 'VORSTAND');
common_pkg.insert_access_item('PERS_MILITAER_STATUS', 'RW', 'VORSTAND');

common_pkg.insert_access_item('PERS_MILITAER_MUSIKER', 'RW', 'MILITAER');
common_pkg.insert_access_item('PERS_MILITAER_STATUS', 'RW', 'MILITAER');
common_pkg.insert_access_item('PERS_MILITAER_EINTEILUNG', 'RW', 'MILITAER', 'Zugriff auf Mitgliedsattribut Militäreinteilung');
common_pkg.insert_access_item('PERS_MILITAER_GRAD', 'RW', 'MILITAER', 'Zugriff auf Mitgliedsattribut Militärgrad');
common_pkg.insert_access_item('PERS_MILITAER_FUNKTION', 'RW', 'MILITAER', 'Zugriff auf Mitgliedsattribut Militräfunktion');

common_pkg.insert_access_item('VERE_VERE_ID', 'RO', 'VORSTAND', 'Zugriff auf Vereinsattribut Verband');
common_pkg.insert_access_item('VERE_VETY_ID', 'RW', 'SEKRETAER', 'Zugriff auf Vereinsattribut Vereinstyp');
common_pkg.insert_access_item('VERE_VETY_ID', 'RO', 'VORSTAND');

common_pkg.insert_access_item('VERE_VERE_ID', 'RW', 'ADMIN');
common_pkg.insert_access_item('VERE_VERE_ID', 'RW', 'SEKRETAER');
common_pkg.insert_access_item('VERE_ADMIN', 'RW', 'ADMIN', 'Zugriff auf Vereinsattribut Administrator');
common_pkg.insert_access_item('VERE_ADMIN', 'RO', 'SEKRETAER');
common_pkg.insert_access_item('VERE_MILITAER', 'RW', 'ADMIN', 'Zugriff auf Vereinsattribut Militärverantwortlicher');

common_pkg.insert_access_item('VEREIN', 'RW', 'SEKRETAER', 'Zugriff auf Vereinsattribute');
common_pkg.insert_access_item('VEREIN', 'RW', 'VORSTAND');
common_pkg.insert_access_item('VEREIN', 'RW', 'ADMIN');


common_pkg.insert_access_item('EHRENMITGLIED', 'RW', 'SEKRETAER', 'Zugriff auf Verwatung der Ehrenmitglieder');
common_pkg.insert_access_item('EHRENMITGLIED', 'RW', 'ADMIN');
common_pkg.insert_access_item('EHRENMITGLIED', 'RW', 'VORSTAND');
common_pkg.insert_access_item('VETERAN', 'RW', 'SEKRETAER', 'Zugriff auf Veteranenverwaltung');
common_pkg.insert_access_item('VETERAN', 'RW', 'ADMIN');
common_pkg.insert_access_item('VETERAN', 'RW', 'VORSTAND');

common_pkg.insert_access_item('INSTRUMENT', 'RW', 'VORSTAND', 'Zugriff auf Instrumentenverwaltung');
common_pkg.insert_access_item('INSTRUMENT', 'RO', 'BK_SEKRETAER');
common_pkg.insert_access_item('INSTRUMENT', 'RO', 'TK_SEKRETAER');
common_pkg.insert_access_item('INSTRUMENT', 'RO', 'MK_CHEF');
common_pkg.insert_access_item('INSTRUMENT', 'RO', 'MILITAER');

--Mitglieder ohne Verein
common_pkg.insert_access_item('PERSON_OHNE_VEREIN', 'RW', 'SEKRETAER', 'Sichtbarkeit vereinsloser Personen');
common_pkg.insert_access_item('PERSON_OHNE_VEREIN', 'RW', 'ADMIN');
common_pkg.insert_access_item('PERSON_OHNE_VEREIN', 'RW', 'MK_CHEF');
common_pkg.insert_access_item('PERSON_OHNE_VEREIN', 'RW', 'BK_SEKRETAER');
common_pkg.insert_access_item('PERSON_OHNE_VEREIN', 'RW', 'TK_SEKRETAER');
common_pkg.insert_access_item('PERSON_OHNE_VEREIN', 'RW', 'MILITAER');


--Hinzufügen von Vereinen
common_pkg.insert_access_item('ADD_VEREIN', 'RW', 'SEKRETAER', 'Recht Verein hinzuzufügen');

--Hinzufügen von Mitgliedern zu Vereinen
--bereits durch PERS_VEREINSMITGLIED abgedeckt
--common_pkg.insert_access_item('ADD_VEREINS_MITGLIED', 'RW', 'VORSTAND', 'Sichtbarkeit des Knopf Mitglieder einem Verein hinzuzufügen');

common_pkg.insert_access_item('VERBANDLOSER_VEREIN', 'RW', 'SEKRETAER', 'Sichtbarkeit verbandloser Vereine');

common_pkg.insert_access_item('SUISA', 'RW', 'VORSTAND', 'Zugriff auf Menüpunkt SUISA');
common_pkg.insert_access_item('SUISA', 'RW', 'SEKRETAER');

common_pkg.insert_access_item('AUSWERTUNGEN', 'RW', 'VORSTAND', 'Zugriff auf Menüpunkt AUSWERTUNGEN');
common_pkg.insert_access_item('AUSWERTUNGEN', 'RW', 'SEKRETAER');
common_pkg.insert_access_item('AUSWERTUNGEN', 'RW', 'ADMIN');
common_pkg.insert_access_item('AUSWERTUNGEN', 'RW', 'SEKRETAER');
common_pkg.insert_access_item('AUSWERTUNGEN', 'RW', 'MILITAER');

common_pkg.insert_access_item('JAHRESMELDUNG', 'RW', 'VORSTAND', 'Zugriff auf Menüpunkt Hagresmeldung');
common_pkg.insert_access_item('JAHRESMELDUNG', 'RW', 'SEKRETAER');

common_pkg.insert_access_item('MK_KURS', 'RW', 'SEKRETAER', 'Zugriff auf Kursdiplom');
common_pkg.insert_access_item('MK_KURS', 'RW', 'MK_CHEF');
common_pkg.insert_access_item('MK_KURS', 'RW', 'TK_SEKRETAER');
common_pkg.insert_access_item('MK_KURS', 'RW', 'BK_SEKRETAER');
common_pkg.insert_access_item('MK_KURS', 'RW', 'ADMIN');
common_pkg.insert_access_item('MK_KURS', 'RO', 'VORSTAND');

common_pkg.insert_access_item('MK_JURY', 'RW', 'MK_CHEF', 'Zugriff auf Jury/MK Funktion');
common_pkg.insert_access_item('MK_JURY', 'RW', 'TK_SEKRETAER');
common_pkg.insert_access_item('MK_JURY', 'RW', 'TK_SEKRETAER');
common_pkg.insert_access_item('MK_JURY', 'RW', 'ADMIN');

for a in (select distinct acit_item from stpv_access_item ) loop
	common_pkg.insert_access_item(a.acit_item, 'RW', 'ADMIN');
end loop;

for a in (select distinct acit_item from stpv_access_item 
               where acit_item not like ('MK_%') 
			         and acit_item not in ('SUISA', 'ADD_VEREIN', 'PERSON_OHNE_VEREIN', 'VEREIN', 'JAHRESMELDUNG','AUSWERTUNGEN')
	      ) loop
	common_pkg.insert_access_item(a.acit_item, 'RO', 'SEKRETAER');
	common_pkg.insert_access_item(a.acit_item, 'RO', 'KASSIER');
	common_pkg.insert_access_item(a.acit_item, 'RO', 'MILITAER');
	common_pkg.insert_access_item(a.acit_item, 'RO', 'MK_CHEF');
	common_pkg.insert_access_item(a.acit_item, 'RO', 'TK_SEKRETAER');
	common_pkg.insert_access_item(a.acit_item, 'RO', 'BK_SEKRETAER');
end loop;

end;
/

commit
/


begin
for a in (select acfu.acfu_access_code access_code, funk.funk_code, acit.acit_item from STPV_ACIT_FUNK acfu join stpv_Access_item acit on acfu.acfu_acit_id = acit.acit_id
			join stpv_funktion funk on acfu.acfu_funk_id = funk.funk_id
			where funk_code='SEKRETAER' and acfu.acfu_access_code = 'RO') loop
	common_pkg.insert_access_item(a.acit_item, a.access_code, 'PRAESIDENT');
	end loop;
end;
/

commit
/