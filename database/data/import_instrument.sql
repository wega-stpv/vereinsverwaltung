
/*
select distinct rolle_zentralvorstand_STPV from personenverein@stpv_old;

regexp ersetezen
suche: \h*(\w*),
ersetzend durch: select '\U$1\E', '$1', 'MITGLIED' from dual union


*/
select * from stpv_instrument;
merge into stpv_instrument i using 
(
select instrumentid, beschreibung from instrument@stpv_old 
)
 d
on (i.inst_code = d.instrumentid)
when matched then update set 
  i.inst_name = d.beschreibung
when not matched then insert (i.inst_code, i.inst_name)
values (d.instrumentid, d.beschreibung)
/
commit
/


