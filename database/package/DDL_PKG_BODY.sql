--------------------------------------------------------
--  DDL for Package Body DDL_PKG
--------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY DDL_PKG AS


function get_update_string(tablename varchar2, alias varchar2) return CLOB
is  
  set_val_date varchar2(2000);   
  set_val_number varchar2(2000);   
  set_val_char varchar2(2000);
  content varchar2(2000);  
  --str varchar2(32767);
  str CLOB := empty_clob;
  i number := 1;
begin

   set_val_date:='l_new_value:=to_char(:NEW.<ATTRIB>, ''dd.mm.yyyy'');
  l_old_value:=to_char(:OLD.<ATTRIB>, ''dd.mm.yyyy'');  
  ';
   set_val_number:='l_new_value:=to_char(:NEW.<ATTRIB>);
  l_old_value:=to_char(:OLD.<ATTRIB>);  
  ';
	set_val_char:='l_new_value:=:NEW.<ATTRIB>;
  l_old_value:=:OLD.<ATTRIB>;
  ';
  
  content := '
  l_debug_pos := ''POS<POS>'';   	
  <SET_VALUE>
  if nvl(l_new_value, ''gugus'') != nvl(l_old_value, ''gugus'') then
      insert into STPV_AUDIT_TRAIL (AUTR_ACTION_ID, AUTR_TABLENAME, AUTR_TABLENAME_PK_ID, AUTR_ACTION, AUTR_ATTRIBUT, AUTR_OLD_VALUE, AUTR_NEW_VALUE, AUTR_CREATION_USER)
      values(l_action_id, ''<TABLENAME>'', l_pk_id, l_action, ''<ATTRIB>'', l_old_value, l_new_value, l_modification_user);
  end if;';
  
  for ins in (select 
                case when data_type = 'DATE' then replace(content, '<SET_VALUE>', set_val_date)
			       when data_type = 'NUMBER' then replace(content, '<SET_VALUE>', set_val_number)
				   else replace(content, '<SET_VALUE>', set_val_char)
			   end io_insert, 
			   COLUMN_NAME
          from  ALL_TAB_COLUMNS cols where cols.TABLE_NAME = tablename and data_Type <> 'BLOB'
		  and column_name not in (tablename || '_ID', '<ALIAS>_CREATION_USER', '<ALIAS>_CREATION_DATE', '<ALIAS> _MODIFICATION_USER', '<ALIAS> _MODIFICATION_DATE' )
		  order by column_id ) loop		  
          str := str || replace(replace(replace(replace(ins.io_insert, '<TABLENAME>', tablename), '<POS>', lpad(to_char(i), 2, '0')), '<ATTRIB>',ins.COLUMN_NAME), '<ALIAS>', alias) || chr(10);
		  i := i+1;
		  --dbms_output.put_line(length(str));
  end loop;        
  return str;
end;

procedure add_Standard_Columns(tablename varchar2 , alias varchar2) 
is
begin
	for statment in (select 'alter table ' || tablename || ' add '  || alias || '_' || c.columns  str from (
							select 'CREATION_DATE date' columns from dual 
							union 
							select 'MODIFICATION_DATE date' from dual 
							union
							select 'CREATION_USER varchar2(50)' from dual 
							union 
							select 'MODIFICATION_USER varchar(50)' from dual ) c) loop
		begin
			execute immediate statment.str;
		exception
			when others then
				null;
		end;
end loop;

end;

procedure create_trigger(tablename varchar2, alias varchar2, sequence_name varchar2, pk_alias varchar2 := null) 
as
    l_pk_alias varchar2(200) := pk_alias;
	trigger_statement varchar2(4000) := 'CREATE OR REPLACE TRIGGER %ALIAS%_iu_trg 
BEFORE insert or update on %TABLENAME%
referencing old as oldbuffer new as newbuffer
for each row
  
--------------------------------------------------
-- Wega Informatik AG
-- Purpose : Trigger for table %TABLENAME% for Inserting and Updating
--------------------------------------------------
  
declare
  dummy_id number;
  p_username VARCHAR2(200);
  c_trigger_name constant varchar2(100) := ''%ALIAS%_iu_trg'';
begin
  p_username := nvl(apex_custom_auth.get_username, user);

  if inserting then
    IF :newbuffer.%PK_ALIAS% IS NULL THEN
      SELECT %SEQUENCE%.nextval INTO dummy_id FROM dual;
	  :newbuffer.%PK_ALIAS% := dummy_id;
    END IF;	 
    :newbuffer.%ALIAS%_creation_user := p_username;
    :newbuffer.%ALIAS%_creation_date := sysdate;
  end if;

  if updating then
    :newbuffer.%ALIAS%_modification_user := p_username;
    :newbuffer.%ALIAS%_modification_date := sysdate;
	%UPDATE_STATEMENT%
  end if;  

exception
  when others then
      cm_message_log_pkg.log(i_component => ''TRIGGER''
                         , i_function_name  => c_trigger_name        
                         , i_log_category   => ''ERROR''
                         , i_debug_msg=> sqlerrm
                         , i_user_msg =>null);
    raise;    	
end;
';

begin
   if pk_alias is null then
      l_pk_alias := alias ||'_ID';
   end if;
   trigger_statement := replace(trigger_statement, '%TABLENAME%', tablename);
   trigger_statement := replace(trigger_statement, '%ALIAS%', alias);
   trigger_statement := replace(trigger_statement, '%SEQUENCE%', sequence_name);
   trigger_statement := replace(trigger_statement, '%PK_ALIAS%', l_pk_alias);
   if tablename = 'SHP_SHIPMENT_BOX' then
      trigger_statement := replace(trigger_statement, '%UPDATE_STATEMENT%', 'if :newbuffer.SHBO_BOTY_ID is not null and :newbuffer.SHBO_GROSSWEIGHT is null then 
		  :newbuffer.SHBO_GROSSWEIGHT := shipment_pkg.get_box_netto_gross_weight (:newbuffer.shbo_id, :newbuffer.SHBO_BOTY_ID) ;
      end if;');
   else
		trigger_statement := replace(trigger_statement, '%UPDATE_STATEMENT%','');
   end if;
   dbms_output.put_line(trigger_statement);
   execute immediate trigger_statement;
end;


procedure generate_jn_trigger is
   TYPE VarcharTabTyp IS TABLE OF varchar2(100)  INDEX BY BINARY_INTEGER;   
   tablename  VarcharTabTyp ;
   tablealias VarcharTabTyp ;   
   action_short VarcharTabTyp;
   head varchar2(2000);
   foot varchar2(2000);   
   sqlstatment varchar2(9000);
   l_tablename varchar2(100);
   l_pk_column_name varchar2(100);
   
begin
  head := 'create or replace trigger <TABLENAME>_IUD_TRG 
AFTER INSERT OR UPDATE or DELETE on <TABLENAME>
for each row
--------------------------------------------------
-- Wega Informatik AG
-- Purpose : Trigger for table <TABLENAME>
-- Date: <SYSDATE>
--------------------------------------------------
declare
  dummy_id number;
  p_username VARCHAR2(200);
  c_trigger_name constant varchar2(100) := ''<TABLENAME>_iud_trg'';
  l_debug_pos varchar2(10):= ''POS000'';  
  
  l_action_id STPV_AUDIT_TRAIL.AUTR_action_id%type;   
  l_action STPV_AUDIT_TRAIL.AUTR_action%type;     
  l_old_value STPV_AUDIT_TRAIL.AUTR_OLD_VALUE%type;     
  l_new_value STPV_AUDIT_TRAIL.AUTR_NEW_VALUE%type;     
  l_modification_user STPV_AUDIT_TRAIL.AUTR_CREATION_USER%type;
  l_pk_id varchar2(10);
  
begin  
  if inserting then
    l_action := COMMON_PKG.c_action_insert;
    l_modification_user := :NEW.<ALIAS>_CREATION_USER;
    l_pk_id := :NEW.<PK_ID>;
  end if;
  if updating then
    l_action := COMMON_PKG.c_action_update;
    l_modification_user := :NEW.<ALIAS>_MODIFICATION_USER;
    l_pk_id := :NEW.<PK_ID>;
  end if;
  if deleting then
    l_action := COMMON_PKG.c_action_delete;
    l_modification_user := :OLD.<ALIAS>_MODIFICATION_USER;
    l_pk_id := :OLD.<PK_ID>;
  end if;
  
  SELECT STPV_SEQ.nextval INTO l_action_id FROM dual;    
  ';
  
  
  foot := '
exception
  when others then
      cm_message_log_pkg.log(i_component => ''TRIGGER''
                         , i_function_name  => c_trigger_name        
                         , i_log_category   => ''ERROR''
                         , i_debug_msg=> l_debug_pos || '': '' || sqlerrm
                         , i_user_msg =>null);
    raise;    
end;';

  tablename(0) := 'STPV_VEREIN';  
  tablealias (0) := 'VERE';
  tablename(1) := 'STPV_PERSON';
  tablealias (1) := 'PERS';
  tablename(2) := 'STPV_PERS_VERE';
  tablealias (2) := 'PEVE';
  tablename(3) := 'STPV_PEVE_FUNK';
  tablealias (3) := 'PEFU';
     
  FOR i IN tablename.FIRST .. tablename.LAST LOOP
    l_tablename := tablename(i);
	dbms_output.put_line('generating trigger ' || tablename(i) || ':' || chr(10)) ;
	select coco.column_name into l_pk_column_name from ALL_CONSTRAINTS cons, ALL_CONS_COLUMNS coco 
					where cons.table_name = l_tablename 
						and cons.table_name =  coco.table_name
						and cons.constraint_name = coco.constraint_name
						and constraint_type ='P';
    --for act in action.FIRST .. action.LAST loop      
	  begin
		/*dbms_output.put_line( replace(head, '<TABLENAME>', tablename(i)));
		dbms_output.put_line(get_update_string(tablename(i)));
		dbms_output.put_line(replace(foot, '<TABLENAME>', tablename(i)));
		*/
		execute immediate replace(replace(replace(head, '<TABLENAME>', tablename(i)), '<ALIAS>',  tablealias(i)), '<PK_ID>', l_pk_column_name )
	      || get_update_string(tablename(i), tablealias(i))
		  || replace(foot, '<TABLENAME>', tablename(i)) ;
	
		exception 
		 when others then
			dbms_output.put_line( replace(replace(replace(head, '<TABLENAME>', tablename(i)), '<ALIAS>', tablealias(i)), '<PK_ID>', l_pk_column_name ));
			dbms_output.put_line(get_update_string(tablename(i), tablealias(i)));
			dbms_output.put_line(replace(foot, '<TABLENAME>', tablename(i)));
			raise;
		end;
    
  END LOOP;

end;


END DDL_PKG;

/
