create or replace 
PACKAGE BODY CM_MESSAGE_LOG_PKG
AS
--------------------------------------------------
-- Wega Informatik AG
-- Purpose : Common message function
-- $Date: 2020-04-30$
-- $Author: mbl $
--------------------------------------------------

 procedure internal (  p_app_id in varchar2
                     , p_component in varchar2
					 , p_log_category in varchar2
					 , p_user_msg in varchar2
                     , p_debug_msg in varchar2
					 , p_deb_level in number
					 , p_func in varchar2) is
 PRAGMA AUTONOMOUS_TRANSACTION;

  C_FUNCTIONNAME	VARCHAR2(255)  := 'cm_message-log_pkg.cm_message_log';
  C_DEBUG_LEVEL     NUMBER(1)      := 0;
  C_DEBUGPOS		VARCHAR2(10)   := ' POS1';

  p_debug_level   cm_parameter.para_value_1%TYPE;
  p_sessionid     cm_message_log.sid%TYPE;
  
  l_debug_msg     cm_message_log.debug_msg%type;
  l_user_msg      cm_message_log.user_message%type;

BEGIN

  p_sessionid := nvl(to_number(p_app_id), userenv('sessionid'));
  l_debug_msg := substr(p_debug_msg,1,4000);
  l_user_msg  := substr(p_user_msg,1,300);
  
  --p_debug_level := nvl(CM_PARAMETER_PKG.get_parameter_value('Debug level'), 1);
  begin
  SELECT cp.para_value_1 INTO p_debug_level from cm_parameter cp WHERE cp.para_name = 'Debug level';
  exception
  when no_data_found then
	p_debug_level := 1;
  end;
  
   IF p_log_category = 'DEBUG' THEN
   
    IF p_deb_level <= to_number ( p_debug_level) THEN
      INSERT INTO cm_message_log ( sid, component, log_category, user_message, debug_msg, debug_level, function_name)
                      VALUES ( p_sessionid,  p_component, p_log_category, l_user_msg, l_debug_msg, p_deb_level, p_func);
      
    END IF;                  
  ELSE
      INSERT INTO cm_message_log ( sid, component, log_category, user_message, debug_msg, debug_level, function_name)
                      VALUES ( p_sessionid,  p_component, p_log_category, l_user_msg, l_debug_msg, NULL, p_func);  
  END IF;
  commit;
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line ('cm_msg_log  error> '||substr(sqlerrm,1,200));
    raise;
END internal;

PROCEDURE Log (i_app_id in varchar2 default c_app_id -- := apex_application_install.get_application_id
             , i_component in varchar2
			 , i_log_category in varchar2
			 , i_user_msg in varchar2
             , i_debug_msg in varchar2
			 , i_debug_level in number default 1
			 , i_function_name in varchar2) is
    l_bool boolean;
    l_deb_level number;
  BEGIN
     --dbms_output.put_line ('log  debug-level> '||to_char(i_debug_level)); -- Test
    internal( p_app_id=> i_app_id, p_component=>i_component, p_log_category=>i_log_category, p_user_msg=>i_user_msg,
              p_debug_msg=>i_debug_msg, p_deb_level=>i_debug_level, p_func=>i_function_name);
  exception
    when others then    --ignore any errors writing the log
       raise;
END Log;

 procedure log_debug(i_component in varchar2, i_functionname in varchar2,i_pos in varchar2,i_message in varchar2)
  is
    result boolean;
  begin
    cm_message_log_pkg.log(i_component=>i_component
                        , i_log_category=>'DEBUG'
                        , i_debug_level=>'3'
                        , i_function_name=>i_functionname
                        , i_user_msg =>null
                        , i_debug_msg=>i_pos || ': ' || i_message);
    dbms_output.put_line(i_functionname||':'||i_pos||':'||substr(i_message,1,2000));  
  end log_debug;



function getRevision 
    return varchar2 
  is
    c_functionname   constant varchar2(100) := 'getRevision';
    l_debugPos       varchar2(10) := 'POS 000';
  begin
    return c_revision;
  end getRevision;

END CM_MESSAGE_LOG_PKG;
/