create or replace 
PACKAGE CM_PARAMETER_PKG AS 

--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : Common parameter function
-- $Date: 2020-04-30$  
-- $Author: mbl $   
-------------------------------------------------- 

  c_package_name    constant varchar2(100) := 'CM_PARAMETER_PKG';
  c_revision        constant VARCHAR2(20)  := '$Revision: 122 $' ;

  function getRevision return varchar2;

  function get_environment (p_parameter in varchar2) 
    return cm_parameter.para_value_1%type;

  function get_version
    return cm_parameter.para_value_1%type;
    
  function get_parameter_value(p_parameter in varchar2)
    return cm_parameter.para_value_1%type;
    

 
END CM_PARAMETER_PKG;
/