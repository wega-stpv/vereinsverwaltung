--------------------------------------------------------
--  DDL for Package Body PERSON_PKG
--------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY PERSON_PKG AS


function get_id_separator return varchar2
is
begin
	return c_id_separator;
end;

--return the proper display name of a person
function get_person_displayname(i_pers_id stpv_person.pers_id%type) return varchar2
is 
   l_ret varchar2(250);
begin
   select pers_name || ', ' || pers_vorname into l_ret
     from stpv_person 
     where pers_id= i_pers_id   ; 
   return l_ret;
exception
   when no_data_found then
       return i_pers_id ;
	   
end;

--return the proper display address of a person
function get_person_displayaddress (i_pers_id stpv_person.pers_id%type) return varchar2
is 
   l_ret varchar2(250);
begin
   select pers_strasse || ' ' ||  (select land_code from stpv_land where land_id = pers_land_id )|| '-' || pers_plz || ' ' ||  pers_ort addess_display 
     into l_ret
     from stpv_person 
     where pers_id= i_pers_id   ; 
   return l_ret;
exception
   when no_data_found then
       return i_pers_id ;
	   
end;

-- return a with i_separator separated list of the name of the Funktion of the person in the verein
function get_funktionnames(i_pers_id stpv_person.pers_id%type 
    , i_vere_id stpv_verein.vere_id%type := null
	, i_funk_groups varchar2 := null
	, i_separator varchar2 := ' / '
	, i_group_separator varchar2 := c_id_separator) return varchar2
is 
   l_ret varchar2(250);
begin
   select listagg(apex_lang.lang(funk_name), i_separator) WITHIN GROUP (ORDER BY apex_lang.lang(funk_name)) into l_ret
   from stpv_v_peve_funktion where peve_pers_id = i_pers_id
      and peve_vere_id = nvl(i_vere_id, peve_vere_id)
	  and funk_group in (select column_value from table(apex_string.split(nvl(i_funk_groups, funk_group), i_group_separator) ));
	  --= nvl(i_funk_group, funk_group); 
     
   return l_ret;
exception
   when no_data_found then
       return null ;
end;

-- returns a with c_id_separator colon separated list of the ids of the Funktion of the person in the verein. 
-- If i_vere_id is ommited all Funktion of the person are returned 
function get_funktion_ids(i_pers_id stpv_person.pers_id%type 
    , i_vere_id stpv_verein.vere_id%type := null
	, i_funk_groups varchar2 := null
	, i_group_separator varchar2 := c_id_separator) return varchar2
is 
   l_ret varchar2(250);
begin
   select listagg(funk_id, c_id_separator) into l_ret
   from stpv_v_peve_funktion where peve_pers_id = i_pers_id
      and peve_vere_id = nvl(i_vere_id, peve_vere_id)
	  and funk_group in (select column_value from table(apex_string.split(nvl(i_funk_groups, funk_group), i_group_separator) ));
	  --= nvl(i_funk_group, funk_group); 
     
   return l_ret;
exception
   when no_data_found then
       return null ;
end;


-- return a with i_separator separated list of the name of the Instrument of the person in the verein
function get_instrumentnames(i_pers_id stpv_person.pers_id%type 
    , i_vere_id stpv_verein.vere_id%type := null
	, i_separator varchar2 := ' / ') return varchar2
is 
   l_ret varchar2(250);
begin
   select listagg(apex_lang.lang(inst_name), i_separator) WITHIN GROUP (ORDER BY apex_lang.lang(inst_name)) into l_ret
   from stpv_v_peve_instrument where peve_pers_id = i_pers_id
      and peve_vere_id = nvl(i_vere_id, peve_vere_id);
     
   return l_ret;
exception
   when no_data_found then
       return null ;
end;


-- returns a with c_id_separator colon separated list of the ids of the Instrument of the person in the verein. 
-- If i_vere_id is ommited all Instrument of the person are returned 
function get_instrument_ids(i_pers_id stpv_person.pers_id%type 
    , i_vere_id stpv_verein.vere_id%type := null) return varchar2
is 
   l_ret varchar2(250);
begin
   select listagg(inst_id, c_id_separator) into l_ret
   from stpv_v_peve_instrument where peve_pers_id = i_pers_id
      and peve_vere_id = nvl(i_vere_id, peve_vere_id);
     
   return l_ret;
exception
   when no_data_found then
       return null ;
end;

--return a colon separated list of the person-ids of the attached Funktion
function get_person_ids(l_funk_id stpv_funktion.funk_id%type, i_vere_id stpv_verein.vere_id%type :=  null) return varchar2
is 
   l_ret varchar2(4000);
begin
   select listagg(peve_pers_id, c_id_separator) into l_ret
   from stpv_v_peve_funktion where funk_id = l_funk_id
      and peve_vere_id = nvl(i_vere_id, peve_vere_id)
	  and exists (select  1 from stpv_person where pers_id = peve_pers_id 
	  and pers_status > 0
	  ); 
     
   return l_ret;
exception
   when no_data_found then
       return null ;
end;

--return a with i_separator separated list of the person-displaynames who has the specified funktion in the specified verein
function get_person_displaynames(l_funk_id stpv_funktion.funk_id%type, i_vere_id stpv_verein.vere_id%type := null, i_separator varchar2 := c_id_separator) return varchar2
is 
   l_ret varchar2(4000);
begin
   select substr(listagg(get_person_displayname(peve_pers_id),  i_separator) WITHIN GROUP (ORDER BY get_person_displayname(peve_pers_id)) , 1, 500) into l_ret
   from stpv_v_peve_funktion where funk_id = l_funk_id
      and peve_vere_id = nvl(i_vere_id, peve_vere_id)
	  and exists (select  1 from stpv_person where pers_id = peve_pers_id and pers_status > 0
	  ); 
     
   return l_ret;
exception
   when no_data_found then
       return null ;
end;

-- returns a with i_group_separator separated list of the ids of the verein to which the person is attached.
function get_verein_ids(i_pers_id stpv_person.pers_id%type 
	, i_group_separator varchar2 := c_id_separator) return varchar2
is 
   l_ret varchar2(250);
begin
   select listagg(peve_vere_id, i_group_separator) into l_ret
   from stpv_pers_vere 
   where peve_pers_id = i_pers_id
    and peve_vere_id in (select peve_vere_id from stpv_v_peve_funktion 
                        where  peve_pers_id = i_pers_id
                             and (/*funk_group = 'MITGLIED' or */funk_code = 'MITGLIED')
						);
     
   return l_ret;
exception
   when no_data_found then
       return null ;
end;

/*
   merges the funktion of a person in a verein
   no deletion is done
*/
procedure merge_person_verein_funktion(i_pers_id number, i_vere_id number, i_funk_id number)
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.merge_person_verein_funktion';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := ' POS1';
begin
	CM_MESSAGE_LOG_PKG.LOG_debug(i_component=> 'APPLICATION'
	   , i_message=>'try merging vere_id/pers_id/funk_id:' || i_vere_id || '/' || i_pers_id || '/' || i_funk_id
	   , i_pos=>c_debugpos
	   , i_functionname=> c_functionname);
	merge into stpv_pers_vere peve using 
	(
		select i_vere_id vere_id, i_pers_id pers_id from dual where  i_pers_id is not null
	) inse
	on (nvl(peve_vere_id, -1) = nvl(inse.vere_id, -1) and peve_pers_id = inse.pers_id)
	when not matched then insert (peve.peve_vere_id, peve.peve_pers_id)
	values (inse.vere_id, inse.pers_id);
	
	c_debugpos := ' POS2';	
	merge into stpv_peve_funk pefu using 
	(
		select peve_id, i_funk_id funk_id 
		   from stpv_pers_vere 
		   where peve_pers_id = i_pers_id
			 and peve_vere_id = i_vere_id 
	) inse
	on (pefu.pefu_peve_id = inse.peve_id and pefu.pefu_funk_id = inse.funk_id)
	when not matched then insert (pefu.pefu_peve_id, pefu.pefu_funk_id)
	values (inse.peve_id, inse.funk_id);
	CM_MESSAGE_LOG_PKG.LOG_debug(i_component=> 'APPLICATION'
	   , i_message=>'try merging vere_id/pers_id/funk_id:' || i_vere_id || '/' || i_pers_id || '/' || i_funk_id || '-Done'
	   , i_pos=>c_debugpos
	   , i_functionname=> c_functionname);
exception
   when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;
end;

/*
merges all functions in i_funk_ids of person i_pers_id in the Verein i_vere_id
the id list i_funk_ids must be separated by c_id_separator
*/
procedure merge_person_verein_funktionen(i_vere_id number, i_pers_id number, i_funk_ids varchar2, i_funk_group varchar2 := null)
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.merge_person_verein_funktionen';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := ' POS1';
   l_peve_id number;
begin
	begin
		select peve_id 
		   into l_peve_id
		   from stpv_pers_vere
		   where peve_vere_id = i_vere_id and peve_pers_id = i_pers_id
			or i_vere_id is null and peve_vere_id is null and peve_pers_id = i_pers_id;
		c_debugpos := ' POS2';
		--zuerst die weggefallenen Funktionen löschen
		for f in (
		   --alle bisherigen Funktionen der Person im Verein
			select pefu_funk_id funk_id
				   from stpv_peve_funk join stpv_funktion funk on pefu_funk_id =  funk.funk_id
					where pefu_peve_id = l_peve_id
					    and nvl(i_funk_group, funk_group) = funk_group
			minus
			   --die neuen
			select pefu_funk_id
				   from stpv_peve_funk join stpv_funktion funk on pefu_funk_id =  funk.funk_id
				   where pefu_peve_id = l_peve_id
				     and nvl(i_funk_group, funk_group) = funk_group
					 and pefu_funk_id in (select column_value funk_id from table(apex_string.split(i_funk_ids, c_id_separator) ))
		) loop
			c_debugpos := ' POS3';
			delete from stpv_peve_funk 
			where pefu_funk_id = f.funk_id
				and pefu_peve_id = l_peve_id;
			
		end loop;
		
		c_debugpos := ' POS4';		 
		delete from stpv_pers_vere
		where peve_id = l_peve_id
		  and not exists (select 1 from stpv_peve_funk where pefu_peve_id = l_peve_id)
		  and not exists (select 1 from stpv_peve_inst where pein_peve_id = l_peve_id);
	exception
		when no_data_found  then
			null;
	end;
	c_debugpos := ' POS5';
	--den rest nun mergen
   /*for f in (select funk_code from stpv_funktion where funk_id in (select column_value from table(apex_string.split(i_funk_ids, c_id_separator)))) loop
	merge_personen_verein_funktion(i_vere_id, i_pers_id, f.funk_code);*/
    for f in (select column_value funk_id from table(apex_string.split(i_funk_ids, c_id_separator))) loop
	    c_debugpos := ' POS6';
		CM_MESSAGE_LOG_PKG.LOG_debug(i_component=> 'APPLICATION'
	   , i_message=>'try merging vere_id/pers_id/funk_id:' || i_vere_id || '/' || i_pers_id || '/' || f.funk_id
	   , i_pos=>c_debugpos
	   , i_functionname=> c_functionname);
		merge_person_verein_funktion(i_pers_id, i_vere_id, f.funk_id);
	end loop;
	c_debugpos := ' POS7';
exception
   when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;
end;

/*
the id list i_pers_ids must be separated by c_id_separator
*/
procedure merge_personen_verein_funktion (i_vere_id number, i_pers_ids varchar2, i_funk_code varchar2)
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.merge_personen_verein_funktion';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := ' POS1';
   l_funk_id number;
begin
   CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'DEBUG'
	   , i_user_msg=>null
	   , i_debug_msg=>c_debugpos || '/i_vere_id: ' || i_vere_id || '/i_pers_ids: ' || i_pers_ids || '/i_funk_code: ' || i_funk_code 
	   , i_function_name=> c_functionname);
   select funk_id into l_funk_id from stpv_funktion where funk_code = i_funk_code;
	for peve in (
	   --alle bisherigen
		select peve.peve_id 
		   from stpv_pers_vere peve join stpv_v_peve_funktion on peve.peve_id = pefu_peve_id 
						   and pefu_funk_id = l_funk_id 
		   where peve.peve_vere_id = i_vere_id
		minus
		--die neuen
		select peve.peve_id
		   from stpv_pers_vere peve join stpv_v_peve_funktion on peve.peve_id = pefu_peve_id 
				and pefu_funk_id = l_funk_id 
		   where peve.peve_pers_id in (select column_value pers_id from table(apex_string.split(i_pers_ids, c_id_separator) ))
			 and peve.peve_vere_id = i_vere_id
	) loop
        c_debugpos := ' POS2';
		delete from stpv_peve_funk 
		where pefu_funk_id = l_funk_id
			and pefu_peve_id = peve.peve_id;
		
		c_debugpos := ' POS3';		 
		delete from stpv_pers_vere
		where peve_id = peve.peve_id
		  and not exists (select 1 from stpv_peve_funk where pefu_peve_id = peve.peve_id)
		  and not exists (select 1 from stpv_peve_inst where pein_peve_id = peve.peve_id)
		  ;
	end loop;
	c_debugpos := ' POS4';
	for p in (select column_value pers_id from table(apex_string.split(i_pers_ids, c_id_separator) )) loop
		merge_person_verein_funktion(p.pers_id, i_vere_id, l_funk_id);
	end loop;
	/*merge into stpv_pers_vere peve using 
	(
		select i_vere_id vere_id, column_value pers_id from table(apex_string.split(i_pers_ids, c_id_separator) ) 
	) inse
	on (peve_vere_id = inse.vere_id and peve_pers_id = inse.pers_id)
	when not matched then insert (peve.peve_vere_id, peve.peve_pers_id)
	values (inse.vere_id, inse.pers_id);
	
	c_debugpos := ' POS5';	
	merge into stpv_peve_funk pefu using 
	(
		select peve_id, l_funk_id funk_id 
		   from stpv_pers_vere 
		   where peve_pers_id in (select column_value pers_id from table(apex_string.split(i_pers_ids, c_id_separator) ))
			 and peve_vere_id = i_vere_id 
	) inse
	on (pefu.pefu_peve_id = inse.peve_id and pefu.pefu_funk_id = inse.funk_id)
	when not matched then insert (pefu.pefu_peve_id, pefu.pefu_funk_id)
	values (inse.peve_id, inse.funk_id);
	*/
exception 
   when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;
        
end;

/*
   merges the instrument of a person in a verein
   no deletion is done
*/
procedure merge_person_verein_instrument(i_pers_id number, i_vere_id number, i_inst_id number)
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.merge_person_verein_instrument';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := ' POS1';
begin
	CM_MESSAGE_LOG_PKG.LOG_debug(i_component=> 'APPLICATION'
	   , i_message=>'try merging vere_id/pers_id/inst_id:' || i_vere_id || '/' || i_pers_id || '/' || i_inst_id
	   , i_pos=>c_debugpos
	   , i_functionname=> c_functionname);
	merge into stpv_pers_vere peve using 
	(
		select i_vere_id vere_id, i_pers_id pers_id from dual 
	) inse
	on (nvl(peve_vere_id, -1) = nvl(inse.vere_id, -1) and peve_pers_id = inse.pers_id)
	when not matched then insert (peve.peve_vere_id, peve.peve_pers_id)
	values (inse.vere_id, inse.pers_id);
	
	c_debugpos := ' POS2';	
	merge into stpv_peve_inst pein using 
	(
		select peve_id, i_inst_id inst_id 
		   from stpv_pers_vere 
		   where peve_pers_id = i_pers_id
			 and peve_vere_id = i_vere_id 
	) inse
	on (pein.pein_peve_id = inse.peve_id and pein.pein_inst_id = inse.inst_id)
	when not matched then insert (pein.pein_peve_id, pein.pein_inst_id)
	values (inse.peve_id, inse.inst_id);
exception
   when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;
end;


--merges all instruments in i_inst_ids of person i_pers_id in the Verein i_vere_id
procedure merge_person_verein_instrumente(i_vere_id number, i_pers_id number, i_inst_ids varchar2)
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.merge_person_verein_instrumente';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := ' POS1';
   l_peve_id number;
begin
	begin
		select peve_id 
		   into l_peve_id
		   from stpv_pers_vere
		   where peve_vere_id = i_vere_id and peve_pers_id = i_pers_id
			or i_vere_id is null and peve_vere_id is null and peve_pers_id = i_pers_id;
		c_debugpos := ' POS2';
		--zuerst die weggefallenen Instrumente löschen
		for f in (
		   --alle bisherigen Instrument der Person im Verein
			select pein_inst_id inst_id
				   from stpv_peve_inst 
					where pein_peve_id = l_peve_id
			minus
			   --die neuen
			select pein_inst_id
				   from stpv_peve_inst 
				   where pein_peve_id = l_peve_id
					 and pein_inst_id in (select column_value inst_id from table(apex_string.split(i_inst_ids, c_id_separator) ))
		) loop
			c_debugpos := ' POS3';
			delete from stpv_peve_inst
			where pein_inst_id = f.inst_id
				and pein_peve_id = l_peve_id;
			
		end loop;
		
		c_debugpos := ' POS4';		 

	exception
		when no_data_found  then
			null;
	end;
	c_debugpos := ' POS5';
	--den rest nun mergen
   /*for f in (select funk_code from stpv_funktion where funk_id in (select column_value from table(apex_string.split(i_funk_ids, c_id_separator)))) loop
	merge_personen_verein_funktion(i_vere_id, i_pers_id, f.funk_code);*/
    for f in (select column_value inst_id from table(apex_string.split(i_inst_ids, c_id_separator))) loop
	    c_debugpos := ' POS6';
		CM_MESSAGE_LOG_PKG.LOG_debug(i_component=> 'APPLICATION'
	   , i_message=>'try merging vere_id/pers_id/inst_id:' || i_vere_id || '/' || i_pers_id || '/' || f.inst_id
	   , i_pos=>c_debugpos
	   , i_functionname=> c_functionname);
		merge_person_verein_instrument(i_pers_id, i_vere_id, f.inst_id);
	end loop;
	c_debugpos := ' POS7';
exception
   when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;
end;

/*
function get_password_hash(i_username varchar2)
return varchar2
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.get_password_hash';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := ' POS1';
begin
   return HASH(i_username);
exception 
   when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;
end;
*/


function hash_password (i_pers_user_id stpv_person.pers_user_id%type, i_password varchar2) return varchar2
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.hash';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := ' POS1';
   l_ret varchar2(4000);
begin
   select 
      --standard_hash(upper(i_pers_user_id) || i_password, 'MD5') 
	  DBMS_CRYPTO.HASH(RAWTOHEX(upper(i_pers_user_id) || i_password), DBMS_CRYPTO.HASH_SH256)
      into l_ret
   from dual;
   return l_ret;
   --return standard_hash(i_string, 'MD5') ;
   --return DBMS_CRYPTO.HASH(RAWTOHEX(i_string, DBMS_CRYPTO.HASH_MD5));
exception 
   when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;
        
end;

function authenticate (
    p_username in varchar2,
    p_password in varchar2 )
    return boolean
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.authenticate';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := ' POS1';
    --l_user stpv_person.user_name%type := upper(p_username);
    l_stored_password  stpv_person.pers_password%type;
	l_hashed_password  stpv_person.pers_password%type;
    l_user_id   stpv_person.pers_user_id%type;
begin
    CM_MESSAGE_LOG_PKG.LOG_debug(i_component=> 'APPLICATION'
	   , i_message=>'try fetching user_id:' || p_username 
	   , i_pos=>c_debugpos
	   , i_functionname=> c_functionname);
	   
    select pers_user_id, pers_password, hash_password(pers_user_id, p_password) 
      into l_user_id, l_stored_password, l_hashed_password
      from stpv_person
     where pers_user_id = lower(p_username);
	CM_MESSAGE_LOG_PKG.LOG_debug(i_component=> 'APPLICATION'
	   , i_message=>'fetched  user_id/stored pw/hash:' || l_user_id || '/' || l_stored_password|| '/' || l_hashed_password
	   , i_pos=>c_debugpos
	   , i_functionname=> c_functionname);

	 if l_stored_password = l_hashed_password then 
		APEX_UTIL.SET_AUTHENTICATION_RESULT(0);
		
        CM_MESSAGE_LOG_PKG.LOG_debug(i_component=> 'APPLICATION'
	   , i_message=>'password correct'
	   , i_pos=>c_debugpos
	   , i_functionname=> c_functionname);
	   
		return true;
	 else 
		APEX_UTIL.SET_AUTHENTICATION_RESULT(4);
		
        CM_MESSAGE_LOG_PKG.LOG_debug(i_component=> 'APPLICATION'
	   , i_message=>'password wrong'
	   , i_pos=>c_debugpos
	   , i_functionname=> c_functionname);
	   
		return false;
	 end if ;
--    return l_pwd = rawtohex(sys.dbms_crypto.hash (
 --                      sys.utl_raw.cast_to_raw(p_password||l_id||l_user),
  --                     sys.dbms_crypto.hash_sh512 ));
					   
exception
    when NO_DATA_FOUND then 
		APEX_UTIL.SET_AUTHENTICATION_RESULT(1);
	return false;
	when others then 
        -- We don't know what happened so log an unknown internal error
        APEX_UTIL.SET_AUTHENTICATION_RESULT(7);
        -- And save the SQL Error Message to the Auth Status.
        APEX_UTIL.SET_CUSTOM_AUTH_STATUS(sqlerrm);
        return false;
        
end;

/*
procedure save_hashed_passwd(i_pers_id number, i_password varchar2)
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.save_hashed_passwd';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := ' POS1';
   l_user_id   stpv_person.pers_user_id%type;
   l_password varchar2(4000);
begin
    select hash_password(pers_user_id, i_password) 
      into l_password
      from stpv_person
     where pers_id = i_pers_id;
	 
	  CM_MESSAGE_LOG_PKG.LOG_debug(i_component=> 'APPLICATION'
	   , i_message=>'storing pers_id/hash: ' || i_pers_id|| '/'|| l_password
	   , i_pos=>c_debugpos
	   , i_functionname=> c_functionname);
	   
    update stpv_person set pers_password = l_password where pers_id = i_pers_id;
   --return DBMS_CRYPTO.HASH(RAWTOHEX(i_string, DBMS_CRYPTO.HASH_MD5));
exception 
   when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;     
end;
*/ 

/*
return a value > 0 , if the person has a funktion in any verein
*/
function has_Funktion(p_pers_id stpv_person.pers_id%type, p_funk_code stpv_funktion.funk_code%type) return number
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.has_Funktion';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := ' POS1';
   l_user_id   stpv_person.pers_user_id%type;
   l_password varchar2(4000);
    l_Ret number := 0;
begin
   common_pkg.debug('p_pers_id/p_funk_code' || to_char(p_pers_id) || '/' || p_funk_code, c_functionname, c_debugpos);	
   select count(*) into l_ret from stpv_person join stpv_v_peve_funktion on pers_id = peve_pers_id
	where pers_id = p_pers_id 
	and funk_code=p_funk_code;
   return l_ret;
exception  
   when no_data_found then
      return 0;
	when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;  
end;


function is_Verbandsfunktionaer(p_user_id stpv_person.pers_user_id%type) return number
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.hasRole';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := ' POS1';
   l_Ret number := 0;
begin
	select distinct 1 
	into l_ret
	from stpv_v_person_verein
	where
	exists (
		select 1 from table(apex_string.split(verein_funktion_ids , c_id_separator)) join stpv_funktion 
		on funk_id = column_value 
		where funk_group = 'VEREIN'
		) 
		and verein_funktion_ids is not null
	    and vere_vety_id in (select vety_id from stpv_verein_Typ where vety_name = 'Verband')
		and upper(pers_user_id) = upper(p_user_id);
	return l_ret;

exception  
   when no_data_found then
      return 0;
	when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;  
end;

/*Role is a funktion on Vere-level 0 -> Dachverband if p_vety_id is null
or if p_vety_id is set has a funtion on verein with the specified type
*/

function has_Role(p_user_id stpv_person.pers_user_id%type, p_funk_code stpv_funktion.funk_code%type, p_vety_id number := null) return number
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.hasRole';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := ' POS1';
   l_user_id   stpv_person.pers_user_id%type;
   l_password varchar2(4000);
    l_Ret number := 0;
begin
   select count(*) into l_ret from stpv_person join stpv_v_peve_funktion on pers_id = peve_pers_id
	where upper(pers_user_id) = upper(p_user_id) 
	and funk_code=p_funk_code 
	and peve_vere_id in (select vere_id from stpv_verein where vere_vere_id is null or nvl(p_Vety_id,-1) = vere_vety_id);
   return l_ret;
exception  
   when no_data_found then
      return 0;
	when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;  
end;


/* returns 1 verein with target_vere_id is a member of the verein root_Vere_id*/
function is_member(root_vere_id number, target_vere_id number) 
	return number
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.is_memeber';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := 'POS1';
	l_ret number :=0;
begin
	select count(*) 
		into l_ret 
	from (select vere_id
		,vere_name
		,level
		from 
		stpv_verein
		   start with vere_id = root_vere_id
		   CONNECT BY PRIOR vere_id = vere_vere_id 
		   ) 
	where vere_id = target_vere_id;
    return l_ret;
exception
	when others then
			CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
		   , i_log_category=>'ERROR'
		   , i_user_msg=>SQLERRM
		   , i_debug_msg=>c_debugpos
		   , i_function_name=> c_functionname);
		   raise;  
end
;


/*
   get the the requestor's access_code for specified verein  
*/
function get_access_code_vere(i_item stpv_access_item.acit_item%type
    , i_request_pers_id stpv_person.pers_id%type 
	, i_vere_id stpv_verein.vere_id%type ) 
	return stpv_acit_funk.acfu_access_code%type
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.get_access_code_vere';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := 'POS1';
   l_Ret stpv_acit_funk.acfu_access_code%type;
begin 
   	CM_MESSAGE_LOG_PKG.LOG_debug(i_component=> 'APPLICATION'
	   , i_message=>'try to get access_code from item/i_request_pers_id/vere_id:' || i_item || '/' || i_request_pers_id || '/' || i_vere_id
	   , i_pos=>c_debugpos
	   , i_functionname=> c_functionname);
	   
	with acc as (
		select a.acit_item
		, peve_pers_id 
		, peve_vere_id
		, max(a.acfu_access_code) acfu_access_code
        from stpv_v_accessitem_funktion a 
			join stpv_v_peve_funktion f 
				on f.funk_id = a.funk_id 
		group by a.acit_item
		, peve_pers_id
		, peve_vere_id
        , a.acfu_access_code
		)
     select nvl(max(acfu_access_code), 'NO') 
	 into l_ret 
	 from acc
	 where peve_pers_id = i_request_pers_id 
           and acit_item =i_item
           and person_pkg.is_member (peve_vere_id, i_vere_id)>0 
       ;
    /*      
	with acc as (
		select a.acit_item
		--, pers_user_id
		, peve_pers_id 
		, peve_vere_id
		, max(a.acfu_access_code) acfu_access_code
		from stpv_v_accessitem_funktion a 
			join stpv_v_peve_funktion f 
				on f.funk_id = a.funk_id 
		--	join stpv_person 
		--		on pers_id = peve_pers_id
		group by a.acit_item
		--, pers_user_id
		, peve_pers_id
		, peve_vere_id
		)
	select nvl(max(acfu_access_code), 'NO') into l_ret from acc, 
	(select vere_id
	,vere_name
	,level
	from 
	stpv_verein
	   start with vere_id = i_vere_id
	   --(select distinct peve_vere_id from acc where upper(pers_user_id) = upper(i_pers_user_id) and peve_vere_id = i_vere_id)
	   CONNECT BY PRIOR vere_id = vere_vere_id ) v
	where 
		acit_item = i_item
		--and upper(pers_user_id) = upper(i_pers_user_id)
		and peve_pers_id = i_request_pers_id
        --and peve_vere_id = i_vere_id
		--and vere_id = i_vere_id
		and peve_vere_id = vere_id
--	order by pers_user_id, peve_vere_id
	;
	*/
	common_pkg.debug('Found '|| l_ret, c_functionname, c_debugpos);
	return l_ret;
exception 
	when
		no_data_found then
			return 'NO';
end;

function get_access_code_adresse(
      i_request_user_id stpv_person.pers_user_id%type
	, i_target_pers_id number ) 
	return stpv_acit_funk.acfu_access_code%type
is 
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.get_access_code_adresse';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := 'POS1';
    l_msg  varchar2(4000);
	l_acc_code stpv_acit_funk.acfu_access_code%type := 'NO';
begin
   --l_acc_code := person_pkg.get_access_code('MITGLIED_ADRESSE', i_request_user_id, i_target_pers_id);
   c_debugpos := 'POS2';
   l_msg := 'l_acc_code:' || l_acc_code;
   common_pkg.debug(l_msg, c_functionname, c_debugpos);				
			
   --spezialfälle Adressen von Funktionären
   if l_acc_code<'RO' and  has_funktion(i_target_pers_id, 'SEKRETAER') > 0 then 
        --Addressen von Sekretären 
		c_debugpos := 'POS3';
        l_acc_code  := nvl(person_pkg.get_access_code('ADRESSE_SEKRETAERE', i_request_user_id, i_target_pers_id), l_acc_code);
    end if;
   l_msg := 'l_acc_code:' || l_acc_code;
   common_pkg.debug(l_msg, c_functionname, c_debugpos);		
   
    if l_acc_code<'RO' and  has_funktion(i_target_pers_id, 'KASSIER') > 0then 
        --Addressen von Kassier 
		c_debugpos := 'POS4';
        l_acc_code  := nvl(person_pkg.get_access_code('ADRESSE_KASSIERS', i_request_user_id, i_target_pers_id), l_acc_code) ;
    end if;
   l_msg := 'l_acc_code:' || l_acc_code;
   common_pkg.debug(l_msg, c_functionname, c_debugpos);	
   
    if l_acc_code<'RO' and  has_funktion(i_target_pers_id, 'PRAESIDENT') > 0 then 
        --Addressen von Praesident 
		c_debugpos := 'POS5';
        l_acc_code  := nvl(person_pkg.get_access_code('ADRESSE_PRAESIDENTEN', i_request_user_id, i_target_pers_id), l_acc_code);
    end if;

   l_msg := 'l_acc_code:' || l_acc_code;
   common_pkg.debug(l_msg, c_functionname, c_debugpos);				
    if l_acc_code<'RO' and  has_funktion(i_target_pers_id, 'VETERAN') > 0 then 
        --Addressen von Verteranen 
		c_debugpos := 'POS5';
        l_acc_code  := nvl(person_pkg.get_access_code('ADRESSE_VETERANEN', i_request_user_id, i_target_pers_id), l_acc_code);
    end if;
	
    return l_acc_code;
exception 
	when 
		others then
			CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
		   , i_log_category=>'ERROR'
		   , i_user_msg=>SQLERRM
		   , i_debug_msg=>c_debugpos
		   , i_function_name=> c_functionname);
		   raise;

end;

/* Personen, die noch keinem Verein zugeweisen wurde, dazu benötigt es aber Berechtigunen auf Dachverbandsebene */
function get_access_code_person_ohne_verein(
      i_request_user_id stpv_person.pers_user_id%type
	, i_target_pers_id number := null) 
	return stpv_acit_funk.acfu_access_code%type
is 
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.get_access_code_person_ohne_verein';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := 'POS1';
   l_msg  varchar2(4000);
   l_acc_code stpv_acit_funk.acfu_access_code%type := 'NO';
   l_ohne_verein number := 0;
begin
   l_msg := 'entering: ' || l_acc_code;
   common_pkg.debug(l_msg, c_functionname, c_debugpos);				
	
    select count(*) into l_ohne_verein from stpv_pers_vere where peve_pers_id = nvl(i_target_pers_id, -1); 
	c_debugpos := 'POS2';
    if l_ohne_verein = 0 then
		select max(nvl(person_pkg.get_Access_code('PERSON_OHNE_VEREIN', i_request_user_id, i_target_vere_id=>vere_id), 'RO') )
		   into l_acc_code
		   from stpv_verein
		   where vere_vere_id is null;
    end if;
	
	l_msg := 'found l_acc_code:' || l_acc_code;
	common_pkg.debug(l_msg, c_functionname, c_debugpos);	
   return l_acc_code;
exception 
	when 
		others then
			CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
		   , i_log_category=>'ERROR'
		   , i_user_msg=>SQLERRM
		   , i_debug_msg=>c_debugpos
		   , i_function_name=> c_functionname);
		   raise;

end;


/*
returns the max of requestor's access_code (NO->RO->RW) for target_pers_id and/or target_vere_id
i_target_vety_id is only evaluated in combination of the delivered target_vere_id
todo: change parameter to i_item, i_modifying_user_id stpv_person.pers_user_id%type, i_modified_pers_id number
*/
function get_access_code(i_item stpv_access_item.acit_item%type
    , i_request_user_id stpv_person.pers_user_id%type
	, i_target_pers_id number := null
	, i_target_vere_id number := null
	, i_target_vety_id number := null) 
	return stpv_acit_funk.acfu_access_code%type
is
   c_functionname  VARCHAR2(255)  := 'PERSON_PKG.get_access_code';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := 'POS1';
   l_request_pers_id number;
   l_msg  varchar2(4000);
   l_acc_code stpv_acit_funk.acfu_access_code%type := 'NO';
   l_Ret stpv_acit_funk.acfu_access_code%type := l_acc_code;
begin
	l_msg := 'try to get access_code from item/i_request_user_id/i_target_pers_id/i_target_vere_id:'  
	    || i_item 
		|| '/' || i_request_user_id
		|| '/' || i_target_pers_id
		|| '/' || i_target_vere_id;
	common_pkg.debug(l_msg, c_functionname, c_debugpos);
    select pers_id into l_request_pers_id from stpv_person where upper(pers_user_id) = upper(i_request_user_id);
	
	c_debugpos := 'POS2';
--    for v in (select column_value vere_id from table(apex_string.split(nvl(i_vere_ids, '-1'), ':'))) loop
	if i_target_pers_id is not null then 
		if /*l_acc_code < 'RW' and */ i_item = 'PERSON_OHNE_VEREIN' then
			c_debugpos := 'POS3a';
			common_pkg.debug('l_acc_code/i_item/i_target_pers_id:' || l_acc_code || '/'||i_item|| '/'||i_target_pers_id, c_functionname, c_debugpos);
			l_acc_code := get_access_code_person_ohne_verein(i_request_user_id, i_target_pers_id);
			if l_acc_code > l_ret then
				l_ret :=  l_acc_code;
			end if;
		else
			for v in (select peve_vere_id vere_id from stpv_pers_vere where peve_pers_id = i_target_pers_id) loop
				c_debugpos := 'POS3b';
				
				l_acc_code := nvl(get_access_code_vere(i_item, l_request_pers_id, v.vere_id), 'NO');	
				common_pkg.debug('l_acc_code/i_item:' || l_acc_code || '/'||i_item, c_functionname, c_debugpos);
				if l_acc_code < 'RW' and i_item = 'MITGLIED_ADRESSE' then
					l_acc_code := get_access_code_adresse(i_request_user_id, i_target_pers_id);
				end if;
				
				c_debugpos := 'POS4';
				l_msg := 'got access_code: ' ||l_acc_code;
				common_pkg.debug(l_msg, c_functionname, c_debugpos);
				if l_acc_code > l_ret then
					l_ret :=  l_acc_code;
				end if;
			end loop;
		
		end if;
	elsif i_target_vere_id is not null then
	    /*if i_item = 'VERBANDLOSER_VEREIN' then
		--hier reicht es aus, wenn VERBANDLOSER_VEREIN auf irgendeinem Verein des Typs i_target_vety_id besteht
			for v in (select vere_id from stpv_verein where nvl(i_target_vety_id, vere_vety_id ) = vere_vety_id ) loop
				c_debugpos := 'POS5';
				l_acc_code := get_access_code_vere(i_item, l_request_pers_id, v.vere_id);
				if l_acc_code > l_ret then
					l_ret :=  l_acc_code;
				end if;
			end loop;
		else */
			for v in (select vere_id from stpv_verein 
			                         where (vere_id = i_target_vere_id or i_item = 'VERBANDLOSER_VEREIN') 
									         and nvl(i_target_vety_id, vere_vety_id ) = vere_vety_id ) loop
				c_debugpos := 'POS5';
				l_acc_code := get_access_code_vere(i_item, l_request_pers_id, v.vere_id);
				if l_acc_code > l_ret then
					l_ret :=  l_acc_code;
				end if;
				--l_ret := get_access_code_vere(i_item, l_request_pers_id, v.vere_id);
			end loop;
		--end if;
	elsif i_target_vety_id is not null then
		for v in (select vere_id from stpv_verein where i_target_vety_id = vere_vety_id ) loop
				c_debugpos := 'POS5';
				l_acc_code := get_access_code_vere(i_item, l_request_pers_id, v.vere_id);
				if l_acc_code > l_ret then
					l_ret :=  l_acc_code;
				end if;
				--l_ret := get_access_code_vere(i_item, l_request_pers_id, v.vere_id);
			end loop;
	else
		c_debugpos := 'POS6';
		common_pkg.debug('l_acc_code/i_item/i_target_pers_id:' || l_acc_code || '/'||i_item|| '/'||i_target_pers_id, c_functionname, c_debugpos);
		if i_item = 'PERSON_OHNE_VEREIN' then
			l_acc_code := get_access_code_person_ohne_verein(i_request_user_id);
			if l_acc_code > l_ret then
				l_ret :=  l_acc_code;
			end if;
		else 
		  c_debugpos := 'POS7';
		  common_pkg.debug('l_acc_code/i_item/i_request_user_id:' || l_acc_code || '/'||i_item|| '/'||i_request_user_id, c_functionname, c_debugpos);
		--oder von irgendwo eine Berechtigung auf das item hat
			select nvl(max(acfu_access_code), 'NO') into l_ret
			from stpv_v_accessitem_funktion a
				join stpv_v_peve_funktion f 
					on f.funk_id = a.funk_id 
					where peve_pers_id = (select pers_id from stpv_person where pers_user_id = lower(i_request_user_id))
					and acit_item = i_item
					;
		--l_ret := 'NO';
		end if;

	end if;
	c_debugpos := 'POS7';
	if l_request_pers_id = i_target_pers_id then	   
		c_debugpos := 'POS8';
		if l_ret < 'RO' then
			l_ret := 'RO';
		end if;
	end if;
	return l_ret;
exception 
	when 
		others then
			CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
		   , i_log_category=>'ERROR'
		   , i_user_msg=>SQLERRM
		   , i_debug_msg=>c_debugpos
		   , i_function_name=> c_functionname);
		   raise;  
end;

--delete all dependencies of a person like stpv_pers_vere etc.
procedure delete_person_dependencies(i_pers_id stpv_person.pers_id%type) 
is 

begin
	for p in (select peve_id from stpv_v_person_verein where pers_id = i_pers_id) loop
		delete from stpv_peve_funk where pefu_peve_id = p.peve_id;
		delete from stpv_peve_inst where pein_peve_id = p.peve_id;
	end loop;
	
	delete from STPV_AUSBILDUNG where ausb_pers_id = i_pers_id;
	delete from STPV_WETTSPIEL  where wett_pers_id = i_pers_id;
	delete from stpv_pers_vere where peve_pers_id = i_pers_id;
end;

END PERSON_PKG;
/