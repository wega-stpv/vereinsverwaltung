--------------------------------------------------------
--  DDL for Package PERSON_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE PERSON_PKG AS 
--------------------------------------------------------------------------------
-- PERSON_PKG - Package for APEX module
--------------------------------------------------------------------------------
-- History
-- Date       Version Name              Note
--------------------------------------------------------------------------------
-- 2020-05-05 0.30    M.Blattner     inital

--------------------------------------------------------------------------------
c_id_separator varchar2(1):= ':';

function get_id_separator return varchar2;

--return the proper display name of a person
function get_person_displayname(i_pers_id stpv_person.pers_id%type) return varchar2;

--return the proper display address of a person
function get_person_displayaddress (i_pers_id stpv_person.pers_id%type) return varchar2;

--return a colon seperated list of the ids of the attached funktion
function get_funktion_ids(i_pers_id stpv_person.pers_id%type
                         , i_vere_id stpv_verein.vere_id%type := null
						 , i_funk_groups varchar2 := null
						 , i_group_separator varchar2 := c_id_separator) 
return varchar2;

-- return a colon separated list of the name of the Funktion of the person in the verein
function get_funktionnames(i_pers_id stpv_person.pers_id%type 
    , i_vere_id stpv_verein.vere_id%type := null
	, i_funk_groups varchar2 := null
	, i_separator varchar2 := ' / '
	, i_group_separator varchar2 := c_id_separator
	) return varchar2;
	

--return a colon seperated list of the ids of the attached instruments
function get_instrument_ids(i_pers_id stpv_person.pers_id%type
                         , i_vere_id stpv_verein.vere_id%type := null) 
return varchar2;

-- return a colon separated list of the name of the Instrumente of the person in the verein
function get_instrumentnames(i_pers_id stpv_person.pers_id%type 
    , i_vere_id stpv_verein.vere_id%type := null
	, i_separator varchar2 := ' / '
	) return varchar2;
		
	
--return a colon separated list of the person-ids of the attached funktion
function get_person_ids(l_funk_id stpv_funktion.funk_id%type, i_vere_id stpv_verein.vere_id%type :=  null) 
return varchar2;

-- returns a colon separated list of the ids of the verein to which the person is attached.
function get_verein_ids(i_pers_id stpv_person.pers_id%type 
	, i_group_separator varchar2 := c_id_separator) return varchar2;
	
--   merges the Funktion of a person in a verein.  no deletion is done!
procedure merge_person_verein_funktion(i_pers_id number, i_vere_id number, i_funk_id number);

--merges all functions in i_funk_ids of person i_pers_id in the Verein i_vere_id
procedure merge_person_verein_funktionen(i_vere_id number, i_pers_id number, i_funk_ids varchar2,i_funk_group varchar2 := null);

--merges the function for the list of persons i_pers_ids
procedure merge_personen_verein_funktion (i_vere_id number, i_pers_ids varchar2, i_funk_code varchar2);


-- merges the instrument of a person in a verein --no deletion is done
procedure merge_person_verein_instrument(i_pers_id number, i_vere_id number, i_inst_id number);

--merges all instruments in i_inst_ids of person i_pers_id in the Verein i_vere_id
procedure merge_person_verein_instrumente(i_vere_id number, i_pers_id number, i_inst_ids varchar2);

--return a separated list of the person-displaynames who has the specified funktion in the specified verein
function get_person_displaynames(l_funk_id stpv_funktion.funk_id%type, i_vere_id stpv_verein.vere_id%type := null, i_separator varchar2 := c_id_separator) 
return varchar2;

--function get_password_hash(i_password varchar2) return varchar2;

--procedure save_hashed_passwd(i_pers_id number, i_password varchar2);

function authenticate (
    p_username in varchar2,
    p_password in varchar2 )
    return boolean;

function hash_password (i_pers_user_id stpv_person.pers_user_id%type, i_password varchar2) return varchar2;
--return a value > 0 , if the person has a funktion in any verein
function has_Funktion(p_pers_id stpv_person.pers_id%type, p_funk_code stpv_funktion.funk_code%type) return number;
function has_Role(p_user_id stpv_person.pers_user_id%type, p_funk_code stpv_funktion.funk_code%type, p_vety_id number := null) return number;
function is_Verbandsfunktionaer(p_user_id stpv_person.pers_user_id%type) return number;

/* returns 1 if verein with target_vere_id is a member(leaf) of the verein root_Vere_id*/
function is_member(root_vere_id number, target_vere_id number) 
   return number;
/*returns the max of requestor's access_code (NO->RO->RW) from all vere_ids delivered as a colon separated list */
function get_access_code(
      i_item stpv_access_item.acit_item%type
    , i_request_user_id stpv_person.pers_user_id%type
	, i_target_pers_id number := null
	, i_target_vere_id number := null
	, i_target_vety_id number := null) 
	return stpv_acit_funk.acfu_access_code%type;

--delete all dependencies of a person like stpv_pers_vere etc.
procedure delete_person_dependencies(i_pers_id stpv_person.pers_id%type);

END PERSON_PKG;
/