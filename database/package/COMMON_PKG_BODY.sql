--------------------------------------------------------
--  DDL for Package Body COMMON_PKG
--------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY COMMON_PKG AS

procedure debug(i_msg varchar2, i_functionname varchar2, i_pos varchar2)
is
begin
	CM_MESSAGE_LOG_PKG.LOG_debug(i_component=> 'APPLICATION'
	   , i_message=>i_msg
	   , i_pos=>i_pos
	   , i_functionname=> i_functionname);
end;

function get_lipa_disp_value(
   i_MODULE CM_LIST_PARAMETER.LIPA_MODULE%type
   , i_NAME CM_LIST_PARAMETER.LIPA_NAME%type
   , i_VALUE CM_LIST_PARAMETER.LIPA_VALUE%type) 
return varchar2 
is 
   l_ret CM_LIST_PARAMETER.LIPA_DISP_VALUE%type;
begin
   select lipa_disp_value 
   into  l_ret
   from cm_list_parameter 
   where lipa_module = i_MODULE 
      and lipa_name = i_NAME 
	  and lipa_value = i_value;
	return l_ret;
exception 
   when no_Data_found then
      return nvl(i_value, '');
end;

function get_LOV_PERSON(p_where varchar2 := null) return varchar2
is
   l_lov varchar2(4000) := 'select person_pkg.get_person_displayname(pers_id) display_name,
pers_id v
from stpv_person pers
<where> order by
pers_name, pers_vorname 
';
begin
   return replace(l_lov, '<where>', ' where ' || p_where);
end;

/*
-- returns a colon separated list of the ids of the Funktion of the person in the verein. 
-- If i_vere_id is ommited all Funktion of the person are returned 
function get_access_item_funktion_ids(i_acit_id stpv_access_item.acitid%type
	, i_funk_groups varchar2 := null
	, i_group_separator varchar2 := ':') return varchar2
is 
   l_ret varchar2(250);
begin
   select listagg(funk_id, ':') into l_ret
   from stpv_v_peve_funk where peve_pers_id = i_pers_id
      and peve_vere_id = nvl(i_vere_id, peve_vere_id)
	  and funk_group in (select column_value from table(apex_string.split(nvl(i_funk_groups, funk_group), i_group_separator) ));
	  --= nvl(i_funk_group, funk_group); 
     
   return l_ret;
exception
   when no_data_found then
       return null ;
end;
*/


procedure insert_access_item(i_access_item varchar2, i_access_code STPV_ACIT_FUNK.ACFU_ACCESS_CODE%type
                           , i_funk_CODE STPV_FUNKTION.FUNK_CODE%type
						   , i_acit_description varchar2 default null)
is
   c_functionname  VARCHAR2(255)  := 'COMMON_PKG.insert_access_item';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := 'POS1';
   l_acit_id number;
   l_funk_id number;
begin
    c_debug := 'try insert access_item ' || i_access_item;
    debug(c_debug, c_functionname,c_debugpos);
    merge into stpv_access_item dst
	using (select i_access_item ACIT_ITEM from dual) src
	on (src.ACIT_ITEM = dst.ACIT_ITEM)
	when not matched then
	insert (dst.ACIT_ITEM, dst.acit_description)
	values (src.ACIT_ITEM, i_acit_description)
	when matched then 
	update set dst.acit_description = nvl(i_acit_description, dst.acit_description)
	;
	
	c_debugpos := 'POS2';
	select acit_id into l_acit_id from stpv_access_item where acit_item = i_access_item;
	c_debugpos := 'POS3';
	select funk_id into l_funk_id from stpv_funktion where funk_code = i_funk_CODE;
	
	c_debugpos := 'POS4';
	c_debug := 'try insert acit_id/access_code/funk_id ' || l_acit_id || '/' || i_access_code|| '//' || l_funk_id;
	debug(c_debug, c_functionname,c_debugpos);
	
	merge into STPV_ACIT_FUNK dst
	using (select l_acit_id acit_id, i_access_code ACCESS_CODE, l_Funk_id funk_id from dual) src
	on ( dst.ACFU_ACIT_ID = src.ACIT_ID and dst.ACFU_ACCESS_CODE = src.ACCESS_CODE and dst.ACFU_FUNK_ID = src.funk_id)
	when not matched then
	insert (dst.ACFU_ACIT_ID, dst.ACFU_ACCESS_CODE, dst.ACFU_FUNK_ID)
	values (src.acit_id, src.ACCESS_CODE, src.funk_id);
	
	commit;
	
exception  
	when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;  
end;

procedure merge_meldung_year 
is
   c_functionname  VARCHAR2(255)  := 'COMMON_PKG.merge_suisa_year';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := 'POS1';
begin
    --merge SUISA
    merge into stpv_suisa_meldung dst
	using (select trunc(sysdate, 'YEAR') JAHR, vere_id , 'SUISA' SUME_TYPE
	from stpv_verein wh where vere_status = 1
	                      and wh.vere_suisa = 1) src
	on ( src.vere_id = dst.sume_vere_id and src.jahr = trunc(dst.sume_jahr, 'YEAR') and src.sume_Type=dst.sume_type)
	when not matched then
	insert (dst.sume_jahr, dst.sume_vere_id, dst.sume_type)
	values (src.jahr, src.vere_id, 'SUISA')
	;
	--merge vereinsmeldung
	merge into stpv_suisa_meldung dst
	using (select trunc(sysdate, 'YEAR') JAHR, vere_id , 'VEREIN' SUME_TYPE
	from stpv_verein wh where vere_status = 1) src
	on ( src.vere_id = dst.sume_vere_id and src.jahr = trunc(dst.sume_jahr, 'YEAR') and src.sume_Type=dst.sume_type)
	when not matched then
	insert (dst.sume_jahr, dst.sume_vere_id, dst.sume_type)
	values (src.jahr, src.vere_id, 'VEREIN')
	;
exception  
	when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;  
end;


function get_no_active_members(p_vere_id stpv_verein.vere_id%type) return number
is
   c_functionname  VARCHAR2(255)  := 'COMMON_PKG.get_no_active_members';
   c_debug         VARCHAR2(1000) := NULL;
   c_debugpos      VARCHAR2(10)   := 'POS1';
   l_no number;
begin
    select count(*) 
	into l_no 
	from stpv_v_person_Verein pv where peve_vere_id = p_vere_id 
	and pers_status=1 
	and exists (select 1 from STPV_V_PEVE_FUNKTION where 
	funk_code='MITGLIED'
	and pefu_peve_id = pv.peve_id
    and peve_pers_id = pv.pers_id)
	;	
	return l_no;
	
exception  
	when others then  
	  CM_MESSAGE_LOG_PKG.LOG(i_component=> 'APPLICATION'
	   , i_log_category=>'ERROR'
	   , i_user_msg=>SQLERRM
	   , i_debug_msg=>c_debugpos
	   , i_function_name=> c_functionname);
	   raise;  
end;

END COMMON_PKG;

/ 