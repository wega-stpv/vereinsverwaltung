create or replace 
PACKAGE BODY CM_PARAMETER_PKG AS 

--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : Common parameter function
-- $Date: 2014-10-01 15:57:07 +0200 (Mi, 01 Okt 2014) $  
-- $Author: blattnm1 $  
-- $HeadURL: http://researchdev.roche.com/svn/safety/SafetyPlan/trunk/database/package/cm_parameter_pkg_body.sql $  
-- $Revision: 836 $  
-------------------------------------------------- 

  /*
  little helper functions.
 */

function get_parameter_value(p_parameter in varchar2) return cm_parameter.para_value_1%type
  is
    c_functionname   constant varchar2(100) := 'get_parameter_value';
    l_debugPos       varchar2(10) := 'POS 000';
    l_value_1        cm_parameter.para_value_1%type;   
  begin
    SELECT cmpa.para_value_1
        INTO l_value_1
    FROM cm_parameter cmpa
        WHERE upper(cmpa.para_name) = upper(p_parameter);
        
    return l_value_1;
  exception
    when no_data_found then
        cm_message_log_pkg.log(i_component => c_package_name
                        , i_function_name  => c_functionname        
                        , i_log_category   => 'ERROR'
                        , i_debug_msg=> l_debugpos || ': couldn''t find parameter: ' || p_parameter
                        , i_user_msg =>null);
        return null;
    when others then
        cm_message_log_pkg.log(i_component => c_package_name
                        , i_function_name  => c_functionname        
                        , i_log_category   => 'ERROR'
                        , i_debug_msg=> l_debugpos || ': ' || sqlerrm
                        , i_user_msg =>'looking for parameter: ' || p_parameter);
        return null;
  end get_parameter_value;

function get_environment (p_parameter in varchar2) 
    return cm_parameter.para_value_1%type
is
begin
    return get_parameter_value('Environment');
END get_environment;  

function get_version 
    return cm_parameter.para_value_1%type
is
begin
  return get_parameter_value('version');
END get_version;

function getRevision 
    return varchar2 
  is
    c_functionname   constant varchar2(100) := 'getRevision';
    l_debugPos       varchar2(10) := 'POS 000';
  begin
    return c_revision;
  end getRevision;

END CM_PARAMETER_PKG;
/