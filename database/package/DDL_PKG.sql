--------------------------------------------------------
--  DDL for Package DDL_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE DDL_PKG AS 
--------------------------------------------------------------------------------
-- DDL_PKG - Package for STPV module
--------------------------------------------------------------------------------
-- History
-- Date       Version Name              Note
--------------------------------------------------------------------------------
-- 2020-04-23 0.10    M. Blattner       Initial Version
--------------------------------------------------------------------------------
  
  c_package_name    constant varchar2(100) := 'DDL_PKG';
  c_revision        constant VARCHAR2(20)  := '$Revision: 122 $' ;

	procedure add_Standard_Columns(tablename varchar2 , alias  varchar2) ;
	
	procedure create_trigger(tablename varchar2, alias varchar2, sequence_name varchar2, pk_alias varchar2 := null) ;

	procedure generate_jn_trigger;
	
END DDL_PKG;

/
