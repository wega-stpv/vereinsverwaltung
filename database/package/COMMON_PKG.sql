--------------------------------------------------------
--  DDL for Package COMMON_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE COMMON_PKG AS 
--------------------------------------------------------------------------------
-- COMMON_PKG - Package for APEX shipment module
--------------------------------------------------------------------------------
-- History
-- Date       Version Name              Note
--------------------------------------------------------------------------------
-- 2020-04-09 0.10    M.Blattner     inital
--------------------------------------------------------------------------------
  
  c_package_name    constant varchar2(100) := 'COMMON_PKG';
  c_revision        constant VARCHAR2(20)  := '$Revision: 122 $' ;
  
  c_action_update  constant varchar2(10) := 'UPDATE';
  c_action_insert  constant varchar2(10) := 'INSERT';
  c_action_delete  constant varchar2(10) := 'DELETE';

procedure debug(i_msg varchar2, i_functionname varchar2, i_pos varchar2);

function get_lipa_disp_value(
      i_MODULE CM_LIST_PARAMETER.LIPA_MODULE%type
    , i_NAME CM_LIST_PARAMETER.LIPA_NAME%type
	, i_VALUE CM_LIST_PARAMETER.LIPA_VALUE%type) 
	return varchar2 ;

function get_LOV_PERSON(p_where varchar2 := null) return varchar2;

-- returns a colon separated list of the ids of the Funktion of the person in the verein. 
-- If i_vere_id is ommited all Funktion of the person are returned 
/*function get_funktion_ids(i_pers_id stpv_person.pers_id%type 
    , i_vere_id stpv_verein.vere_id%type := null
	, i_funk_groups varchar2 := null
	, i_group_separator varchar2 := ':') return varchar2;
*/

procedure insert_access_item(i_access_item varchar2, i_access_code STPV_ACIT_FUNK.ACFU_ACCESS_CODE%type
                           , i_funk_CODE STPV_FUNKTION.FUNK_CODE%type
						   , i_acit_description varchar2 default null);

procedure merge_meldung_year; 

function get_no_active_members(p_vere_id stpv_verein.vere_id%type) return number;

END COMMON_PKG;
/