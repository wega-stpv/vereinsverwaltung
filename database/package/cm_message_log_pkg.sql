create or replace 
PACKAGE CM_MESSAGE_LOG_PKG
AS

--------------------------------------------------
-- Wega Informatik AG
-- Purpose : Common message function
-- $Date: 2013-12-20 15:43:51 +0100 (Fr, 20 Dez 2013) $
-- $Author: blattnm1 $
-- $HeadURL: http://researchdev.roche.com/svn/safety/SafetyPlan/trunk/database/package/cm_message_log_pkg_spec.sql $
-- $Revision: 456 $
--------------------------------------------------

  C_FUNCTIONNAME	VARCHAR2(255)  := 'CM_MESSAGE_LOG_PKG';
  C_DEBUG_LEVEL		VARCHAR2(2000) :=  NULL;
  C_DEBUGPOS	  	VARCHAR2(10)   := ' POS0';
  C_VERSION 	  	VARCHAR2(20)   := 'V 1.0.0 23.01.2013';
  
  c_revision        constant VARCHAR2(20)  := '$Revision: 456 $' ;
  
  c_app_id varchar2(20) := '100';

  function getRevision return varchar2;
  
  PROCEDURE Log (i_app_id in varchar2 default c_app_id   -- tbd not possible:= apex_application_install.get_application_id   -- APEX Application ID, falls null, dann wird �ber Package app_id gesetzt
               , i_component in varchar2         -- INSTALL, APPLICATION : APPLICATION_PAGE03
               , i_log_category in varchar2      -- ERROR, INFO, WARNING, BENCHMARK, GENERAL
			   , i_user_msg in varchar2          -- message shown to end user
               , i_debug_msg in varchar2         -- Information
			   , i_debug_level in number default 1 -- 0 -> kein insert in cm_message_log / default 1 -> insert in cm_message_log
			   , i_function_name in varchar2);   -- c_functionname in PLSQL

  /*
  -- Bsp :
  cm_message_log_pkg.log(        
                                  i_component => 'APPLICATION_PAGE25'
                        			 , i_log_category => 'ERROR'
			                         , i_user_msg => SQLERRM
                                     , i_debug_msg => 'Freitext.... mit Varablen etc.'
                                     , i_function_name => c_function_name);

  */

  procedure log_debug(i_component in varchar2, i_functionname in varchar2,i_pos in varchar2,i_message in varchar2);

END CM_MESSAGE_LOG_PKG;
/