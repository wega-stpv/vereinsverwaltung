
exec ddl_pkg.create_trigger('STPV_AUDIT_TRAIL', 'AUTR', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_LAND', 'LAND', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_VEREIN_TYP', 'VETY', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_PERSON', 'PERS', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_FUNKTION', 'FUNK', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_VEREIN', 'VERE', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_PERS_VERE', 'PEVE', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_PEVE_FUNK', 'PEFU', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_PEVE_INST', 'PEIN', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_ACCESS_ITEM', 'ACIT', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_ACIT_FUNK', 'ACFU', 'STPV_SEQ');
exec ddl_pkg.create_trigger('CM_LIST_PARAMETER', 'LIPA', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_SUISA_MELDUNG', 'SUME', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_INSTRUMENT', 'INST', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_AUSBILDUNG', 'AUSB', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_WETTSPIEL', 'WETT', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_KOMPOSITION', 'KOMP', 'STPV_SEQ');
exec ddl_pkg.create_trigger('STPV_SUME_KOMP', 'SUKO', 'STPV_SEQ');

@melo.sql
@pers.sql

