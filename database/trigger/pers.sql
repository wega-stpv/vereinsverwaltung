create or replace TRIGGER PERS_setpassword_trg 
BEFORE insert or update on STPV_PERSON
referencing old as oldbuffer new as newbuffer
for each row

--------------------------------------------------
-- Wega Informatik AG
-- Purpose : Trigger for table STPV_PERSON for Inserting and Updating
--------------------------------------------------

declare
  dummy_id number;
  p_username VARCHAR2(200);
  c_trigger_name constant varchar2(100) := 'PERS_setpassword_trg';
begin
    if inserting then
		if :newbuffer.pers_user_id is not null then
			:newbuffer.pers_user_id := trim(lower(:newbuffer.pers_user_id));
		end if;
		if :newbuffer.pers_password is not null 
		   and :newbuffer.PERS_USER_ID IS NOT NULL
		   and :newbuffer.PERS_ID IS NOT NULL
		   THEN
		     :newbuffer.PERS_PASSWORD := person_pkg.hash_password(:newbuffer.PERS_USER_ID, :newbuffer.PERS_PASSWORD) ;
		END IF;	
	end if;
    if updating then
		if :newbuffer.pers_user_id is not null then
			:newbuffer.pers_user_id := trim(lower(:newbuffer.pers_user_id));
		end if;
		if :newbuffer.pers_password is not null 
		   and :newbuffer.PERS_USER_ID IS NOT NULL
		   and :newbuffer.PERS_ID IS NOT NULL
		   and :newbuffer.PERS_PASSWORD <> nvl(:oldbuffer.PERS_PASSWORD, 'rotz') --nvl(:oldbuffer.PERS_PASSWORD, 'rotz')
		   THEN
		     :newbuffer.PERS_PASSWORD := person_pkg.hash_password(:newbuffer.PERS_USER_ID, :newbuffer.PERS_PASSWORD) ;
		END IF;
    --else 
	--   :newbuffer.PERS_PASSWORD := :oldbuffer.PERS_PASSWORD;
    end if;	

exception
  when others then
      cm_message_log_pkg.log(i_component => 'TRIGGER'
                         , i_function_name  => c_trigger_name        
                         , i_log_category   => 'ERROR'
                         , i_debug_msg=> sqlerrm
                         , i_user_msg =>null);
    raise;    	
end;
