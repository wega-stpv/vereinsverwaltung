create or replace TRIGGER MELO_iu_trg 
BEFORE insert or update on CM_MESSAGE_LOG
referencing old as oldbuffer new as newbuffer
for each row

--------------------------------------------------
-- Wega Informatik AG
-- Purpose : Trigger for table CM_MESSAGE_LOG for Inserting and Updating
--------------------------------------------------

declare
  dummy_id number;
  p_username VARCHAR2(200);
  c_trigger_name constant varchar2(100) := 'MELO_iu_trg';
begin
  p_username := nvl(apex_custom_auth.get_username, user);

  if inserting then
    IF :newbuffer.ID IS NULL THEN
      SELECT STPV_SEQ.nextval INTO dummy_id FROM dual;
	  :newbuffer.ID := dummy_id;
    END IF;	 
    :newbuffer.creation_user := p_username;
    :newbuffer.creation_date := sysdate;
  end if;

  if updating then
    :newbuffer.modification_user := p_username;
    :newbuffer.modification_date := sysdate;
	
  end if;  

exception
  when others then
      cm_message_log_pkg.log(i_component => 'TRIGGER'
                         , i_function_name  => c_trigger_name        
                         , i_log_category   => 'ERROR'
                         , i_debug_msg=> sqlerrm
                         , i_user_msg =>null);
						 
    raise;    	
end;
