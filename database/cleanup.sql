
drop table cm_message_log;
drop table cm_list_parameter;
drop table cm_parameter;

drop table STPV_AUDIT_TRAIL;
drop table STPV_PEVE_FUNK;
drop table STPV_PEVE_INST;
drop table STPV_PERS_VERE;
drop table STPV_ACIT_FUNK;
drop table STPV_ACCESS_ITEM;

drop table STPV_AUSBILDUNG;
drop table STPV_INSTRUMENT; 
drop table STPV_SUME_KOMP;
drop table STPV_WETTSPIEL;
drop table STPV_KOMPOSITION;
drop table STPV_SUISA_MELDUNG;
drop table STPV_FUNKTION;
drop table stpv_person;
drop table stpv_verein;

drop table STPV_LAND; 
drop table STPV_VEREIN_TYP;

drop sequence stpv_seq;

EXECUTE DBMS_STATS.GATHER_SCHEMA_STATS(ownname => 'STPV');