CREATE TABLE STPV_ACCESS_ITEM
(
	ACIT_ID           NUMBER(10),
	ACIT_ITEM         varchar2(100),
	ACIT_CREATION_USER VARCHAR2(30 BYTE) NOT NULL ENABLE,
    ACIT_CREATION_DATE DATE NOT NULL ENABLE,
    ACIT_MODIFICATION_USER VARCHAR2(30 BYTE),
    ACIT_MODIFICATION_DATE DATE,
    CONSTRAINT ACIT_ID_PK PRIMARY KEY (ACIT_ID) using index
)
/

comment on column STPV_ACCESS_ITEM.ACIT_ITEM  is 'either a database table column, a apex page, ...'
/

alter table STPV_ACCESS_ITEM
add ACIT_DESCRIPTION varchar2(500)
/