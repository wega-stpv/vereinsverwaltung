select 'alter table ' ||table_name || ' modify ' ||column_name||' varchar2(100);' 
from ALL_TAB_COLS where table_name like 'STPV_%'
and column_name like '%TION_USER%'
and data_length<> 100;

alter table STPV_VEREIN modify VERE_CREATION_USER varchar2(100);
alter table STPV_VEREIN modify VERE_MODIFICATION_USER varchar2(100);
alter table STPV_VEREIN_TYP modify VETY_CREATION_USER varchar2(100);
alter table STPV_VEREIN_TYP modify VETY_MODIFICATION_USER varchar2(100);