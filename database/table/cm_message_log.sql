--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : InstallationsScript create cm_message_log
-- $Date: 2020-04-30$  
-- $Author: mbl $ 
-------------------------------------------------- 

prompt ## Create Table CM_MESSAGE_LOG


CREATE TABLE CM_MESSAGE_LOG 
   (		ID NUMBER(12,0) NOT NULL ENABLE, 
  SID NUMBER(32) NOT NULL ENABLE, 
	COMPONENT VARCHAR2(30 BYTE) NOT NULL ENABLE, 
  LOG_CATEGORY VARCHAR2 (30) NOT NULL ENABLE,
	USER_MESSAGE VARCHAR2(300 BYTE) , 
  DEBUG_MSG VARCHAR2(4000 BYTE) ,
  DEBUG_LEVEL NUMBER(1) ,
  FUNCTION_NAME VARCHAR2(300 BYTE) ,
  CALL_STACK VARCHAR2(2000 BYTE) ,
	CREATION_USER VARCHAR2(30 BYTE) NOT NULL ENABLE, 
	CREATION_DATE DATE NOT NULL ENABLE, 
	MODIFICATION_USER VARCHAR2(30 BYTE) NULL, 
	MODIFICATION_DATE DATE NULL, 
	 CONSTRAINT CM_MESSAGE_LOG_PK PRIMARY KEY (ID) )
;
 

   COMMENT ON COLUMN CM_MESSAGE_LOG.ID IS 'PK';
   COMMENT ON COLUMN CM_MESSAGE_LOG.SID IS 'Oracle Session ID of user logged in Oracle-function : userenv("sessionid")';
   COMMENT ON COLUMN CM_MESSAGE_LOG.COMPONENT IS 'Architecture-Component by which this message was created: Predefined basic components are (list is extendable): INSTALL �by installation/upgrade kit  APPLICATION �by application at runtime ANY � any other component (DEFAULT)';
   COMMENT ON COLUMN CM_MESSAGE_LOG.LOG_CATEGORY IS 'Category to which this message belongs:
Predefined basic categories are (list is extendable):
ERROR � error message
WARNING � warning message
INFO � info message
BENCHMARK � message logged for benchmarking
GENERAL � any other category (DEFAULT)';
 
   COMMENT ON COLUMN CM_MESSAGE_LOG.USER_MESSAGE IS 'Message as shown to end-user. Can be standard-Oracle messages as well.';
   COMMENT ON COLUMN CM_MESSAGE_LOG.DEBUG_MSG IS 'Information about context in which the exception occurred. This is free text string. Content can be setting variables, debug position-pointer within code, etc.';
   COMMENT ON COLUMN CM_MESSAGE_LOG.DEBUG_LEVEL IS 'Information about the DEBUG LEVEL. ';      
   COMMENT ON COLUMN CM_MESSAGE_LOG.FUNCTION_NAME IS 'Contains always �C_FUNCTIONNAME� with �revision no�.
default is �UNKNOWN�
';
         
   COMMENT ON COLUMN CM_MESSAGE_LOG.CALL_STACK IS 'Oracle-function: dbms_utility.FORMAT_CALL_STACK';
   COMMENT ON COLUMN CM_MESSAGE_LOG.CREATION_USER IS 'Oracle-account name, Oracle-variable : user';
   COMMENT ON COLUMN CM_MESSAGE_LOG.CREATION_DATE IS 'Date/Time on which message occurred Oracle-variable : sysdate';
   COMMENT ON COLUMN CM_MESSAGE_LOG.MODIFICATION_USER IS 'Oracle-account name, Oracle-variable : user';
   COMMENT ON COLUMN CM_MESSAGE_LOG.MODIFICATION_DATE IS 'Date/Time on which message was updated, Oracle-variable : sysdate';
 
