CREATE TABLE STPV_FUNKTION
(
    FUNK_ID    NUMBER(10),
    FUNK_CODE  VARCHAR2(25) not null,
	FUNK_NAME  VARCHAR2(50),
	FUNK_GROUP VARCHAR2(50),
    FUNK_CREATION_USER VARCHAR2(30 BYTE) NOT NULL ENABLE,
    FUNK_CREATION_DATE DATE NOT NULL ENABLE,
    FUNK_MODIFICATION_USER VARCHAR2(30 BYTE),
    FUNK_MODIFICATION_DATE DATE,
    CONSTRAINT FUNK_ID_PK PRIMARY KEY (FUNK_ID) using index,
	constraint FUNK_CODE_UK unique (FUNK_CODE) using index
)
/

comment on  column STPV_FUNKTION.FUNK_GROUP is 'hiermit können Funktionen logisch gruppiert werden. Bereich sind z.B. MITGLIED, VEREIN, MK'
/

ALTER TABLE STPV_FUNKTION
add (
    FUNK_STPV    NUMBER(1,0),
    FUNK_RV      NUMBER(1,0),
    FUNK_VEREIN  NUMBER(1,0)
)
/
comment on  column STPV_FUNKTION.FUNK_STPV is 'Eingabe bzw. Anzeige auf Ebene STPV'
/
comment on  column STPV_FUNKTION.FUNK_RV is 'Eingabe bzw. Anzeige auf Ebene RV'
/
comment on  column STPV_FUNKTION.FUNK_VEREIN is 'Eingabe bzw. Anzeige auf Ebene Verein'
/

alter table STPV_FUNKTION
add FUNK_IS_ROLE number(1,0) default 0
/