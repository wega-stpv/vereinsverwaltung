--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : InstallationsScript create cm_list_parameter
-- $Date: 2020-04-30$  
-- $Author: mbl $    
-------------------------------------------------- 

prompt ## Create Table CM_LIST_PARAMETER


CREATE TABLE CM_LIST_PARAMETER
   ( LIPA_ID NUMBER(12,0) NOT NULL ENABLE, 
     LIPA_MODULE    VARCHAR2(200 BYTE) NOT NULL ENABLE,
     LIPA_NAME      VARCHAR2(100) NOT NULL ENABLE, 
	 lipa_seq       number(10),
     LIPA_TYPE      VARCHAR2(3 BYTE) NOT NULL ENABLE, 
     LIPA_DISP_VALUE   VARCHAR2(200 BYTE) NOT NULL ENABLE,
     LIPA_VALUE        VARCHAR2(200 BYTE) NOT NULL ENABLE, 
     LIPA_DESCRIPTION  VARCHAR2(200 BYTE) ,
     LIPA_EXPIRES_ON   DATE,
     LIPA_CREATION_USER VARCHAR2(30 BYTE) NOT NULL ENABLE, 
     LIPA_CREATION_DATE DATE NOT NULL ENABLE, 
     LIPA_MODIFICATION_USER VARCHAR2(30 BYTE) NULL, 
     LIPA_MODIFICATION_DATE DATE NULL, 
	 CONSTRAINT CM_LIPA_PK PRIMARY KEY (LIPA_ID) ) ;
 
   COMMENT ON COLUMN CM_LIST_PARAMETER.LIPA_ID IS 'PK';
   COMMENT ON COLUMN CM_LIST_PARAMETER.LIPA_MODULE IS 'For grouping PARAMETER';
   COMMENT ON COLUMN CM_LIST_PARAMETER.LIPA_NAME  IS 'identifying name of the parameter';
   COMMENT ON COLUMN CM_LIST_PARAMETER.LIPA_SEQ  IS 'defining sequence of lov';
   COMMENT ON COLUMN CM_LIST_PARAMETER.LIPA_TYPE  IS 'data type the parameter should be of';
   COMMENT ON COLUMN CM_LIST_PARAMETER.LIPA_VALUE  IS 'parameter value';
   COMMENT ON COLUMN CM_LIST_PARAMETER.LIPA_DISP_VALUE  IS 'Display value';
   COMMENT ON COLUMN CM_LIST_PARAMETER.LIPA_DESCRIPTION  IS 'Documentation of the parameter';
   COMMENT ON COLUMN CM_LIST_PARAMETER.LIPA_EXPIRES_ON IS 'Expires on date.';
   COMMENT ON COLUMN CM_LIST_PARAMETER.LIPA_CREATION_USER IS 'User by which row was inserted';
   COMMENT ON COLUMN CM_LIST_PARAMETER.LIPA_CREATION_DATE IS 'User by which row was updated. ';
   COMMENT ON COLUMN CM_LIST_PARAMETER.LIPA_MODIFICATION_USER IS 'User-account by which row was updated. In case dedicated db logon-users, the logon-user is entered here, in 
case of connection pooling, the real username is entered here!';
   COMMENT ON COLUMN CM_LIST_PARAMETER.LIPA_MODIFICATION_DATE IS 'Date/Time on which row was updated. This column can also be used to detect competing updates in J2EE or other 
web-applications When created first, Modification date is the same as creation date.';
 