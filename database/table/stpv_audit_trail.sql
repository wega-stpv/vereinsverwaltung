--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : 
-- $Date: 2015-02-17 10:51:17 +0100 (Di, 17 Feb 2015) $  
-- $Author:  $  
-- $HeadURL:  $  
-- $Revision: 1053 $  
--------------------------------------------------

create table STPV_AUDIT_TRAIL
(
	AUTR_ID                       NUMBER(10),    -- ID 
	AUTR_TABLENAME                varchar2(50),   
	AUTR_TABLENAME_PK_ID          NUMBER(10),  -- PK of the audited table 
	AUTR_ACTION_ID                NUMBER(10),   
	AUTR_ACTION                   VARCHAR2(10),
	AUTR_ATTRIBUT                 VARCHAR2(50),
	AUTR_OLD_VALUE                VARCHAR2(2000), 
	AUTR_NEW_VALUE                VARCHAR2(2000), 
	CONSTRAINT "AUTR_ID_PK" PRIMARY KEY ("AUTR_ID") using index
)
/

exec ddl_pkg.add_Standard_Columns('STPV_AUDIT_TRAIL', 'AUTR')
/
COMMENT ON COLUMN STPV_AUDIT_TRAIL.AUTR_ID                       IS 'ID'
/
COMMENT ON COLUMN STPV_AUDIT_TRAIL.AUTR_TABLENAME                IS 'name of the table to which the modifications belongs '
/
COMMENT ON COLUMN STPV_AUDIT_TRAIL.AUTR_TABLENAME_PK_ID          IS 'ID to identify which modifications belong to the some action (insert, update, delete)'
/
COMMENT ON COLUMN STPV_AUDIT_TRAIL.AUTR_ACTION_ID                IS 'PK of the audited table'
/

create index AUTR_TABLENAME_PK_ID_I on STPV_AUDIT_TRAIL(AUTR_TABLENAME_PK_ID)
/