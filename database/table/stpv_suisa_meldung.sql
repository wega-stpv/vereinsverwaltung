CREATE TABLE STPV_SUISA_MELDUNG
(
    SUME_ID    NUMBER(10),
	SUME_VERE_ID  NUMBER(10),
	SUME_JAHR DATE,
	--SUME_SUISA number(1) default 0,
	SUME_ABGESCHLOSSEN number(1) default 0,
	
    SUME_CREATION_USER VARCHAR2(30 BYTE) NOT NULL ENABLE,
    SUME_CREATION_DATE TIMESTAMP NOT NULL ENABLE,
    SUME_MODIFICATION_USER VARCHAR2(30 BYTE),
    SUME_MODIFICATION_DATE TIMESTAMP,
	
	CONSTRAINT SUME_VERE_FK FOREIGN KEY (SUME_VERE_ID) REFERENCES STPV_VEREIN (VERE_ID), 
	
	CONSTRAINT SUME_ID_PK PRIMARY KEY (SUME_ID) using index
)
/


create index SUME_VERE_FK_I on STPV_SUISA_MELDUNG(SUME_VERE_ID)
/

--misbrauch der tabelle zur jährlichen Meldung der Vereine, dass alle Vereinsdaten korrekt sind
alter table STPV_SUISA_MELDUNG
add 
	SUME_TYPE varchar2(10) default 'SUISA'
/

comment on  column STPV_SUISA_MELDUNG.SUME_TYPE is '"Misbrauch" diesser tabelle zur jährlichen Meldung der Vereine, dass alle Vereinsdaten korrekt sind'
/