--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : InstallationsScript create cm_parameter
-- $Date: 2020-04-30$  
-- $Author: mbl $  
-------------------------------------------------- 
  
prompt ## Create Table CM_PARAMETER

  
CREATE TABLE CM_PARAMETER 
   (		PARA_ID NUMBER(12,0) NOT NULL ENABLE, 
		PARA_MODULE VARCHAR2(32 BYTE) NOT NULL ENABLE, 
		PARA_NAME VARCHAR2(32 BYTE) NOT NULL ENABLE, 
		PARA_TYPE VARCHAR2(20 BYTE) NOT NULL ENABLE, 
		PARA_VALUE_1 VARCHAR2(255 BYTE) NOT NULL ENABLE,
		PARA_VALUE_2 VARCHAR2(255 BYTE)     NULL,
		PARA_RESTART_REQUIRED VARCHAR2(255 BYTE)  NULL,
		PARA_DESCRIPTION VARCHAR2(255 BYTE) NOT NULL ENABLE,
		PARA_CREATION_USER VARCHAR2(30 BYTE) NOT NULL ENABLE, 
		PARA_CREATION_DATE DATE NOT NULL ENABLE, 
		PARA_MODIFICATION_USER VARCHAR2(30 BYTE) NULL, 
		PARA_MODIFICATION_DATE DATE NULL, 
		 CONSTRAINT CM_PARAMETER_PK PRIMARY KEY (PARA_ID) )
/
 

   COMMENT ON COLUMN CM_PARAMETER.PARA_ID IS 'PK';

   COMMENT ON COLUMN CM_PARAMETER.PARA_MODULE IS 'For grouping parameters';
 
   COMMENT ON COLUMN CM_PARAMETER.PARA_NAME IS 'identifying name of the parameter';
 
   COMMENT ON COLUMN CM_PARAMETER.PARA_TYPE IS 'data type the parameter should be of';
 
   COMMENT ON COLUMN CM_PARAMETER.PARA_VALUE_1 IS 'parameter value';
   
   COMMENT ON COLUMN CM_PARAMETER.PARA_VALUE_2 IS 'Another value. Allows for a mapping functionality within the CM_PARAMETER table';
      
   COMMENT ON COLUMN CM_PARAMETER.PARA_RESTART_REQUIRED IS 'Is a restart of the server required for a change on  this parameter value to take effect?';
         
   COMMENT ON COLUMN CM_PARAMETER.PARA_DESCRIPTION IS 'Documentation of the parameter';
            
   COMMENT ON COLUMN CM_PARAMETER.PARA_CREATION_USER IS 'User by which row was inserted';
 
   COMMENT ON COLUMN CM_PARAMETER.PARA_CREATION_DATE IS 'User by which row was updated. ';
 
   COMMENT ON COLUMN CM_PARAMETER.PARA_MODIFICATION_USER IS 'User-account by which row was updated. In case dedicated db logon-users, the logon-user is entered here, in case of connection pooling, the real username is entered here!';
 
   COMMENT ON COLUMN CM_PARAMETER.PARA_MODIFICATION_DATE IS 'Date/Time on which row was updated. This column can also be used to detect competing updates in J2EE or other web-applications When created first, Modification date is the same as creation date.';
 

exec ddl_pkg.create_trigger('CM_PARAMETER', 'PARA', 'STPV_SEQ')
/

 insert into cm_parameter(PARA_MODULE, PARA_name, PARA_TYPE, PARA_VALUE_1, PARA_DESCRIPTION)
 values('LOGGING', 'Debug level', 'CHR', '3', 'defines the which level (1-6) of debug information should logged ')
 /
 
 commit
 /