 CREATE TABLE STPV_VEREIN_TYP
   (	
	VETY_ID NUMBER (10) NOT NULL ENABLE, 
	VETY_CODE VARCHAR2(2 BYTE) NOT NULL ENABLE, 
	VETY_NAME VARCHAR2(40 BYTE) NOT NULL ENABLE, 
	 UNIQUE (VETY_CODE) using index enable,
	 CONSTRAINT VETY_ID_PK PRIMARY KEY (VETY_ID) using index
	)
/

exec ddl_pkg.add_Standard_Columns('STPV_VEREIN_TYP', 'VETY')
/